#!/bin/bash                                                                                                                          
#$ -q 'all.q@fai01.cs.uni-saarland.de,all.q@fai02.cs.uni-saarland.de,all.q@fai03.cs.uni-saarland.de,all.q@fai04.cs.uni-saarland.de,all.q@fai05.cs.uni-saarland.de,all.q@fai06.cs.uni-saarland.de,all.q@fai07.cs.uni-saarland.de,all.q@fai08.cs.uni-saarland.de'           
                                                                           
RUN_FOLDER=/mnt/data_server/torralba/JAIR17-Undoability/exp/planner_experiments
COMPILED_BENCHMARKS=/mnt/data_server/torralba/JAIR17-Undoability/compiled_benchmarks

if [ $# -ne 2 ]; then
    echo "Usage ./script_lemmy planner compiler_type"
    exit
fi

PLANNER_TYPE="$1"
COMPILER_TYPE="$2"

domains=(`ls $COMPILED_BENCHMARKS/$COMPILER_TYPE`)

counter=0
for domain in ${domains[@]}; do
problem_list=(`find $COMPILED_BENCHMARKS/$COMPILER_TYPE/$domain -name "undo-problem-test*.pddl"`)
num_problems=${#problem_list[@]}

if [ "$num_problems" -gt 0 ]; then 
#cat > pbs_gen_mprime << EOF
cat > cff_$counter << EOF   
#!/bin/bash

#$ -S /bin/bash 
#$ -t 1-$num_problems
#$ -o pbs.out 
#$ -e pbs.err 
#$ -p 0
#$ -q 'all.q@fai01.cs.uni-saarland.de,all.q@fai02.cs.uni-saarland.de'           
                                              
problems=(${problem_list[@]})

num_p="\$((\$SGE_TASK_ID  % $num_problems))"                                                                                                                                                                      
#Define domain and exec folder                                                                                                                                                                                      
problemtest="\${problems["\$num_p"]}"                                                                                                                                                                               
compiler_type=\`echo \$problemtest | awk -F / '{print \$7}'\`
domain=\`echo \$problemtest | awk -F / '{print \$8}'\`
problem=\`echo \$problemtest | awk -F / '{print \$9}'\`
action=\`echo \$problemtest | awk -F / '{print \$10}'\`

$RUN_FOLDER/run_experiment.py "\$domain" "\$problem" "\$action" "\$compiler_type" "$PLANNER_TYPE" --max_time 600
EOF
echo $domain $num_problems
qsub cff_$counter
counter=$[$counter +1]
fi
done

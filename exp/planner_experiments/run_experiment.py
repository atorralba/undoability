#!/usr/bin/env python
# -d domain -p problem -a action.pddl -c compile_type (generous, exact, generous_rel) -t cff_type (partial_3, partial_5) -T max_time (default: 300) 

import argparse
import subprocess
import os 
import shutil
import glob

TESTBOT_DIR = '/mnt/data_server/torralba/JAIR17-Undoability/exp/testbot'
BENCHMARKS_DIR='/mnt/data_server/torralba/JAIR17-Undoability/compiled_benchmarks'
RESULTS_DIR='/mnt/data_server/torralba/JAIR17-Undoability/results'

def get_planner_exec (planner_suffix, DIR):
    CFF_DIR='/mnt/data_server/torralba/JAIR17-Undoability/planners/ContingentFF-unsolvedleaves'
    CLG_DIR='/mnt/data_server/torralba/JAIR17-Undoability/planners/CLG'
    SDR_DIR='/mnt/data_server/torralba/JAIR17-Undoability/planners/SDR'
    POPRP_DIR='/mnt/data_server/torralba/JAIR17-Undoability/planners/ContingentFF-unsolvedleaves'

    if planner_suffix.startswith('cff'):
        (name, exec_file) = ("ff", CFF_DIR + '/ff')
        if planner_suffix == 'cff':
            extra_arguments = '-a 0 -m 0 -h 3'
        elif planner_suffix.startswith('cff_partial_'):
            extra_arguments = '-I -H -g %s' % planner_suffix [12:]
        else:
            extra_arguments = '-a 0 -m 0'
            extra_arguments += ' -h 1' if 'h1' in planner_suffix else ' -h 3'
            extra_arguments += ' -H' if 'pref' in planner_suffix else ''
            
        arguments = '-p %s/ -o domain-undo.pddl -f problem-undo.pddl %s' % (DIR, extra_arguments)

    elif planner_suffix.startswith('clg'):
        #Copy the whole planner to the target location
        shutil.copytree(CLG_DIR, DIR + '/CLG')
        (name, exec_file) = ("plan", DIR + '/CLG/plan')
        arguments = '%s/domain-undo.pddl %s/problem-undo.pddl' % (DIR, DIR)
    elif planner_suffix.startswith('sdr'):
        #Copy the whole planner to the target location
        shutil.copytree(SDR_DIR, DIR + '/SDR')
        (name, exec_file) = ("plan", DIR + '/SDR/plan')
        arguments = '%s/domain-undo.pddl %s/problem-undo.pddl' % (DIR, DIR)

    elif planner_suffix.startswith('poprp'):
        #Copy the whole planner to the target location
        shutil.copytree(POPRP_DIR, DIR + '/POPRP')
        (name, exec_file) = ("plan", DIR + '/POPRP/plan')
        arguments = '%s/domain-undo.pddl %s/problem-undo.pddl' % (DIR, DIR)

    else:
        print('unknown planner exec for: %s', planner_suffix)
        exit()

    return (name, exec_file, arguments)

parser = argparse.ArgumentParser(description='Run experiment of undoability.')

parser.add_argument('domain')
parser.add_argument('problem')
parser.add_argument('action')
parser.add_argument('compiler_suffix')
parser.add_argument('planner_suffix')
parser.add_argument('-t', '--max_time', type=int, default=300)

args = parser.parse_args()


DIR='/'.join([RESULTS_DIR, args.planner_suffix, args.compiler_suffix, args.domain, args.problem, args.action])

if not os.path.exists(DIR):
    os.makedirs(DIR)
    (planner_name, planner_exec, planner_arguments) = get_planner_exec(args.planner_suffix, DIR)

#print (planner_name, planner_exec, planner_arguments)

    domain_file = "/".join ([BENCHMARKS_DIR, args.compiler_suffix, args.domain, args.problem, "domain-undo.pddl"])
    problem_file = "/".join ([BENCHMARKS_DIR, args.compiler_suffix, args.domain, args.problem, args.action])

    shutil.copyfile(domain_file, DIR + '/domain-undo.pddl')
    shutil.copyfile(problem_file, DIR + '/problem-undo.pddl')    
    shutil.copytree(TESTBOT_DIR, DIR + '/testbot')

    f = open(DIR + '/testbot/conf.tb', "w")
    f.write('exec ["%s"];\n' % planner_arguments) 
    f.close()

    os.chdir(DIR + '/testbot')

    
    TESTBOT_CALL = ['./testbot.py', '-s', planner_exec, '-f', DIR + '/testbot/conf.tb', '-D',  DIR + '/testbot/conf.db', '-t', str(args.max_time), '-m', '3',  '-c', '1', '-o', DIR+'/testbot_output']
    
    subprocess.call(TESTBOT_CALL)

#cleanup

    res_dir = DIR + ('/testbot/%s/results/' % planner_name)
    for filename in os.listdir(res_dir): 
        shutil.copy(res_dir + filename, DIR + '/' + filename)
    
    shutil.move(DIR + '/testbot/%s/%s.db' % (planner_name, planner_name), DIR + '/sys.db')
    shutil.rmtree(DIR + '/testbot')
    if os.path.exists(DIR + '/CLG'):
        shutil.rmtree(DIR + '/CLG')    
    if os.path.exists(DIR + '/SDR'):
        shutil.rmtree(DIR + '/SDR')    
else:
    print "res dir already exists", DIR

#!/bin/bash
#1=domain 2=problem 3=suffix for compile script 4=suffix for CFF 5=action.pddl
DOMAIN="$1"
PROBLEM="$2"
ACTION="$3"
COMPILER_SUFFIX="$4"

BENCHMARKS_DIR=/mnt/data_server/torralba/undoability/compiled_benchmarks_final
CFF_DIR=/mnt/data_server/torralba/undoability/cff_experiments/src/ContingentFF-unsolvedleaves
RESULTS_DIR=/mnt/data_server/torralba/undoability/results_partial3_final


DIR="$RESULTS_DIR"/"$COMPILER_SUFFIX"/"$DOMAIN"/"$PROBLEM"/"$ACTION"
if [ ! -d "$DIR" ]; then
    mkdir -p "$DIR"

    cp $BENCHMARKS_DIR/$COMPILER_SUFFIX/$DOMAIN/$PROBLEM/domain-undo.pddl "$DIR"/domain-undo.pddl
    cp $BENCHMARKS_DIR/$COMPILER_SUFFIX/$DOMAIN/$PROBLEM/$3 "$DIR"/problem-undo.pddl

    cp -r /mnt/data_server/torralba/undoability/cff_experiments/src/testbot "$DIR"/testbot
    cat > "$DIR"/testbot/conf.tb << EOF   
exec ["-p $DIR/ -o domain-undo.pddl -f problem-undo.pddl -I -H -g 3"];
EOF

    cd "$DIR"/testbot
    ./testbot.py -s "$CFF_DIR"/ff -f "$DIR"/testbot/conf.tb -D "$DIR"/testbot/conf.db -t 600 -m 4 -c 1 &> "$DIR"/testbot_output.out

#cleanup
    mv "$DIR"/testbot/ff/results/* "$DIR"
    mv "$DIR"/testbot/ff/ff.db "$DIR"/sys.db
    rm -rf "$DIR"/testbot 
fi

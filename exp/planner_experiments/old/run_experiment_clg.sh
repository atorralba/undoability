#!/bin/bash
#1=domain 2=problem 3=suffix for compile script 4=suffix for CFF 5=action.pddl
DOMAIN="$1"
PROBLEM="$2"
ACTION="$3"
COMPILER_SUFFIX="$4"

BENCHMARKS_DIR=/mnt/data_server/torralba/Undoability/compiled_benchmarks
CLG_DIR=/mnt/data_server/torralba/Undoability/planners/CLG
RESULTS_DIR=/mnt/data_server/torralba/Undoability/results/clg

DIR="$RESULTS_DIR"/"$COMPILER_SUFFIX"/"$DOMAIN"/"$PROBLEM"/"$ACTION"
if [ ! -d "$DIR" ]; then
    mkdir -p "$DIR"

    cp $BENCHMARKS_DIR/$COMPILER_SUFFIX/$DOMAIN/$PROBLEM/domain-undo.pddl "$DIR"/domain-undo.pddl
    cp $BENCHMARKS_DIR/$COMPILER_SUFFIX/$DOMAIN/$PROBLEM/$3 "$DIR"/problem-undo.pddl
    cp -r /mnt/data_server/torralba/Undoability/exp/testbot "$DIR"/testbot
    cp -r "$CLG_DIR" "$DIR"/CLG

    cat > "$DIR"/testbot/conf.tb << EOF   
exec ["$DIR/domain-undo.pddl $DIR/problem-undo.pddl"];
EOF

    cd "$DIR"/testbot
    ./testbot.py -s "$DIR"/CLG/plan -f "$DIR"/testbot/conf.tb -D "$DIR"/testbot/conf.db -t 300 -m 4 -c 1 &> "$DIR"/testbot_output.out

#cleanup
    mv "$DIR"/testbot/plan/results/* "$DIR"
    mv "$DIR"/testbot/plan/plan.db "$DIR"/sys.db
#    mv "$DIR"/CLG/run/new-d.pddl "$DIR"/new-d.pddl
#    mv "$DIR"/CLG/run/new-p.pddl "$DIR"/new-p.pddl
    rm -rf "$DIR"/testbot 
    rm -rf "$DIR"/CLG 
fi

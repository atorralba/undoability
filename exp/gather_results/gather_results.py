#!/usr/bin/python2
import os
import sqlite3
import sys
from shutil import copyfile
import argparse
import re

def valid_table(regexp, tablename):
     return re.match(regexp, tablename) or tablename == 'admin_status'

# def remove_invalid_tables(result_db):
#     con = sqlite3.connect(result_db)
#     with con:
#         cur = con.cursor()
#         cur.execute("begin")
#         for table in cur.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchall():
#             tablename = table[0]
#             if not valid_table (tablename):
#                 cur.execute("DROP TABLE " + tablename + ";")            
#             con.commit()


#Attach the list of other dbs to db
def copy_db(db, odb, tables_regexp):
    # if all_tables: 
    #     copyfile(sys.argv[2], result_db)
    # else:        
    con = sqlite3.connect(db)
    with con:
        cur = con.cursor()
        cur.execute("begin")
        print "importing ", odb
        cur.execute("attach ? as otherdb", (odb, ))
        for table in cur.execute("SELECT name FROM otherdb.sqlite_master WHERE type='table';").fetchall():
            tablename = table[0]
            if valid_table(tables_regexp, tablename):
                print "Create table ", tablename
                cur.execute("CREATE TABLE " + tablename + " AS SELECT * from otherdb." + tablename + ";")            
        con.commit()


#Attach the list of other dbs to db
def merge_dbs(db, others, tables_regexp):
    for odb in others:
        con = sqlite3.connect(db)
        with con:
            cur = con.cursor()
            cur.execute("begin")
            print "importing ", odb
            cur.execute("attach ? as otherdb", (odb, ))
            for table in cur.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchall():
                 tablename = table[0]
                 if valid_table(tables_regexp, tablename):
                            #print tablename
                      cur.execute("insert into " + tablename + " select * from otherdb." + tablename)
            con.commit()

    

# Get list of databases
#exec_dirs = [d for d in os.listdir('.') if os.path.isdir(d) and d.startswith('exec_')]
#db_path = '/cgamer/cgamer.db'
#result_db = 'results.db'
#dbs = map(lambda d  : d + db_path, exec_dirs)

parser = argparse.ArgumentParser(description='Gather data from different databases.')
parser.add_argument('--database', metavar='-DB', default='res.db', 
                    help='target database where the results will be stored')
parser.add_argument('--tables', metavar='-T',default='.*', 
                    help='regexp describing tables to be copied')
parser.add_argument('--src', metavar='-S', nargs='+', type=str, help='list of tables to be copied')

options = vars(parser.parse_args())

# if len(sys.argv) < 3: 
#     print "Usage gather_results.py <result_filename> <bd1> <bd2> ..." 
#     print sys.argv
#     exit(0)

if not options['src'] or len(options['src']) < 1:
    parser.print_help()
    exit(0)
result_db = options['database']
if os.path.exists(result_db):
    print "Aggregating data to: ", result_db
    merge_dbs(result_db, options['src'], options['tables'])
else:
    print "Creating: ", result_db
    copy_db(result_db, options['src'][0], options['tables'])
    merge_dbs(result_db, options['src'][1:], options['tables'])


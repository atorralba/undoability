#!/usr/bin/python2.7
from collections import defaultdict
import re
#import bz2
import os
import sys

sys.path.append('./autobot')

import argparse                 # parser for command-line options

sys.path.append('./scriptsjair/scripts14/autobot') #To get autobot
import autobot
from autobot import sqltools

DBSPEC_UNDO = """
data_undo {
      domain               text  :domain            Error;
      problem              text  :problem           Error;
      config               text  :config            Error;
      action_name          text  :action_name       Error;
      action_schema        text  :action_schema       Error;
      type                 text  :type           Error;
      plan_length         integer  :plan_length    None;
      time                real  :time            None;
}
""" 

def parse_invertible (path_invertible_file, config, domain, problem):            
	print ("Parsing: ", path_invertible_file)
	p = re.compile(r'[^(]*\((?P<action_name>([^\)]*))\)')
	tmp = {}
	f = open(path_invertible_file)
	#with bz2.BZ2File(path_invertible_file) if path_invertible_file[-4:] == ".bz2" else open (path_invertible_file, 'r') as stream:
	for line in f.readlines ():						
		m = p.match(line)
		if m: 
			action_name = m.groupdict()['action_name']
			placeholders = {"domain" : domain, "problem" : problem, "config" : config, "action_name" : action_name, "type" : "unknown", "plan_length" : "-1" , \
					"action_schema" : action_name.split(" ")[0] if " " in action_name else action_name } 
			if "Not invertible" in line:
				placeholders["type"]="not invertible"
			if "Atleast invertible" in line:
				placeholders["type"]="Atleast invertible"
			if "Null effects" in line:
				placeholders["type"]="null"
			if "Invertible" in line:
				placeholders["type"] = "invertible"
				placeholders["plan_length"]="1"

			tmp[action_name] = placeholders
	return tmp

def read_problem_file(path_undo_file):
	problem_path = path_undo_file+"problem-undo.pddl"
	f = open(problem_path)
	line = f.readline()
	p = re.compile(r'[^(]*\((?P<action_name>([^\)]*))\)')
	m = p.match(line)
	return m.groupdict()['action_name']
	

regexp_time = re.compile(r"(?P<time>([\d]+.[\d]+)) seconds total time")
regexp_tree_layers = re.compile(r"tree layers: (?P<layers>([\d]+))")
regexp_num_actions = re.compile(r"total nr. actions: (?P<actions>([\d]+))")

def parse_cff_file(path_undo_file, domain, problem, config, action_name): 
	placeholders = {"domain" : domain, "problem" : problem, "config" : config, "action_name" : action_name, "type" : "unknown", "plan_length" : "-1", \
			"action_schema" : action_name.split(" ")[0] if " " in action_name else action_name, "time" : 0 } 
	
	result_path = (path_undo_file+"/000.log") if os.path.exists(path_undo_file+"/000.log") else (path_undo_file+"/testbot_output.log")

	
	print ("Parsing: " + result_path)
	f = open(result_path)
	action_type = "unknown"
	for line in f.readlines(): 
                line = line.strip()
		if line.find("goal can be simplified to FALSE")!=-1:
			action_type="not undoable in preprocess"		
		elif line.find("initial state failed! problem proved unsolvable!")!=-1:
			action_type="not undoable"
		elif line.find("empty plan")!=-1:
			action_type="empty plan"
		elif "too many relevant facts" in line:
			action_type="error_max_facts"
			break
		elif "found plan as follows" in line:
			action_type="undoable"
		elif regexp_time.match(line):
			placeholders["time"] = regexp_time.match(line).groupdict()["time"]
		elif regexp_num_actions.match(line):
			placeholders["actions"] = regexp_num_actions.match(line).groupdict()["actions"]
		elif regexp_tree_layers.match(line):
			placeholders["plan_length"] = regexp_tree_layers.match(line).groupdict()["layers"]

                        	

	placeholders["type"]=action_type
	return placeholders
	

def cleanup_name(action_name):
	return " ".join(action_name.lower().split())

def parse_data(data_name):
	results = []
	res = {}
        resdir = data_name if not "/" in data_name else (data_name.split("/")[-4] if data_name[-1] != "/" else data_name.split("/")[-5])
	
	config = data_name if not "/" in data_name else (data_name.split("/")[-3] if data_name[-1] != "/" else data_name.split("/")[-4])
	domain = data_name if not "/" in data_name else (data_name.split("/")[-2] if data_name[-1] != "/" else data_name.split("/")[-3])
	problem = data_name if not "/" in data_name else (data_name.split("/")[-1] if data_name[-1] != "/" else data_name.split("/")[-2])
	path_invertible_file = "/mnt/data_server/torralba/undoability/compiled_benchmarks/%s/%s/%s/metadata/invertible.txt" % (config, domain, problem)      
	res = parse_invertible(path_invertible_file, config, domain, problem)	
	#  res  = {"move x y" : {"domain" : domain, "problem" : problem, "config": config, "action_name" : "move x y", "type" : "invertible", "plan_length" : -1}]
	path_undoable_files = "/mnt/data_server/torralba/undoability/%s/%s/%s/%s/" % (resdir, config, domain, problem)
	# loop over the instances of a problem
	if os.path.isdir(path_undoable_files):
		for dirs in os.listdir (path_undoable_files):
			path_undo_file = path_undoable_files+dirs+'/'
			action_name = cleanup_name(read_problem_file(path_undo_file))
			res_undoable = parse_cff_file(path_undo_file, domain, problem, config, action_name)
			
			if (action_name in res):
				res[action_name] = res_undoable
			else:
				print "Action from nowhere: " + action_name
				print res
				exit(0)

			
	for key in res:
		results.append(res[key]) 
	return results

def gather_db(dbname, resdir):
    results = []
    results = parse_data (resdir)
    istats = defaultdict (list)
    dbspec = autobot.dbtools.DBVerbatim (DBSPEC_UNDO)
    for placeholders in results: 
        for itable in dbspec:
            if itable.datap ():
                itable.poll(placeholders, istats)

    for dbtable in dbspec:
        db = sqltools.dbtest (dbname)     # connect to the sql database
        db.create_table_if_not_exists (dbtable)         # create the table
        db.insert_data (dbtable, istats[dbtable.get_name()]) # and write data
        db.close ()         # close and exit

parser = argparse.ArgumentParser(description='Gather data from the output file and insert it into a database.')
parser.add_argument('--database', metavar='-DB', 
                    help='target database where the results will be stored')
parser.add_argument('--dir', metavar='-r', default = '',
                    help='results dir')

options = vars(parser.parse_args())
if not options['database']:
    parser.print_help()
    exit(0)

gather_db(options['database'], options['dir'])

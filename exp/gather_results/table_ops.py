#!/usr/bin/python2 

from __future__ import division

import os
import argparse
import re
import itertools
import math
import sys

sys.path.append('./report')

from print_data import *
from gather_data import *

from operator import is_not
from functools import partial
# from scipy import stats as scistats

short_schema_names = {"empty-shaker/pour-shaker-to-shot/pour-shot-to-used-shaker/pour-shot-to-clean-shaker" : "empty-shaker/pour-*", 
                      "refill-shot/clean-shot/shake/empty-shot/fill-shot" : "(refill/clean/fill)-shot/shake", 
                      "serve-sandwich/make-sandwich-no-gluten/make-sandwich/put-on-tray/serve-sandwich-no-gluten" : "make/put/serve*", 
                      "paint-up/paint-down" : "paint-*", 
                      "homefromfreecell/sendtohome-b/sendtohome" : "*home*", 
                      "sendtofree/sendtonewcol" : "sendto(free/newcol)", 
                      "begin-inverse-splice" : "begin-inv-splice", 
                      "begin-inverse-splice-special-case" : "begin-inv-splice-special", 
                      "begin-transpose-splice/begin-transverse-splice" : "begin-(transpose/transverse)-splice", 
                      "continue-splice-1/continue-inverse-splice-1b/continue-inverse-splice-1a" : "continue-*-1", 
                      "continue-splice-2/continue-inverse-splice-2" : "continue-*-2", 
                      "end-splice-1/end-inverse-splice-1a/end-inverse-splice-1b" : "end-*splice-1", 
                      "end-splice-2/end-inverse-splice-2" : "end-*splice-2", 
                      "jump-continue-move/end-move/jump-new-move" : "(end/jump-*)-move", 
                      "pop-unitarypipe/push-unitarypipe" : "(pop/push)-unitarypipe", 
                      "push-start/pop-start" : "(push/pop)-start", 
                      "push-unitarypipe/pop-unitarypipe" : "(push/pop)-unitarypipe", 
                      "sample-rock/sample" : "sample-rock/sample", 
                      "move-two/move-l-up/move-l-left/move-l-right/move-square/move-l-down" : "move*", 
                      "tal-to-col/tal-to-home-d/tal-to-col-h/tal-to-col-g/tal-to-col-f/tal-to-col-e/tal-to-col-d/tal-to-col-c/tal-to-col-b/tal-to-home/tal-to-home-c/tal-to-home-b/move-col-to-col-b" : "tal-to-*/move-col-to-col-b", 
                      "tal-to-col/tal-to-col-h/tal-to-col-g/tal-to-col-f/tal-to-col-e/tal-to-col-d/tal-to-col-c/tal-to-col-b/tal-to-home/tal-to-home-d/tal-to-home-c/tal-to-home-b/move-col-to-col-b" : "tal-to-*/move-col-to-col-b", 
                      "turn-deck/home-to-col-a/home-to-col" : "turn-deck/home-to-*", 
                      "cut-board-large/do-saw-large" : "(cut-board/do-saw)-large", 
                      "do-immersion-varnish/do-spray-varnish" : "do-(immersion/spray)-varnish", 
                      "do-spray-varnish/do-immersion-varnish" : "do-(immersion/spray)-varnish", 
                      "do-saw-small/cut-board-medium/cut-board-small/do-saw-medium" : "(do-saw/cut-board)-(small/medium)", 
                      "cut-board-small/do-saw-medium/cut-board-medium/do-saw-small"  : "(do-saw/cut-board)-(small/medium)", 
                      "end-splice-2/end-splice-1/begin-inverse-splice-special-case/begin-transpose-splice/begin-transverse-splice/begin-inverse-splice/continue-splice-2/end-inverse-splice-1a/end-inverse-splice-1b/reset-1/begin-cut/continue-inverse-splice-2/end-cut-2/end-cut-1/end-inverse-splice-2" : "end*/begin*/reset-1/continue-splice-2/continue-inverse-splice-2", 
                      "continue-inverse-splice-1b/continue-inverse-splice-1a" : "continue-inverse-splice-1*", 
                      "pop-unitarypipe/pop-end/push-unitarypipe/pop-start/push-start" : "pop*/push-(start/unitarypipe)", 
                      "end-splice-2/end-splice-1/begin-inverse-splice-special-case/begin-transpose-splice/begin-transverse-splice/begin-inverse-splice/continue-splice-2/end-inverse-splice-1a/reset-1/end-inverse-splice-1b/end-cut-2/end-cut-1/continue-inverse-splice-2/end-inverse-splice-2" : "end*/begin*/reset-1/continue-(inverse)-splice-2", "put-right/put-down/gripper-left/get-right/get-left/gripper-down/put-left/base-cart-up/base-cart-down/get-down/base-down/base-right/put-up/base-cart-right/base-up/ungrasp-cart/base-left/get-up/gripper-right/gripper-up/base-cart-left" : "*",
}


short_domain_names = {
    "barman-opt14" :            "\\barmanshort" ,           
    "childsnack-opt14" :        "\\childsnackshort",        
    "depot" :                   "\\depotsshort" ,           
    "floortile-opt14" :         "\\floortileshort" ,        
    "freecell" :                "\\freecellshort" ,         
    "ged-opt14" :               "\\gedshort" ,              
    "grid" :                    "\\gridshort" ,             
    "hiking-opt14" :            "\\hikingshort" ,           
    "miconic" :                 "\\miconicshort" ,          
    "mprime" :                  "\\mprimeshort" ,           
    "nomystery-opt11-strips" :  "\\nomysteryshort" ,        
    "parking-opt14" :           "\\parkingshort" ,          
    "pegsol-opt11-strips" :     "\\pegsolshort" ,           
    "pipesworld-notankage" :    "\\pipesnotankshort" ,      
    "pipesworld-tankage" :      "\\pipestankshort" ,        
    "rovers" :                  "\\roversshort" ,           
    "satellite" :               "\\satelliteshort" ,        
    "scanalyzer-opt11-strips" : "\\scanalyzershort" ,       
    "sokoban-opt11-strips" :    "\\sokobanshort" ,          
    "storage" :                 "\\storageshort" ,          
    "tidybot-opt14" :            "\\tidybotshort" ,           
    "tetris-opt14" :            "\\tetrisshort" ,           
    "thoughtful-sat14" :        "\\thoughtfulshort" ,       
    "tpp" :                     "\\tppshort" ,              
    "woodworking-opt11-strips" :"\\woodworkingshort" ,      
    "zenotravel" :              "\\zenotravelshort" ,       
}


tokenize = re.compile(r'(\d+)|(\D+)').findall
def natural_sortkey(string):          
    return tuple(int(num) if num else alpha for num, alpha in tokenize(string))


def composite_data(data_problem, name,  f):
    for d in data_problem:
        problems = data_problem[d][data_problem[d].keys()[0]].keys()
        data_problem[d][name] = {p : f(data_problem[d], p) for p in problems }

def get_value(f, values, d):
    value = f(values, d)
    return value if isinstance(value, (int, float)) else "--"


def favg(l):
        return (sum(l, 0.0) / len(l)) if l else "--"

def fmedian(l):
        return  sorted(l)[len(l)//2]

def to_str_float(d):
    try:
        return "{:.1f}".format(round(float(d), 1))
    except:
        return str(d)

def ensure_sum1(data, keys):
    for k in keys: 
        data[k] = float(to_str_per(data[k]))/100.0
    total = sum([float(data[k]) for k in keys])
    data[keys[-1]] -= (total - 1)
    return data
    
def to_str_per(d):
    try:
        return str(int(round(100*float(d))))
    except:
        return str(d)

def to_str_int(d):
    try:
        return str(int(d))
    except:
        return str(d)

def transform_values (values):
    null_effects = float(values["null"]) if "null" in values else 0.0
    not_undoable = float(values["not undoable"]) if "not undoable" in values else 0.0
    not_undoable_heuristic = float(values["not undoable in heuristic"]) if "not undoable in heuristic" in values else 0.0
    not_undoable_preprocess = float(values["not undoable in preprocess"]) if "not undoable in preprocess" in values else 0.0
    undoable = float(values["undoable"]) if "undoable" in values else 0.0
    error_max_facts = float(values["error_max_facts"]) if "error_max_facts" in values else 0.0    
    timeout = (float(values["timeout"]) if "timeout" in values else 0.0)
    memout = (float(values["memout"]) if "memout" in values else 0.0)
    unknown = error_max_facts + timeout + memout + (float(values["unknown"]) if "unknown" in values else 0.0)
    invertible = float(values["invertible"]) if "invertible" in values else 0.0
    not_invertible = float(values["not invertible"]) if "not invertible" in values else 0.0
    atleast_invertible = float(values["Atleast invertible"]) if "Atleast invertible" in values else 0.0

    #Removed actions with null effects from total
    total_cff = not_undoable +  not_undoable_preprocess + not_undoable_heuristic + undoable + unknown
    total = total_cff + invertible + atleast_invertible + not_invertible

    percentage_undoable = ((undoable/total_cff) if total_cff > 0 else 0.0)
    percentage_not_undoable = ((not_undoable)/total_cff if total_cff > 0 else 0.0)
    percentage_not_undoable_heuristic = ((not_undoable_heuristic)/total_cff if total_cff > 0 else 0.0)
    percentage_not_undoable_preprocess = ((not_undoable_preprocess)/total_cff if total_cff > 0 else 0.0)
    percentage_fail = ((unknown/total_cff) if total_cff > 0 else 1.0)

    if percentage_undoable +  percentage_not_undoable + percentage_not_undoable_heuristic + percentage_not_undoable_preprocess + percentage_fail != 1:
        print percentage_undoable, percentage_not_undoable, percentage_not_undoable_heuristic, percentage_not_undoable_preprocess, percentage_fail 

    percentage_memout = ((memout/total_cff) if total_cff > 0 else 0.0)
    percentage_timeout = ((timeout/total_cff) if total_cff > 0 else 0.0)
   
    failed_classified = "" if percentage_fail else "--" 
    if percentage_memout:
        failed_classified += to_str_per(percentage_memout) + "M"
    if percentage_timeout:
        failed_classified += to_str_per(percentage_timeout) + "T"
    if percentage_fail > percentage_memout +percentage_timeout :
        failed_classified += to_str_per(percentage_fail - (percentage_memout + percentage_timeout)) + "?"

    res = {"invertible": (invertible + atleast_invertible)/total if total > 0 else "--", 
           #"undoable" :  (undoable + not_invertible*percentage_undoable)/total if total > 0 else "--",
           #"not_undoable" :  (not_undoable + not_invertible*percentage_not_undoable)/total if total > 0 else "--",
           #"not_undoable_preprocess" :  (not_undoable_preprocess + not_invertible*percentage_not_undoable_preprocess)/total if total > 0 else "--",
           #"failed" :  (unknown + not_invertible*percentage_fail)/total if total > 0 else "--",
           "undoable" :  (percentage_undoable) if total > 0 else "--",
           "not_undoable" :  (percentage_not_undoable) if total > 0 else "--",
           "not_undoable_heuristic" :  (percentage_not_undoable_heuristic) if total > 0 else "--",
           "not_undoable_preprocess" :  (percentage_not_undoable_preprocess) if total > 0 else "--",
           "failed" :  (percentage_fail),
           "failed_classified" :  failed_classified,
           "cff_success" : 1 - unknown/total_cff if total_cff > 0 else "--",
           "pname" : values["pname"],
           "tmin" : values["tmin"] if "tmin" in values else -1,
           "tmax" : values["tmax"] if "tmax" in values else -1,
           "tavg" : values["tavg"] if "tavg" in values else -1,
           "lmin" : values["lmin"] if "lmin" in values else -1,
           "lmax" : values["lmax"] if "lmax" in values else -1,
           "lavg" : values["lavg"] if "lavg" in values else -1,
           "smin" : values["smin"] if "smin" in values else -1,
           "smax" : values["smax"] if "smax" in values else -1,
           "savg" : values["savg"] if "savg" in values else -1,
           }
    return res


def almostEqual (d1, d2, debug): 
    if debug: 
        print d1, d2
    for k in d1: 
        if isinstance(d1[k], (int, float, long, complex)) and isinstance(d2[k], (int, float, long, complex)):
            if k.startswith("savg") or k.startswith("smin") or k.startswith("smax"):
                pass
            elif k in ["invertible", "not_undoable_preprocess", "not_undoable", "not_undoable_heuristic", "undoable", "cff_success"]:
                if abs(d2[k] - d1[k]) > 0.02 and (min(d2[k], d1[k]) == 0 or abs(d2[k] - d1[k])/max(d2[k], d1[k]) > 0.02):
                    if debug: 
                        print "FALSE:", k
                    return False
            elif k.startswith("failed"):
                pass
            elif k.startswith("tavg") or k.startswith("tmin") or k.startswith("tmax"):
                pass
                #if abs(d2[k] - d1[k]) > 1 and (min(d2[k], d1[k]) == 0 or abs(d2[k] - d1[k])/max(d2[k], d1[k]) > 0.1):
                #    if debug: 
                #        print k
                #    return False
            elif k.startswith("lavg") or k.startswith("lmin") or k.startswith("lmax") or k.startswith("coverage_some") or k.startswith("coverage_all"):  
                if d2[k] != d1[k]:
                    if debug: 
                        print "FALSE:", k
                    return False
            else:
                print k, d1[k], d2[k]
                exit()
        else:
            pass
            # if d2[k] != d1[k]:
            #     print "FALSE:", k, d2[k], d1[k] 
            #     if debug: 
            #         print "FALSE:", k, d2[k], d1[k] 
            #     return False
            
    return True
def remove_duplicates (datadd):
    datadd_filtered = {} 
    for k1 in datadd:
        domain = k1.split("_")[0]
        duplicated_name = None
        for k2 in datadd_filtered:
            if domain == k2.split("_")[0]:
                if almostEqual(datadd[k1], datadd_filtered[k2], k1.split("_")[1] == 'pop-start' and k2.split("_")[1] == 'push-unitarypipe'  ):
                    duplicated_name = k2
                    break

        if duplicated_name: 
            datadd_filtered [duplicated_name + "/" + k1.split("_")[1]] = datadd_filtered [duplicated_name]
            datadd_filtered.pop(duplicated_name) 
        else:
            datadd_filtered [k1] = datadd[k1] 
    for k1 in datadd_filtered: 
        schema_name = k1.split("_")[1]
        #print k1, schema_name
        if schema_name in short_schema_names:
            new_name = k1.replace(schema_name, short_schema_names[schema_name])
            datadd_filtered [new_name] = datadd_filtered[k1] 
            datadd_filtered.pop(k1)
            
    return datadd_filtered

def transform_types(data):
    datap = {}
    #Put in dictionary
    for (key, typ, num, lmin, lmax, lavg, tmin, tmax, tavg, smin, smax, savg) in data:
        if not key in datap:
            datap[key] = {}
        datap[key][typ] = num
        datap[key]["pname"] = key[2]
        if (typ == "not undoable" or typ == "not undoable in preprocess" or typ == "undoable"):
            datap[key]["tmin"] = tmin
            datap[key]["tmax"] = tmax
            datap[key]["tavg"] = tavg
        if (typ == "undoable"):
            datap[key]["lmin"] = lmin
            datap[key]["lmax"] = lmax
            datap[key]["lavg"] = lavg
            datap[key]["smin"] = smin
            datap[key]["smax"] = smax
            datap[key]["savg"] = savg
        
    # Group by domain
    proborder = {}
    datadd = {}
    for keyp in sorted(datap.keys(), key = lambda x : natural_sortkey(x[2])):
        (config, domain, problem, schema) = keyp

        if not domain in proborder: 
            id_prob = 0
            proborder [domain] = [problem]
        elif problem not in proborder[domain]:
            id_prob = len (proborder [domain]) 
            proborder [domain].append(problem)
        else:
            id_prob = proborder[domain].index(problem) 


        key = "_".join([domain, schema])
        if key not in datadd:
            datadd[key]= {}
        
        for (k, v) in transform_values(datap[keyp]).items():
            if k not in datadd[key]:
                datadd[key][k] = []

            datadd[key][k].append(v)

    new_datadd = {}
    # Summarize data
    for key in datadd:
        min_ts = [tmin for tmin in datadd [key]["tmin"] if tmin >= 0] 
        max_ts = [tmax for tmax in datadd [key]["tmax"] if tmax >= 0]
        avg_ts = [tavg for tavg in datadd [key]["tavg"] if tavg >= 0]
        min_len = [lmin for lmin in datadd [key]["lmin"] if lmin >= 0] 
        max_len = [lmax for lmax in datadd [key]["lmax"] if lmax >= 0]
        avg_len = [lavg for lavg in datadd [key]["lavg"] if lavg >= 0]

        min_suc = [smin for smin in datadd [key]["smin"] if smin >= 0] 
        max_suc = [smax for smax in datadd [key]["smax"] if smax >= 0]
        avg_suc = [savg for savg in datadd [key]["savg"] if savg >= 0]

        new_datadd [key] = {"undoable" : favg([v for v in  datadd [key]["undoable"] if v != "--"]),
                            "cff_success" : favg([v for v in  datadd [key]["cff_success"] if v != "--"]),
                            "invertible": favg([v for v in datadd [key]["invertible"]if v != "--"]),
                            "not_undoable" : favg([v for v in datadd [key]["not_undoable"]if v != "--"]),
                            "not_undoable_preprocess" : favg([v for v in datadd [key]["not_undoable_preprocess"]if v != "--"]),
                            "not_undoable_heuristic" : favg([v for v in datadd [key]["not_undoable_heuristic"]if v != "--"]),
                            "failed" : favg([v for v in datadd [key]["failed"]if v != "--"]),
                            "tmin" : min(min_ts) if min_ts else "--",
                            "tavg" : favg(avg_ts) if avg_ts else "--",
                            "tmax" : max(max_ts) if max_ts else "--",
                            "lmin" : min(min_len) if min_len else "--",
                            "lavg" : favg(avg_len) if avg_len else "--",
                            "lmax" : max(max_len) if max_len else "--",
                            "smin" : min(min_suc) if min_suc else "--",
                            "savg" : favg(avg_suc) if avg_suc else "--",
                            "smax" : max(max_suc) if max_suc else "--",
                        }
        
        domain = key.split("_")[0]
        coverage_all= 0
        coverage_some = 0
        failed_translator = 0
        for i in range(0, 4):
            if i < len(proborder[domain]) and proborder[domain][i] in datadd[key]["pname"]: 
                problem = proborder[domain][i]
                idprob = datadd[key]["pname"].index(problem)
                for k in ["tmin", "tmax", "tavg", "lmin", "lmax", "lavg", "smin", "smax", "savg", "failed", "failed_classified", "pname"]:
                    new_datadd[key]["%s%d" % (k, i)] = datadd [key][k][idprob] if datadd [key][k][idprob] >= 0 else "--"
                if datadd [key]["cff_success"][idprob] > 0:
                    coverage_some += 1

                if datadd [key]["failed"][idprob] == 0:
                    coverage_all += 1
            else:
                failed_translator += 1
                for k in ["tmin", "tmax", "tavg", "lmin", "lmax", "lavg", "smin", "smax", "savg", "failed", "failed_classified", "pname"]:
                    new_datadd[key]["%s%d" % (k, i)] = "--"
                    
        if failed_translator and new_datadd[key]["invertible"] < 1:
            for k in ["undoable", "cff_success",  "not_undoable", "not_undoable_preprocess", "not_undoable_heuristic"]:  
                if new_datadd[key][k] != "--":
                    new_datadd[key][k] = float(new_datadd[key][k])*(1.0 - failed_translator/4.0)
            new_datadd[key]["failed"] = new_datadd[key]["failed"]*(1.0 - failed_translator/4.0) #+ (failed_translator - new_datadd[key]["invertible"])/4.0
                
        new_datadd[key]["coverage_some"] = coverage_some
        new_datadd[key]["coverage_all"] = coverage_all

        #new_datadd[key] = ensure_sum1(new_datadd[key], ["undoable", "not_undoable", "not_undoable_preprocess", "failed"])
        
        
    datadd = remove_duplicates(new_datadd) 
                        
    # Filter data
    invertible =  [k for k, v in datadd.items() if v["invertible"] == 1]
    failed =  [k for k, v in datadd.items() if v["failed"] == 1]
    #datadd =  dict((k, v) for k, v in datadd.items() if v["invertible"] < 1 and v["failed"] < 1)
    datadd =  dict((k, v) for k, v in datadd.items() if v["invertible"] < 1)

    return datadd, invertible, failed



def norm(s):
    return s.lower().strip().replace("_", "-")

def perform_query(config, domain): 
    conditions = ""
    if config or domain:
        cond_config = "config = '%s'" % config if config else ""
        cond_domain = ("%s domain = '%s'" % (" and " if config else "", domain))  if domain else ""
        conditions = " where " + cond_config + cond_domain

    query= "select config, domain, problem, action_schema, type, count(*), min(plan_actions), max(plan_actions), avg(plan_actions), min(time), max(time), avg(time), min(plan_success), max(plan_success), avg(plan_success) from data_undo %s  group by config, domain, problem, action_schema, type;" % conditions

    data = gather_query(options['database'], query)
    data_list = [((norm(config), norm(domain), norm(problem), norm(schema)), typ, num, lmin , lmax , lavg , tmin , tmax , tavg, smin, smax, savg) for (config, domain, problem, schema, typ, num, lmin , lmax , lavg , tmin , tmax , tavg, smin, smax, savg ) in data]
    data_sorted = sorted(data_list, key = lambda x : natural_sortkey(x[0][2]))
    return data_sorted

    
parser = argparse.ArgumentParser(description='Reports data from planning experiments.')
parser.add_argument('--database', metavar='-DB', default='res.db', help='results database')
parser.add_argument('--config', metavar='-c', help='configuration')
parser.add_argument('--domain', metavar='-d', help='configuration')
parser.add_argument('--print', action='store_true', help='configuration')
parser.add_argument('--probnames', action='store_true', help='Include data about the name of the problem pddl files that correspond to p01-p04')
parser.add_argument('--shortdomainnames', action='store_true', help='Replace domain names for their short version')
parser.add_argument('--scaling', action='store_true', help='Replace domain names for their short version')
#parser.add_argument('--classify', metavar='-C', default='probschema', help='configuration')
options = vars(parser.parse_args())

data = perform_query(options['config'] if 'config' in options else None, options['domain'] if 'domain' in options else None)
dd, invertible, failed = transform_types (data)
#data, failed, invertible = filter_data(data)

columns_sorted = ["invertible", "undoable", "lavg", "not_undoable_preprocess", "not_undoable_heuristic", "not_undoable",  "cff_success", "coverage_some", "coverage_all"]
if options["scaling"]: 
    columns_sorted += ["failed_classified0", "tavg0", "failed_classified1", "tavg1", "failed_classified2", "tavg2", "failed_classified3",  "tavg3" ,]

    if options["probnames"]: 
        columns_sorted += ["savg" , "pname0", "pname1", "pname2", "pname3",]

fstr = {"invertible" : to_str_per, "undoable" : to_str_per, "lavg": to_str_float, "not_undoable_preprocess": to_str_per, "not_undoable_heuristic": to_str_per, "not_undoable": to_str_per, "failed": to_str_per, "failed0": to_str_per, "tavg0": to_str_float, "failed1": to_str_per, "tavg1": to_str_float, "failed2": to_str_per, "tavg2": to_str_float, "failed3": to_str_per,  "tavg3" : to_str_float, "savg" : to_str_per, "pname0" : str, "pname1" : str, "pname2" : str, "pname3" : str, "failed_classified1" : str, "failed_classified2" : str, "failed_classified3" : str, "failed_classified0" : str, "coverage_some" : str, "coverage_all" : str, "cff_success" : to_str_per }

print ("domain & schema & " + " & ".join(columns_sorted) + "\\\\")
#print_data(domain_list, query_list, planner_header, dd, options['style'])
previous_dom_name = None
for row in sorted(dd.keys()):
    dom_name = row[:row.find("_")].replace ("_", "-")
    if previous_dom_name != dom_name: 
        print "\\hline"
        previous_dom_name = dom_name
    if options['shortdomainnames'] and dom_name in short_domain_names:
        dom_name = short_domain_names[dom_name]
    act_name = row[row.find("_")+1:].replace ("_", "-")
    
    print (" & ".join([dom_name, act_name] + [fstr[col](dd[row][col]) if col in dd[row] else '--' for col in columns_sorted]) + "\\\\")

if options["print"]:
    print ("List of failed problems: ")
    for p in sorted(list(failed)): 
        print (p)
        

    print ("List of invertible problems: ")
    for p in sorted(list(invertible)): 
        print (p)

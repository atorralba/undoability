#!/bin/bash

DIR="/mnt/data_server/torralba/Undoability/results_600/$1"
TYPES=("exact")
#TYPES=("generous_rel")
#TYPES=("exact_rel")

for typ in "${TYPES[@]}"; do 
for dom in `ls /mnt/data_server/torralba/Undoability/compiled_benchmarks/$typ`; do 
for prob in `ls /mnt/data_server/torralba/Undoability/compiled_benchmarks/$typ/$dom`; do 
    echo python gather.py --database tmp.db $DIR/$typ/$dom/$prob
    python gather.py --database $2.db   --dir "$DIR/$typ/$dom/$prob"
done

done


done


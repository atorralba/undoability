#!/usr/bin/python2

from collections import defaultdict
import re
import sqlite3
import os
import sys

sys.path.append('./autobot')
sys.path.append('./report')
from config_dbspec import *

import argparse                 # parser for command-line options

import autobot
from autobot import sqltools
from gather_data import *

#UNDO_FOLDER = "/mnt/data_server/torralba/JAIR17-Undoability/"
UNDO_FOLDER = "/home/alvaro/Desktop/undo/undoability/"

COMPILED_BENCHMARKS_FOLDER = "%s/compiled_benchmarks/" % UNDO_FOLDER
RESULTS_FOLDER = "%s/results/" % UNDO_FOLDER



DBSPEC_UNDO  = """
data_undo  {
    domain                        text     :domain                        Error;
    problem                       text     :problem                       Error;
    config                        text     :config                        Error;
    action_name                   text     :action_name                   Error;
    action_schema                 text     :action_schema                 Error;
    num_overwritten_combinations  integer  :num_overwritten_combinations  Error;
    type                          text     :type                          Error;
    is_invertible                 integer  :is_invertible                 Error;
    skipped                     integer  :skipped Error;
    proved_undoable                   integer  :proved_undoable                   Error;
    proved_not_undoable               integer  :proved_not_undoable               Error;
}

data_plan {
  domain          text     :domain             Error;
  problem         text     :problem            Error;
  config          text     :config             Error;
  action_name     text     :action_name        Error;
  overwritten_id  integer  :overwritten_id     Error;
  action_schema   text     :action_schema      Error;
  planner         text     :planner            Error;
  type            text     :type               Error;
  success         integer  :success            Error;
  plan_layers     integer  :plan_layers        None;
  plan_actions    integer  :plan_actions       None;
  unknown_facts   integer  :num_unknown_facts  None;
  evaluations     integer  :evaluations        None;  
  plan_success    real     :plan_success       None;
  time            real     :time               None;
  file_id         text     :file_id            None;
  run_cff         integer  :run_cff            None;
  s_time          real     :sys_time           None;
}

""" 
           
def parse_invertible (path_invertible_file, config, domain, problem):
	all_invertible = True
	#print ("Parsing: ", path_invertible_file)
	p = re.compile(r'[^(]*\((?P<action_name>([^\)]*))\)')
	tmp = {}
	f = open(path_invertible_file)
        print path_invertible_file
	#with bz2.BZ2File(path_invertible_file) if path_invertible_file[-4:] == ".bz2" else open (path_invertible_file, 'r') as stream:
	for line in f.readlines ():						
		m = p.match(line)
		if m: 
			action_name = cleanup_name(m.groupdict()['action_name'])
			placeholders = {"domain" : domain, "problem" : problem, "config" : config, "action_name" : action_name, "type" : "unknown", "plan_layers" : "-1", "plan_actions" : "-1" , "expanded" : "0", "action_schema" : action_name.split(" ")[0] if " " in action_name else action_name, 'is_invertible' : 0, 'proved_not_undoable' : 0, 'proved_undoable' : 0, "num_overwritten_combinations" : 0, "skipped" : 1}

                        
			if "Not invertible" in line:
				placeholders["type"]="not invertible"
			if "Atleast invertible" in line:
                                placeholders['is_invertible'] = 1
				placeholders["type"]="Atleast invertible"
			if "Null effects" in line:
                                placeholders['is_invertible'] = 1
				placeholders["type"]="null"
			if "Invertible" in line:
				placeholders["type"] = "invertible"
				placeholders["plan_length"]="1"
                                placeholders['is_invertible'] = 1

			if placeholders["type"] == 'unknown' or placeholders["type"] == 'not invertible': 
				all_invertible = False

                        if placeholders["type"] == 'unknown':
                                print ("WARNING: I don't know whether %s is invertible or not in %s" % (action_name, path_invertible_file))

                        placeholders['proved_undoable'] = placeholders['is_invertible']

                                
			tmp[action_name] = placeholders

	return (tmp, all_invertible)

def read_problem_file(path_undo_file):
	problem_path = path_undo_file+"problem-undo.pddl"
        print problem_path
        f = open(problem_path)
	line = f.readline()
	p = re.compile(r'[^(]*\((?P<action_name>([^\)]*))\)')
        m = p.match(line)
        
        line2 = f.readline()
        num = int(line2.split("-")[-1][:-2])
        
	return (cleanup_name(m.groupdict()['action_name']), num)
	

regexp_time = re.compile(r"(?P<time>([\d]+.[\d]+)) seconds total time")
regexp_tree_layers = re.compile(r"tree layers: (?P<layers>([\d]+))")
regexp_tree_layers_clg = re.compile(r"Longest branch: (?P<layers>([\d]+))")
regexp_num_actions = re.compile(r"total nr. actions: (?P<actions>([\d]+))")
regexp_num_actions_clg = re.compile(r"Total number of printed actions: (?P<actions>([\d]+))")
regexp_evaluations = re.compile(r"[ ]*(?P<time>([\d]+.[\d]+)) seconds \([ ]*(?P<time2>([\d]+.[\d]+)) pure\) evaluating (?P<evaluations>([\d]+)) states")


def parse_plan_success (plan):
        plan_map = {}
        for a in plan: 
                a = a.strip()
                if "--------" in a: 
                        continue
                sp = a.split("---")
                if len(sp) == 3:
                        (key, action, succ) = sp
                        succ2 = None
                elif "(empty plan)" == a: 
                        return 0
                else:
                        (key, action, succ, succ2) = sp

                plan_map[key.strip()] = (action.strip(), succ.split(":")[1].strip(), succ2.split(":")[1].strip() if succ2 else None)
                
        return traverse_plan(plan_map, '0||0', 1.0)

def traverse_plan(plan, curr, P):
        if curr not in plan:
                return P
        (action, s1, s2) = plan[curr]
        if action == "GIVE-UP":
                return 0
        else: 
                if s2:
                        return traverse_plan(plan, s1, P/2.0) + traverse_plan(plan, s2, P/2.0)
                else:
                        return traverse_plan(plan, s1, P)


def get_admin_status(path): 
    dbfile = path + "/sys.db"

    if not os.path.exists(dbfile):
	    return (-1, "error db missing")

    admin_status = "error msg missing"
    sys_time = -1
    con = sqlite3.connect(dbfile)
    with con:
	    cur = con.cursor()
	    
	    data = gather_query(dbfile, "select msg from admin_status;")
	    if data: 
		    admin_status = data[0][0]

	    data = cur.execute("select cputime from sys_time;").fetchall()
	    if data: 
		    sys_time = data[0][0]
	    else:
		    data = cur.execute("select elapsedseconds from admin_time;").fetchall()
		    if  data: 
			    sys_time = float(data[0][0]) - 1
	
    return (sys_time, admin_status)


def get_num_unknown(path_undo_file): 
	f = open(path_undo_file + "/problem-undo.pddl")
	text = f.read()
	f.close()
	num_unknowns = text.count("UNKNOWN")
	return num_unknowns//3
	

def parse_sys (path_undo_file):
	db = path_undo_file + '/sys.db'
		
	return None


def parse_planner_file(path_undo_file, domain, problem, config, planner, action_name): 
	placeholders = {"domain" : domain, "problem" : problem, "config" : config, "planner" : planner, "action_name" : action_name, "type" : "unknown", "plan_length" : "-1", \
			"action_schema" : action_name.split(" ")[0] if " " in action_name else action_name, "time" : 0 , "success" : 0} 


	(sys_time, status_msg) = get_admin_status(path_undo_file)
	placeholders["sys_time"] = sys_time
	placeholders ["num_unknown_facts"] = get_num_unknown(path_undo_file)

	placeholders ["file_id"] = path_undo_file
	placeholders ["run_cff"] = '1'

	result_path = (path_undo_file+"/000.log") if os.path.exists(path_undo_file+"/000.log") else (path_undo_file+"/testbot_output.log")
	
	if not os.path.exists(result_path): 
		print ("Error: log file does not exists", result_path, status_msg)

	f = open(result_path)
	action_type = "unknown"
        reading_plan = False
        plan = []
	lines = f.readlines()
	if not lines and status_msg != "memout": 
		print "Error: log file empty ", result_path, status_msg
		
	for line in lines: 
                line = line.strip()

                if reading_plan:
                        if len(line) == 0:
                                reading_plan = False
                                continue
                        else:
                                plan.append(line)
		if line.find("goal can be simplified to FALSE")!=-1:
                        placeholders["success"] = 1 
			action_type="not undoable in preprocess"		
		elif line.find("initial state failed! problem proved unsolvable!")!=-1:
			placeholders["success"] = 1
                        action_type="not undoable"
		elif line.find("best first search space empty! problem proven unsolvable.")!=-1:
			placeholders["success"] = 1
                        action_type="not undoable"
		elif line.find("empty plan")!=-1:
			placeholders["success"] = 1
                        action_type="empty plan"
		elif "too many relevant facts" in line:
			action_type="error_max_facts"
			break
		elif "found plan as follows" in line or  "found legal plan as follows" in line:
			action_type="undoable"
                        placeholders["success"] = 1
                        #print "Found undoable" 
                        reading_plan = True
		elif regexp_time.match(line):
			placeholders["time"] = regexp_time.match(line).groupdict()["time"]
		elif regexp_num_actions.match(line):
			placeholders["plan_actions"] = regexp_num_actions.match(line).groupdict()["actions"]
		elif regexp_num_actions_clg.match(line):
			placeholders["plan_actions"] = regexp_num_actions_clg.match(line).groupdict()["actions"]
		elif regexp_tree_layers.match(line):
			placeholders["plan_layers"] = regexp_tree_layers.match(line).groupdict()["layers"]
		elif regexp_tree_layers_clg.match(line):
			placeholders["plan_layers"] = regexp_tree_layers_clg.match(line).groupdict()["layers"]
                elif regexp_evaluations.match(line):
			placeholders["evaluations"] = regexp_evaluations.match(line).groupdict()["evaluations"]

        success = parse_plan_success(plan)
        if action_type == 'undoable' and success == 0:
                action_type = 'not undoable'
	if status_msg == "memout":
		action_type = "memout"
	elif status_msg == "timeout":
		action_type = "timeout"
        if action_type == 'not undoable' and 'evaluations' in placeholders and  placeholders["evaluations"] == "1":
                action_type = 'not undoable in heuristic' 
                placeholders["success"] = 1

        placeholders["plan_success"] = success
	placeholders["type"] = action_type

	return placeholders
	

def cleanup_name(action_name):
	return " ".join(action_name.lower().split())


        return results
        #print "Parsing ", action
        #domain_dir = os.path.join (planner_dir, domain)
        #for logfile in filter(lambda x : x[-8:] == ".log.bz2", os.listdir(domain_dir)):
        #    problem = logfile[:-8]
       #     placeholders = {"domain" : domain, "problem" : problem, "config" : pconfig, "action_name" : action_name, "type" : type}
      #      filename = os.path.join (domain_dir, logfile)
     #       process_results(filename, placeholders, data_readers)
    #        results.append(placeholders)
   # return results




parser = argparse.ArgumentParser(description='Gather data from the output file and insert it into a database.')
parser.add_argument('--database', metavar='-DB', 
                    help='target database where the results will be stored')

options = vars(parser.parse_args())
if not options['database']:
    parser.print_help()
    exit(0)


planners = os.listdir(RESULTS_FOLDER)
configs = os.listdir(COMPILED_BENCHMARKS_FOLDER)


istats = defaultdict (list)
dbspec = autobot.dbtools.DBVerbatim (DBSPEC_UNDO)

                        
for config in configs:
        domains = os.listdir("%s/%s" % (COMPILED_BENCHMARKS_FOLDER, config))
        for domain in domains:
                problems = os.listdir("%s/%s/%s" % (COMPILED_BENCHMARKS_FOLDER, config, domain))
                for problem in problems:
	                path_invertible_file = "%s/%s/%s/%s/metadata/invertible.txt" % (COMPILED_BENCHMARKS_FOLDER, config, domain, problem)
                        if not os.path.exists(path_invertible_file):
                                print ("WARNING: missing file " + path_invertible_file)
                                continue

	                (compiler_results, all_invertible) = parse_invertible(path_invertible_file, config, domain, problem)
                        for planner in planners:
                                local_compiler_results = {}
	                        path_undoable_files = "%s/%s/%s/%s/%s/" % (RESULTS_FOLDER, planner, config, domain, problem)
	                        # loop over the instances of a problem
	                        if os.path.isdir(path_undoable_files):
		                        for dirs in os.listdir (path_undoable_files):
			                        path_undo_file = path_undoable_files+dirs+'/'
			                        (action_name, overwritten_id) = read_problem_file(path_undo_file)
			                        planner_results = parse_planner_file(path_undo_file, domain, problem, config, planner, action_name)
			                        planner_results ["overwritten_id"] = overwritten_id
			                        # if (action_name in compiler_results):
				                #         compiler_results [action_name] = res_undoable
			                        # else:
				                #         print "Action from nowhere: " + action_name
				                #         exit(-1)

                                                if not action_name in local_compiler_results:
                                                       local_compiler_results[action_name] = {"num_overwritten_combinations" : 1, 'proved_not_undoable' : 0, 'proved_undoable' : 0 }
                                                else:
                                                       local_compiler_results[action_name]["num_overwritten_combinations"] += 1
                                                        
                                                dbspec["data_plan"].poll(planner_results, istats)
                                                
                                                if "not undoable" in planner_results['type']:
                                                        local_compiler_results[action_name]['proved_not_undoable'] += 1
                                                elif "undoable" in planner_results['type']:
                                                        local_compiler_results[action_name]['proved_undoable'] += 1
                                                        
                                        for action_name in local_compiler_results:
                                                assert (compiler_results[action_name]["num_overwritten_combinations"] == 0 or
                                                        compiler_results[action_name]["num_overwritten_combinations"] == local_compiler_results[action_name]["num_overwritten_combinations"])
                                                compiler_results[action_name]["num_overwritten_combinations"] = local_compiler_results[action_name]["num_overwritten_combinations"]

                                                compiler_results[action_name]["skipped"] = 0
                                                if local_compiler_results[action_name]['proved_not_undoable'] == local_compiler_results[action_name]["num_overwritten_combinations"]:
                                                        compiler_results[action_name]['proved_not_undoable'] = 1

                                                if local_compiler_results[action_name]['proved_undoable'] == local_compiler_results[action_name]["num_overwritten_combinations"]:
                                                        compiler_results[action_name]['proved_undoable'] = 1
	                        elif not all_invertible: 
			                print "Not found results directory: ", path_undoable_files
		                        #exit(-1)

                        for grounded_action in compiler_results:
                                dbspec["data_undo"].poll(compiler_results[grounded_action], istats)

                        

                





for dbtable in dbspec:
        db = sqltools.dbtest (options['database'])     # connect to the sql database
        db.create_table_if_not_exists (dbtable)         # create the table
        db.insert_data (dbtable, istats[dbtable.get_name()]) # and write data
        db.close ()         # close and exit

                

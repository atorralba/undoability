import sqlite3
import itertools
import re
import sys
import math 

# each sql query is of type: 
#   <function>(select domain, problem, <data> from <table> where pconfig='<planner>');
#   We use tuples of the form (<data>, <table>, <planner>, <function>)
query_ops   = ('translatorops', 'data_preprocess_operators', 'bd-disjdt100k-nom')
query_h2ops = ('numops', 'data_preprocess_operators', 'bd-disjdt100k-nom')


# Make query gets a tuple representing a sql query and returns {domain:
# {problem : data} } (or domain : data) if rep_problem is specified
def make_query (cur, query, domain_list, keys):
    res = {}    
    sql_query = "select %s, %s from %s where pconfig='%s' %s" % tuple( [", ".join(keys)] + list(query) + (["and valid='1'"] if query[1] == 'data_plan' else [""]))

    rowdata = cur.execute(sql_query).fetchall()
    for data_tuple in rowdata:
        domain  = data_tuple[0]
        problem = data_tuple[1]
        data    = data_tuple[2] if len(query) == 3 else query[3] (data_tuple[2:])
        if domain not in domain_list:
            continue
        if domain not in res: 
            res [domain] = {}
        res[domain][problem] = data
    return res

def get_rep_problem (data_rep):
    res = {}
    for domain in data_rep: 
        itemmax = max(data_rep[domain].items(), key=lambda x : x[1])
        res [domain] = itemmax[0]
    return res

def gather_data(database, data_queries, domain_list, keys=["domain", "problem"]):
    con = sqlite3.connect(database)
    data = {d : {} for d in domain_list}
    with con:
        cur = con.cursor()
        # We gather all the data from representative problems
        for dq in data_queries: 
            datadq = make_query (cur, data_queries[dq], domain_list, keys)
            for domain in datadq:
                data [domain] [dq] = datadq[domain]    
    return data

def gather_query(database, query):
    con = sqlite3.connect(database)
    data = None
    with con:
        cur = con.cursor()
        data = cur.execute(query).fetchall()

    return data


def time_score(planners, prob_data):
    best = float(min(map(lambda x : x[1], prob_data)))
    res = {}
    for pd in prob_data:
        res[pd[0]] = round(100.0/(1 + math.log10(pd[1]/best)))/100.0
    
    return res


def time_score_2(planners, prob_data):
    res = {}
    for pd in prob_data:
        res[pd[0]] = round(100.0/(1 + math.log10(pd[1]/1.0)))/100.0
    
    return res


def cov_score(planners, prob_data):
    res = {key : 0 for key in planners}
    for pd in prob_data:
        res[pd[0]] += 1
    return res

# Computes the total score for the given domains
# Input: score_domains map (domain => problem => score)
# Output: total score
def compute_total_score(t_solved, domains, planners, f_score):
    problems_domain['score'] = len(domains) 
    problems_domain['scorecov'] = len(domains) 
    considered = set()
    # a) replace xxx11 and xxx08 by xxx0811
    doms = set(domains)
    sc = {p : 0 for p in planners} #Initialize score to 0
    for (d1, d2) in duplicated_problems:
        #Join d1 and d2
        if d1 in doms and d2 in doms:
            problems_domain['score'] -= 1
            problems_domain['scorecov'] -= 1
            doms.remove(d1)
            doms.remove(d2)
            num_problems = problems_domain[d1] + problems_domain[d2]
            sc_dom = {p : 0.0 for p in planners}
            for dp1, dp2 in duplicated_problems[(d1, d2)]: 
                num_problems -= 1
                #os.system('diff -q testdomains/%s/%s/problem.pddl testdomains/%s/%s/problem.pddl' % (d1, dp1, d2, dp2))
                #os.system('diff -q testdomains/%s/%s/domain.pddl testdomains/%s/%s/domain.pddl' % (d1, dp1, d2, dp2))
                if (d1, dp1) in t_solved or (d2, dp2) in t_solved:
                    considered.add((d1, dp1))
                    considered.add((d2, dp2))
                    for s in (f_score(planners, t_solved[(d1, dp1)]).items() if (d1, dp1) in t_solved else []) + (f_score(planners, t_solved[(d2, dp2)]).items() if (d2, dp2) in t_solved else []):
                        sc_dom [s[0]] += s[1]/(2.0) # Add score of the duplicated problem

            for prob in t_solved:
                if not prob in considered and (prob[0] == d1 or prob[0] == d2):
                    for s in f_score(planners, t_solved[prob]).items():
                        sc_dom [s[0]] += s[1]

            for pconfig in sc_dom:
                sc[pconfig] += sc_dom[pconfig]/float(num_problems)

    for prob in t_solved:
        if prob[0] in doms:
            for s in f_score(planners, t_solved[prob]).items():
                sc [s[0]] += s[1]/float(problems_domain[prob[0]])
    return sc


    #             sc1 = p1[dp1]
    #             sc2 = p2[dp2]
    #             p1.remove(dp1)
    #             p2.remove(dp2)
    #             p3['dup'+ dp1 + '-' + dp2] = (sc1 + sc2)/2.0
    #         for p in p1:
    #             p3['p1-'+ p] = p1[p]
    #         for p in p2:
    #             p3['p2-'+ p] = p2[p]
    #         score_domains [d1 + d2] = p3

    # for prob in score_domains:   
    #     domain = prob[0]
    #     print domain
    #     score = f_score(planners, t_solved[prob])
    #     coverage = cov_score(planners, t_solved[prob])
    #     for pconfig in score:
    #         data[domain][pconfig] += score[pconfig]
    #         data['total'][pconfig] += score[pconfig]
    #         data['totalcov'][pconfig] += coverage[pconfig]

    # # b) Score each domain
    # res_domains = map (lambda x : sum(x.values())/float(len(x)), score_domains.values())
    # return sum(res_domains)
 
def gather_data_score(database, domain_list, planner_list, domain_regexp, planner_regexp, f_score, portfolios = {}):
    ts_sql = "select domain, problem, pconfig, atime from data_plan where valid='1';"
    t_solved = {}
    domains = set()
    planners = set()
    domain_planner = set()
    con = sqlite3.connect(database)
    with con:
        cur = con.cursor()
        rowdata = cur.execute(ts_sql).fetchall()
        for (domain, problem, pconfig, atime) in rowdata:
            if (not domain_list or domain in domain_list) and \
               (not planner_list or pconfig in planner_list) and \
               re.match(domain_regexp, domain) and re.match(planner_regexp, pconfig):
                domains.add(domain)
                planners.add(pconfig)
                domain_planner.add((domain, pconfig))
                if (domain, problem) not in t_solved:
                    t_solved [(domain, problem)] = []
                t_solved [(domain, problem)].append((pconfig, atime))

        #Check whether if we have data for every domain and pconfig
        for data in itertools.product(domain_list if domain_list else domains, planners):
            if data not in domain_planner:
                if data not in cur.execute("SELECT domain, pconfig from admin_status").fetchall():
                    print >> sys.stderr, "WARNING, there is no data for: ", data
                    
    #Add portfolios: 
    for port in portfolios: 
        planners = planners.union(set([port]))
        for prob in t_solved:   
            best = float("inf")
            for pconfig, time in t_solved[prob]:
                if pconfig in portfolios[port] and best > time: 
                    best = time
            if best < float("inf"):
                t_solved[prob].append((port, best))

    for d in domain_list:
        if d not in domains:
            domains.add(d)
    
    data = {key : {key : 0 for key in planners} for key in domains.union(set(['total', 'score', 'totalcov', 'scorecov']))}
    data_cov = {key : {key : 0 for key in planners} for key in domains}

    for prob in t_solved:   
        domain = prob[0]
        score = f_score(planners , t_solved[prob])
        coverage = cov_score(planners, t_solved[prob])
        for pconfig in score:
            data[domain][pconfig] += score[pconfig]
            data['total'][pconfig] += score[pconfig]
            data['totalcov'][pconfig] += coverage[pconfig]
            data_cov[domain][pconfig] += coverage[pconfig]

    data['score'] = compute_total_score(t_solved, domains, planners, f_score)
    data['scorecov'] = compute_total_score(t_solved, domains, planners, cov_score)

            
    #Add summary rows
    #data["total"] = {planner : sum(map(lambda (k,v) : v [planner] if k in domains else 0, data.iteritems())) for planner in planners}
    #data["score"] = {planner : sum(map(lambda (k,v) : v [planner]/float(problems_domain[k]) if k in domains else 0, data.iteritems())) for planner in planners}
    #data["totalcov"] = {planner : sum(map(lambda (k,v) : v [planner] if k in domains_cov else 0, data_cov.iteritems())) for planner in planners_cov}
    #data["scorecov"] = {planner : sum(map(lambda (k,v) : v [planner]/float(problems_domain[k]) if k in domains_cov else 0, data_cov.iteritems())) for planner in planners_cov}

    return domains, planners, data, data_cov


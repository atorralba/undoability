import re
import sys


from string import Template     # substitution

#Alvaro: BotTestCase now has a list of data_readers. This are objects with a parse method. 
#Here, we define several basic DataReaders, though the users may define their own classes.
class DataReaderRegexp: 
    def __init__(self, regexp, namespace = None):
        (self.namespace, self.regexp, self.index) = \
         (namespace, re.compile(regexp), 0)

    def init(self):
        self.index = 0

    def parse (self, line, placeholders):
        # print namespace, "PARSE: ", line
        restat = self.regexp.match (line)
        if (restat):
            #print "Matched: ", line
            data = restat.groupdict ()
            if self.namespace:
                data ["index"] = self.index
                self.index += 1
                if self.namespace not in placeholders:
                    placeholders [self.namespace] = []
                placeholders [self.namespace].append(data)
            else:
                placeholders.update(data)


DBSPEC_PREPROCESS = """
data_preprocess_operators {
      planner              text  :planner           Error;
      pconfig              text  :pconfig           Error;
      domain               text  :domain            Error;
      problem              text  :problem           Error;
      translatorvars    integer  :translatorvars    None;
      translatorfacts   integer  :translatorfacts   None;
      translatorops     integer  :translatorops     None;
      timetranslate        real  :translatetime     None;
      sasvars           integer  :sasvars           None;
      sasfluents        integer  :sasfluents        None;
      derivedvars       integer  :derivedvariables  None;
      numops            integer  :numops            None;
      mutexfw           integer  :nummutexfw        None;
      mutexbw           integer  :nummutexbw        None;
      numexactlyone     integer  :numexactlyone     None;
      potentialpre      integer  :potentialpre     None;
      opswithpotentialpre integer :opspotentialpre  None;
      potentialcontradict integer :potentialcontradict  None;
      augmentedpre      integer  :augmentedpre     None;
      opswithaugmentedpre integer :opsaugmentedpre  None;
      timepreprocess       real  :timepreprocess    None;
      timeh2               real  :timeh2            None;
      iterationsh2      integer  :iterationsh2      None;
      tasksize          integer  :tasksize          None;
}
"""               


def preprocessor_readers(): 
    data_readers = []
    global_regexps = ["Translator variables: (?P<translatorvars>(\d+))",
                      "Translator facts: (?P<translatorfacts>(\d+))",
                      "Translator operators: (?P<translatorops>(\d+))", 
                      "Done! \[(?P<timetranslate>(\d*[.\d+]*))s CPU", 
                      "Number of vars: (?P<sasvars>(\d+))",
                      "Preprocessor facts: (?P<sasfluents>(\d+))",
                      "Preprocessor derived variables: (?P<derivedvariables>(\d+))", 
                      "Preprocessor operators: (?P<numops>(\d+))", 
                      "Preprocessor mutex groups fw: (?P<nummutexfw>(\d+)) bw: (?P<nummutexbw>(\d+)) exactly one: (?P<numexactlyone>(\d+))",
                      "Potential preconditions: (?P<potentialpre>(\d+))", 
                      "Ops with potential preconditions: (?P<opswithpotentialpre>(\d+))", 
                      "Potential preconditions contradict effects: (?P<potentialcontradict>(\d+))",
                      "Augmented preconditions: (?P<augmentedpre>(\d+))", 
                      "Ops with augmented preconditions: (?P<opswithaugmentedpre>(\d+))", 
                      "Preprocess time: (?P<timepreprocess>(\d*[.\d+]*))", 
                      "Total mutex and disambiguation time: (?P<timeh2>(\d*[.\d+]*)) iterations: (?P<iterationsh2>(\d*[.\d+]*))", 
                      "Preprocessor task size: (?P<tasksize>(\d*[.\d+]*))"]

    for regexp in global_regexps:
        data_readers.append(DataReaderRegexp(regexp))
    return data_readers


DBSPEC_FD_LAYER = """
data_flayer {
      planner     text  :planner       Error;
      pconfig     text  :pconfig       Error;
      domain      text  :domain        Error;
      problem     text  :problem       Error;
      id         integer   @estep:index       Error;
      fvalue     integer   @estep:fvalue      Error;
      evaluated  integer   @estep:evaluated   Error;
      expanded   integer   @estep:expanded    Error;
      totaltime  real   @estep:totaltime   Error;
}
"""

DBSPEC_FD_SOLVED = """
data_fdsolved {
    planner                 text     :planner                 Error;
    pconfig                 text     :pconfig                 Error;
    domain                  text     :domain                  Error;
    problem                 text     :problem                 Error;
    plan_length             integer  :planlength              None;
    plan_cost               integer  :plancost                None;
    initial_h               integer  :initialh                None;
    expanded                integer  :expanded                None;
    reopened                integer  :reopened                None;
    evaluated               integer  :evaluated               None;
    evaluations             integer  :evaluations             None;
    generated               integer  :generated               None;
    dead_ends               integer  :deadends                None;
    expandeduntillastjump   integer  :expandeduntillastjump   None;
    reopeneduntillastjump   integer  :reopeneduntillastjump   None;
    evaluateduntillastjump  integer  :evaluateduntillastjump  None;
    generateduntillastjump  integer  :generateduntillastjump  None;
    hash_size               integer  :hashsize                None;
    hash_bucket             integer  :hashbucket              None;
    search_time             real  :searchtime              None;
    total_time              real  :totaltime               None;
    peak_memory             integer  :peakmemory              None;
}
"""



def fd_layer_readers():
    data_readers = []
    #f = 1 [1 evaluated, 0 expanded, t=0s]
    data_step_regexp = 'f = (?P<fvalue>(\d+)) \[(?P<evaluated>(\d+)) evaluated, (?P<expanded>(\d+)) expanded, t=(?P<totaltime>([\d*.]*\d+))s\]'
    data_readers.append(DataReaderRegexp(data_step_regexp, "estep"))
    return data_readers

def fd_solved_readers():
    data_readers = []
    fd_global_regexps = ['Plan length: (?P<planlength>(\d+)) step\(s\).',
                         'Plan cost: (?P<plancost>(\d+))',
                         'Initial state h value: (?P<initialh>(\d+)).',
                         'Expanded (?P<expanded>(\d+)) state\(s\).',
                         'Reopened (?P<reopened>(\d+)) state\(s\).',
                         'Evaluated (?P<evaluated>(\d+)) state\(s\).',
                         'Evaluations: (?P<evaluations>(\d+))',
                         'Generated (?P<generated>(\d+)) state\(s\).',
                         'Dead ends: (?P<deadends>(\d+)) state\(s\).',
                         'Expanded until last jump: (?P<expandeduntillastjump>(\d+)) state\(s\).',
                         'Reopened until last jump: (?P<reopeneduntillastjump>(\d+)) state\(s\).',
                         'Evaluated until last jump: (?P<evaluateduntillastjump>(\d+)) state\(s\).',
                         'Generated until last jump: (?P<generateduntillastjump>(\d+)) state\(s\).',
                         'Search space hash size: (?P<hashsize>(\d+))',
                         'Search space hash bucket count: (?P<hashbucket>(\d+))',
                         'Search time: (?P<searchtime>((\d*.)?\d+))s',
                         'Total time: (?P<totaltime>((\d*.)?\d+))s',
                         'Peak memory: (?P<peakmemory>(\d+)) KB'
    ]    
    for regexp in fd_global_regexps:
        data_readers.append(DataReaderRegexp(regexp))
    
    return data_readers


DBSPEC_CGAMER_LAYER = """
data_sym_step {
      planner       text   :planner           Error;
      pconfig       text   :pconfig           Error;
      domain        text   :domain            Error;
      problem       text   :problem           Error;
      id         integer   @estep:index       Error;
      costtype      text   @estep:costtype    Error;
      dir           text   @estep:direction   Error;
      g          integer   @estep:g_value     Error;
      f          integer   @estep:f_value     Error;
      states        real   @estep:stepstates  Error;
      nodes      integer   @estep:stepnodes   Error;
      time          real   @estep:timestep    Error;
      totaltime     real   @estep:totaltime                Error;
      totalnodes integer   @estep:totalnodes               Error;
      totalmemory   real   @estep:totalmem                 Error;
}
"""

def cgamer_layer_readers():
    #StepCost: forward g=69 f=74 [194442.0 states, 148111 nodes] in 1.349s beforeDuplicates: [451260.0 states, 251189 nodes] beforeSpurious: [194442.0 states, 148111 nodes] timeDuplicates: 0.107s, timeSpurious: 0.107s, total time: 32.417s, total nodes: 1905117, total memory: 455.862896M
    #StepZero: forward g=1 f=2 [9.0 states, 69 nodes] in 0.001s beforeDuplicates: [9.0 states, 69 nodes] beforeSpurious: [9.0 states, 69 nodes] timeDuplicates: 0.0s, timeSpurious: 0.0s, total time: 3.444s, total nodes: 8843, total memory: 283.97984
    data_readers = []
    bdd_str = Template('\[(?P<$varstates>(\d*.\d+(?:[eE][+-]*\d+)?)) states, (?P<$varnodes>(\d+)) nodes\]')
    time_str = Template('(?P<$var>(\d*.\d+))s')

    data_step_str = Template('$type $dir g=$g f=$f $bdd in $time beforeDuplicates: $bddduplicate beforeSpurious: $bddspurious timeDuplicates: $timeduplicates, timeSpurious: $timespurious, total time: $totaltime, total nodes: $totalnodes, total memory: $totalmem')

    step_info = {'type' : 'Step(?P<costtype>(Cost|Zero)):', 
                 'dir' : '(?P<direction>(forward|backward))', 
                 'g' : '(?P<g_value>(\d+))', 
                 'f' : '(?P<f_value>(\d+))', 
                 'bdd' : bdd_str.substitute(varstates='stepstates', varnodes='stepnodes'),
                 'time' : time_str.substitute(var='timestep'),
                 'bddduplicate' : bdd_str.substitute(varstates='beforeduplicatesstates', varnodes='beforeduplicatesnodes'),
                 'bddspurious' : bdd_str.substitute(varstates='beforespuriousstates', varnodes='beforespuriousnodes'),
                 'timeduplicates' : time_str.substitute(var='timeduplicates'),
                 'timespurious' : time_str.substitute(var='timespurious'),
                 'totaltime' : time_str.substitute(var='totaltime'),
                 'totalnodes' : '(?P<totalnodes>(\d+))',
                 'totalmem' : '(?P<totalmem>(\d*.\d+))M'
             }
    #print data_step_str.substitute(step_info)
    #re.compile(data_step_str.substitute(step_info))
    data_readers.append(DataReaderRegexp(data_step_str.substitute(step_info), "estep"))

    return data_readers



DBSPEC_MaS = """
data_mas {
    planner                 text     :planner                 Error;
    pconfig                 text     :pconfig                 Error;
    domain                  text     :domain                  Error;
    problem                 text     :problem                 Error;
    id                      integer  :masindex               None;
    mas_time                real     :masmastime             None;
    add_time                real     :masaddtimegen          None;
    tabular_entries         integer  :mastabentries          None;
    tabular_memory          integer  :mastabmemory           None;
    add_nodes               integer  :masaddnodes            None;    
    add_memory              integer  :masaddmemory           None;    
    pruned_states           integer  :masprunedstates        None;   
    percentage_pruned_states real    :maspercpruned          None;   
    peak_memory             integer  :masabspeak             None;
}
"""




def add_mas_readers():
    data_readers = []
    #data_readers.append(DataReaderMaSLevel())
    #data_readers.append(DataReaderHValues('mash', 'HValues'))
    #data_readers.append(DataReaderHValues('masg', 'GValues'))
    #data_readers.append(DataReaderHValues('masf', 'FValues'))
    #data_readers.append(DataReaderRegexp("(First|Next) variable: #(?P<var>(\d+))", 'masv'))

    regexps = ["Pruned states: (?P<masprunedstates>(\d+))",
              "Percentage pruned states: (?P<maspercpruned>(\d+.\d+))", 
              "Done initializing merge-and-shrink heuristic \[(?P<masmastime>((\d+.)?\d+))s\] initial h value: (\d+) tabular_entries: (?P<mastabentries>(\d+)) tabular_memory: (?P<mastabmemory>(\d+)) bytes", 
              "Done computing ADD \[(?P<masaddtimegen>((\d+.)?\d+))s\] size: (?P<masaddnodes>(\d+)) nodes (?P<masaddmemory>(\d+)) bytes",
              "Estimated peak memory for abstraction: (?P<masabspeak>(\d+)) bytes"]
    
    for reg in regexps:
        data_readers.append(DataReaderRegexp(reg))
    return data_readers



DATA_READERS = {"preprocess" : preprocessor_readers(), "fd_layer" : fd_layer_readers(), "fd_solved" : fd_solved_readers(),  "cgamer_layers" : cgamer_layer_readers(), "add_mas" : add_mas_readers()
#, "unsol" : unsolvable_readers()
} 

DB_SPECS = {"preprocess" : DBSPEC_PREPROCESS, "fd_layer" : DBSPEC_FD_LAYER, "fd_solved" : DBSPEC_FD_SOLVED, "cgamer_layers" : DBSPEC_CGAMER_LAYER, "add_mas" : DBSPEC_MaS
#, "unsol" : DBSPEC_UNSOLVABLE 
}

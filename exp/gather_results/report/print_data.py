import sys

def print_header (header, colalign=[]):
    r = False
    rest = []
    num = []
    n = 0
    for h in header:
        if isinstance(h[1], list):
            r = True
            rest += h[1]
        else: 
            rest += [("", "")]
        if isinstance (h[1], list):
            n += len(to_list(h[1]))
        else:
            n+=1
        num.append(n)

    print '&' + ' & '.join(map (lambda (i, x) : "\\multicolumn{%d}{c%s}{%s}" % (len(to_list(x[1])), '|' if r and colalign and colalign[num[i]] == 'r|' else '','\\' + x[0]) 
                                if isinstance (x[1], list) else ('\\' + x[0] if len(x[0]) else ""), enumerate(header))) + (' \\\\\\hline' if not r else '\\\\')
    
    if r:
        print_header(rest, colalign)
    
def print_latex(rows, columns, header, formatted_data):
    print "% ", " ".join(sys.argv)
    colalign = ["|r|"] + ["r" for col in columns]
    if any(isinstance (h[1], list) for h in header):
        num = 0
        for h in header: 
            num += len(to_list(h[1])) if isinstance (h[1], list) else 1
            colalign[num] = "r|"
    else: 
        colalign[-1] = "r|"

    print "\\begin{tabular}{%s}" % ("".join(colalign))
    print "\\hline"
    if header:
        print_header(header, colalign)
    else:
        print " & ".join(["domain"] + map(lambda col : clean_latex_domain(col), columns)) + "\\\\\\hline"
    first_row = True
    for row in rows:
        if row != 'hline' and row != "*":
            data_to_print = [formatted_data[row][col] if ("*" not in formatted_data or col not in formatted_data["*"])
                             else "\\multirow{%d}{*}{%s}" % (len(set(rows) - set(["domain", "*", "hline"])), formatted_data["*"][col])
                                   if first_row else ""
                             for col in columns]
            print " & ".join([clean_latex_domain(row)] + data_to_print) + "\\\\" #\\hline"
            first_row = False
        else:
            print "\\hline"
    print "\\hline"
    print "\\end{tabular}"

def print_plain(rows, columns, formatted_data):
    print "domain", " ".join(map(lambda x: str(x), columns))
    for row in rows:
        if row != 'hline' and row != '*':
            print row, " ".join([formatted_data[row][col]  if ("*" not in formatted_data or col not in formatted_data["*"]) \
                                                           else formatted_data["*"][col] \
                                                           for col in columns])

def print_data (rows, columns, header, formatted_data, style):
    if style == 'latex':
        print_latex(rows, columns, header, formatted_data)
    else:
        print_plain(rows, columns, formatted_data)


marker_best = {'latex' : '\\bf%s', 'plain' : '*%s'}
marker_best_cov = {'latex' : '*%s', 'plain' : '#%s'}

  
def get_formatted_data(data, format_data):
    return {dom : {col : format_data[dom].format(data[dom][col]) for col in data[dom]} for dom in data}

def get_formatted_data_col(data, format_data):
    return {dom : {col : (format_data[col].format(data[dom][col]) if isinstance(data[dom][col], (int, float)) else data[dom][col])  for col in data[dom]} for dom in data}
        
def mark_best_column (data, formatted_data, mark_str):
    for row in data:
        if len(data[row]) > 1:
            best = max(data[row].values()) # if all equals do not highlight anything
            if min(data[row].values()) < best:
                for col in formatted_data[row]:
                    if data[row][col]/best >= 0.99:
                        formatted_data[row][col] = mark_str % formatted_data[row][col]

def mark_best (data, formatted_data, mark_str):
    best = 0.01  # if the max is 0 do not highlight anything
    for row in data:
        best = max(best, max(data[row].values())) 
    for row in data:
        for col in formatted_data[row]:
            if data[row][col]/best >= 0.99:
                formatted_data[row][col] = mark_str % formatted_data[row][col]


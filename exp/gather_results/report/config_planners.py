planners_img_ct = [("nos", [("nodisj"       , "bd-nodisj-nos"),
                               ("notr"         , "bd-nobdd-nos"),
                               ("ctnormal{}"   , "bd-ct_normal-nos"),
                               ("ctnormal{5}"  ,  "bd-ct_normal_5-nos"),
                               ("ctnormal{10}" , "bd-ct_normal_10-nos"), 
                               ("ctnormal{20}" , "bd-ct_normal_20-nos")]),
                      ("nom", [("nodisj"       , "bd-nodisj-nom"),
                               ("notr"         , "bd-nobdd-nom"),
                               ("ctnormal{}"   , "bd-ct_normal-nom"),
                               ("ctnormal{5}"  , "bd-ct_normal_5-nom"),
                               ("ctnormal{10}" , "bd-ct_normal_10-nom"),
                               ("ctnormal{20}" , "bd-ct_normal_20-nom")])]

planners_img_bd = [("nodisj"       , "bd-nodisj-nom"),
                   ("notr"         , "bd-nobdd-nom"),
                   # ("ctrandom{}"   , "bd-ct_random-nom"), 
                   # ("ctinverse{}"  , "bd-ct_inverse-nom"),
                   # ("ctdynamic{}"  , "bd-ct_dynamic-nom"),
                   ("ctnormal{}"   , "bd-ct_normal-nom"),
#                   ("ctnormal{5}"  , "bd-ct_normal_5-nom"),
#                   ("ctnormal{10}" , "bd-ct_normal_10-nom"),
                   ("ctnormal{20}" , "bd-ct_normal_20-nom"), 
                   ("disjdt{100k}", "bd-disjdt100k-nom"), 
                   #                   ("disjsm{100k}", "bd-disjsm100k-nom"), 
                   #                   ("disjctnormal{100k}{5}", "bd-disjct100k_normal_5-nom"),
                   #                   ("disjctnormal{100k}{20}", "bd-disjct100k_normal_20-nom")
                    ]

planners_img_bd_disj = [("nodisj", "bd-nodisj-nom"),
                        #("notr", "bd-nobdd-nom"),
                        #("ctnormal{20}", "bd-ct_normal_20-nom"),
                        ("disjdt{1}", "bd-disjdt1k-nom"), 
                        ("disjdt{100k}", "bd-disjdt100k-nom"),
                        ("disjsm{100k}", "bd-disjsm100k-nom"),
                        ("disjctnormal{100k}{}", "bd-disjct100k_normal_0-nom"),
                        ("disjctnormal{100k}{5}", "bd-disjct100k_normal_5-nom"),
                        ("disjctnormal{100k}{20}", "bd-disjct100k_normal_20-nom")]

planners_img_bd_disj_size = [("nodisj", "bd-nodisj-nom"),
                             ("notr", "bd-nobdd-nom"),
                             ("disjdt{5k}", "bd-disjdt5k-nom"), 
                             ("disjdt{10k}", "bd-disjdt10k-nom"), 
                             ("disjdt{50k}", "bd-disjdt50k-nom"), 
                             ("disjdt{100k}", "bd-disjdt100k-nom"),
                             ("disjdt{1M}", "bd-disjdt1000k-nom"),
                             ("disjsm{5k}", "bd-disjsm5k-nom"), 
                             ("disjsm{10k}", "bd-disjsm10k-nom"), 
                             ("disjsm{50k}", "bd-disjsm50k-nom"), 
                             ("disjsm{100k}", "bd-disjsm100k-nom"),
                             ("disjsm{1M}", "bd-disjsm1000k-nom")]


planners_img_astar = [("nodisj"       , "astar-nodisj-nom"),
                   ("notr"         , "astar-nobdd-nom"),
                   # ("ctrandom{}"   , "bd-ct_random-nom"), 
                   # ("ctinverse{}"  , "bd-ct_inverse-nom"),
                   # ("ctdynamic{}"  , "bd-ct_dynamic-nom"),
                   ("ctnormal{}"   , "astar-ct_normal-nom"),
#                   ("ctnormal{5}"  , "bd-ct_normal_5-nom"),
#                   ("ctnormal{10}" , "bd-ct_normal_10-nom"),
                   ("ctnormal{20}" , "astar-ct_normal_20-nom"), 
                      ("disjdt{100k}", "astar-disjdt100k-nom"), 
                      #                   ("disjsm{100k}", "bd-disjsm100k-nom"), 
                      #                   ("disjctnormal{100k}{5}", "bd-disjct100k_normal_5-nom"),
                      #                   ("disjctnormal{100k}{20}", "bd-disjct100k_normal_20-nom")
                    ]


planners_img_fw_nom = [ ("nodisj", "fw-nodisj-nom"), ("notr", "fw-noimg-nom"),
                        ("ctnormal{20}", "fw-ct_normal_20-nom"),
                        ("disjdt{100k}", "fw-disjdt100k-nom")]


planners_img_bw_nom = [("nodisj", "bw-nodisj-nom"), ("notr", "bw-nobdd-nom"),
                        ("ctnormal{20}", "bw-ct_normal_20-nom"), ("disjdt{100k}", "bw-disjdt100k-nom")]

planners_img_unidir = [("fw", planners_img_fw_nom), ("bw", planners_img_bw_nom)]

planners_inv_bd = [("nos", "bd-disjdt100k-nos"), ("nom", "bd-disjdt100k-nom"), ("mut{1k}", "bd-disjdt100k-m1k"), ("mut{100k}", "bd-disjdt100k-m100k"), ("edel", "bd-disjdt100k-edel"), ("edet", "bd-disjdt100k-edet")]

planners_inv_fw = [("nos", "fw-disjdt100k-nos"), ("nom", "fw-disjdt100k-nom"), ("mut{100k}", "fw-disjdt100k-m100k"), ("edel", "fw-disjdt100k-edel")] #
planners_inv_bw = [("nos", "bw-disjdt100k-nos"),("nom", "bw-disjdt100k-nom"), ("mut{100k}", "bw-disjdt100k-m100k"), ("edel", "bw-disjdt100k-edel")] # 
planners_inv_unidir = [("fw", planners_inv_fw), ("bw", planners_inv_bw)]

planners_inv_minimization = [("nomut", "bd-disjdt100k-nom"), ("mut{100k}", "bd-disjdt100k-m100k"), ("mres", "bd-disjdt100k-mres100k"), ("mcons", "bd-disjdt100k-mcon100k"), ("mnpa", "bd-disjdt100k-mnpa100k"), ("mlic", "bd-disjdt100k-mlic100k")]

planners_inv_astar = [("nos", "astar-disjdt100k-nos"), 
                      ("nom", "astar-disjdt100k-nom"), 
                      ("mut{1k}", "astar-disjdt100k-m1k"),
                      ("mut{100k}", "astar-disjdt100k-m100k")]


planners_sfw_mas =[("listastar", 
                    [
                        ("Gamer",  [("none", [("multicolumn{1}{c|}{}", "sym-sfw")]), 
                                    ("bop", [("numprint{100}k", "sym-sfw-mas_rev_bop100k"), ("multicolumn{1}{r|}{\\numprint{200}k}", "sym-sfw-mas_rev_bop200k")]),
                                    ("gop", [("numprint{100}k", "sym-sfw-mas_rev_gop100k"), ("multicolumn{1}{c|}{$\\infty$}", "sym-sfw-mas_rev_gop")])]), 
                        ("fdrev",  [("none", [("multicolumn{1}{c|}{}",  "fd_rev-sfw")]), 
                                    ("bop", [("numprint{100}k", "fd_rev-sfw-mas_rev_bop100k"), ("multicolumn{1}{r|}{\\numprint{200}k}", "fd_rev-sfw-mas_rev_bop200k")]),
                                    ("gop", [("numprint{100}k", "fd_rev-sfw-mas_rev_gop100k"), ("multicolumn{1}{c|}{$\\infty$}", "fd_rev-sfw-mas_rev_gop")])]), 
                        ("fdcggoalran",  [("none", [("multicolumn{1}{c|}{}",  "fd_cggoalran-sfw")]), 
                                    ("bop", [("numprint{100}k", "fd_cggoalran-sfw-mas_rev_bop100k"), 
                                             ("multicolumn{1}{r|}{\\numprint{200}k}", "fd_cggoalran-sfw-mas_rev_bop200k")]),
                                    ("gop", [("numprint{100}k", "fd_cggoalran-sfw-mas_rev_gop100k"), ("multicolumn{1}{c|}{$\\infty$}", "fd_cggoalran-sfw-mas_rev_gop")])])]),
                   ("astar", 
                    [   ("none", [("xspace", [("multicolumn{1}{c|}{}", "seq-opt-blind")])]),
                        ("Gamer", [("bop", [("numprint{100}k", "sym-astar-mas_add_rev_bop100k"), 
                                            ("multicolumn{1}{r|}{\\numprint{200}k}", "sym-astar-mas_add_rev_bop200k")]),  
                                   ("gop", [("numprint{100}k", "sym-astar-mas_add_rev_gop100k"), 
                                            ("multicolumn{1}{c|}{$\\infty$}", "sym-astar-mas_add_rev_gop")])]),   
                                        
                        ("fdrev",  [("bop", [("numprint{100}k", "fd-astar-mas_add_rev_bop100k"), 
                                            ("multicolumn{1}{r|}{\\numprint{200}k}", "fd-astar-mas_add_rev_bop200k")]),  
                                   ("gop", [("numprint{100}k", "fd-astar-mas_add_rev_gop100k"), 
                                            ("multicolumn{1}{c|}{$\\infty$}", "fd-astar-mas_add_rev_gop")])]),   
                        ("fdcggoalran",  [("bop", [("numprint{100}k", "fd-astar-mas_add_cggoalran_bop100k"), 
                                            ("multicolumn{1}{r|}{\\numprint{200}k}", "fd-astar-mas_add_cggoalran_bop200k")]),  
                                   ("gop", [("numprint{100}k", "fd-astar-mas_add_cggoalran_gop100k"), 
                                            ("multicolumn{1}{c|}{$\\infty$}", "fd-astar-mas_add_cggoalran_gop")])])
                    ])
               ]


planners_spmas =[("symorder", [("sper",  [("xspace --", "sym-astar-sp10M"), ("abspdb{}", "sym-astar-sp10M_pdbs_rev")]), 
                               ("spmas", [("shbop{10k}", "sym-astar-sp10M_smas_bop10k"), ("shgop{10k}", "sym-astar-sp10M_smas_gop10k")]),
                               ("mas", [("shbop{10k}", "sym-astar-mas_add_rev_bop10k"), ("shgop{10k}", "sym-astar-mas_add_rev_gop10k")])]),
                 ("fdorder", [("sper",  [("xspace --", "fd-astar-sp10M"), ("abspdb{}", "fd-astar-sp10M_pdbs_rev")]), 
                               ("spmas", [("shbop{10k}", "fd-astar-sp10M_smas_bop10k"), ("shgop{10k}", "fd-astar-sp10M_smas_gop10k")]),
                               ("mas", [("shbop{10k}", "fd-astar-mas_add_rev_bop10k"), ("shgop{10k}", "fd-astar-mas_add_rev_gop10k")])]),
                 ("xspace", [("textnormal{LM-}", [("multicolumn{1}{c}{cut}", "seq-opt-lmcut")])])]

planners_spmas_nm =[("symorder", [("sper",  [("xspace --", "sym-astar-sp10M_nom_nma"), ("abspdb{}", "sym-astar-sp10M_pdbs_rev_nom_nma")]), 
                               ("spmas", [("shbop{10k}", "sym-astar-sp10M_smas_bop10k_nom_nma"), ("shgop{10k}", "sym-astar-sp10M_smas_gop10k_nom_nma")]),
                               ("mas", [("shbop{10k}", "sym-astar-mas_add_rev_bop10k"), ("shgop{10k}", "sym-astar-mas_add_rev_gop10k")])]),
                 ("fdorder", [("sper",  [("xspace --", "fd-astar-sp10M_nom_nma"), ("abspdb{}", "fd-astar-sp10M_pdbs_rev_nom_nma")]), 
                               ("spmas", [("shbop{10k}", "fd-astar-sp10M_smas_bop10k_nom_nma"), ("shgop{10k}", "fd-astar-sp10M_smas_gop10k_nom_nma")]),
                               ("mas", [("shbop{10k}", "fd-astar-mas_add_rev_bop10k"), ("shgop{10k}", "fd-astar-mas_add_rev_gop10k")])]),
                 ("xspace", [("textnormal{LM-}", [("multicolumn{1}{c|}{cut}", "seq-opt-lmcut")])])]


planners_symba =[("symorder", [("none",  "sym-sbd"),
                               ("pdbs", [("rev", "sym-sbd10M-phpdb1_rev_nms10M"), ("lev", "sym-sbd10M-phpdb1_lev_nms10M")]), 
                               ("smas", [("bop10k", "sym-sbd10M-phsmas_bop10k_nms10M"), ("gop10k", "sym-sbd10M-phsmas_gop10k_nms10M")])]),
                 ("fdorder", [("none",  "fd-sbd"),
                               ("pdbs", [("rev", "fd-sbd10M-phpdb1_rev_nms10M"), ("lev", "fd-sbd10M-phpdb1_lev_nms10M")]), 
                               ("smas", [("bop10k", "fd-sbd10M-phsmas_bop10k_nms10M"), ("gop10k", "fd-sbd10M-phsmas_gop10k_nms10M")])])]


planners_blind_fw = [("fdblind", "seq-opt-blind"),  ("Gamer", "fw-nodisj-nom"),  ("cGamer", "fw-disjdt100k-edel")]
planners_blind_bw = [("fdrblind", "nodisamb-tree"),  ("Gamer", "bw-nodisj-nom"),  ("cGamer", "bw-disjdt100k-edel")]
planners_blind = [("gamerfw",  planners_blind_fw), ("gamerbw",  planners_blind_bw)]


planners_state_art = [("astar", [("masbop", "seq-opt-mas-bop"), ("masgop", "seq-opt-mas-gop"),# ("mas", "seq-opt-mas-bop|seq-opt-mas-gop"), 
                                 ("textnormal{LM-}", [("multicolumn{1}{c}{cut}", "seq-opt-lmcut")]),
                                 ("best", "seq-opt-lmcut|seq-opt-mas-bop|seq-opt-mas-gop")]), 
                      ("Gamer", [("bd", "bd-nodisj-nom"), ("bddastar", "astar-nodisj-nom")]),
                      ("cGamer", [("bd", "bd-disjdt100k-edel") , ("bddastar", "astar-disjdt100k-m100k")])]


planners_state_art2 = [("astar", [("mas", [("bop", "seq-opt-mas-bop"), ("gop", "seq-opt-mas-gop")]),# ("mas", "seq-opt-mas-bop|seq-opt-mas-gop"), 
                                 ("textnormal{LM-}", [("multicolumn{1}{c}{cut}", "seq-opt-lmcut")]),
                                  ("best", "seq-opt-lmcut|seq-opt-mas-bop|seq-opt-mas-gop")]),
                       ("Gamer", [("bd", "bd-nodisj-nom"), ("bddastar", "astar-nodisj-nom")]),
                       ("cGamer", [("bd (*)", "bd-disjdt100k-edel") , ("bddastar", "astar-disjdt100k-m100k")]), 
                       ("bddastar + \\mas", [("symorder", [("bop", "sym-sfw-mas_rev_bop100k")]),
                                      ("fdorder", [("bop", "fd_rev-sfw-mas_rev_bop200k"), ("gop", "fd_rev-sfw-mas_rev_gop")])]), 
                       ("astar + \\spmas", [("sper", "sym-astar-sp10M"), ("sppdb", "sym-astar-sp10M_pdbs1_goalcgran"), 
                                        ("smas", [("bop", "sym-astar-sp10M_smas_bop100k")]), 
                                        ("spmasmulti", 
                                         [("bop + \\pdb (*)", "sym-astar-spmas10M_smasbop10k_pdbs1cggoalran_pdbs1cggoallev_pdbs1rev_pdbs1cggoalran"), 
                                          ("pdb", "sym-astar-spmas10M_pdbs1cggoalran_pdbs1rev_pdbs1lev_pdbs1cggoallev")])
                                    ]), 
                       ("symba", [("bd", "sym-sbd") , ("sppdb (*)", "sym-sbd10M-phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M"), 
                                  ("smas (*)", "sym-sbd10M-phsmas_bop10k_nms10M_phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M")])]



predefined_planners = {"none" : ['none'], "img_ct" : planners_img_ct, "img_bd" : planners_img_bd, "img_bd_disj" : planners_img_bd_disj,
                       "img_ud" : planners_img_unidir, "img_bd_disjsize" : planners_img_bd_disj_size, 
                       'inv_bd' : planners_inv_bd, 'inv_ud' : planners_inv_unidir, 'inv_min' : planners_inv_minimization, 
                       "img_astar" : planners_img_astar, "inv_astar" : planners_inv_astar,
                       "sfw_mas" : planners_sfw_mas, 
                       "spmas" : planners_spmas, "spmas_nm" : planners_spmas_nm, "symba" : planners_symba, 
                       "blind" : planners_blind, "sart" : planners_state_art, "sart2" : planners_state_art2} 


planners_ct_params = [("ctnormal{}", "multicolumn{1}{c|}{1}", "bd-ct_normal-nom"), ("ctnormal{}", "multicolumn{1}{c|}{5}", "bd-ct_normal_5-nom"), ("ctnormal{}", "multicolumn{1}{c|}{10}", "bd-ct_normal_10-nom"), ("ctnormal{}", "multicolumn{1}{c|}{20}", "bd-ct_normal_20-nom"), ("ctnormal{}", "multicolumn{1}{c|}{$\\infty$}", "bd-nobdd-nom"), ("ctdynamic{}", "multicolumn{1}{c|}{1}", "bd-ct_dynamic-nom"), ("ctdynamic{}", "multicolumn{1}{c|}{5}", "bd-ct_dynamic_5-nom"), ("ctdynamic{}", "multicolumn{1}{c|}{10}", "bd-ct_dynamic_10-nom"), ("ctdynamic{}", "multicolumn{1}{c|}{20}", "bd-ct_dynamic_20-nom"), ("ctdynamic{}", "multicolumn{1}{c|}{$\\infty$}", "bd-nobdd-nom"), ("ctinverse{}", "multicolumn{1}{c|}{1}", "bd-ct_inverse-nom"), ("ctinverse{}", "multicolumn{1}{c|}{5}", "bd-ct_inverse_5-nom"), ("ctinverse{}", "multicolumn{1}{c|}{10}", "bd-ct_inverse_10-nom"), ("ctinverse{}", "multicolumn{1}{c|}{20}", "bd-ct_inverse_20-nom"), ("ctinverse{}", "multicolumn{1}{c|}{$\\infty$}", "bd-nobdd-nom"), ("ctrandom{}", "multicolumn{1}{c|}{1}", "bd-ct_random-nom"), ("ctrandom{}", "multicolumn{1}{c|}{5}", "bd-ct_random_5-nom"), ("ctrandom{}", "multicolumn{1}{c|}{10}", "bd-ct_random_10-nom"), ("ctrandom{}", "multicolumn{1}{c|}{20}", "bd-ct_random_20-nom"), ("ctrandom{}", "multicolumn{1}{c|}{$\\infty$}", "bd-nobdd-nom")] 

planners_disj_params= [
    ("*", "multicolumn{1}{c|}{1}", "bd-nodisj-nom"),
    ("disjdt{}", "multicolumn{1}{c|}{1k}", "bd-disjdt1k-nom"),
    ("disjdt{}", "multicolumn{1}{c|}{10k}", "bd-disjdt10k-nom"),
    ("disjdt{}", "multicolumn{1}{c|}{100k}", "bd-disjdt100k-nom"),
    ("disjdt{}", "multicolumn{1}{c|}{1M}", "bd-disjdt1000k-nom"),
    ("disjdt{}", "multicolumn{1}{c|}{$\\infty$}", "bd-disjdtinf-nom"),
    ("disjsm{}", "multicolumn{1}{c|}{1k}", "bd-disjsm1k-nom"), 
    ("disjsm{}", "multicolumn{1}{c|}{10k}", "bd-disjsm10k-nom"),
    ("disjsm{}", "multicolumn{1}{c|}{100k}", "bd-disjsm100k-nom"),
    ("disjsm{}", "multicolumn{1}{c|}{1M}", "bd-disjsm1000k-nom"),
    ("disjsm{}", "multicolumn{1}{c|}{$\\infty$}", "bd-disjsminf-nom"),
    ("disjctnormal{}{}", "multicolumn{1}{c|}{1k}", "bd-disjct1k_normal_0-nom"),
    ("disjctnormal{}{}", "multicolumn{1}{c|}{10k}", "bd-disjct10k_normal_0-nom"),
    ("disjctnormal{}{}", "multicolumn{1}{c|}{100k}", "bd-disjct100k_normal_0-nom"),
    ("disjctnormal{}{}", "multicolumn{1}{c|}{1M}", "bd-disjct1000k_normal_0-nom"),
    ("disjctnormal{}{}", "multicolumn{1}{c|}{$\\infty$}", "bd-disjctinf_normal_0-nom"),
    ("disjctnormal{20}{}", "multicolumn{1}{c|}{1k}", "bd-disjct1k_normal_20-nom"),
    ("disjctnormal{20}{}", "multicolumn{1}{c|}{10k}", "bd-disjct10k_normal_20-nom"),
    ("disjctnormal{20}{}", "multicolumn{1}{c|}{100k}", "bd-disjct100k_normal_20-nom"),
    ("disjctnormal{20}{}", "multicolumn{1}{c|}{1M}", "bd-disjct1000k_normal_20-nom"),
    ("disjctnormal{20}{}", "multicolumn{1}{c|}{$\\infty$}", "bd-disjctinf_normal_20-nom"),
    ("disjctdynamic{}{}", "multicolumn{1}{c|}{1k}", "bd-disjct1k_dynamic_0-nom"),
    ("disjctdynamic{}{}", "multicolumn{1}{c|}{10k}", "bd-disjct10k_dynamic_0-nom"),
    ("disjctdynamic{}{}", "multicolumn{1}{c|}{100k}", "bd-disjct100k_dynamic_0-nom"),
    ("disjctdynamic{}{}", "multicolumn{1}{c|}{1M}", "bd-disjct1000k_dynamic_0-nom"),
    ("disjctdynamic{}{}", "multicolumn{1}{c|}{$\\infty$}", "bd-disjctinf_dynamic_0-nom"),
    ("disjctdynamic{20}{}", "multicolumn{1}{c|}{1k}", "bd-disjct1k_dynamic_20-nom"),
    ("disjctdynamic{20}{}", "multicolumn{1}{c|}{10k}", "bd-disjct10k_dynamic_20-nom"),
    ("disjctdynamic{20}{}", "multicolumn{1}{c|}{100k}", "bd-disjct100k_dynamic_20-nom"),
    ("disjctdynamic{20}{}", "multicolumn{1}{c|}{1M}", "bd-disjct1000k_dynamic_20-nom"),
    ("disjctdynamic{20}{}", "multicolumn{1}{c|}{$\\infty$}", "bd-disjctinf_dynamic_20-nom")
    # ("disjetnormal{20}{}", "multicolumn{1}{c|}{1k}", "bd-disjet1k_normal_20-nom"),
    # ("disjetnormal{20}{}", "multicolumn{1}{c|}{10k}", "bd-disjet10k_normal_20-nom"),
    # ("disjetnormal{20}{}", "multicolumn{1}{c|}{100k}", "bd-disjet100k_normal_20-nom"),
    # ("disjetnormal{20}{}", "multicolumn{1}{c|}{1M}", "bd-disjet1000k_normal_20-nom")
]



planners_perfecth =  [
                      # ("fdran{}", "multicolumn{1}{c|}{\\masbop}", "fd-astar-mas_add_ran_bop"), 
                      # ("fdran{}", "multicolumn{1}{c|}{\\sbwnonstop}", "fd_ran-sbw_nms_nonstop"),
                      # ("fdran{}", "multicolumn{1}{c|}{\\sbw}", "fd_ran-sbw"), 
                      ("Randomorder{}", "multicolumn{1}{c|}{\\sbwnonstop}", "sym_ran-sbw_nms_nonstop"), 
                      ("Randomorder{}", "multicolumn{1}{c|}{\\masbop}", "sym-astar-mas_add_ran_bop"), 
                      ("Randomorder{}", "multicolumn{1}{c|}{\\sbw}", "sym_ran-sbw"), 
                      ("fdcggoallev{}", "multicolumn{1}{c|}{\\masbop}", "fd-astar-mas_add_cggoallev_bop"), 
                      ("fdcggoalran{}", "multicolumn{1}{c|}{\\masbop}", "fd-astar-mas_add_cggoalran_bop"), 
                      ("fdgoalcglev{}", "multicolumn{1}{c|}{\\masbop}", "fd-astar-mas_add_goalcgran_bop"), 
                      ("fdlev{}", "multicolumn{1}{c|}{\\masbop}", "fd-astar-mas_add_lev_bop"), 
                      ("fdrev{}", "multicolumn{1}{c|}{\\masbop}", "fd-astar-mas_add_rev_bop"),
                      ("symcggoallev{}", "multicolumn{1}{c|}{\\masbop}", "sym-astar-mas_add_cggoallev_bop"), 
                      ("symcggoalran{}", "multicolumn{1}{c|}{\\masbop}", "sym-astar-mas_add_cggoalran_bop"), 
                      ("symgoalcglev{}", "multicolumn{1}{c|}{\\masbop}", "sym-astar-mas_add_goalcgran_bop"), 
                      ("symlev{}", "multicolumn{1}{c|}{\\masbop}", "sym-astar-mas_add_lev_bop"), 
                      ("symrev{}", "multicolumn{1}{c|}{\\masbop}", "sym-astar-mas_add_rev_bop"),                      
                      ("fdcggoallev{}", "multicolumn{1}{c|}{\\sbwnonstop}", "fd_cggoallev-sbw_nms_nonstop"), 
                      ("fdcggoalran{}", "multicolumn{1}{c|}{\\sbwnonstop}", "fd_cggoalran-sbw_nms_nonstop"), 
                      ("fdgoalcglev{}", "multicolumn{1}{c|}{\\sbwnonstop}", "fd_goalcgran-sbw_nms_nonstop"), 
                      ("fdlev{}", "multicolumn{1}{c|}{\\sbwnonstop}", "fd_lev-sbw_nms_nonstop"), 
                      ("fdrev{}", "multicolumn{1}{c|}{\\sbwnonstop}", "fd_rev-sbw_nms_nonstop"),
                      ("symcggoallev{}", "multicolumn{1}{c|}{\\sbwnonstop}", "sym_cggoallev-sbw_nms_nonstop"), 
                      ("symcggoalran{}", "multicolumn{1}{c|}{\\sbwnonstop}", "sym_cggoalran-sbw_nms_nonstop"), 
                      ("symgoalcglev{}", "multicolumn{1}{c|}{\\sbwnonstop}", "sym_goalcgran-sbw_nms_nonstop"), 
                      ("symlev{}", "multicolumn{1}{c|}{\\sbwnonstop}", "sym_lev-sbw_nms_nonstop"), 
                      ("symrev{}", "multicolumn{1}{c|}{\\sbwnonstop}", "sym_rev-sbw_nms_nonstop"),
                      ("fdcggoallev{}", "multicolumn{1}{c|}{\\sbw}", "fd_cggoallev-sbw"), 
                      ("fdcggoalran{}", "multicolumn{1}{c|}{\\sbw}", "fd_cggoalran-sbw"), 
                      ("fdgoalcglev{}", "multicolumn{1}{c|}{\\sbw}", "fd_goalcgran-sbw"), 
                      ("fdlev{}", "multicolumn{1}{c|}{\\sbw}", "fd_lev-sbw"), 
                      ("fdrev{}", "multicolumn{1}{c|}{\\sbw}", "fd_rev-sbw"),
                      ("symcggoallev{}", "multicolumn{1}{c|}{\\sbw}", "sym_cggoallev-sbw"), 
                      ("symcggoalran{}", "multicolumn{1}{c|}{\\sbw}", "sym_cggoalran-sbw"), 
                      ("symgoalcglev{}", "multicolumn{1}{c|}{\\sbw}", "sym_goalcgran-sbw"), 
                      ("symlev{}", "multicolumn{1}{c|}{\\sbw}", "sym_lev-sbw"), 
                      ("symrev{}", "multicolumn{1}{c|}{\\sbw}", "sym_rev-sbw")]




planners_spmas_abs = [
    ("multicolumn{1}{|c|}{\\sper}", ("symorder", "per"), "sym-astar-sp10M"), 
#    ("multicolumn{1}{|c|}{pdbsrev}", ("symorder", "per"), "sym-astar-sp10M_pdbs_rev"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("symorder", "per"), "sym-astar-sp10M_pdbs1_rev"), 
#    ("multicolumn{1}{|c|}{pdbslev}", ("symorder", "per"), "sym-astar-sp10M_pdbs_lev"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("symorder", "per"), "sym-astar-sp10M_pdbs1_lev"), 
#    ("multicolumn{1}{|c|}{pdbscggoallev}", ("symorder", "per"), "sym-astar-sp10M_pdbs_cggoallev"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("symorder", "per"), "sym-astar-sp10M_pdbs1_cggoallev"), 
#    ("multicolumn{1}{|c|}{pdbscggoalran}", ("symorder", "per"), "sym-astar-sp10M_pdbs_cggoalran"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("symorder", "per"), "sym-astar-sp10M_pdbs1_cggoalran"), 
#    ("multicolumn{1}{|c|}{pdbsgoalcglev}", ("symorder", "per"), "sym-astar-sp10M_pdbs_goalcgran"), 
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("symorder", "per"), "sym-astar-sp10M_pdbs1_goalcgran"), 
 #   ("multicolumn{1}{|c|}{pdbsran}", ("symorder", "per"), "sym-astar-sp10M_pdbs_ran"), 
    ("multicolumn{1}{|c|}{\\pdbsran}", ("symorder", "per"), "sym-astar-sp10M_pdbs1_ran"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("symorder", "per"), "sym-astar-sp10M_smas_bop10k"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("symorder", "per"), "sym-astar-sp10M_smas_gop10k"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("symorder", "per"), "sym-astar-sp10M_smas_sfh10k"), 
    ("multicolumn{1}{|c|}{\\sper}", ("symorder", "noper"), "sym-astar-sp10M_nop"), 
#    ("multicolumn{1}{|c|}{pdbsrev}", ("symorder", "noper"), "sym-astar-sp10M_pdbs_rev_nop"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("symorder", "noper"), "sym-astar-sp10M_pdbs1_rev_nop"), 
#    ("multicolumn{1}{|c|}{pdbslev}", ("symorder", "noper"), "sym-astar-sp10M_pdbs_lev_nop"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("symorder", "noper"), "sym-astar-sp10M_pdbs1_lev_nop"), 
#    ("multicolumn{1}{|c|}{pdbscggoallev}", ("symorder", "noper"), "sym-astar-sp10M_pdbs_cggoallev_nop"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("symorder", "noper"), "sym-astar-sp10M_pdbs1_cggoallev_nop"), 
#    ("multicolumn{1}{|c|}{pdbscggoalran}", ("symorder", "noper"), "sym-astar-sp10M_pdbs_cggoalran_nop"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("symorder", "noper"), "sym-astar-sp10M_pdbs1_cggoalran_nop"), 
#    ("multicolumn{1}{|c|}{pdbsgoalcglev}", ("symorder", "noper"), "sym-astar-sp10M_pdbs_goalcgran_nop"), 
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("symorder", "noper"), "sym-astar-sp10M_pdbs1_goalcgran_nop"), 
#    ("multicolumn{1}{|c|}{pdbsran}", ("symorder", "noper"), "sym-astar-sp10M_pdbs_ran_nop"), 
    ("multicolumn{1}{|c|}{\\pdbsran}", ("symorder", "noper"), "sym-astar-sp10M_pdbs1_ran_nop"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("symorder", "noper"), "sym-astar-sp10M_smas_bop10k_nop"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("symorder", "noper"), "sym-astar-sp10M_smas_gop10k_nop"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("symorder", "noper"), "sym-astar-sp10M_smas_sfh10k_nop"), 
    ("multicolumn{1}{|c|}{\\sper}", ("fdorder", "per"), "fd-astar-sp10M"), 
#    ("multicolumn{1}{|c|}{pdbsrev}", ("fdorder", "per"), "fd-astar-sp10M_pdbs_rev"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("fdorder", "per"), "fd-astar-sp10M_pdbs1_rev"), 
#    ("multicolumn{1}{|c|}{pdbslev}", ("fdorder", "per"), "fd-astar-sp10M_pdbs_lev"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("fdorder", "per"), "fd-astar-sp10M_pdbs1_lev"), 
#    ("multicolumn{1}{|c|}{pdbscggoallev}", ("fdorder", "per"), "fd-astar-sp10M_pdbs_cggoallev"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("fdorder", "per"), "fd-astar-sp10M_pdbs1_cggoallev"), 
#    ("multicolumn{1}{|c|}{pdbscggoalran}", ("fdorder", "per"), "fd-astar-sp10M_pdbs_cggoalran"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("fdorder", "per"), "fd-astar-sp10M_pdbs1_cggoalran"), 
#    ("multicolumn{1}{|c|}{pdbsgoalcglev}", ("fdorder", "per"), "fd-astar-sp10M_pdbs_goalcgran"), 
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("fdorder", "per"), "fd-astar-sp10M_pdbs1_goalcgran"), 
#    ("multicolumn{1}{|c|}{pdbsran}", ("fdorder", "per"), "fd-astar-sp10M_pdbs_ran"), 
    ("multicolumn{1}{|c|}{\\pdbsran}", ("fdorder", "per"), "fd-astar-sp10M_pdbs1_ran"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("fdorder", "per"), "fd-astar-sp10M_smas_bop10k"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("fdorder", "per"), "fd-astar-sp10M_smas_gop10k"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("fdorder", "per"), "fd-astar-sp10M_smas_sfh10k"),
    ("multicolumn{1}{|c|}{\\sper}", ("fdorder", "noper"), "fd-astar-sp10M_nop"), 
#    ("multicolumn{1}{|c|}{pdbsrev}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs_rev_nop"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs1_rev_nop"), 
#    ("multicolumn{1}{|c|}{pdbslev}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs_lev_nop"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs1_lev_nop"), 
#    ("multicolumn{1}{|c|}{pdbscggoallev}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs_cggoallev_nop"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs1_cggoallev_nop"), 
#    ("multicolumn{1}{|c|}{pdbscggoalran}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs_cggoalran_nop"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs1_cggoalran_nop"), 
#    ("multicolumn{1}{|c|}{pdbsgoalcglev}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs_goalcgran_nop"), 
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs1_goalcgran_nop"), 
#    ("multicolumn{1}{|c|}{pdbsran}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs_ran_nop"), 
    ("multicolumn{1}{|c|}{\\pdbsran}", ("fdorder", "noper"), "fd-astar-sp10M_pdbs1_ran_nop"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("fdorder", "noper"), "fd-astar-sp10M_smas_bop10k_nop"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("fdorder", "noper"), "fd-astar-sp10M_smas_gop10k_nop"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("fdorder", "noper"), "fd-astar-sp10M_smas_sfh10k_nop"), 
#    ("multicolumn{1}{|c|}{\\smasgopnma{10k}}", ("symorder", "per"), "sym-astar-sp10M_smas_gop10k_nma"), 
#    ("multicolumn{1}{|c|}{\\smasbopnma{10k}}", ("symorder", "per"), "sym-astar-sp10M_smas_bop10k_nma"), 
#   ("multicolumn{1}{|c|}{\\smasbopnma{10k}}", "fdorder", "fd-astar-sp10M_smas_bop10k_nma"), 
#    ("multicolumn{1}{|c|}{\\smasgopnma{10k}}", "fdorder", "fd-astar-sp10M_smas_gop10k_nma"), 
] 


planners_spmas_nm_summary = [
    ("multicolumn{1}{|c|}{\\sper}", ("symorder", "nomut{}"), "sym-astar-sp10M_nom_nma"), 
    ("multicolumn{1}{|c|}{pdbs}", ("symorder", "nomut{}"), "sym-astar-sp10M_pdbs_rev_nom_nma"), 
    ("multicolumn{1}{|c|}{\\shbop{10k}}", ("symorder", "nomut{}"), "sym-astar-sp10M_smas_bop10k_nom_nma"), 
    ("multicolumn{1}{|c|}{\\shgop{10k}}", ("symorder", "nomut{}"), "sym-astar-sp10M_smas_gop10k_nom_nma"), 
    ("multicolumn{1}{|c|}{\\sper}", ("symorder", "mut{}"), "sym-astar-sp10M"), 
    ("multicolumn{1}{|c|}{pdbs}", ("symorder", "mut{}"), "sym-astar-sp10M_pdbs_rev"), 
    ("multicolumn{1}{|c|}{\\shbop{10k}}", ("symorder", "mut{}"), "sym-astar-sp10M_smas_bop10k"), 
    ("multicolumn{1}{|c|}{\\shgop{10k}}", ("symorder", "mut{}"), "sym-astar-sp10M_smas_gop10k"), 
    ("multicolumn{1}{|c|}{\\sper}", ("fdorder", "nomut{}"), "fd-astar-sp10M_nom_nma"), 
    ("multicolumn{1}{|c|}{pdbs}", ("fdorder", "nomut{}"), "fd-astar-sp10M_pdbs_rev_nom_nma"), 
    ("multicolumn{1}{|c|}{\\shbop{10k}}", ("fdorder", "nomut{}"), "fd-astar-sp10M_smas_bop10k_nom_nma"), 
    ("multicolumn{1}{|c|}{\\shgop{10k}}", ("fdorder", "nomut{}"), "fd-astar-sp10M_smas_gop10k_nom_nma"), 
    ("multicolumn{1}{|c|}{\\sper}", ("fdorder", "mut{}"), "fd-astar-sp10M"), 
    ("multicolumn{1}{|c|}{pdbs}", ("fdorder", "mut{}"), "fd-astar-sp10M_pdbs_rev"), 
    ("multicolumn{1}{|c|}{\\shbop{10k}}", ("fdorder", "mut{}"), "fd-astar-sp10M_smas_bop10k"), 
    ("multicolumn{1}{|c|}{\\shgop{10k}}", ("fdorder", "mut{}"), "fd-astar-sp10M_smas_gop10k"), 
] 

planners_symba_nrb_summary = [
    ("multicolumn{1}{|c|}{\\sper}", ("symorder", "fullconf"), "sym-sbd"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("symorder", "fullconf"), "sym-sbd10M-phpdb1_rev_nms10M"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("symorder", "fullconf"), "sym-sbd10M-phpdb1_lev_nms10M"),
    ("multicolumn{1}{|c|}{\\pdbsran}", ("symorder", "fullconf"), "sym-sbd10M-phpdb1_ran_nms10M"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("symorder", "fullconf"), "sym-sbd10M-phpdb1_cggoallev_nms10M"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("symorder", "fullconf"), "sym-sbd10M-phpdb1_cggoalran_nms10M"),
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("symorder", "fullconf"), "sym-sbd10M-phpdb1_goalcgran_nms10M"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("symorder", "fullconf"), "sym-sbd10M-phsmas_bop10k_nms10M"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("symorder", "fullconf"), "sym-sbd10M-phsmas_gop10k_nms10M"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("symorder", "fullconf"), "sym-sbd10M-phsmas_sfh10k_nms10M"), 
#    ("multicolumn{1}{|c|}{\\multipdb}", ("symorder", "fullconf"),  "sym-sbd10M-phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M"),
    ("hline\\multicolumn{1}{|c|}{\\multismas}", ("symorder", "fullconf"),  "sym-sbd10M-phsmas_bop10k_nms10M_phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M"),
    ("hline\\multicolumn{1}{|c|}{\\bestpdb}", ("symorder", "fullconf"), "sym-sbd10M-phpdb1_rev_nms10M|sym-sbd10M-phpdb1_lev_nms10M|sym-sbd10M-phpdb1_ran_nms10M|sym-sbd10M-phpdb1_cggoallev_nms10M|sym-sbd10M-phpdb1_cggoalran_nms10M|sym-sbd10M-phpdb1_goalcgran_nms10M"),
    ("multicolumn{1}{|c|}{\\bestabs}", ("symorder", "fullconf"), "sym-sbd10M-phpdb1_rev_nms10M|sym-sbd10M-phpdb1_lev_nms10M|sym-sbd10M-phpdb1_ran_nms10M|sym-sbd10M-phpdb1_cggoallev_nms10M|sym-sbd10M-phpdb1_cggoalran_nms10M|sym-sbd10M-phpdb1_goalcgran_nms10M|sym-sbd10M-phsmas_bop10k_nms10M|sym-sbd10M-phsmas_gop10k_nms10M|sym-sbd10M-phsmas_sfh10k_nms10M"),
    ("multicolumn{1}{|c|}{\\best}", ("symorder", "fullconf"), "sym-sbd|sym-sbd10M-phpdb1_rev_nms10M|sym-sbd10M-phpdb1_lev_nms10M|sym-sbd10M-phpdb1_ran_nms10M|sym-sbd10M-phpdb1_cggoallev_nms10M|sym-sbd10M-phpdb1_cggoalran_nms10M|sym-sbd10M-phpdb1_goalcgran_nms10M|sym-sbd10M-phsmas_bop10k_nms10M|sym-sbd10M-phsmas_gop10k_nms10M|sym-sbd10M-phsmas_sfh10k_nms10M"),   
 ("multicolumn{1}{|c|}{\\sper}", ("symorder", "noper"), "sym-sbd"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("symorder", "noper"), "sym-sbd10M-phpdb1_rev_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("symorder", "noper"), "sym-sbd10M-phpdb1_lev_nms10M_nop"),
    ("multicolumn{1}{|c|}{\\pdbsran}", ("symorder", "noper"), "sym-sbd10M-phpdb1_ran_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("symorder", "noper"), "sym-sbd10M-phpdb1_cggoallev_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("symorder", "noper"), "sym-sbd10M-phpdb1_cggoalran_nms10M_nop"),
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("symorder", "noper"), "sym-sbd10M-phpdb1_goalcgran_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("symorder", "noper"), "sym-sbd10M-phsmas_bop10k_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("symorder", "noper"), "sym-sbd10M-phsmas_gop10k_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("symorder", "noper"), "sym-sbd10M-phsmas_sfh10k_nms10M_nop"),
#    ("multicolumn{1}{|c|}{\\multipdb}", ("symorder", "noper"),  "sym-sbd10M-phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M_nop"),
    ("hline\\multicolumn{1}{|c|}{\\multismas}", ("symorder", "noper"),  "sym-sbd10M-phsmas_bop10k_nms10M_phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M_nop"),
    ("hline\\multicolumn{1}{|c|}{\\bestpdb}", ("symorder", "noper"), "sym-sbd10M-phpdb1_rev_nms10M_nop|sym-sbd10M-phpdb1_lev_nms10M_nop|sym-sbd10M-phpdb1_ran_nms10M_nop|sym-sbd10M-phpdb1_cggoallev_nms10M_nop|sym-sbd10M-phpdb1_cggoalran_nms10M_nop|sym-sbd10M-phpdb1_goalcgran_nms10M_nop"),
    ("multicolumn{1}{|c|}{\\bestabs}", ("symorder", "noper"), "sym-sbd10M-phpdb1_rev_nms10M_nop|sym-sbd10M-phpdb1_lev_nms10M_nop|sym-sbd10M-phpdb1_ran_nms10M_nop|sym-sbd10M-phpdb1_cggoallev_nms10M_nop|sym-sbd10M-phpdb1_cggoalran_nms10M_nop|sym-sbd10M-phpdb1_goalcgran_nms10M_nop|sym-sbd10M-phsmas_bop10k_nms10M_nop|sym-sbd10M-phsmas_gop10k_nms10M_nop|sym-sbd10M-phsmas_sfh10k_nms10M_nop"),  
    ("multicolumn{1}{|c|}{\\best}", ("symorder", "noper"), "sym-sbd|sym-sbd10M-phpdb1_rev_nms10M_nop|sym-sbd10M-phpdb1_lev_nms10M_nop|sym-sbd10M-phpdb1_ran_nms10M_nop|sym-sbd10M-phpdb1_cggoallev_nms10M_nop|sym-sbd10M-phpdb1_cggoalran_nms10M_nop|sym-sbd10M-phpdb1_goalcgran_nms10M_nop|sym-sbd10M-phsmas_bop10k_nms10M_nop|sym-sbd10M-phsmas_gop10k_nms10M_nop|sym-sbd10M-phsmas_sfh10k_nms10M_nop"),  
    ("multicolumn{1}{|c|}{\\sper}", ("symorder", "rbw"), "sym-sbd"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("symorder", "rbw"), "sym-sbd10M-phpdb1_rev_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("symorder", "rbw"), "sym-sbd10M-phpdb1_lev_nms10M_rbw"),
    ("multicolumn{1}{|c|}{\\pdbsran}", ("symorder", "rbw"), "sym-sbd10M-phpdb1_ran_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("symorder", "rbw"), "sym-sbd10M-phpdb1_cggoallev_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("symorder", "rbw"), "sym-sbd10M-phpdb1_cggoalran_nms10M_rbw"),
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("symorder", "rbw"), "sym-sbd10M-phpdb1_goalcgran_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("symorder", "rbw"), "sym-sbd10M-phsmas_bop10k_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("symorder", "rbw"), "sym-sbd10M-phsmas_gop10k_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("symorder", "rbw"), "sym-sbd10M-phsmas_sfh10k_nms10M_rbw"),
#    ("multicolumn{1}{|c|}{\\multipdb}", ("symorder", "rbw"),  "sym-sbd10M-phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M_rbw"),
    ("hline\\multicolumn{1}{|c|}{\\multismas}", ("symorder", "rbw"),  "sym-sbd10M-phsmas_bop10k_nms10M_phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M_rbw"),
    ("hline\\multicolumn{1}{|c|}{\\bestpdb}", ("symorder", "rbw"), "sym-sbd10M-phpdb1_rev_nms10M_rbw|sym-sbd10M-phpdb1_lev_nms10M_rbw|sym-sbd10M-phpdb1_ran_nms10M_rbw|sym-sbd10M-phpdb1_cggoallev_nms10M_rbw|sym-sbd10M-phpdb1_cggoalran_nms10M_rbw|sym-sbd10M-phpdb1_goalcgran_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\bestabs}", ("symorder", "rbw"), "sym-sbd10M-phpdb1_rev_nms10M_rbw|sym-sbd10M-phpdb1_lev_nms10M_rbw|sym-sbd10M-phpdb1_ran_nms10M_rbw|sym-sbd10M-phpdb1_cggoallev_nms10M_rbw|sym-sbd10M-phpdb1_cggoalran_nms10M_rbw|sym-sbd10M-phpdb1_goalcgran_nms10M_rbw|sym-sbd10M-phsmas_bop10k_nms10M_rbw|sym-sbd10M-phsmas_gop10k_nms10M_rbw|sym-sbd10M-phsmas_sfh10k_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\best}", ("symorder", "rbw"), "sym-sbd|sym-sbd10M-phpdb1_rev_nms10M_rbw|sym-sbd10M-phpdb1_lev_nms10M_rbw|sym-sbd10M-phpdb1_ran_nms10M_rbw|sym-sbd10M-phpdb1_cggoallev_nms10M_rbw|sym-sbd10M-phpdb1_cggoalran_nms10M_rbw|sym-sbd10M-phpdb1_goalcgran_nms10M_rbw|sym-sbd10M-phsmas_bop10k_nms10M_rbw|sym-sbd10M-phsmas_gop10k_nms10M_rbw|sym-sbd10M-phsmas_sfh10k_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\sper}", ("fdorder", "fullconf"), "fd-sbd"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("fdorder", "fullconf"), "fd-sbd10M-phpdb1_rev_nms10M"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("fdorder", "fullconf"), "fd-sbd10M-phpdb1_lev_nms10M"),
    ("multicolumn{1}{|c|}{\\pdbsran}", ("fdorder", "fullconf"), "fd-sbd10M-phpdb1_ran_nms10M"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("fdorder", "fullconf"), "fd-sbd10M-phpdb1_cggoallev_nms10M"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("fdorder", "fullconf"), "fd-sbd10M-phpdb1_cggoalran_nms10M"),
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("fdorder", "fullconf"), "fd-sbd10M-phpdb1_goalcgran_nms10M"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("fdorder", "fullconf"), "fd-sbd10M-phsmas_bop10k_nms10M"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("fdorder", "fullconf"), "fd-sbd10M-phsmas_gop10k_nms10M"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("fdorder", "fullconf"), "fd-sbd10M-phsmas_sfh10k_nms10M"), 
#    ("multicolumn{1}{|c|}{\\multipdb}", ("fdorder", "fullconf"),  "fd-sbd10M-phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M"),
    ("hline\\multicolumn{1}{|c|}{\\multismas}", ("fdorder", "fullconf"),  "fd-sbd10M-phsmas_bop10k_nms10M_phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M"),
    ("hline\\multicolumn{1}{|c|}{\\bestpdb}", ("fdorder", "fullconf"), "fd-sbd10M-phpdb1_rev_nms10M|fd-sbd10M-phpdb1_lev_nms10M|fd-sbd10M-phpdb1_ran_nms10M|fd-sbd10M-phpdb1_cggoallev_nms10M|fd-sbd10M-phpdb1_cggoalran_nms10M|fd-sbd10M-phpdb1_goalcgran_nms10M"),
    ("multicolumn{1}{|c|}{\\best}", ("fdorder", "fullconf"), "fd-sbd|fd-sbd10M-phpdb1_rev_nms10M|fd-sbd10M-phpdb1_lev_nms10M|fd-sbd10M-phpdb1_ran_nms10M|fd-sbd10M-phpdb1_cggoallev_nms10M|fd-sbd10M-phpdb1_cggoalran_nms10M|fd-sbd10M-phpdb1_goalcgran_nms10M|fd-sbd10M-phsmas_bop10k_nms10M|fd-sbd10M-phsmas_gop10k_nms10M|fd-sbd10M-phsmas_sfh10k_nms10M"),
    ("multicolumn{1}{|c|}{\\bestabs}", ("fdorder", "fullconf"), "fd-sbd10M-phpdb1_rev_nms10M|fd-sbd10M-phpdb1_lev_nms10M|fd-sbd10M-phpdb1_ran_nms10M|fd-sbd10M-phpdb1_cggoallev_nms10M|fd-sbd10M-phpdb1_cggoalran_nms10M|fd-sbd10M-phpdb1_goalcgran_nms10M|fd-sbd10M-phsmas_bop10k_nms10M|fd-sbd10M-phsmas_gop10k_nms10M|fd-sbd10M-phsmas_sfh10k_nms10M"),
    ("multicolumn{1}{|c|}{\\sper}", ("fdorder", "noper"), "fd-sbd"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("fdorder", "noper"), "fd-sbd10M-phpdb1_rev_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("fdorder", "noper"), "fd-sbd10M-phpdb1_lev_nms10M_nop"),
    ("multicolumn{1}{|c|}{\\pdbsran}", ("fdorder", "noper"), "fd-sbd10M-phpdb1_ran_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("fdorder", "noper"), "fd-sbd10M-phpdb1_cggoallev_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("fdorder", "noper"), "fd-sbd10M-phpdb1_cggoalran_nms10M_nop"),
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("fdorder", "noper"), "fd-sbd10M-phpdb1_goalcgran_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("fdorder", "noper"), "fd-sbd10M-phsmas_bop10k_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("fdorder", "noper"), "fd-sbd10M-phsmas_gop10k_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("fdorder", "noper"), "fd-sbd10M-phsmas_sfh10k_nms10M_nop"),
#    ("multicolumn{1}{|c|}{\\multipdb}", ("fdorder", "noper"),  "fd-sbd10M-phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M_nop"),
    ("hline\\multicolumn{1}{|c|}{\\multismas}", ("fdorder", "noper"),  "fd-sbd10M-phsmas_bop10k_nms10M_phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M_nop"),
    ("hline\\multicolumn{1}{|c|}{\\bestpdb}", ("fdorder", "noper"), "fd-sbd10M-phpdb1_rev_nms10M_nop|fd-sbd10M-phpdb1_lev_nms10M_nop|fd-sbd10M-phpdb1_ran_nms10M_nop|fd-sbd10M-phpdb1_cggoallev_nms10M_nop|fd-sbd10M-phpdb1_cggoalran_nms10M_nop|fd-sbd10M-phpdb1_goalcgran_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\best}", ("fdorder", "noper"), "fd-sbd|fd-sbd10M-phpdb1_rev_nms10M_nop|fd-sbd10M-phpdb1_lev_nms10M_nop|fd-sbd10M-phpdb1_ran_nms10M_nop|fd-sbd10M-phpdb1_cggoallev_nms10M_nop|fd-sbd10M-phpdb1_cggoalran_nms10M_nop|fd-sbd10M-phpdb1_goalcgran_nms10M_nop|fd-sbd10M-phsmas_bop10k_nms10M_nop|fd-sbd10M-phsmas_gop10k_nms10M_nop|fd-sbd10M-phsmas_sfh10k_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\bestabs}", ("fdorder", "noper"), "fd-sbd10M-phpdb1_rev_nms10M_nop|fd-sbd10M-phpdb1_lev_nms10M_nop|fd-sbd10M-phpdb1_ran_nms10M_nop|fd-sbd10M-phpdb1_cggoallev_nms10M_nop|fd-sbd10M-phpdb1_cggoalran_nms10M_nop|fd-sbd10M-phpdb1_goalcgran_nms10M_nop|fd-sbd10M-phsmas_bop10k_nms10M_nop|fd-sbd10M-phsmas_gop10k_nms10M_nop|fd-sbd10M-phsmas_sfh10k_nms10M_nop"), 
    ("multicolumn{1}{|c|}{\\sper}", ("fdorder", "rbw"), "fd-sbd"), 
    ("multicolumn{1}{|c|}{\\pdbsrev}", ("fdorder", "rbw"), "fd-sbd10M-phpdb1_rev_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\pdbslev}", ("fdorder", "rbw"), "fd-sbd10M-phpdb1_lev_nms10M_rbw"),
    ("multicolumn{1}{|c|}{\\pdbsran}", ("fdorder", "rbw"), "fd-sbd10M-phpdb1_ran_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\pdbscggoallev}", ("fdorder", "rbw"), "fd-sbd10M-phpdb1_cggoallev_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\pdbscggoalran}", ("fdorder", "rbw"), "fd-sbd10M-phpdb1_cggoalran_nms10M_rbw"),
    ("multicolumn{1}{|c|}{\\pdbsgoalcglev}", ("fdorder", "rbw"), "fd-sbd10M-phpdb1_goalcgran_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\smasbop{10k}}", ("fdorder", "rbw"), "fd-sbd10M-phsmas_bop10k_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\smasgop{10k}}", ("fdorder", "rbw"), "fd-sbd10M-phsmas_gop10k_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\smassfh{10k}}", ("fdorder", "rbw"), "fd-sbd10M-phsmas_sfh10k_nms10M_rbw"), 
#    ("multicolumn{1}{|c|}{\\multipdb}", ("fdorder", "rbw"),  "fd-sbd10M-phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M_rbw"),
    ("hline\\multicolumn{1}{|c|}{\\multismas}", ("fdorder", "rbw"),  "fd-sbd10M-phsmas_bop10k_nms10M_phpdb1_cggoalran_nms10M_phpdb1_goalcglev_nms10M_phpdb1_rev_nms10M_rbw"),
    ("hline\\multicolumn{1}{|c|}{\\bestpdb}", ("fdorder", "rbw"), "fd-sbd10M-phpdb1_rev_nms10M_rbw|fd-sbd10M-phpdb1_lev_nms10M_rbw|fd-sbd10M-phpdb1_ran_nms10M_rbw|fd-sbd10M-phpdb1_cggoallev_nms10M_rbw|fd-sbd10M-phpdb1_cggoalran_nms10M_rbw|fd-sbd10M-phpdb1_goalcgran_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\bestabs}", ("fdorder", "rbw"), "fd-sbd10M-phpdb1_rev_nms10M_rbw|fd-sbd10M-phpdb1_lev_nms10M_rbw|fd-sbd10M-phpdb1_ran_nms10M_rbw|fd-sbd10M-phpdb1_cggoallev_nms10M_rbw|fd-sbd10M-phpdb1_cggoalran_nms10M_rbw|fd-sbd10M-phpdb1_goalcgran_nms10M_rbw|fd-sbd10M-phsmas_bop10k_nms10M_rbw|fd-sbd10M-phsmas_gop10k_nms10M_rbw|fd-sbd10M-phsmas_sfh10k_nms10M_rbw"), 
    ("multicolumn{1}{|c|}{\\best}", ("fdorder", "rbw"), "fd-sbd|fd-sbd10M-phpdb1_rev_nms10M_rbw|fd-sbd10M-phpdb1_lev_nms10M_rbw|fd-sbd10M-phpdb1_ran_nms10M_rbw|fd-sbd10M-phpdb1_cggoallev_nms10M_rbw|fd-sbd10M-phpdb1_cggoalran_nms10M_rbw|fd-sbd10M-phpdb1_goalcgran_nms10M_rbw|fd-sbd10M-phsmas_bop10k_nms10M_rbw|fd-sbd10M-phsmas_gop10k_nms10M_rbw|fd-sbd10M-phsmas_sfh10k_nms10M_rbw"), 
    # ("multicolumn{1}{|c|}{\\best2}", ("symorder", "fullconf"), "sym-sbd|sym-sbd10M-phpdb1_rev_nms10M|sym-sbd10M-phpdb1_lev_nms10M|sym-sbd10M-phpdb1_ran_nms10M|sym-sbd10M-phpdb1_cggoallev_nms10M|sym-sbd10M-phpdb1_cggoalran_nms10M|sym-sbd10M-phpdb1_goalcgran_nms10M|sym-sbd10M-phsmas_bop10k_nms10M|sym-sbd10M-phsmas_gop10k_nms10M|sym-sbd10M-phsmas_sfh10k_nms10M|sym-sbd|sym-sbd10M-phpdb1_rev_nms10M_nop|sym-sbd10M-phpdb1_lev_nms10M_nop|sym-sbd10M-phpdb1_ran_nms10M_nop|sym-sbd10M-phpdb1_cggoallev_nms10M_nop|sym-sbd10M-phpdb1_cggoalran_nms10M_nop|sym-sbd10M-phpdb1_goalcgran_nms10M_nop|sym-sbd10M-phsmas_bop10k_nms10M_nop|sym-sbd10M-phsmas_gop10k_nms10M_nop|sym-sbd10M-phsmas_sfh10k_nms10M_nop|sym-sbd|sym-sbd10M-phpdb1_rev_nms10M_rbw|sym-sbd10M-phpdb1_lev_nms10M_rbw|sym-sbd10M-phpdb1_ran_nms10M_rbw|sym-sbd10M-phpdb1_cggoallev_nms10M_rbw|sym-sbd10M-phpdb1_cggoalran_nms10M_rbw|sym-sbd10M-phpdb1_goalcgran_nms10M_rbw|sym-sbd10M-phsmas_bop10k_nms10M_rbw|sym-sbd10M-phsmas_gop10k_nms10M_rbw|sym-sbd10M-phsmas_sfh10k_nms10M_rbw"),   
    # ("multicolumn{1}{|c|}{\\best2}", ("symorder", "rbw"), "sym-sbd|sym-sbd10M-phpdb1_rev_nms10M_rbw|sym-sbd10M-phpdb1_lev_nms10M_rbw|sym-sbd10M-phpdb1_ran_nms10M_rbw|sym-sbd10M-phpdb1_cggoallev_nms10M_rbw|sym-sbd10M-phpdb1_cggoalran_nms10M_rbw|sym-sbd10M-phpdb1_goalcgran_nms10M_rbw|sym-sbd10M-phsmas_bop10k_nms10M_rbw|sym-sbd10M-phsmas_gop10k_nms10M_rbw|sym-sbd10M-phsmas_sfh10k_nms10M_rbw"), 
    # ("multicolumn{1}{|c|}{\\best2}", ("symorder", "noper"), "sym-sbd|sym-sbd10M-phpdb1_rev_nms10M_nop|sym-sbd10M-phpdb1_lev_nms10M_nop|sym-sbd10M-phpdb1_ran_nms10M_nop|sym-sbd10M-phpdb1_cggoallev_nms10M_nop|sym-sbd10M-phpdb1_cggoalran_nms10M_nop|sym-sbd10M-phpdb1_goalcgran_nms10M_nop|sym-sbd10M-phsmas_bop10k_nms10M_nop|sym-sbd10M-phsmas_gop10k_nms10M_nop|sym-sbd10M-phsmas_sfh10k_nms10M_nop"),  
    # ("multicolumn{1}{|c|}{\\best2}", ("fdorder", "fullconf"), "fd-sbd|fd-sbd10M-phpdb1_rev_nms10M|fd-sbd10M-phpdb1_lev_nms10M|fd-sbd10M-phpdb1_ran_nms10M|fd-sbd10M-phpdb1_cggoallev_nms10M|fd-sbd10M-phpdb1_cggoalran_nms10M|fd-sbd10M-phpdb1_goalcgran_nms10M|fd-sbd10M-phsmas_bop10k_nms10M|fd-sbd10M-phsmas_gop10k_nms10M|fd-sbd10M-phsmas_sfh10k_nms10M|fd-sbd|fd-sbd10M-phpdb1_rev_nms10M_rbw|fd-sbd10M-phpdb1_lev_nms10M_rbw|fd-sbd10M-phpdb1_ran_nms10M_rbw|fd-sbd10M-phpdb1_cggoallev_nms10M_rbw|fd-sbd10M-phpdb1_cggoalran_nms10M_rbw|fd-sbd10M-phpdb1_goalcgran_nms10M_rbw|fd-sbd10M-phsmas_bop10k_nms10M_rbw|fd-sbd10M-phsmas_gop10k_nms10M_rbw|fd-sbd10M-phsmas_sfh10k_nms10M_rbw|fd-sbd|fd-sbd10M-phpdb1_rev_nms10M_nop|fd-sbd10M-phpdb1_lev_nms10M_nop|fd-sbd10M-phpdb1_ran_nms10M_nop|fd-sbd10M-phpdb1_cggoallev_nms10M_nop|fd-sbd10M-phpdb1_cggoalran_nms10M_nop|fd-sbd10M-phpdb1_goalcgran_nms10M_nop|fd-sbd10M-phsmas_bop10k_nms10M_nop|fd-sbd10M-phsmas_gop10k_nms10M_nop|fd-sbd10M-phsmas_sfh10k_nms10M_nop"),
    # ("multicolumn{1}{|c|}{\\best2}", ("fdorder", "noper"), "fd-sbd|fd-sbd10M-phpdb1_rev_nms10M_nop|fd-sbd10M-phpdb1_lev_nms10M_nop|fd-sbd10M-phpdb1_ran_nms10M_nop|fd-sbd10M-phpdb1_cggoallev_nms10M_nop|fd-sbd10M-phpdb1_cggoalran_nms10M_nop|fd-sbd10M-phpdb1_goalcgran_nms10M_nop|fd-sbd10M-phsmas_bop10k_nms10M_nop|fd-sbd10M-phsmas_gop10k_nms10M_nop|fd-sbd10M-phsmas_sfh10k_nms10M_nop"), 
    # ("multicolumn{1}{|c|}{\\best2}", ("fdorder", "rbw"), "fd-sbd|fd-sbd10M-phpdb1_rev_nms10M_rbw|fd-sbd10M-phpdb1_lev_nms10M_rbw|fd-sbd10M-phpdb1_ran_nms10M_rbw|fd-sbd10M-phpdb1_cggoallev_nms10M_rbw|fd-sbd10M-phpdb1_cggoalran_nms10M_rbw|fd-sbd10M-phpdb1_goalcgran_nms10M_rbw|fd-sbd10M-phsmas_bop10k_nms10M_rbw|fd-sbd10M-phsmas_gop10k_nms10M_rbw|fd-sbd10M-phsmas_sfh10k_nms10M_rbw"), 
]


predefined_planners_summary = {"ct_params" : planners_ct_params, "disj_params" : planners_disj_params, "perfecth" : planners_perfecth, "spmas_abs" : planners_spmas_abs, "spmas_nm" : planners_spmas_nm_summary, "symba_rbw" : planners_symba_nrb_summary} 


def to_list(planners): 
    #For each element in the list that is a list it removes the list and insers elements [a, [b, c], [d]] => [a, b, c, d]
    def unlist(lists): 
        res = []
        for l in lists: 
            elem = l[1] if isinstance(l, tuple) else l
            if isinstance (elem, list):
                res += unlist(elem)
            else:
                res.append(elem)
        return res

    return unlist(map (lambda x : to_list(x[1]) if isinstance(x[1], list) else x[1], planners))

def parse_portfolios(planner_list):
    res_list = []
    res_port_map = {}
    portfolio_planners = []
    for p in planner_list:
        if "|" in p:
            res_port_map[p] = []
            for planner in p.split("|"):
                portfolio_planners.append(planner)    
                res_port_map[p].append(planner)
        else:
            res_list.append(p)
        
    for p in portfolio_planners:
        if not p in res_list:
            res_list.append(p)
    return res_list, res_port_map

def remove_duplicates(config_list): 
    res = []
    for e in config_list:
        if not e in res:
            res.append(e)
    return res

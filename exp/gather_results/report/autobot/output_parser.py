#!/usr/bin/python
# -*- coding: utf-8 -*-

# imports
# -----------------------------------------------------------------------------
import bz2                      # bzip2 compression service
import logging                  # loggers
import os                       # os services
import re                       # regular expressions

from collections import defaultdict

import dbparser                 # parsing of database specification files
import dbtools                  # database specification files
import sqltools                 # sqlite3 database access






#Alvaro: BotTestCase now has a list of data_readers. This are objects with a parse method. 
#Here, we define several basic DataReaders, though the users may define their own classes.
class DataReaderRegexp: 
    def __init__(self, regexp, namespace = None):
        (self.namespace, self.regexp, self.index) = \
         (namespace, re.compile(regexp), 0)

    def init(self):
        self.index = 0

    def parse (self, line, placeholders):
        # print namespace, "PARSE: ", line
        restat = self.regexp.match (line)
        if (restat):
            #print "Matched: ", line
            data = restat.groupdict ()
            if self.namespace:
                data ["index"] = self.index
                self.index += 1
                if self.namespace not in placeholders:
                    placeholders [self.namespace] = []
                placeholders [self.namespace].append(data)
            else:
                placeholders.update(data)


class DataReaderRegexpSplit: 
    def __init__(self, regexp, namespace, varname, separator = " "):
        (self.namespace, self.regexp, self.varname, self.separator, self.index) = \
         (namespace, regexp, varname, separator, 0)

    def init(self):
        self.index = 0

    def parse (self, line, placeholders):
        #print "PARSE: ", line
        restat = re.match (self.regexp, line)
        if (restat):
            #print (" Matched: %s", line)
            data = restat.groupdict()
            if self.varname in data:
                splitted_data = data[self.varname].strip().split(self.separator)
                for index2, d in enumerate(splitted_data):
                    datacopy = dict(data)
                    datacopy["index"] = self.index
                    datacopy["index2"] = index2
                    datacopy[self.varname] = d
                    #print datacopy
                    # add data from the regexp to the dictionary 
                    if self.namespace not in placeholders:
                        placeholders [self.namespace] = []
                    
                    placeholders [self.namespace].append(datacopy)
                self.index += 1
                        

class OutputParser (object):
    """
    Base class of all testbots. This class is equipped with an
    argument parser that can be reused/extended
    """

    # default string used for tst/db files that are passed as verbatim strings
    # and thus, have no name
    # -----------------------------------------------------------------------------
    defaultname = "<processed>"

    # regular epression for recognizing pairs (var, val) in the stdout
    # -----------------------------------------------------------------------------
    statregexp = " >[\t ]*(?P<varname>[a-zA-Z ]+):[ ]+(?P<value>([0-9]+\.[0-9]+|[0-9]+))"

    # Alvaro: flexible data readers configurable for the users
    # -----------------------------------------------------------------------------
    data_readers = []

    # logging services
    # -----------------------------------------------------------------------------
    _loglevel = logging.INFO            # default logging level


    # -----------------------------------------------------------------------------
    # check_flags
    #
    # check the parameters given to the automated execution of this instance
    # -----------------------------------------------------------------------------
    def check_flags (self, solver, tstfile, dbfile, timeout, memory, check, directory):

        """
        check the parameters given to the automated execution of this instance
        """

        # and perform the same validation with regard to the db file
        if (dbfile and dbfile != BotTestCase.defaultname and
            (not os.access (dbfile, os.F_OK) or
             not os.access (os.path.dirname (dbfile), os.R_OK))):
            self._logger.critical ("""
 The database specification file does not exist or it resides in an unreachable location
 Use '--help' for more information
""")
            raise ValueError (" The database specification file is not accessible")


    # -----------------------------------------------------------------------------
    # test
    #
    # invokes the execution of the given solver *in the same directory where it
    # resides* for solving all cases specified in 'tstspec' using the allotted
    # timeout and memory. The results are stored in 'resultsdir'; the solver is
    # sampled every 'check' seconds and different stats are stored in
    # 'stats'. Output files are named after 'output' and variable substitutions
    # specified in mainplaceholders are allwoed. 'dbspec' contains the database
    # specification used to store different data. If a prologue/epilogue is
    # given (they should be a subclass of BotAction ) then its __call__ method
    # is invoked before/after the execution of the solver with regard to every
    # test case
    # -----------------------------------------------------------------------------
    def test (self, solver, tstspec, dbspec, timeout, memory, output, check,
              resultsdir, compress, mainplaceholders, stats, prologue, epilogue):
        """
        invokes the execution of the given solver *in the same directory where
        it resides* for solving all cases specified in 'tstspec' using the
        allotted timeout and memory. The results are stored in 'resultsdir'; the
        solver is sampled every 'check' seconds and different stats are stored
        in 'stats'. Output files are named after 'output' and variable
        substitutions specified in mainplaceholders are allwoed. 'dbspec'
        contains the database specification used to store different data. If a
        prologue/epilogue is given (they should be a subclass of BotAction )
        then its __call__ method is invoked before/after the execution of the
        solver with regard to every test case
        """

        def _sub (string, D):
            """
            substitute in string the ocurrence of every keyword in D with its value
            if it appears preceded by '$' in string. Similar to Template.substitute
            but it also allows the substitution of strings which do not follow the
            convention of python variable names
            """

            result = string                                 # initialization
            for (ire, isub) in D.items ():                  # for all keys
                result = re.sub ('\$'+ire, isub, result)    # substitute
            return result                                   # and return

        solverdir = os.path.split (solver) [0]
        # Alvaro: Copy solverdir to have a security copy that will be restored in Epilogue
        solverdirtmpcopy = solverdir + '.tmpcopy'
        shutil.copytree (solverdir, solverdirtmpcopy)
        

        # now, for each test case
        for itst in tstspec:

            # initialize the dictionary with the value of some placeholders
            placeholders = {'index'       : itst.get_id (),
                            'name'        : os.path.basename (solver),
                            'date'        : datetime.datetime.now ().strftime ("%Y-%m-%d"),
                            'time'        : datetime.datetime.now ().strftime ("%H:%M:%S")}

            # and now, add the values of all the directives in this testcase and
            # all the arguments given to the main script
            placeholders.update (itst.get_values ())
            placeholders.update (mainplaceholders)

            # and also with the position of every argument (so that $1 can be
            # interpreted as the first parameter, $2 as the second, and so on)
            # ---note that these numerical indices are casted to strings for the
            # convenience of other functions
            placeholders.update (dict (zip([str(i) for i in range(0,len(itst.get_args ()))],
                                           itst.get_args ())))

            # compute the right name of the output file using the placeholders
            # if any was given there
            outputprefix = _sub (output, placeholders)

            # if a prologue was given, execute it now passing all parameters
            # (including the start run time which is computed right now)
            startruntime=time.time ()
            if prologue:
                action = prologue (solver=solver, tstspec=tstspec, itest=itst,
                                   dbspec=dbspec, timeout=timeout, memory=memory,
                                   output=outputprefix, check=check, basedir=self._directory,
                                   resultsdir=resultsdir, compress=compress,
                                   placeholders=placeholders, stats=stats,
                                   startruntime=startruntime)
                action (self._logger)

            # invoke the execution of this test case and record the start run
            # time and end run time
            self._logger.info ('\t%s' % itst)

            self.run (os.path.abspath (solver), resultsdir,
                      itst.get_id (), itst.get_args (), dbspec,
                      outputprefix, placeholders,
                      stats, check, timeout, memory, compress)

            # finally, if an epilogue was given, execute it now passing by also
            # the end run time
            if epilogue:
                action = epilogue (solver=solver, tstspec=tstspec, itest=itst,
                                   dbspec=dbspec, timeout=timeout, memory=memory,
                                   output=outputprefix, check=check, basedir=self._directory,
                                   resultsdir=resultsdir, compress=compress,
                                   placeholders=placeholders, stats=stats,
                                   startruntime=startruntime, endruntime=time.time ())
                action (self._logger)

            self.clean_results (os.path.abspath (solver), resultsdir,
                                itst.get_id (), itst.get_args (), dbspec,
                                outputprefix, placeholders,
                                stats, check, timeout, memory, compress)


            #Alvaro: Restore solver dir
            shutil.rmtree(solverdir)
            shutil.copytree (solverdirtmpcopy, solverdir)
        shutil.rmtree(solverdirtmpcopy)


    # -----------------------------------------------------------------------------
    # process_results
    #
    # it processes the given resultsfile (which is expected to have the standard
    # output of the process) which resides at the given directory and updates
    # the dictionary stats with the value of all variables found in all the data
    # tables in dbspec (either appearing in the standard output file or the
    # contents of files). This is done by updating the placeholders and then
    # invoking the 'poll' method in every data table
    # -----------------------------------------------------------------------------
    def process_results (self, directory, resultsfile, dbspec, placeholders, stats):
        """
        it processes the given resultsfile (which is expected to have the
        standard output of the process) which resides at the given directory and
        updates the dictionary stats with the value of all variables found in
        all the data tables in dbspec (either appearing in the standard output
        file or the contents of files). This is done by updating the
        placeholders and then invoking the 'poll' method in every data table
        """

        # populate the placeholders with the information retrieved from the
        # resultsfile (i.e., from the standard output of the executable)
        with open (os.path.join (directory, resultsfile), 'r') as stream:
            for data_reader in self.data_readers:
                data_reader.init()
            # now, for each line in the output file
            for iline in stream.readlines ():
                # alvaro: check the extra regexps
                for data_reader in self.data_readers:
                    data_reader.parse(iline, placeholders)

                # Now, this is a data reader 
                # check whether this line contains a stat
                # restat = re.match (self.statregexp, iline)

                # if (restat):
                #     # add this variable to the dictionary
                #     placeholders [restat.group ('varname').rstrip (" ")] = restat.group ('value')

        # also, populate the placeholders with the contents of files if requested by
        # any database table
        for itable in [jtable for jtable in dbspec if jtable.datap ()]:
            for icolumn in [jcolumn for jcolumn in itable if jcolumn.get_vartype () == 'FILEVAR']:
                with open (icolumn.get_variable (), 'r') as stream:
                    placeholders [icolumn.get_variable ()] = stream.read ()

        # now, compute the next row to write in all the data tables, if any
        for itable in dbspec:
            if itable.datap ():
                itable.poll(placeholders, stats)
                # Now, the table is the one that inserts data on stats
                # The reason is that some tables may add different columns to stats 
                # And some tables may not have data to include
                #stats [itable.get_name ()].append (itable.poll (placeholders))


    # -----------------------------------------------------------------------------
    # run
    #
    # executes the specified 'solver' *in the same directory where it resides*
    # (this is fairly convenient in case the solver needs additional input files
    # which are then extracted from a directory relative to the current
    # location) for solving the particular test case (qualified by index). It
    # copies the stdout and stderr of the solver in files named after output
    # (plus either .log or .err) which are then moved to the specified results
    # directory. The forked process is pinged every 'check' seconds and it is
    # launched with computational resources 'timeout' and 'memory'. 'dbspec'
    # contains the database specification used to store sys and data information
    # where placeholders specify the variable substitutions to be performed
    # -----------------------------------------------------------------------------
    def run (self, solver, resultsdir, index, spec, dbspec, output, placeholders, stats,
             check, timeout, memory, compress):
        """
        executes the specified 'solver' *in the same directory where it resides*
        (this is fairly convenient in case the solver needs additional input
        files which are then extracted from a directory relative to the current
        location) for solving the particular test case (qualified by index). It
        copies the stdout and stderr of the solver in files named after output
        (plus either .log or .err) which are then moved to the specified results
        directory. The forked process is pinged every 'check' seconds and it is
        launched with computational resources 'timeout' and 'memory'. 'dbspec'
        contains the database specification used to store sys and data
        information where placeholders specify the variable substitutions to be
        performed
        """

        # Initialization
        total_vsize = 0

        # create a timer
        runtimer = timetools.Timer ()

        # Now, a child is created which will host the solver execution while this
        # process simply monitors the resource comsumption. If any is exceeded the
        # child along with all its processes are killed
        with runtimer:

            # redirect the log and standard output to different files so that the
            # whole output is recorded
            (fdlog, fderr) = (os.open (os.path.join (os.getcwd (), output + ".log"),
                                       os.O_CREAT | os.O_TRUNC | os.O_WRONLY,
                                       0666),
                              os.open (os.path.join (os.getcwd (), output + ".err"),
                                       os.O_CREAT | os.O_TRUNC | os.O_WRONLY,
                                       0666))

            # create the child and record its process identifier
            try:
                child = subprocess.Popen ([solver] + spec,
                                          stdout = fdlog,
                                          stderr = fderr,
                                          cwd=os.path.dirname (solver), 
                                          preexec_fn=os.setsid)
            except OSError:
                self._logger.critical (" OSError raised when invoking the subprocess")
                raise OSError

            except ValueError:
                self._logger.critical (" Popen was invoked with invalid arguments")
                raise ValueError

            child_pid = child.pid

            # initialization
            max_mem   = 0                           # max mem ever used
            total_time = 0
            real_time = 0                           # real time (in seconds)
            time0 = datetime.datetime.now ()        # current time

            timeline = systools.ProcessTimeline ()  # create a process timeline

            #atorralba: Simplified loop and process termination
            while (total_time < timeout and
                   real_time <= 1.5 * timeout and
                   max_mem < memory):
                time.sleep(check)

                # get info of all the processes executed with the process group id
                # of the child and its children and add them to the timeline
                group = systools.ProcessGroup(os.getpgid (child_pid))
                timeline += group

                # compute the wall-clock time
                time1 = datetime.datetime.now ()    # time after sleeping
                real_time = (time1-time0).total_seconds ()  # compute wall clock time accurately

                # Generate the children information before the waitpid call to avoid a
                # race condition. This way, we know that the child_pid is a descendant.
                (pid, status) = os.waitpid (child_pid, os.WNOHANG)
                if ((pid, status) != (0, 0)):
                    break

                # get some stats such as total cpu time, memory, ...
                total_time = timeline.total_time()
                total_vsize = timeline.total_vsize()
                num_processes = timeline.total_processes ()
                num_threads = timeline.total_threads ()

                placeholders ['cputime'] = total_time
                placeholders ['wctime'] = real_time
                placeholders ['vsize'] = total_vsize
                placeholders ['numprocs'] = num_processes
                placeholders ['numthreads'] = num_threads

                # poll all sys tables
                for itable in dbspec:
                    if itable.sysp():
                        itable.poll(placeholders, stats)
                        #stats [itable.get_name()].append (itable.poll (placeholders))

                # update the maximum memory usage
                max_mem = max (max_mem, total_vsize)

            #Terminate process and ensure they are terminated. If fails, return an exception
            self._logger.debug (""" aborting children with SIGTERM ... children found: %s""" % timeline.pids ())
            timeline.terminate()

            admin_keys_value = map(lambda k : placeholders[k[3]], self.admin_keys)
            # record the exit status of this process
            stats ['admin_status'].append (tuple(admin_keys_value + [status]))

            # Even if we got here, there may be orphaned children or something we
            # may have missed due to a race condition. Check for that and kill
            # properly for good measure.
            self._logger.debug (""" [Sanity check] aborting children with SIGKILL for the last time ...
 [Sanity check] children found: %s""" % timeline.pids ())
            timeline.terminate (forceError = True)

            # add the timeline of this execution to the stats
            stats ['admin_timeline'] += (map (lambda x,y:tuple (x+y),
                                              [admin_keys_value]*len (timeline.get_processes ()),
                                              timeline.get_processes ()))

            # close the log and error file descriptors
            os.close (fdlog)
            os.close (fderr)


    # Alvaro: We separe this from run, so that this is executed AFTER epilogue
    def clean_results (self, solver, resultsdir, index, spec, dbspec, output, placeholders, stats,
                       check, timeout, memory, compress):

        def _bz2 (filename, remove=False):
            """
            compress the contents of the given filename and writes the results to a
            file with the same name + '.bz2'. If remove is enabled, the original
            filename is removed
            """

            # open the original file in read mode
            with open(filename, 'r') as input:

                # create a bz2file to write compressed data
                with bz2.BZ2File(filename+'.bz2', 'w', compresslevel=9) as output:

                    # and just transfer data from one file to the other
                    shutil.copyfileobj(input, output)

            # if remove is enabled, remove the original filename
            if (remove):
                os.remove (filename)


        # process the contents of the standard output
        self.process_results (os.getcwd (), output + ".log", dbspec, placeholders, stats)

        # once it has been processed move the .log and .err files to the results
        # directory
        for ilogfile in ['.log', '.err']:

            # compute the input filename
            ifilename = os.path.join (os.getcwd (), output + ilogfile)

            # first, if compression was explicitly requested, then proceed to
            # compress data
            if (compress):
                self._logger.debug (" Compressing the contents of file '%s'" % ifilename)

                _bz2 (ifilename, remove=True)

                # and now move it to its target location with the suffix 'bz2'
                shutil.move (ifilename + '.bz2',
                             os.path.join (resultsdir, output + ilogfile + '.bz2'))


            # if compression was not requested
            else:
                # just move the file to its target location
                shutil.move (os.path.join (os.getcwd (), output + ilogfile),
                             resultsdir)


    # -----------------------------------------------------------------------------
    # wrapup
    #
    # wrapup all the execution performing the last operations
    # -----------------------------------------------------------------------------
    def wrapup (self, tstspec, dbspec, configdir):

        """
        wrapup all the execution performing the last operations
        """

        # copy the file with all the tests cases to the config dir. In case that
        # an instance already processed was directly given then use default
        # names for the tb and db files
        if isinstance (tstspec, tsttools.TstVerbatim):
            with open (os.path.join (configdir, 'tests.tb'), 'w') as tests:
                tests.write (tstspec.data)

        elif isinstance (tstspec, tsttools.TstFile):
            shutil.copy (tstspec.filename,
                         os.path.join (configdir, os.path.basename (tstspec.filename)))
        else:
            raise ValueError (" Incorrect tstspec in wrapup")

        # and also the file with the database specification to the config dir
        if isinstance (dbspec, dbtools.DBVerbatim):
            with open (os.path.join (configdir, 'database.db'), 'w') as database:
                database.write (dbspec.data)

        elif isinstance (dbspec, dbtools.DBFile):
            shutil.copy (dbspec.filename,
                         os.path.join (configdir, os.path.basename (dbspec.filename)))
        else:
            raise ValueError (" Incorrect dbspec in wrapup")



    # -----------------------------------------------------------------------------
    # insert_data
    #
    # creates the table qualified by the instance of DBTable 'dbtable' in the given
    # databasename and writes the specified 'data' into it
    # -----------------------------------------------------------------------------
    def insert_data (self, databasename, dbtable, data):

       """
        creates the table qualified by the instance of DBTable 'dbtable' in the
        given databasename and writes the specified 'data' into it
        """

        # compute the filename
        dbfilename = databasename + '.db'
        self._logger.debug (" Populating '%s' in '%s'" % (dbtable.get_name (), dbfilename))

        # connect to the sql database
        db = sqltools.dbtest (dbfilename)

        # create the table
        db.create_table (dbtable)

        # and write data
        db.insert_data (dbtable, data)

        # close and exit
        db.close ()


    # -----------------------------------------------------------------------------
    # go
    #
    # main service provided by this class. It automates the whole execution
    # according to the given parameters. Solver is either a list of strings that
    # contain paths to a number of solvers that are applied in succession or
    # just a single solver (given also as a string) each one creating a
    # different database according to the specification in dbfile. All
    # executions refer to the same test cases defined in tstfile and are
    # allotted the same computational resources (timeout and memory). To ease
    # integration with other software, both the db and the tests specification
    # file can be given in various formats: as a string (been interpreted as a
    # path to the file to parse); as a verbatim specification (which is an
    # instance of TstVerbatim/DBVerbatim) or as a file already parsed
    # (TstFile/DBFile).
    #
    # The argnamespace is the Namespace of the parser used (which should be an
    # instance of argparse or None). Other (optional) parameters are:
    #
    # output - prefix of the output files that capture the standard out and
    #          error
    # check - time (in seconds) between successive pings to the executable
    # directory - target directory where all output is recorded
    # compress - if true, the files containing the standard output and error are
    #            compressed with bzip2
    # logger - if a logger is given, autobot uses a child of it. Otherwise, it
    #          creates its own logger
    # logfilter - if the client code uses a logger that requires additional
    #             information, a logging.Filter should be given here
    # prologue - if a class is provided here then __call__ () is automatically
    #            invoked before every execution of the solver with every test
    #            case. This class should be a subclass of BotAction so that it
    #            automatically inherits the following attributes: solver,
    #            tstspec, itest, dbspec, timeout, memory, output, check,
    #            resultsdir, compress, placeholders
    # epilogue - if a class is provided here then __call__ () is automatically
    #            invoked after every execution of the solver with every test
    #            case. This class should be a subclass of BotAction so that it
    #            automatically inherits the same attributes described in
    #            prologue
    # enter - much like prologue but __call__ is automatically invoked before
    #         the execution of the solver over the first test case
    # windUp - much like epilogue but __call__ is automatically invoked after
    #          the execution of the current solver with the last test instance
    # quiet - if given, some additional information is skipped
    # -----------------------------------------------------------------------------
    def go (self, solver, tstfile, dbfile, timeout, memory, argnamespace=None,
            output='$index', check=5, directory=os.getcwd (), compress=False,
            logger=None, logfilter=None, prologue=None, epilogue=None,
            enter=None, windUp=None, quiet=False):
        """
        main service provided by this class. It automates the whole execution
        according to the given parameters. Solver is either a list of strings
        that contain paths to a number of solvers that are applied in succession
        or just a single solver (given also as a string) each one creating a
        different database according to the specification in dbfile. All
        executions refer to the same test cases defined in tstfile and are
        allotted the same computational resources (timeout and memory). To ease
        integration with other software, both the db and the tests specification
        file can be given in various formats: as a string (been interpreted as a
        path to the file to parse); as a verbatim specification (which is an
        instance of TstVerbatim/DBVerbatim) or as a file already parsed
        (TstFile/DBFile).

        The argnamespace is the Namespace of the parser used (which should be an
        instance of argparse or None). Other (optional) parameters are:

        output - prefix of the output files that capture the standard out and
                 error
        check - time (in seconds) between successive pings to the executable
        directory - target directory where all output is recorded
        compress - if true, the files containing the standard output and error are
                   compressed with bzip2
        logger - if a logger is given, autobot uses a child of it. Otherwise, it
                 creates its own logger
        logfilter - if the client code uses a logger that requires additional
                    information, a logging.Filter should be given here
        prologue - if a class is provided here then __call__ () is automatically
                   invoked before every execution of the solver with every test
                   case. This class should be a subclass of BotAction so that it
                   automatically inherits the following attributes: solver,
                   tstspec, itest, dbspec, timeout, memory, output, check,
                   resultsdir, compress, placeholders
        epilogue - if a class is provided here then __call__ () is automatically
                   invoked after every execution of the solver with every test
                   case. This class should be a subclass of BotAction so that it
                   automatically inherits the same attributes described in
                   prologue
        enter - much like prologue but __call__ is automatically invoked before
                the execution of the solver over the first test case
        windUp - much like epilogue but __call__ is automatically invoked after
                 the execution of the current solver with the last test instance
        quiet - if given, some additional information is skipped
        """

        # copy the attributes
        (self._solver, self._tstfile, self._dbfile, self._timeout, self._memory,
         self._output, self._check, self._directory, self._compress,
         self._quiet) = \
         (solver, tstfile, dbfile, timeout, memory,
          output, check, directory, compress,
          quiet)

        # logger settings - if a logger has been passed, just create a child of
        # it
        if logger:
            self._logger = logger.getChild ('bots.BotTestCase')

            # in case a filter has been given add it and finally set the log level
            if logfilter:
                self._logger.addFilter (logfilter)

        # otherwise, create a simple logger based on a stream handler
        else:
            self._logger = logging.getLogger(self.__class__.__module__ + '.' +
                                             self.__class__.__name__)
            handler = logging.StreamHandler ()
            handler.setLevel (BotTestCase._loglevel)
            handler.setFormatter (logging.Formatter (" %(levelname)-10s:   %(message)s"))
            self._logger.addHandler (handler)

            # not passing a logger does not mean that other loggers do not exist
            # so that make sure that the log messages generated here are not
            # propagated upwards in the logging hierarchy
            self._logger.propagate = False

        self._logger.debug (" Starting automated execution ...")

        # make the specification of solvers to be a list of solvers even if just
        # a single solver was given
        if type (self._solver) is str:
            self._solver = [self._solver]
        if type (self._solver) is not list:
            raise ValueError (" Incorrect specification of solvers")

        # and now, create the test case and database specifications

        # process the test cases either as a string with a path to the file to
        # parse or just simply copy the specification in case it was given as a
        # verbatim string or as a file already parsed
        if type (self._tstfile) is str:
            self._logger.debug (" Parsing the tests specification file ...")
            self._tstspec = tsttools.TstFile (self._tstfile)
        elif isinstance (self._tstfile, tsttools.TstVerbatim):
            self._logger.debug (" The test cases were given as a verbatim specification")
            self._tstspec = self._tstfile
            self._tstfile = BotTestCase.defaultname
        elif isinstance (self._tstfile, tsttools.TstFile):
            self._logger.debug (" The test cases were given as a file already parsed")
            self._tstspec = self._tstfile
            self._tstfile = self._tstfile.filename
        else:
            raise ValueError (" Incorrect specification of the test cases")

        # proceed similarly in case of the database specification file
        if type (self._dbfile) is str:
            self._logger.debug (" Parsing the database specification file ...")
            self._dbspec  = dbtools.DBFile (self._dbfile)
        elif isinstance (self._dbfile, dbtools.DBVerbatim):
            self._logger.debug (" The database was given as a verbatim specification")
            self._dbspec = self._dbfile
            self._dbfile = BotTestCase.defaultname
        elif isinstance (self._dbfile, dbtools.DBFile):
            self._logger.debug (" The database was given as a file already parsed")
            self._dbspec = self._dbfile
            self._dbfile = self._dbfile.filename
        else:
            raise ValueError (" Incorrect specification of the database")

        # check that all parameters are valid
        self.check_flags (solver, self._tstfile, self._dbfile,
                          timeout, memory, check, directory)

        # and now, unless quiet is enabled, show the flags
        if (not self._quiet):

            self.show_switches (solver, self._tstfile, self._dbfile, timeout, memory,
                                check, directory, compress)

        # at last, run the experiments going through every solver
        if not solver:
            self._logger.warning (" No solver was given")

        for isolver in self._solver:

            # create an empty dictionary of stats
            istats = defaultdict (list)

            solvername = os.path.basename (isolver)

            self._logger.info (" Starting experiments with solver '%s'" % solvername)

            # initialize the placeholders with the parameters passed to the main
            # script. These are given in argnamespace. Since the argparser
            # automatically casts type according to their type field, they are
            # all converted into strings here to allow a uniform treatment
            placeholders = {}
            if argnamespace:
                for index, value in argnamespace.__dict__.items ():
                    placeholders [index] = str (value)

            # setup the necessary environment and retrieve the directories to be
            # used in the experimentation
            (resultsdir, configdir, logdir) = self.setup (solvername, self._directory)

            # write all the log information in the logdir
            self.fetch (logdir)

            # in case it is requested to execute an *enter* action do it now
            # if a prologue was given, execute it now
            if enter:
                action = enter (solver=isolver, tstspec=self._tstspec, dbspec=self._dbspec, data_readers = self.data_readers,
                                timeout=self._timeout, memory=self._memory,
                                check=self._check, basedir=self._directory,
                                resultsdir=resultsdir, compress=self._compress,
                                placeholders=placeholders, stats=istats)
                action (self._logger)

            # record the start time
            self._starttime = datetime.datetime.now ()

            # now, invoke the execution of all tests with this solver
            self.test (isolver, self._tstspec, self._dbspec, self._timeout, self._memory,
                       self._output, self._check, resultsdir, self._compress,
                       placeholders, istats, prologue, epilogue)

            # record the end time of this solver
            self._endtime = datetime.datetime.now ()

            # and wrapup
            self.wrapup (self._tstspec, self._dbspec, configdir)

            # finally, write down all the information to a sqlite3 db
            databasename = os.path.join (self._directory, solvername, solvername)
            self._logger.info (" Writing data into '%s.db'" % databasename)

            # admin tables are not populated using the poll method in every
            # dbtable. Instead, their contents are inserted manually in either
            # "run" or here
            istats ['admin_params'] = [(isolver, self._tstfile, self._dbfile, self._check, self._timeout, self._memory)]
            istats ['admin_tests'] = self._tstspec.get_defs ()
            istats ['admin_time'] = [(self._starttime, self._endtime,
                                      (self._endtime - self._starttime).total_seconds ())]
            istats ['admin_version'] = [('autobot', __version__, __revision__[1:-1], __date__ [1:-1])]

            # now, create the admin tables and populate all data tables
            self.create_admin_tables ()
            for itable in self._dbspec:
                self.insert_data (databasename, itable, istats[itable.get_name ()])

            # similarly to *enter*, in case a *windUp* action is given, execute
            # it now before moving to the next solver
            if windUp:
                action = windUp (solver=isolver, tstspec=self._tstspec, dbspec=self._dbspec, data_readers = self.data_readers,
                                 timeout=self._timeout, memory=self._memory,
                                 check=self._check, basedir=self._directory,
                                 resultsdir=resultsdir, compress=self._compress,
                                 placeholders=placeholders, stats=istats)
                action (self._logger)

        self._logger.debug (" Exiting from the automated execution ...")


# Local Variables:
# mode:python
# fill-column:80
# End:

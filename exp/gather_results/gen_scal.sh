

sqlite3 testcffh1.db "select t1.num, avg(time) from ((SELECT domain, problem, count(*) as num from data_undo group by domain, problem) t1 join (SELECT domain, problem, time from data_undo where type = 'undoable') t2 on t1.domain = t2.domain and t1.problem = t2.problem) group by t1.num;" | sort | uniq | sed 's/|/ /g' > scal/undoable

sqlite3 testcffh1.db "select t1.num, avg(time) from ((SELECT domain, problem, count(*) as num from data_undo group by domain, problem) t1 join (SELECT domain, problem, time from data_undo where type = 'not undoable' ) t2 on t1.domain = t2.domain and t1.problem = t2.problem) group by t1.num;" | sort | uniq | sed 's/|/ /g' > scal/notundoable_search

sqlite3 testcffh1.db "select t1.num, avg(time) from ((SELECT domain, problem, count(*) as num from data_undo group by domain, problem) t1 join (SELECT domain, problem, time from data_undo where type = 'not undoable in preprocess' ) t2 on t1.domain = t2.domain and t1.problem = t2.problem) group by t1.num;" | sort | uniq | sed 's/|/ /g' > scal/notundoable_preprocess

sqlite3 testcffh1.db "select t1.num, avg(time) from ((SELECT domain, problem, count(*) as num from data_undo group by domain, problem) t1 join (SELECT domain, problem, time from data_undo where type = 'not undoable in heuristic') t2 on t1.domain = t2.domain and t1.problem = t2.problem) group by t1.num;" | sort | uniq | sed 's/|/ /g' > scal/notundoable_heuristic

sqlite3 testcffh1.db "select t1.num from ((SELECT domain, problem, count(*) as num from data_undo group by domain, problem) t1 join (SELECT domain, problem, time from data_undo where type = 'timeout' or type = 'memout' or type = 'unknown') t2 on t1.domain = t2.domain and t1.problem = t2.problem) group by t1.num;" | sort | uniq | sed 's/$/ 350/' > scal/error


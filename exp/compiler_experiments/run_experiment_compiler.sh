#!/bin/bash
#1=domain 2=problem 3=suffix for compile script 4=compiler_dir 5=domains_dir 6=results_dir
TESTBOT_DIR=/mnt/data_server/torralba/JAIR17-Undoability/exp/testbot
COMPILER_DIR="$4"
DOMAINS_DIR="$5"
RESULTS_DIR="$6"

FINAL_DIR="$RESULTS_DIR"/"$3"/"$1"/"$2"
DIR=/mnt/data_server/torralba/JAIR17-Undoability/TMP_RESULTS/"$3"/"$1"/"$2"
EXEC_NAME=compile.sh

if [ -d "$DIR" ]; then
    exit
fi
if [ -d "$FINAL_DIR" ]; then
    exit
fi

mkdir -p "$DIR"

cp -r "$COMPILER_DIR" "$DIR"/compiler
cp -r "$TESTBOT_DIR" "$DIR"/testbot

echo "$DOMAINS_DIR/$1/$2 $DIR ${@:7}"
cat > "$DIR"/testbot/conf.tb << EOF   
exec ["$3 $DOMAINS_DIR/$1/$2 $DIR ${@:7}"];
EOF

cd "$DIR"/testbot
echo ./testbot.py -s "$DIR"/compiler/"$EXEC_NAME" -f "$DIR"/testbot/conf.tb -D "$DIR"/testbot/conf.db -t 986400 -m 4 -c 10 -o "$DIR"/testbot_output
./testbot.py -s "$DIR"/compiler/"$EXEC_NAME" -f "$DIR"/testbot/conf.tb -D "$DIR"/testbot/conf.db -t 986400 -m 4 -c 10 -o "$DIR"/testbot_output

#cleanup: move everything into metadata

mkdir "$DIR"/metadata
mv "$DIR"/testbot/"$EXEC_NAME"/results/* "$DIR"/metadata
mv "$DIR"/testbot/"$EXEC_NAME"/"$EXEC_NAME".db "$DIR"/metadata/sys.db
mv "$DIR"/compiler/*.txt "$DIR"/metadata
mv "$DIR"/domain.pddl "$DIR"/metadata
mv "$DIR"/problem.pddl "$DIR"/metadata
mv "$DIR"/invariants.pddl "$DIR"/metadata
mv "$DIR"/fddump "$DIR"/metadata

#Cleanup PDDL files so that they work with the planners
sed -i ':a;N;$!ba;s/\n/ /g' "$DIR"/*.pddl 
sed -i 's/)/)\n/g' "$DIR"/*.pddl 

#Delete 
rm -rf "$DIR"/compiler
rm -rf "$DIR"/testbot 

# Move the results to the final directory
mkdir -p "$FINAL_DIR"
cp -r "$DIR"/* "$FINAL_DIR"
rm -rf "$DIR"

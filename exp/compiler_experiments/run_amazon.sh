#!/bin/bash

domains=("amazon-cloud-y0w" "amazon-cloud-y0wo")

for a in {0..1}; do
    problem="p.pddl"  
    
    domain="${domains["$a"]}"
    
    nohup /mnt/data_server/torralba/Undoability/exp/compiler_experiments/run_experiment_compiler.sh "$domain" "$problem" "generous_rel" "/mnt/data2/torralba/compiler" "/mnt/data_server/torralba/Undoability/benchmarks/undoability-benchmarks-without-costs-selection" "/mnt/data_server/torralba/Undoability/compiled_benchmarks_repair" "1000" &

    nohup /mnt/data_server/torralba/Undoability/exp/compiler_experiments/run_experiment_compiler.sh "$domain" "$problem" "exact_rel" "/mnt/data2/torralba/compiler" "/mnt/data_server/torralba/Undoability/benchmarks/undoability-benchmarks-without-costs-selection" "/mnt/data_server/torralba/Undoability/compiled_benchmarks_repair" "1000" &

done

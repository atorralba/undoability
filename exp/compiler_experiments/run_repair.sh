#!/bin/bash

domains=("scanalyzer-opt11-strips")

problems=("p07.pddl")


for a in {0..1}; do
    problem="${problems["$a"]}"                    
    domain="${domains["$a"]}"                                                                                                                                                                                
    nohup /mnt/data_server/torralba/Undoability/exp/compiler_experiments/run_experiment_compiler.sh "$domain" "$problem" "generous_rel" "/mnt/data2/torralba/compiler" "/mnt/data_server/torralba/Undoability/benchmarks/undoability-benchmarks-without-costs-selection" "/mnt/data_server/torralba/Undoability/compiled_benchmarks_repair" "50" &
done

#!/bin/bash                                                                                                                                                                                                     

UNDODIR=/mnt/data_server/torralba/JAIR17-Undoability/
DOMAINS_DIR="$UNDODIR"/benchmarks
RESULTS_DIR="$UNDODIR"/compiled_benchmarks
RUN_FOLDER="$UNDODIR"/exp/compiler_experiments
COMPILER_DIR="$UNDODIR"/compiler

NUM_PROBLEMS="50"
SUFFIXES=('simple_exact' 'simple_exact_rel')

domains=`ls $DOMAINS_DIR`

num_total_problems=0
total_problem_list=()
total_domain_list=()
for domain in ${domains[@]}; do
    problem_list=(`ls $DOMAINS_DIR/$domain/*.pddl | grep -v domain | awk -F / '{print $(NF)}'`)
    num_problems=`ls $DOMAINS_DIR/$domain/*.pddl | grep -v domain | wc -l`
    num_total_problems=$((num_problems + num_total_problems))
    for p in ${problem_list[@]}; do 
	total_domain_list=( "${total_domain_list[@]}" "\"$domain\"" )	
	total_problem_list=( "${total_problem_list[@]}" "\"$p\"" )
    done
done



for SUFFIX in ${SUFFIXES[@]}; do
    cat > pbs.$SUFFIX << EOF   
#!/bin/bash                                                                                                                                                                                                                                                                                                                                                                                              
#$ -S /bin/bash 
#$ -t 1-$num_total_problems
#$ -o pbs.out 
#$ -e pbs.err 
#$ -p 0
#$ -q 'all.q@fai11.cs.uni-saarland.de,all.q@fai12.cs.uni-saarland.de'

domains=(${total_domain_list[@]})
problems=(${total_problem_list[@]})

num_p="\$((\$SGE_TASK_ID  % $num_total_problems))"
#Define domain and exec folder                                                                                                                                                                                      
problem="\${problems["\$num_p"]}"                                                                                                                                                                                
domain="\${domains["\$num_p"]}"                                                                                                                                                                                
$RUN_FOLDER/run_experiment_compiler.sh "\$domain" "\$problem" "$SUFFIX" "$COMPILER_DIR" "$DOMAINS_DIR" "$RESULTS_DIR" "$NUM_PROBLEMS"

EOF
#qsub pbs.$SUFFIX

done


#! /usr/bin/env python
import sys
import os
import re

if "simple" in sys.argv[1] or "poprp" in sys.argv[1]:
    print ("undo_simple.lsp")
else:
    print ("undo.lsp")

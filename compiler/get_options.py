#! /usr/bin/env python
import sys
import os
import re
options_name = sys.argv[1].split("_")

options  = []

if "generous" in options_name:
    options += [":generous", "t"]
   
if "rel" in options_name:
    options += [":relevance-pruning", "t"]

if "poprp" in options_name:
    options += [":use-unknown", "nil", ":avoid-constants", "nil"]


print (" ".join(options))

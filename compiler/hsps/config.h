
#ifndef CONFIG_H
#define CONFIG_H

//#define NTYPE_FLOAT
#define NTYPE_RATIONAL
#define RATIONAL_ARITHMETIC_CHECK_OVERFLOW
#define RATIONAL_ARITHMETIC_PRINT_OVERFLOW_WARNINGS
#define RATIONAL_ARITHMETIC_ABORT_ON_OVERFLOW
#define RATIONAL_ARITHMETIC_INTEGER_SPECIAL_CASE
#define SAFE_RATIONAL_PRECISION SHRT_MAX
// #define TRACE_PRINT_RATIONAL_ARITHMETIC

// #define CHECK_TABLE_INDEX
// #define CHECK_VECTOR_INDEX
// #define CHECK_HEURISTIC_CONSISTENCY
// #define CHECK_INCREMENTAL_EVAL
// #define ASSUME_UNIT_COST
#define APPLY_D_MIN_TRICK
#define APPLY_NCW_NOOP_TRICK

// #define APPLY_PATH_MAX

// store extra stats
// #define EVAL_EXTRA_STATS
// #define SEARCH_EXTRA_STATS
#define AH_EXTRA_STATS
// #define PROC_EXTRA_STATS

// running print extra stats (samples)
// #define PRINT_EXTRA_STATS

// #define RSS_FROM_RUSAGE_MAX
// #define RSS_FROM_PSINFO
#define RSS_FROM_PROCFS_STAT
// #define RSS_FROM_MALLINFO

#define RESOURCE_CHECK_INTERVAL 1
// #define PERIODIC_RESOURCE_CHECK
// #define HI_FREQ_STACK_CHECK
#define EXIT_ON_SECOND_INTERRUPT

// Namespacing

#define USE_HSPS_NAMESPACE

#ifdef USE_HSPS_NAMESPACE

#define BEGIN_HSPS_NAMESPACE namespace hsps {
#define END_HSPS_NAMESPACE   }
#define HSPS hsps

#else

#define BEGIN_HSPS_NAMESPACE
#define END_HSPS_NAMESPACE
#define HSPS

#endif

// Compatibility defines

#define HAVE_FINITE

#include <string.h>
#ifndef strndup
inline char* strndup(char* s, unsigned int n)
{
  char* d = new char[n+1];
  strncpy(d, s, n);
  d[n] = 0;
  return d;
}
#endif

#ifndef assert
#include <assert.h>
#endif

#ifdef __GNUC__
// This shaves off another 10% or so in my experiments.
#define ALIGN __attribute__((aligned))
#else
#define ALIGN
#endif

#endif

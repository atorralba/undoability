#! /usr/bin/env python
import sys
import os
import re
task_filename = sys.argv[1]

dirname, basename = os.path.split(task_filename)
domain_filename = os.path.join(dirname, "domain.pddl")
if not os.path.exists(domain_filename) and re.match(r"p[0-9][0-9]\b", basename):
    domain_filename = os.path.join(dirname, basename[:4] + "domain.pddl")
if not os.path.exists(domain_filename) and re.match(r"p[0-9][0-9]\b", basename):
    domain_filename = os.path.join(dirname, basename[:3] + "-domain.pddl")
if not os.path.exists(domain_filename) and re.match(r"p[0-9][0-9]\b", basename):
    domain_filename = os.path.join(dirname, "domain_" + basename)
if not os.path.exists(domain_filename) and basename.endswith("-problem.pddl"):
    domain_filename = os.path.join(dirname, basename[:-13] + "-domain.pddl")
if not os.path.exists(domain_filename):
    domain_filename = os.path.join(dirname, "domain_" + basename)
if not os.path.exists(domain_filename):
    raise SystemExit("Error: Could not find domain file using "
                     "automatic naming rules.")
print (domain_filename)

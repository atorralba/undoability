#!/bin/bash

# Mandatory arguments
#$1 - option name
#$2 - problem file 
#$3 - results directory
#$4 - number of action schemas to compile (NOT OPTIONAL!!!)

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd $DIR

RES="$3"
problem_file="$2"
domain_file=`./get_domain_file.py "$problem_file"`
undolsp_file=`./get_undolsp_file.py "$1"`
options=`./get_options.py "$1"`


mkdir -p "$RES"
echo $domain_file
echo $problem_file
echo $RES

cp "$domain_file" "$RES"/domain.pddl
cp "$problem_file" "$RES"/problem.pddl

if [[ "$1" == *"noinv" ]]; then
    echo "Not using invariants"    
else
    ./hsps/pddlcat -find-c -find-mutex -verify -a "$RES"/domain.pddl "$RES"/problem.pddl > "$RES"/invariants.pddl

    options="$options :invariant-file \"$RES/invariants.pddl\""
fi


ecl -load "src/inval.lsp" -load "src/rsk.lsp" -load "src/simplify.lsp" -load "src/tools.lsp" -load "src/$undolsp_file" \
    -eval "(undo-compile \"$RES/domain.pddl\" \"$RES/problem.pddl\" $options :num-actions-schema \"$4\")" \
    -eval "(quit)" 

mv invertible.txt "$RES"
mv domain-undo.pddl "$RES"
mv undo-* "$RES"
mv fddump "$RES"

sed -i ':a;N;$!ba;s/\n/ /g' "$RES"/*.pddl  &> /dev/null
sed -i 's/)/)\n/g' "$RES"/*.pddl  &> /dev/null
sed -i ':a;N;$!ba;s/)\n)/))/g' "$RES"/*.pddl  &> /dev/null

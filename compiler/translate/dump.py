#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys

def python_version_supported():
    major, minor = sys.version_info[:2]
    return (major == 2 and minor >= 7) or (major, minor) >= (3, 2)

if not python_version_supported():
    sys.exit("Error: Translator only supports Python >= 2.7 and Python >= 3.2.")


import axiom_rules
import fact_groups
import instantiate
import normalize
import pddl
import pddl_parser

import random 
random.seed(2015)

from pyparsing import OneOrMore, nestedExpr

class AuxAction: 
    def __init__(self, a):
        self.name = a.name
        predel = [x[1] for x in a.del_effects] + a.precondition
        self.add_effects = [x[1] for x in a.add_effects if x[1] not in predel]
        self.del_effects = [x[1] for x in a.add_effects if x[1] not in predel]
        self.precondition = a.precondition
        self.pres = set(a.precondition)
        self.dels = set(x[1] for x in a.del_effects)
        self.adds = set(self.add_effects)
        self.is_cond = any (x[0] !=[] for x in (a.add_effects + a.del_effects))

# each fact in seta is mutex with at least one fact in setb
def all_inconsistent (seta, setb, mutex_groups):
    return all (any (any (a in m and b in m for m in mutex_groups) for b in setb) for a in seta)

def is_invertible(a, actions, mutex_groups):
    #1 add (a) is inconsistent with pre(a), each fact in add (a) is
    # mutex with at least one fact in pre(a)
    if not all_inconsistent(a.add_effects, a.precondition, mutex_groups):
        #print (a.name , " not invertible because add is consistent with pre")
        return False

    #2 del(a) is subset of pre(a)
    if not a.dels.issubset(a.pres):
        #print (a.name , " not invertible because del is not subset of pre")
        return False

    #3 Exist a', add (a') = del (a) and del (a') = add (a) and
    #         pre(a') subseteq (pre(a) + add (a)) \ del (a) 
    preadd_nodel = (a.pres.union(a.adds)).difference(a.dels)
    for a2 in actions:
        if a.adds == a2.dels and \
           a.dels == a2.adds and \
           a2.pres.issubset(preadd_nodel):
            return a2

    #print (a.name , " not invertible because there is no a2")
    return False

def is_atleast_invertible(a, actions, mutex_groups):       
    preadd_nodel = (a.pres.union(a.adds)).difference(a.dels)
    for a2 in actions:
        # 1) pre(a') subset of (pre(a) + add (a)) \ del (a);
        # 2) del (a) subset of add (a') 
        # 3) del (a') is inconsistent with pre(a), ie, each fact in del (a') is mutex with at least one fact in pre(a).
        if a2.pres.issubset(preadd_nodel) and \
           a.dels.issubset(a2.adds) and \
           all_inconsistent((x for x in a2.dels), a.precondition, mutex_groups):
            #print ("with ", a2.name)
            return a2
    return False
    

def classify_actions(actions, mutex_groups, generous, destination ):
    aux_actions = [AuxAction(a) for a in actions]
    if any(x.is_cond for x in aux_actions):
        print ("ERROR: conditional effects ", file=sys.stderr)
        exit()
    print ("Normalized actions")

    rest = []
    for (i, a) in enumerate(aux_actions):
        if i % 100 == 0: 
            print ("    ", i, " actions classified")
        if (not a.add_effects and (generous or not a.del_effects)):
            print ("Null effects: ", a.name, file=destination)
            continue
        
        act_inv = is_invertible (a, aux_actions, mutex_groups) 
        if act_inv:
            print ("Invertible: ", a.name, " with ", act_inv.name, file=destination)
            continue
            
        if generous:
            act_inv = is_atleast_invertible (a, aux_actions, mutex_groups)
            if act_inv:
                print ("Atleast invertible: ", a.name, " with ", act_inv.name, file=destination)
                continue
        print ("Not invertible: ", actions[i].name, file=destination)
        rest.append(actions[i])
    return rest
            
def print_random_actions(actions, num_actions_schema, skip_actions_schema, destination=sys.stdout):
    actions_map = {}
    for act in actions:
        action_name = act.name[1:].split(" ")[0]
        if not action_name in actions_map:
            actions_map [action_name] = [act.name]
        else:
            actions_map [action_name].append(act.name)

    for a in actions_map: 
        action_list = actions_map[a]
        random.shuffle(action_list)
        if (len(action_list) > skip_actions_schema):
            for act in action_list[skip_actions_schema:skip_actions_schema+num_actions_schema]:
                print("  " + act, file=destination)

def print_atom(atom, destination=sys.stdout, indent=0, end='\n'):
    print((" " * indent) + "(" + atom.predicate, file=destination, end='')
    for arg in atom.args:
        print(" " + arg, file=destination, end='')
    print(")", file=destination, end=end)

def print_mutex_group(group, destination=sys.stdout, indent=0, end='\n'):
    print((" " * indent) + "(", file=destination, end='')
    for i in range(len(group)):
        if i > 0:
            print(" ", file=destination, end='')
        print_atom(group[i], destination=destination, indent=0, end='')
    print(")", file=destination, end=end)

def dump(task, num_actions_schema, skip_actions_schema, destination=sys.stdout, destination_invertible=sys.stdout, generous=False, pddlcat_file=""):
    print("Normalize")
    normalize.normalize(task)
    print("Instantiate")
    (goal_relaxed_reachable, atoms, actions, axioms,
     reachable_action_params) = instantiate.explore(task)
    print("Comptuting fact groups")
    (groups, mutex_groups, tk) = fact_groups.compute_groups(
        task, atoms, reachable_action_params,
        partial_encoding=True)

    print("Reading pddlcat invariants")
    try:
        pddlcat_invariants = read_pddlcat_invariants(pddlcat_file, atoms)
    except:
        pddlcat_invariants = []

    print("Classify actions: ", len(actions))
    nonclassified_actions = classify_actions(actions, pddlcat_invariants + mutex_groups, generous, destination_invertible)
    print ("Actions classified: ", len(actions), " remaining")
    print("(:atoms", file=destination)
    for atom in atoms:
        print_atom(atom, destination=destination, indent=2)
    print(" )", file=destination)

    print("(:actions", file=destination)
    for act in actions:
        print("  " + act.name, file=destination);
    print(" )", file=destination)

    print("(:nonclassified_actions", file=destination)
    if num_actions_schema == 0:
        for act in nonclassified_actions:
            print("  " + act.name, file=destination);
    else:
        print_random_actions(nonclassified_actions, num_actions_schema, skip_actions_schema, destination=destination)
    print(" )", file=destination)


    print("(:mutex-groups", file=destination)
    for group in mutex_groups:
        if len(group) > 1:
            print_mutex_group(group, destination=destination, indent=2)
    print(" )", file=destination)


def pddlcat_to_fd(a, atoms):
    try:
        return next(x for x in atoms if a[0] == x.predicate and a[1:] == list(x.args))
    except:
        print("WARNING: ", a, " excluded from mutex group", file=sys.stderr)
        return None

def read_pddlcat_invariants(pddlcat_file, atoms):
    #raise Exception("yada")  ## to escape error when pyparsing not installed
    f = open(pddlcat_file, "r")
    mutex_groups = []
    reading_invariant = False
    
    contents = f.read()
    if contents:
        data = OneOrMore(nestedExpr()).parseString(contents) 

        for d in data[0]:
            if d[0] == ":invariant":
                mg = d[2]
                if mg[1] != '1':
                    continue                
                mgfd = filter(None, [pddlcat_to_fd(a, atoms) for a in mg[2:]])
                if len(mgfd) > 1:
                    mutex_groups.append(mgfd)
                
    return mutex_groups

if __name__ == '__main__':
    # print("sys.argv = " + str(sys.argv))

    if len(sys.argv) < 6:
        print(sys.argv[0] + " <domain> <problem> <pddlcat_invariants> <destination_file> <destination_file_invertible> <only_generous> [<num_actions_schema> [<skip_actions_schema>]]")
        sys.exit(0)

    num_actions_schema = int(sys.argv[7]) if len(sys.argv) > 7 else 0
    skip_actions_schema = int(sys.argv[8]) if len(sys.argv) >8 else 0

    task = pddl_parser.open(task_filename=sys.argv[2],
                            domain_filename=sys.argv[1])

    if len(sys.argv) > 3:
        dumpfile = open(sys.argv[4], "w")
        dumpinvertiblefile = open(sys.argv[5], "w")
        dump(task, num_actions_schema, skip_actions_schema, destination=dumpfile, destination_invertible=dumpinvertiblefile, generous=(sys.argv[6]=="T"), pddlcat_file=sys.argv[3])
        dumpfile.close()
        dumpinvertiblefile.close()
    else:
        dump(task)





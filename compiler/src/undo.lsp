
;set larger limit  c-stack. By default is 128KB. We set it to 512MB
;(ext:set-limit 'ext:c-stack (* 512 1024 1024))

;; Files that the undo-compiler depends on: load if running
;; interactively or as a script.

;(setq *compiler-path* "/home/path/pkg/inval/")
;(load (concatenate 'string *compiler-path* "inval.lsp"))
;(load (concatenate 'string *compiler-path* "rsk.lsp"))
;(load (concatenate 'string *compiler-path* "simplify.lsp"))
;(load (concatenate 'string *compiler-path* "tools.lsp"))

;; Make domain definition for undo-compilation. Arguments are the same
;; as for make-domain-definition (defined in rsk.lsp).
(defun make-undo-domain
  (name reqs types objects constants predicates functions axioms actions
	&key (generous nil) (relevance-pruning nil))
  (let ((static-pred (collect-static-predicates predicates actions axioms)))
    (make-domain-definition
     name reqs types objects constants
     ;; create additional predicates:
     (append
      (mapflat #'(lambda (pred)
		   (append (list pred)
			   (if (not (find (car pred) static-pred))
			       (list (suffix-atom pred '-was-true)
				     (suffix-atom pred '-was-false)
				     (suffix-atom pred '-is-ok)))
			   (if relevance-pruning
			       (list (prefix-atom pred 'relevant-)))))
	       predicates)
      (if relevance-pruning
	  (mapcar #'(lambda (action)
		      (make-action-predicate action 'relevant-))
		  actions)))
     functions axioms
     ;; actions:
     (append
      ;; modified original actions:
      (mapcar #'(lambda (action)
		  (make-undo-action action predicates :generous generous
				    :relevance-pruning relevance-pruning))
	      actions)
      ;; sensing actions:
      (mapcar #'(lambda (pred)
		  (make-sensing-action
		   pred :relevance-pruning relevance-pruning))
	      (remove-if #'(lambda (pred) (member (car pred) static-pred))
			 predicates))
      ))))

;; Augment action with undo compilation effects.
(defun make-undo-action
  (action predicates &key (generous nil) (relevance-pruning nil))
  (cons (car action)
	(reassoc ':effect
		 (let ((aug-eff (make-undo-effects
				 (assoc-val ':effect (cdr action))
				 predicates :generous generous)))
		   (if (> (length aug-eff) 1) (cons 'and aug-eff)
		     (car aug-eff)))
		 (if relevance-pruning
		     (reassoc ':precondition
			      (merge-conjunctions
			       (assoc-val ':precondition (cdr action))
			       (make-action-predicate action 'relevant-))
			      (cdr action))
		   (cdr action))
		 )))

;; Create sensing actions for predicate:
(defun make-sensing-action (predicate &key (relevance-pruning nil))
  (if relevance-pruning
      (list (symnumcat 'sense- (car predicate))
	    (cons ':parameters (cdr predicate))
	    (cons ':precondition
		  (cons (symnumcat 'relevant- (car predicate))
			(mapcar #'car (cdr predicate))))
	    (cons ':observe (cons (car predicate)
				  (mapcar #'car (cdr predicate)))))
    (list (symnumcat 'sense- (car predicate))
	  (cons ':parameters (cdr predicate))
	  (cons ':observe (cons (car predicate)
				(mapcar #'car (cdr predicate)))))
    ))

;; Augment an effect formula with the compilation effects.
;; Returns a list (conjunction) of formulas, without the 'and.
(defun make-undo-effects (eform predicates &key (generous nil))
  (cond
   ((eq (car eform) 'and)
    (make-undo-effects-list (cdr eform) predicates :generous generous))
   ((eq (car eform) 'forall)
    (when (not (= (length eform) 3))
      (error "ill-formed effect formula: ~a" eform))
    (let ((aug (make-undo-effects (third eform) predicates :generous generous)))
      (list 'forall (second eform)
	    (if (> (length aug) 1) (cons 'and aug) aug))))
   ((eq (car eform) 'when)
    (when (not (= (length eform) 3))
      (error "ill-formed effect formula: ~a" eform))
    (mapcar #'(lambda (eff)
		(merge-effect-conditions (second eform) eff))
	    (make-undo-effects (third eform) predicates :generous generous)))
   ((eq (car eform) 'not)
    (when (not (= (length eform) 2))
      (error "ill-formed effect formula: ~a" eform))
	  (if generous 
	  ;; in generous mode, exclude the add of -is ok for an atom
	  ;; that was false because it cannot be deleted anyway. 
	      (list eform
		    (list 'when (suffix-atom (second eform) '-was-true)
		    (list 'not (suffix-atom (second eform) '-is-ok))))
	    (list eform
		  (list 'when (suffix-atom (second eform) '-was-true)
                  (list 'not (suffix-atom (second eform) '-is-ok)))
          	  (list 'when (suffix-atom (second eform) '-was-false)
	          (suffix-atom (second eform) '-is-ok)))))
   ((assoc (car eform) predicates)
    (if generous
	;; in generous mode, exclude the deletion of -is-ok for an atom
	;; that was false in init state when it is added:
	(list eform
	      (list 'when (suffix-atom eform '-was-true)
		    (suffix-atom eform '-is-ok)))
      ;; else, include both effects:
      (list eform
	    (list 'when (suffix-atom eform '-was-false)
		  (list 'not (suffix-atom eform '-is-ok)))
	    (list 'when (suffix-atom eform '-was-true)
		  (suffix-atom eform '-is-ok)))))
   (t (list eform))
   ))

(defun make-undo-effects-list (elist predicates &key (generous nil))
  (mapflat #'(lambda (eff)
	       (make-undo-effects eff predicates :generous generous))
	   elist))

(defun merge-effect-conditions (econd eff)
  (cond ((eq (car eff) 'when)
	 (list 'when (merge-conjunctions econd (second eff)) (third eff)))
	(t (list 'when econd eff))))

;; Add a suffix/prefix to the predicate in an atom:
(defun suffix-atom (atom suffix)
  (cons (symnumcat (car atom) suffix) (cdr atom)))

(defun prefix-atom (atom prefix)
  (cons (symnumcat prefix (car atom)) (cdr atom)))

;; Make a predicate out of action name with a prefix + params:
(defun make-action-predicate (action prefix)
  (cons (symnumcat prefix (car action))
	(mapcar #'car (assoc-val ':parameters (cdr action)))))

(defun make-undo-problems
  (domain-name ground-action ground-atoms mutex-map scis actions
   static-pred static-fun init types objects &key (generous nil)
   (relevance-pruning nil) (relevant-actions nil) (relevant-facts nil))
  ;; (format t "make-undo-problems: ~&~a ground invariants~%" (length scis))
  (let ((static-facts (filter-atoms-by-predicates init static-pred))
	(counter 0))
    (mapcar #'(lambda (cas)
		(let ((ig (make-undo-init-and-goal
			   ground-atoms static-facts scis (first cas)
			   (second cas) (third cas) (fourth cas)
			   relevance-pruning relevant-actions relevant-facts :generous generous)))
		  (make-problem-definition
		   (symnumcat 'undo- ground-action '-
			      (setq counter (+ counter 1)))
		   domain-name objects (first ig) (cons 'and (second ig))
		   nil nil)
		  ))
	    (make-undo-cases
	     ground-action ground-atoms mutex-map actions static-pred
	     static-fun init types objects :generous generous))
    ))

(defun make-undo-cases (ground-action atoms mutex-map actions static-pred
		        static-fun facts types objects &key (generous nil))
  (let* ((pre (ground-action-precondition ground-action actions static-pred
					  static-fun facts types objects))
	 (eff (ground-action-effect ground-action actions static-pred
				    static-fun facts types objects))
	 (true-pre (filter-true-atoms pre))
	 (false-pre (union (filter-false-atoms pre)
			   (implied-false-atoms true-pre mutex-map)
			   :test #'equal))
	 (true-post (union (filter-true-atoms eff)
			   (set-difference true-pre (filter-false-atoms eff)
					   :test #'equal)
			   :test #'equal))
	 (false-post (union (filter-false-atoms eff)
			    (set-difference false-pre (filter-true-atoms eff)
					    :test #'equal)
			    :test #'equal))
	 (overwritten-true (set-difference (filter-true-atoms eff)
					   (union false-pre true-pre
						  :test #'equal)
					   :test #'equal))
	 (overwritten-false (set-difference (filter-false-atoms eff)
					    (union false-pre true-pre
						   :test #'equal)
					    :test #'equal))
	 )
    (if generous
	;; in generous mode, we always return only a single case, with
	;; all overwritten atoms true in the state before:
	(list (list (append (union overwritten-true overwritten-false
				   :test #'equal) true-pre)
		    false-pre true-post false-post))
      ;; else, generate a case for each split (2-partitions) of the
      ;; overwritten variables:
      (mapcar #'(lambda (split)
		  (list (append (first split) true-pre)
			(append (second split) false-pre)
			true-post false-post))
	      (all-subsets
	       (union overwritten-true overwritten-false
		      :test #'equal) nil nil nil)))
    ))

(defun all-subsets (set in out subsets)
  (if (endp set) (cons (list in out) subsets)
    (let ((tmp (all-subsets (cdr set) (cons (car set) in) out subsets)))
      (all-subsets (cdr set) in (cons (car set) out) tmp))))

(defun get-atoms-from-literal-list (lits)
  (mapcar #'(lambda (lit) (if (eq (car lit) 'not) (cadr lit) lit)) lits))

(defun negate-literal (lit)
  (if (eq (car lit) 'not) (cadr lit) (list 'not lit)))

;; Construct init and goal lists of atoms for a given case.
;; Atoms is the list of all ground, non-static atoms.
;; Assumption: any atom that is known in the post-action state
;; (i.e., member of true-post or false-post) is also known in the
;; pre-action state. In other words, the enumeration of overwritten
;; facts is not handled in this function.
;; 'scis' is a list of ground set-constraint invariants.
(defun make-undo-init-and-goal
  (atoms static-facts scis true-pre false-pre true-post false-post
	 relevance-pruning relevant-actions relevant-facts &key (generous nil))
  (let ((init static-facts) (goal nil))
    (dolist (atom atoms)
      (cond
       ;; atom is true in post-action state
       ((find atom true-post :test #'equal)
	(push atom init)
	(cond
	 ;; atom was true in pre-action state
	 ((find atom true-pre :test #'equal)
	  (push (suffix-atom atom '-was-true) init)
	  (push (suffix-atom atom '-is-ok) init)
	  ;; (push (suffix-atom atom '-is-ok) goal)
	  (push atom goal))
	 ;; atom was false in pre-action state: it is not ok
	 ((find atom false-pre :test #'equal)
	  (push (suffix-atom atom '-was-false) init)
	  ;; (push (suffix-atom atom '-is-ok) goal)
	  (unless generous (push (list 'not atom) goal))) ; Alvaro: Avoid including negative goals in rectifiability
	 ;; overwritten atoms are not handled by this function!
	 (t (error "atom ~a is true post (~a, ~a) but unknown pre (~a, ~a)"
		   atom true-post false-post true-pre false-pre))
	 ))
       ;; atom is false in post-action state
       ((find atom false-post :test #'equal)
	(cond
	 ;; atom was true in pre-action state: it is not ok
	 ((find atom true-pre :test #'equal)
	  (push (suffix-atom atom '-was-true) init)
	  ;; (push (suffix-atom atom '-is-ok) goal)
	  (push atom goal))
	 ;; atom was false in pre-action state
	 ((find atom false-pre :test #'equal)
	  (push (suffix-atom atom '-was-false) init)
	  (push (suffix-atom atom '-is-ok) init)
	  ;; (push (suffix-atom atom '-is-ok) goal)
	  (unless generous (push (list 'not atom) goal))) ; Alvaro: Avoid including negative goals in rectifiability
	 ;; overwritten atoms are not handled by this function!
	 (t (error "atom ~a is false post (~a, ~a) but unknown pre (~a, ~a)"
		   atom true-post false-post true-pre false-pre))
	 ))
       ;; atom is unknown in post-action state, and hence unknown
       ;; also in the pre-action state: it is, however, known to be ok.
       ;; note: if the atom is not relevant, and relevance pruning is
       ;; enabled, we include no information about it.
       ((or (find atom relevant-facts :test #'equal)
	    (not relevance-pruning))
	(push (list 'unknown atom) init)
	(push (list 'unknown (suffix-atom atom '-was-true)) init)
	(push (list 'unknown (suffix-atom atom '-was-false)) init)
	(push (list 'oneof
		    (suffix-atom atom '-was-true)
		    (suffix-atom atom '-was-false)) init)
	(push (list 'oneof atom (suffix-atom atom '-was-false)) init)
	;; can we put negative literals in a oneof? no, but we can use or!
	(push (list 'or
		    (list 'not atom)
		    (suffix-atom atom '-was-true)) init)
	(push (suffix-atom atom '-is-ok) init)
	(push (suffix-atom atom '-is-ok) goal))
       ) ; ends cond
      (when relevance-pruning
	(when (find atom relevant-facts :test #'equal)
	  (push (prefix-atom atom 'relevant-) init)))
      ) ; ends dolist
    (dolist (sci scis)
      ;; if a set-constraint is of the type exactly-1, all atoms
      ;; mentioned in it are unknown in the init state of the
      ;; undo problem and relavant (if relevance pruning enabled),
      ;; and no atom in it is negated, then we add a 'oneof clause
      ;; for it.
      (when (and (eq (car sci) 'exactly-n)
		 (eq (cadr sci) 1)
		 (null (intersection (cddr sci) true-post :test #'equal))
		 (null (intersection (cddr sci) false-post :test #'equal))
		 (every #'(lambda (lit) (not (eq (car lit) 'not))) (cddr sci))
		 (if relevance-pruning
		     (subsetp (cddr sci) relevant-facts :test #'equal)
		   t))
	;; (format t "~&adding ~a to init state~%" sci)
	(push (cons 'oneof (cddr sci)) init))
      ;; if a set-constraint is of the type at-most-1, contains exactly
      ;; two atoms, both of which are unknown in the init state of the
      ;; undo problem and relavant (if relevance pruning enabled), then
      ;; we add an 'or' clause for it.
      (when (and (eq (car sci) 'at-most-n)
		 (eq (cadr sci) 1)
		 (eq (length (cddr sci)) 2)
		 (null (intersection (get-atoms-from-literal-list (cddr sci))
				     true-post :test #'equal))
		 (null (intersection (get-atoms-from-literal-list (cddr sci))
				     false-post :test #'equal))
		 (if relevance-pruning
		     (subsetp (get-atoms-from-literal-list (cddr sci))
			      relevant-facts :test #'equal)
		   t))
	;; (format t "~&adding ~a to init state~%" sci)
	(push (list 'or (negate-literal (first (cddr sci)))
		    (negate-literal (second (cddr sci))))
	      init))
      )
    (when relevance-pruning
      (dolist (act relevant-actions)
	(push (prefix-atom act 'relevant-) init)))
    (list init goal)))


(defun filter-true-atoms (alist)
  (remove-if #'(lambda (atom) (eq (car atom) 'not))
	     alist))

(defun filter-false-atoms (alist)
  (mapcar #'second
	  (remove-if #'(lambda (atom) (not (eq (car atom) 'not))) alist)))

(defun implied-false-atoms (atoms mutex-map)
  (let ((res nil))
    (dolist (atom atoms)
      (setq res (union res (cdr (assoc atom mutex-map :test #'equal))
		       :test #'equal)))
    res))

(defun filter-atoms-by-predicates (atoms predicates)
  (remove-if-not #'(lambda (atom) (member (car atom) predicates)) atoms))

;; Returns a list of atomic preconditions. (This will not work if
;; action preconditions contain disjunction.)
(defun ground-action-precondition
  (ground-action actions static-pred static-fun facts types objects)
  (let ((actdef (assoc (car ground-action) actions)))
    (when (null actdef)
      (error "no action ~a defined" ground-action))
    (let ((param (assoc-val ':parameters (cdr actdef)))
	  (prec (assoc-val ':precondition (cdr actdef))))
      (when (not (= (length param) (length (cdr ground-action))))
	(error "wrong number of arguments in ~a" ground-action))
      (let ((preform
	     (simplify-formula
	      (instantiate-quantifiers
	       (instantiate-1
		(pairlis (mapcar #'car param) (cdr ground-action)) prec)
	       nil types objects)
	      static-pred static-fun facts)))
	(cond ((null preform)
	       (error "precondition of ~a is statically false" ground-action))
	      ((eq preform t) nil)
	      ((eq (car preform) 'and) (cdr preform))
	      (t (list preform))
	      )))))

;; Returns a list of atomic and conditional effects.
(defun ground-action-effect
  (ground-action actions static-pred static-fun facts types objects)
  (let ((actdef (assoc (car ground-action) actions)))
    (when (null actdef)
      (error "no action ~a defined" ground-action))
    (let ((param (assoc-val ':parameters (cdr actdef)))
	  (eff (assoc-val ':effect (cdr actdef))))
      (when (not (= (length param) (length (cdr ground-action))))
	(error "wrong number of arguments in ~a" ground-action))
      (simplify-effect
       (instantiate-quantifiers
	(instantiate-1 (pairlis (mapcar #'car param) (cdr ground-action)) eff)
	nil types objects)
       static-pred static-fun facts)
      )))

;; an action a invertible if
;; (1) add(a) is inconsistent with pre(a);
;; (2) del(a) subseteq pre(a); and
;; there is an action a' such that:
;; (3) pre(a') subseteq (pre(a) union add(a)) \ del(a);
;; (4) add (a') = del (a) and del (a') = add (a).
(defun invertible
  (ground-action-list actions mutex-map static-pred static-fun facts
   types objects)
  ;; THIS IS NOT IMPLEMENTED!!
  nil)

;; “at least invertible” is a sufficient criterion for rectifiability.
;; An action a is at least invertible if there is an action a' such that:
;; (1) pre(a') subseteq (pre(a) union add (a)) \ del (a);
;; (2) add(a) superseteq del(a);
;; (3) del(a') is inconsistent with pre(a), ie, each fact in
;;     del(a') is mutex with at least one fact in pre(a).

;; Compute sets of relevant ground actions and atoms (as per Joerg's
;; and Alvaro's mail). undo-action is the ground action definition which
;; we're undo-planning for; actions is the list of action definitions;
;; ground-actions and ground-atoms are lists of ground actions and atoms
;; as produced by fd-ground.
;; The function returns two values, being the lists of relevant ground
;; actions and atoms respectively.
(defun relevance-analysis
  (undo-action actions static-pred static-fun facts types objects
   ground-actions ground-atoms)
  (let* ((relevant-actions nil)
	 (initial-atoms
 	  (filter-false-atoms
	   (ground-action-effect undo-action actions static-pred static-fun
				 facts types objects)))
	 (relevant-atoms initial-atoms)
	 (new-atoms initial-atoms))
    (loop
     ;; loop until no more new atoms
     (when (endp new-atoms) (return))
     (let ((new-atom (car new-atoms))
	   (rem-actions nil))
       ;; pick first atom off new-atoms
       (setq new-atoms (cdr new-atoms))
       ;; for all remaining actions...
       (dolist (act ground-actions)
	 (let ((effs (ground-action-effect
		      act actions static-pred static-fun facts types objects)))
	   (cond
	    ;; if act adds new-atom...
	    ((find new-atom effs :test #'equal)
	     ;; add it's preconditions and deletes to relevant-atoms
	     ;; (if not already there) and to new-atoms.
	     (dolist (del-atom (filter-false-atoms effs))
	       (when (not (find del-atom relevant-atoms :test #'equal))
		 (push del-atom relevant-atoms)
		 (push del-atom new-atoms)))
	     (dolist (pre-atom (filter-true-atoms
				(ground-action-precondition
				 act actions static-pred static-fun facts
				 types objects)))
	       (when (not (find pre-atom relevant-atoms :test #'equal))
		 (push pre-atom relevant-atoms)
		 (push pre-atom new-atoms)))
	     ;; and add action to relevant-actions
	     (push act relevant-actions))
	    ;; otherwise, place action on remaining-actions list
	    (t (push act rem-actions))
	    )))
       (setq ground-actions rem-actions)
       ))
    (format t "~D of ~D actions are relevant~%"
	    (length relevant-actions) (length actions))    
    (format t "~D of ~D atoms are relevant~%"
	    (length relevant-atoms) (length ground-atoms))
    (values relevant-actions relevant-atoms)
    )
  ;; skeleton implementation which returns as relevant all actions/atoms:
  ;; (values ground-actions ground-atoms)
  )

;; (defun ground-action-definition (ground-action actions)
;;   (let ((actdef (assoc (car ground-action) actions)))
;;     (when (null actdef)
;;       (error "definition of ~w not found in ~w" ground-action actions))
;;     (let ((param (assoc-val ':parameters (cdr actdef)))
;; 	  (args (cdr ground-action)))
;;       (when (not (= (length param) (length args)))
;; 	(error "wrong number of args for ~w in ~w" param ground-action))
;;       (instantiate-1 (make-binding param args) (cdr actdef))
;;       )))


(defun undo-compile
  (domain-file problem-file &key (invariant-file nil) (generous nil)
   (relevance-pruning nil) (num-actions-schema "0")  (skip-actions-schema "0"))
  ;;(format t "~&num = ~s, skip = ~s, generous = ~s~%" num-actions-schema skip-actions-schema generous)
  (format t "~&loading ~a and ~a~%" domain-file problem-file)
  (load-files domain-file problem-file)
  (when invariant-file
    (format t "~&loading ~a~%" invariant-file)
    (parse-file invariant-file (read-file invariant-file)))
  (format t "compiling domain...~%")
  (let ((undo-domain
	 (make-undo-domain
	  (symnumcat 'undo- *domain-name*) '(:adl :typing)
	  *types* (union  *constants* *objects*) nil *predicates* *functions* *axioms* *actions*
	  :generous generous :relevance-pruning relevance-pruning))
	(domain-file-name
	 (concatenate 'string (pathname-name domain-file) "-undo.pddl"))
	(problem-base-name
	 (concatenate 'string "undo-" (pathname-name problem-file)))
	(static-pred (collect-static-predicates
		      *predicates* *actions* *axioms*))
	(static-fun (collect-static-functions *functions* *actions*))
	(counter 0)
	;; only ground invariants that are going to be used:
	(scis (mapflat
	       #'(lambda (inv)
		   (ground-invariant inv *init* *types* (union  *constants* *objects*)))
	       (remove-if-not
		#'(lambda (inv)
		    (let ((sc (assoc-val ':set-constraint inv)))
		      (if sc (and (eq (car sc) 'exactly-n) (eq (cadr sc) 1))
			nil)))
		*invariants*)))
	)
    (format t "~a ground invariants~%" (length scis))
    (make-format-PDDL-friendly)
    (format t "writing compiled domain to ~a~%" domain-file-name)
    (with-open-file
     (stream domain-file-name :direction :output)
     (format stream "~&~w~%" undo-domain))
    (format t "grounding...~%")
    (let* ((dump (call-fd-dump domain-file problem-file invariant-file num-actions-schema skip-actions-schema generous))
	   (mutex-map (make-mutex-map (first dump) (fifth dump))))
      (dolist (ground-action (third dump))
	(multiple-value-bind (relevant-actions relevant-facts)
	    (relevance-analysis
	     ground-action *actions* static-pred static-fun *init*
	     *types* *objects* (second dump) (first dump))
	  (let ((problems (make-undo-problems
			   (symnumcat 'undo- *domain-name*) ground-action
			   (first dump) mutex-map scis *actions* static-pred
			   static-fun *init* *types* (union  *constants* *objects*)
			   :generous generous
			   :relevance-pruning relevance-pruning
			   :relevant-actions relevant-actions
			   :relevant-facts relevant-facts)))
	    (dolist (prob problems)
	      (let ((problem-file-name
		     (concatenate 'string problem-base-name "-test-"
				  (princ-to-string (setq counter (+ counter 1)))
				  ".pddl")))
		(format t "writing compiled problem to ~a~%" problem-file-name)
		(with-open-file
		 (stream problem-file-name :direction :output)
		 (format stream "~&;; undo of ~a~%" ground-action)
		 (format stream "~&~w~%" prob))
		))))
	))))

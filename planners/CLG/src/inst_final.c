

/*********************************************************************
 * (C) Copyright 2001 Albert Ludwigs University Freiburg
 *     Institute of Computer Science
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 *********************************************************************/



/*
 * THIS SOURCE CODE IS SUPPLIED  ``AS IS'' WITHOUT WARRANTY OF ANY KIND, 
 * AND ITS AUTHOR AND THE JOURNAL OF ARTIFICIAL INTELLIGENCE RESEARCH 
 * (JAIR) AND JAIR'S PUBLISHERS AND DISTRIBUTORS, DISCLAIM ANY AND ALL 
 * WARRANTIES, INCLUDING BUT NOT LIMITED TO ANY IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND
 * ANY WARRANTIES OR NON INFRINGEMENT.  THE USER ASSUMES ALL LIABILITY AND
 * RESPONSIBILITY FOR USE OF THIS SOURCE CODE, AND NEITHER THE AUTHOR NOR
 * JAIR, NOR JAIR'S PUBLISHERS AND DISTRIBUTORS, WILL BE LIABLE FOR 
 * DAMAGES OF ANY KIND RESULTING FROM ITS USE.  Without limiting the 
 * generality of the foregoing, neither the author, nor JAIR, nor JAIR's
 * publishers and distributors, warrant that the Source Code will be 
 * error-free, will operate without interruption, or will meet the needs 
 * of the user.
 */








/*********************************************************************
 * File: inst_final.c
 * Description: final domain representation functions
 *
 *
 * Author: Joerg Hoffmann 2000
 *
 *********************************************************************/ 









#include "ff.h"

#include "output.h"
#include "memory.h"

#include "inst_pre.h"
#include "inst_final.h"














/********************************
 * POSSIBLY TRUE FACTS ANALYSIS *
 ********************************/








/* local globals for this part
 */

int_pointer lpos[MAX_PREDICATES];
int_pointer lneg[MAX_PREDICATES];
int_pointer luse[MAX_PREDICATES];
int_pointer lindex[MAX_PREDICATES];

int lp;
int largs[MAX_VARS];







void perform_reachability_analysis( void )

{
  
  int size, i, j, k, adr, num,num_bra, pargtype;
  Bool fixpoint;
  Facts *f;
  NormOperator *no;
  EasyTemplate *t1, *t2;
  NormEffect *ne;
  Action *tmp, *a;
  Bool *had_hard_template;
  PseudoAction *pa;
  PseudoActionEffect *pae;
  NormEffect * nb;
  
  gactions = NULL;
  gnum_actions = 0;
  
  for ( i = 0; i < gnum_predicates; i++ ) {
    size =  1;
    for ( j = 0; j < garity[i]; j++ ) {
      pargtype = gpredicates_args_type[i][j];
      size *= gtype_size[pargtype];
    }
    
    lpos[i] = ( int_pointer ) calloc( size, sizeof( int ) );
    lneg[i] = ( int_pointer ) calloc( size, sizeof( int ) );
    luse[i] = ( int_pointer ) calloc( size, sizeof( int ) );
    lindex[i] = ( int_pointer ) calloc( size, sizeof( int ) );
    
    for ( j = 0; j < size; j++ ) {
      lpos[i][j] = 0;
      lneg[i][j] = 1;/* all facts but initials are poss. negative */
      luse[i][j] = 0;
      lindex[i][j] = -1;
    }
  }
  
  had_hard_template = ( Bool * ) calloc( gnum_hard_templates, sizeof( Bool ) );
  for ( i = 0; i < gnum_hard_templates; i++ ) {
    had_hard_template[i] = FALSE;
  }
  
  /* mark initial facts as possibly positive, not poss. negative
   */
  for ( i = 0; i < gnum_predicates; i++ ) {
    lp = i;
    for ( j = 0; j < gnum_initial_predicate[i]; j++ ) {
      for ( k = 0; k < garity[i]; k++ ) {
        largs[k] = ginitial_predicate[i][j].args[k];
      }
      adr = fact_adress();
      lpos[lp][adr] = 1;
      lneg[lp][adr] = 0;
    }
  }
  
  /* compute fixpoint
   */
  fixpoint = FALSE;
  while ( !fixpoint ) {
    fixpoint = TRUE;
    
    /* assign next layer of easy templates to possibly positive fixpoint
     */
    t1 = geasy_templates;
    while ( t1 ) {
      no = t1->op;
      for ( i = 0; i < no->num_preconds; i++ ) {
        lp = no->preconds[i].predicate;
        for ( j = 0; j < garity[lp]; j++ ) {
          largs[j] = ( no->preconds[i].args[j] >= 0 ) ?
          no->preconds[i].args[j] : t1->inst_table[DECODE_VAR( no->preconds[i].args[j] )];
        }
        if ( !lpos[lp][fact_adress()] ) {
          break;
        }
      }
      
      if ( i < no->num_preconds ) {
        t1 = t1->next;
        continue;
      }
      
      num = 0;
      for ( ne = no->effects; ne; ne = ne->next ) {
        num++;
        /* currently, simply ignore effect conditions and assume
         * they will all be made true eventually.
         */
        for ( i = 0; i < ne->num_adds; i++ ) {
          lp = ne->adds[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = ( ne->adds[i].args[j] >= 0 ) ?
            ne->adds[i].args[j] : t1->inst_table[DECODE_VAR( ne->adds[i].args[j] )];
          }
          adr = fact_adress();
          if ( !lpos[lp][adr] ) {
            /* new relevant fact! (added non initial)
             */
            lpos[lp][adr] = 1;
            lneg[lp][adr] = 1;
            luse[lp][adr] = 1;
            if ( gnum_relevant_facts == MAX_RELEVANT_FACTS ) {
              printf("\ntoo many relevant facts! increase MAX_RELEVANT_FACTS (currently %d)\n\n",
                     MAX_RELEVANT_FACTS);
              exit( 1 );
            }
            grelevant_facts[gnum_relevant_facts].predicate = lp;
            for ( j = 0; j < garity[lp]; j++ ) {
              grelevant_facts[gnum_relevant_facts].args[j] = largs[j];
            }
            lindex[lp][adr] = gnum_relevant_facts;
            gnum_relevant_facts++;
            fixpoint = FALSE;
          }
        }
      }
      
      num_bra = 0;
      if (no->operatore->is_obs)
        for (nb = no->branches; nb; nb = nb->next) {
          /* Calculates no of branches */
          num_bra++;
          
          /* currently, simply ignore effect conditions and assume
           * they will all be made true eventually.
           */
          for ( i = 0; i < nb->num_adds; i++ ) {
            lp = nb->adds[i].predicate;
            for ( j = 0; j < garity[lp]; j++ ) {
              largs[j] = ( nb->adds[i].args[j] >= 0 ) ?
              nb->adds[i].args[j] : t1->inst_table[DECODE_VAR( nb->adds[i].args[j] )];
            }
            adr = fact_adress();
            if ( !lpos[lp][adr] ) {
              /* new relevant fact! (added non initial)
               */
              lpos[lp][adr] = 1;
              lneg[lp][adr] = 1;
              luse[lp][adr] = 1;
              if ( gnum_relevant_facts == MAX_RELEVANT_FACTS ) {
                printf("\ntoo many relevant facts! increase MAX_RELEVANT_FACTS (currently %d)\n\n",
                       MAX_RELEVANT_FACTS);
                exit( 1 );
              }
              grelevant_facts[gnum_relevant_facts].predicate = lp;
              for ( j = 0; j < garity[lp]; j++ ) {
                grelevant_facts[gnum_relevant_facts].args[j] = largs[j];
              }
              lindex[lp][adr] = gnum_relevant_facts;
              gnum_relevant_facts++;
              fixpoint = FALSE;
            }
          }
        }
      
      /* Makes up actions list from norm_operators and 
       * easy templates.
       */
      tmp = new_Action();
      tmp->norm_operator = no;
      for ( i = 0; i < no->num_vars; i++ ) {
        tmp->inst_table[i] = t1->inst_table[i];
      }
      tmp->name = no->operatore->name;
      tmp->num_name_vars = no->operatore->number_of_real_params;
      make_name_inst_table_from_NormOperator( tmp, no, t1 );
      tmp->next = gactions;
      tmp->num_effects = num;
      tmp->num_branches = num_bra;
      gactions = tmp;
      gnum_actions++;
      
      t2 = t1->next;
      if ( t1->next ) {
        t1->next->prev = t1->prev;
      }
      if ( t1->prev ) {
        t1->prev->next = t1->next;
      } else {
        geasy_templates = t1->next;
      }
      free_single_EasyTemplate( t1 );
      t1 = t2;
    }
    
    /* now assign all hard templates that have not been transformed
     * to actions yet.
     */
    for ( i = 0; i < gnum_hard_templates; i++ ) {
      if ( had_hard_template[i] ) {
        continue;
      }
      pa = ghard_templates[i];
      
      for ( j = 0; j < pa->num_preconds; j++ ) {
        lp = pa->preconds[j].predicate;
        for ( k = 0; k < garity[lp]; k++ ) {
          largs[k] = pa->preconds[j].args[k];
        }
        if ( !lpos[lp][fact_adress()] ) {
          break;
        }
      }
      
      if ( j < pa->num_preconds ) {
        continue;
      }
      
      for ( pae = pa->effects; pae; pae = pae->next ) {
        /* currently, simply ignore effect conditions and assume
         * they will all be made true eventually.
         */
        for ( j = 0; j < pae->num_adds; j++ ) {
          lp = pae->adds[j].predicate;
          for ( k = 0; k < garity[lp]; k++ ) {
            largs[k] = pae->adds[j].args[k];
          }
          adr = fact_adress();
          if ( !lpos[lp][adr] ) {
            /* new relevant fact! (added non initial)
             */
            lpos[lp][adr] = 1;
            lneg[lp][adr] = 1;
            luse[lp][adr] = 1;
            if ( gnum_relevant_facts == MAX_RELEVANT_FACTS ) {
              printf("\ntoo many relevant facts! increase MAX_RELEVANT_FACTS (currently %d)\n\n",
                     MAX_RELEVANT_FACTS);
              exit( 1 );
            }
            grelevant_facts[gnum_relevant_facts].predicate = lp;
            for ( k = 0; k < garity[lp]; k++ ) {
              grelevant_facts[gnum_relevant_facts].args[k] = largs[k];
            }
            lindex[lp][adr] = gnum_relevant_facts;
            gnum_relevant_facts++;
            fixpoint = FALSE;
          }
        }
      }
      
      tmp = new_Action();
      tmp->pseudo_action = pa;
      for ( j = 0; j < pa->operatore->num_vars; j++ ) {
        tmp->inst_table[j] = pa->inst_table[j];
      }
      
      num_bra = 0;
      if (pa->operatore->is_obs) {
        /* Never has to came here */
        printf("\n\tERROR: we've an observation as HARD template\n");
        exit(-1);
        /* Calculates no of branches */
        num_bra++;
        
        /* Transform Literal branches into NormEffects */
      }
      
      
      tmp->name = pa->operatore->name;
      tmp->num_name_vars = pa->operatore->number_of_real_params;
      make_name_inst_table_from_PseudoAction( tmp, pa );
      tmp->next = gactions;
      tmp->num_effects = pa->num_effects;
      tmp->num_branches = num_bra;
      gactions = tmp;
      gnum_actions++;
      
      had_hard_template[i] = TRUE;
    }
  }
  
  free( had_hard_template );
  
  gnum_pp_facts = gnum_initial + gnum_relevant_facts;
  
  if ( gcmd_line.display_info == 118 ) {
    printf("\nreachability analysys came up with:");
    
    printf("\n\npossibly positive facts:");
    for ( f = ginitial; f; f = f->next ) {
      printf("\n");
      print_Fact( f->fact );
    }
    printf("\nrelevant facts without evaluating the effects\n");
    for ( i = 0; i < gnum_relevant_facts; i++ ) {
      printf("\n");
      print_Fact( &(grelevant_facts[i]) );
    }
    
    printf("\n\nthis yields these %d action templates:", gnum_actions);
    for ( i = 0; i < gnum_operators; i++ ) {
      printf("\n\noperator %s:", goperators[i]->name);
      for ( a = gactions; a; a = a->next ) {
        if ( ( a->norm_operator && 
              a->norm_operator->operatore !=  goperators[i] ) ||
            ( a->pseudo_action &&
             a->pseudo_action->operatore !=  goperators[i] ) ) {
          continue;
        }
        printf("\ntemplate: ");
        for ( j = 0; j < goperators[i]->number_of_real_params; j++ ) {
          printf("%s", gconstants[a->name_inst_table[j]]);
          if ( j < goperators[i]->num_vars-1 ) {
            printf(" ");
          }
        }
      }
    }
    printf("\n\n");
  }
  
}



/* bit complicated to avoid memory explosion when high arity predicates take
 * num_obs ^ arity space. take space for individual arg types only; 
 * must consider pred args in smallest - to - largest - type order to make
 * mapping injective.
 */
int fact_adress( void )

{
  
  int r = 0, b = 1, i, j, min, minj;
  Bool done[MAX_ARITY];
  
  for ( i = 0; i < garity[lp]; i++ ) {
    done[i] = FALSE;
  }
  
  for ( i = 0; i < garity[lp]; i++ ) {
    min = -1;
    minj = -1;
    for ( j = 0; j < garity[lp]; j++ ) {
      if ( !done[j] ) {
        if ( min == -1 ||
            gtype_size[gpredicates_args_type[lp][j]] < min ) {
          min = gtype_size[gpredicates_args_type[lp][j]];
          minj = j;
        }
      }
    }
    if ( minj == -1 || min == -1 ) {
      printf("\n\nmin or minj not made in fact adress?\n\n");
      exit( 1 );
    }
    /* now minj is remaining arg with lowest type size min
     */
    /* need number **within type** here! */
    r += b * gmember_nr[largs[minj]][gpredicates_args_type[lp][minj]];
    b *= min;
    done[minj] = TRUE;
  }
  
  return r;
  
}



void make_name_inst_table_from_NormOperator( Action *a, NormOperator *o, EasyTemplate *t )

{
  
  int i, r = 0, m = 0;
  
  for ( i = 0; i < o->operatore->number_of_real_params; i++ ) {
    if ( o->num_removed_vars > r &&
        o->removed_vars[r] == i ) {
      /* this var has been removed in NormOp;
       * insert type constraint constant
       *
       * at least one there, as empty typed pars ops are removed
       */
      a->name_inst_table[i] = gtype_consts[o->type_removed_vars[r]][0];
      r++;
    } else {
      /* this par corresponds to par m  in NormOp
       */
      a->name_inst_table[i] = t->inst_table[m];
      m++;
    }
  }
  
}



void make_name_inst_table_from_PseudoAction( Action *a, PseudoAction *pa )

{
  
  int i;
  
  for ( i = 0; i < pa->operatore->number_of_real_params; i++ ) {
    a->name_inst_table[i] = pa->inst_table[i];
  }
  
}


















/***********************************************************
 * RELEVANCE ANALYSIS AND FINAL DOMAIN AND PROBLEM CLEANUP *
 ***********************************************************/









/* counts effects for later allocation
 */
int lnum_effects;




Bool already_used ( void )
{
  static Bool first_call = TRUE;
  static Bool *used;
  
  int i;
  
  if ( first_call ) {
    used = ( Bool * ) calloc( gnum_predicates, sizeof( Bool ) );
    for ( i = 0; i < gnum_predicates; i++ ) {
      used[i] = FALSE;
    }
    first_call = FALSE;
  }
  
  if( !used[lp] ) {
    used[lp] = TRUE;
    return FALSE;
  }
  
  return used[lp];
}




void collect_relevant_facts( void )

{
  
  Action *a;
  NormOperator *no;
  NormEffect *ne;
  int i, j, adr;
  PseudoAction *pa;
  PseudoActionEffect *pae;
  
  /* mark all deleted facts; such facts, that are also pos, are relevant.
   */
  for ( a = gactions; a; a = a->next ) {
    if ( a->norm_operator ) {
      no = a->norm_operator;
      
      
      
      if (  no->operatore->is_obs) {
        
        for ( ne = no->branches; ne; ne = ne->next ) {
          for ( i = 0; i < ne->num_dels; i++ ) {
            lp = ne->dels[i].predicate;
            for ( j = 0; j < garity[lp]; j++ ) {
              largs[j] = (  ne->dels[i].args[j] >= 0 ) ?
              ne->dels[i].args[j] : a->inst_table[DECODE_VAR( ne->dels[i].args[j] )];
            }
            adr = fact_adress();
            
            lneg[lp][adr] = 1;
            if ( lpos[lp][adr] &&
                !luse[lp][adr] )
	    {
              luse[lp][adr] = 1;
              lindex[lp][adr] = gnum_relevant_facts;
              if ( gnum_relevant_facts == MAX_RELEVANT_FACTS ) {
                printf("\nincrease MAX_RELEVANT_FACTS! (current value: %d)\n\n",
                       MAX_RELEVANT_FACTS);
                exit( 1 );
              }
              grelevant_facts[gnum_relevant_facts].predicate = lp;
              for ( j = 0; j < garity[lp]; j++ ) {
                grelevant_facts[gnum_relevant_facts].args[j] = largs[j];
              }
              lindex[lp][adr] = gnum_relevant_facts;
              gnum_relevant_facts++;
            }
          }
        }
      }
      
      
      for ( ne = no->effects; ne; ne = ne->next ) {
        for ( i = 0; i < ne->num_dels; i++ ) {
          lp = ne->dels[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = ( ne->dels[i].args[j] >= 0 ) ?
            ne->dels[i].args[j] : a->inst_table[DECODE_VAR( ne->dels[i].args[j] )];
          }
          adr = fact_adress();
          
          lneg[lp][adr] = 1;
          if ( lpos[lp][adr] &&
              !luse[lp][adr] ) {
            luse[lp][adr] = 1;
            lindex[lp][adr] = gnum_relevant_facts;
            if ( gnum_relevant_facts == MAX_RELEVANT_FACTS ) {
              printf("\nincrease MAX_RELEVANT_FACTS! (current value: %d)\n\n",
                     MAX_RELEVANT_FACTS);
              exit( 1 );
            }
            grelevant_facts[gnum_relevant_facts].predicate = lp;
            for ( j = 0; j < garity[lp]; j++ ) {
              grelevant_facts[gnum_relevant_facts].args[j] = largs[j];
            }
            lindex[lp][adr] = gnum_relevant_facts;
            gnum_relevant_facts++;
          }
        }
      }
      
      
    } else {
      pa = a->pseudo_action;
      
      for ( pae = pa->effects; pae; pae = pae->next ) {
        for ( i = 0; i < pae->num_dels; i++ ) {
          lp = pae->dels[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = pae->dels[i].args[j];
          }
          adr = fact_adress();
          
          lneg[lp][adr] = 1;
          if ( lpos[lp][adr] &&
              !luse[lp][adr] ) {
            luse[lp][adr] = 1;
            lindex[lp][adr] = gnum_relevant_facts;
            if ( gnum_relevant_facts == MAX_RELEVANT_FACTS ) {
              printf("\nincrease MAX_RELEVANT_FACTS! (current value: %d)\n\n",
                     MAX_RELEVANT_FACTS);
              exit( 1 );
            }
            grelevant_facts[gnum_relevant_facts].predicate = lp;
            for ( j = 0; j < garity[lp]; j++ ) {
              grelevant_facts[gnum_relevant_facts].args[j] = largs[j];
            }
            lindex[lp][adr] = gnum_relevant_facts;
            gnum_relevant_facts++;
          }
        }
      }
    }
  }
  
  if ( gcmd_line.display_info == 119 ) {
    printf("\n\nfacts selected as relevant:\n\n");
    for ( i = 0; i < gnum_relevant_facts; i++ ) {
      printf("\n%d: ", i);
      print_Fact( &(grelevant_facts[i]) );
    }
  }
  
  lnum_effects = 0;
  
  /* first make place for initial and goal states.
   * (one artificial fact might still be added here)
   */
  make_state( &ggoal_state, gnum_relevant_facts + 1 );
  ggoal_state.max_F = gnum_relevant_facts + 1;
  make_state( &ginitial_state, gnum_relevant_facts + 1 );
  ginitial_state.max_F = gnum_relevant_facts + 1;
  
  create_final_goal_state();
  create_final_initial_state();
  create_final_actions();
  
  if ( gcmd_line.display_info == 120 ) {
    printf("\n\nfinal domain representation is:\n\n");  
    for ( i = 0; i < gnum_operators; i++ ) {
      printf("\n\n------------------operator %s-----------\n\n", goperators[i]->name);
      for ( a = gactions; a; a = a->next ) {
        if ( ( !a->norm_operator &&
              !a->pseudo_action ) ||
            ( a->norm_operator && 
             a->norm_operator->operatore != goperators[i] ) ||
            ( a->pseudo_action &&
             a->pseudo_action->operatore != goperators[i] ) ) {
          continue;
        }
        print_Action( a );
      }
    }
    printf("\n\n--------------------GOAL REACHED ops-----------\n\n");
    for ( a = gactions; a; a = a->next ) {
      if ( !a->norm_operator &&
          !a->pseudo_action ) {
        print_Action( a );
      }
    }
    
    printf("\n\nfinal initial state is:\n\n");
    for ( i = 0; i < ginitial_state.num_F; i++ ) {
      print_ft_name( ginitial_state.F[i] );
      printf("\n");
    }
    printf("\n\nfinal goal state is:\n\n");
    for ( i = 0; i < ggoal_state.num_F; i++ ) {
      print_ft_name( ggoal_state.F[i] );
      printf("\n");
    }
  }
  
}



void create_final_goal_state( void )

{
  
  WffNode *w, *ww;
  int m, i, adr;
  Action *tmp;
  
  set_relevants_in_wff( &ggoal );
  cleanup_wff( &ggoal );
  if ( ggoal->connective == TRU ) {
    printf("\nff: goal can be simplified to TRUE. The empty plan solves it\n\n");
    exit( 1 );
  }
  if ( ggoal->connective == FAL ) {
    printf("\nff: goal can be simplified to FALSE. No plan will solve it\n\n");
    exit( 1 );
  }
  
  switch ( ggoal->connective ) {
    case OR:
      if ( gnum_relevant_facts == MAX_RELEVANT_FACTS ) {
        printf("\nincrease MAX_RELEVANT_FACTS! (current value: %d)\n\n",
               MAX_RELEVANT_FACTS);
        exit( 1 );
      }
      grelevant_facts[gnum_relevant_facts].predicate = -3;
      gnum_relevant_facts++;
      for ( w = ggoal->sons; w; w = w->next ) {
        tmp = new_Action();
        if ( w->connective == AND ) {
          m = 0;
          for ( ww = w->sons; ww; ww = ww->next ) m++;
          tmp->preconds = ( int * ) calloc( m, sizeof( int ) );
          tmp->num_preconds = 0;
          for ( ww = w->sons; ww; ww = ww->next ) {
            lp = ww->fact->predicate;
            for ( i = 0; i < garity[lp]; i++ ) {
              largs[i] = ww->fact->args[i];
            }
            adr = fact_adress();
            tmp->preconds[tmp->num_preconds++] = lindex[lp][adr];
          }
        } else {
          tmp->preconds = ( int * ) calloc( 1, sizeof( int ) );
          tmp->num_preconds = 1;
          lp = w->fact->predicate;
          for ( i = 0; i < garity[lp]; i++ ) {
            largs[i] = w->fact->args[i];
          }
          adr = fact_adress();
          tmp->preconds[0] = lindex[lp][adr];
        }
        tmp->effects = ( ActionEffect * ) calloc( 1, sizeof( ActionEffect ) );
        tmp->num_effects = 1;
        tmp->effects[0].conditions = NULL;
        tmp->effects[0].num_conditions = 0;
        tmp->effects[0].dels = NULL;
        tmp->effects[0].num_dels = 0;
        tmp->effects[0].adds = ( int * ) calloc( 1, sizeof( int ) );
        tmp->effects[0].adds[0] = gnum_relevant_facts - 1;
        tmp->effects[0].num_adds = 1;
        tmp->next = gactions;
        gactions = tmp;
        gnum_actions++;
        lnum_effects++;
      }
      ggoal_state.F[0] = gnum_relevant_facts - 1;
      ggoal_state.num_F = 1;
      break;
      case AND:
      for ( w = ggoal->sons; w; w = w->next ) {
        lp = w->fact->predicate;
        for ( i = 0; i < garity[lp]; i++ ) {
          largs[i] = w->fact->args[i];
        }
        adr = fact_adress();
        ggoal_state.F[ggoal_state.num_F++] = lindex[lp][adr];
      }
      break;
      case ATOM:
      ggoal_state.num_F = 1;
      lp = ggoal->fact->predicate;
      for ( i = 0; i < garity[lp]; i++ ) {
        largs[i] = ggoal->fact->args[i];
      }
      adr = fact_adress();
      ggoal_state.F[0] = lindex[lp][adr];
      break;
      default:
      printf("\n\nwon't get here: non ATOM,AND,OR in fully simplified goal\n\n");
      exit( 1 );
  }
  
}



void set_relevants_in_wff( WffNode **w )

{
  
  WffNode *i;
  int j, adr;
  
  switch ( (*w)->connective ) {
    case AND:
    case OR:
      for ( i = (*w)->sons; i; i = i->next ) {
        set_relevants_in_wff( &i );
      }
      break;
      case ATOM:
      /* no equalities, as fully instantiated
       */
      lp = (*w)->fact->predicate;
      for ( j = 0; j < garity[lp]; j++ ) {
        largs[j] = (*w)->fact->args[j];
      }
      adr = fact_adress();
      
      if ( !lneg[lp][adr] ) {
        (*w)->connective = TRU;
        free( (*w)->fact );
        (*w)->fact = NULL;
        break;
      }
      if ( !lpos[lp][adr] ) {
        (*w)->connective = FAL;
        free( (*w)->fact );
        (*w)->fact = NULL;
        break;
      }
      break;
      default:
      printf("\n\nwon't get here: non NOT,OR,AND in goal set relevants\n\n");
      exit( 1 );
  }
  
}



void create_final_initial_state( void )

{
  
  Facts *f;
  int i, adr;
  
  for ( f = ginitial; f; f = f->next ) {
    lp = f->fact->predicate;
    for ( i = 0; i < garity[lp]; i++ ) {
      largs[i] = f->fact->args[i];
    }
    adr = fact_adress();
    
    if ( !lneg[lp][adr] ) {/* non deleted ini */
      continue;
    }
    
    ginitial_state.F[ginitial_state.num_F++] = lindex[lp][adr];
  }
  
}


/* counts branches, will be useful later
 */
int lnum_branches;


void create_final_actions( void )

{
  
  Action *a, *p, *t;
  NormOperator *no;
  NormEffect *ne;
  NormEffect * nb;
  int i, j, adr;
  PseudoAction *pa;
  PseudoActionEffect *pae;
  
  a = gactions; p = NULL;
  lnum_branches = 0;
  while ( a ) {
    if ( a->norm_operator ) {
      /* action comes from an easy template NormOp
       */
      no = a->norm_operator;
      
      if ( no->num_preconds > 0 ) {
        a->preconds = ( int * ) calloc( no->num_preconds, sizeof( int ) );
      }
      a->num_preconds = 0;
      for ( i = 0; i < no->num_preconds; i++ ) {
        lp = no->preconds[i].predicate;
        for ( j = 0; j < garity[lp]; j++ ) {
          largs[j] = ( no->preconds[i].args[j] >= 0 ) ?
          no->preconds[i].args[j] : a->inst_table[DECODE_VAR( no->preconds[i].args[j] )];
        }
        adr = fact_adress();
        
        /* preconds are lpos in all cases due to reachability analysis
         */
        if ( !lneg[lp][adr] ) {
          continue;
        }
        
        a->preconds[a->num_preconds++] = lindex[lp][adr];
      }
      
      if ( a->num_effects > 0 ) {
        a->effects = ( ActionEffect * ) calloc( a->num_effects, sizeof( ActionEffect ) );
      }
      a->num_effects = 0;
      for ( ne = no->effects; ne; ne = ne->next ) {
        if ( ne->num_conditions > 0 ) {
          a->effects[a->num_effects].conditions =
          ( int * ) calloc( ne->num_conditions, sizeof( int ) );
        }
        a->effects[a->num_effects].num_conditions = 0;
        
        for ( i = 0; i < ne->num_conditions; i++ ) {
          lp = ne->conditions[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = ( ne->conditions[i].args[j] >= 0 ) ?
            ne->conditions[i].args[j] : a->inst_table[DECODE_VAR( ne->conditions[i].args[j] )];
          }
          adr = fact_adress();
          if ( !lpos[lp][adr] ) {/* condition not reachable: skip effect */
            break;
          }
          if ( !lneg[lp][adr] ) {/* condition always true: skip it */
            continue;
          }
          a->effects[a->num_effects].conditions[a->effects[a->num_effects].num_conditions++] =
          lindex[lp][adr];
        }
        
        if ( i < ne->num_conditions ) {/* found unreachable condition: free condition space */
          free( a->effects[a->num_effects].conditions );
          continue;
        }
        
        /* now create the add and del effects.
         */
        if ( ne->num_adds > 0 ) {
          a->effects[a->num_effects].adds = ( int * ) calloc( ne->num_adds, sizeof( int ) );
        }
        a->effects[a->num_effects].num_adds = 0;
        for ( i = 0; i < ne->num_adds; i++ ) {
          lp = ne->adds[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = ( ne->adds[i].args[j] >= 0 ) ?
            ne->adds[i].args[j] : a->inst_table[DECODE_VAR( ne->adds[i].args[j] )];
          }
          adr = fact_adress();
          
          if ( !lneg[lp][adr] ) {/* effect always true: skip it */
            continue;
          }
          
          a->effects[a->num_effects].adds[a->effects[a->num_effects].num_adds++] = lindex[lp][adr];
        }
        
        if ( ne->num_dels > 0 ) {
          a->effects[a->num_effects].dels = ( int * ) calloc( ne->num_dels, sizeof( int ) );
        }
        a->effects[a->num_effects].num_dels = 0;
        for ( i = 0; i < ne->num_dels; i++ ) {
          lp = ne->dels[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = ( ne->dels[i].args[j] >= 0 ) ?
            ne->dels[i].args[j] : a->inst_table[DECODE_VAR( ne->dels[i].args[j] )];
          }
          adr = fact_adress();
          
          if ( !lpos[lp][adr] ) {/* effect always false: skip it */
            continue;
          }
          
          a->effects[a->num_effects].dels[a->effects[a->num_effects].num_dels++] = lindex[lp][adr];
        }
        if ( i < ne->num_dels ) break;
        
        /* this effect is OK. go to next one in NormOp.
         */
        a->num_effects++;
        lnum_effects++;
      }
      /* and finally the branches */
      if ( no->operatore->is_obs ) {
        if ( a->num_branches > 0 ) 
          a->branch = ( ActionEffect * ) calloc( a->num_branches, sizeof( ActionEffect ) );
        
        a->num_branches = 0;
        for ( nb = no->branches; nb; nb = nb->next ) {
          if ( nb->num_conditions > 0 ) {
            a->branch[a->num_branches].conditions =
            ( int * ) calloc( nb->num_conditions, sizeof( int ) );
          }
          a->branch[a->num_branches].num_conditions = 0;
          
          for ( i = 0; i < nb->num_conditions; i++ ) {
            lp = nb->conditions[i].predicate;
            for ( j = 0; j < garity[lp]; j++ ) {
              largs[j] = ( nb->conditions[i].args[j] >= 0 ) ?
              nb->conditions[i].args[j] : a->inst_table[DECODE_VAR( nb->conditions[i].args[j] )];
            }
            adr = fact_adress();
            if ( !lpos[lp][adr] ) {/* condition not reachable: skip effect */
              break;
            }
            if ( !lneg[lp][adr] ) {/* condition always true: skip it */
              continue;
            }
            a->branch[a->num_branches].conditions[a->branch[a->num_branches].num_conditions++] =
            lindex[lp][adr];
          }
          
          if ( i < nb->num_conditions ) {/* found unreachable condition: free condition space */
            free( a->branch[a->num_branches].conditions );
            continue;
          }
          
          /* now create the add and del of the observations.
           */
          if ( nb->num_adds > 0 ) {
            a->branch[a->num_branches].adds = ( int * ) calloc( nb->num_adds, sizeof( int ) );
          }
          a->branch[a->num_branches].num_adds = 0;
          for ( i = 0; i < nb->num_adds; i++ ) {
            lp = nb->adds[i].predicate;
            for ( j = 0; j < garity[lp]; j++ ) {
              largs[j] = ( nb->adds[i].args[j] >= 0 ) ?
              nb->adds[i].args[j] : a->inst_table[DECODE_VAR( nb->adds[i].args[j] )];
            }
            adr = fact_adress();
            
            if ( !lneg[lp][adr] ) {/* effect always true: skip it */
              continue;
            }
            
            a->branch[a->num_branches].adds[a->branch[a->num_branches].num_adds++] = lindex[lp][adr];
          }
          
          if ( nb->num_dels > 0 ) {
            a->branch[a->num_branches].dels = ( int * ) calloc( nb->num_dels, sizeof( int ) );
          }
          a->branch[a->num_branches].num_dels = 0;
          for ( i = 0; i < nb->num_dels; i++ ) {
            lp = nb->dels[i].predicate;
            for ( j = 0; j < garity[lp]; j++ ) {
              largs[j] = ( nb->dels[i].args[j] >= 0 ) ?
              nb->dels[i].args[j] : a->inst_table[DECODE_VAR( nb->dels[i].args[j] )];
            }
            adr = fact_adress();
            
            if ( !lpos[lp][adr] ) {/* effect always false: skip it */
              continue;
            }
            
            a->branch[a->num_branches].dels[a->branch[a->num_branches].num_dels++] = lindex[lp][adr];
          }
          if ( i < nb->num_dels ) break;
          
          /* this effect is OK. go to next one in NormOp.
           */
          a->num_branches++;
          lnum_branches++; 
        }
      }
      if ( ne ) {
        /* we get here if one effect was faulty
         */
        if ( p ) {
          p->next = a->next;
          t = a;
          a = a->next;
          free_single_Action( t );
        } else {
          gactions = a->next;
          t = a;
          a = a->next;
          free_single_Action( t );
        }
      } else {
        p = a;
        a = a->next;
      }
      continue;
    }
    if ( a->pseudo_action ) {
      /* action is result of a PseudoAction
       * an observation should not be a pseudo_action.
       */
      pa = a->pseudo_action;
      
      if ( pa->num_preconds > 0 ) {
        a->preconds = ( int * ) calloc( pa->num_preconds, sizeof( int ) );
      }
      a->num_preconds = 0;
      for ( i = 0; i < pa->num_preconds; i++ ) {
        lp = pa->preconds[i].predicate;
        for ( j = 0; j < garity[lp]; j++ ) {
          largs[j] = pa->preconds[i].args[j];
        }
        adr = fact_adress();
        
        /* preconds are lpos in all cases due to reachability analysis
         */
        if ( !lneg[lp][adr] ) {
          continue;
        }
        
        a->preconds[a->num_preconds++] = lindex[lp][adr];
      }
      
      if ( a->num_effects > 0 ) {
        a->effects = ( ActionEffect * ) calloc( a->num_effects, sizeof( ActionEffect ) );
      }
      a->num_effects = 0;
      for ( pae = pa->effects; pae; pae = pae->next ) {
        if ( pae->num_conditions > 0 ) {
          a->effects[a->num_effects].conditions =
          ( int * ) calloc( pae->num_conditions, sizeof( int ) );
        }
        a->effects[a->num_effects].num_conditions = 0;
        
        for ( i = 0; i < pae->num_conditions; i++ ) {
          lp = pae->conditions[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = pae->conditions[i].args[j];
          }
          adr = fact_adress();
          
          if ( !lpos[lp][adr] ) {/* condition not reachable: skip effect */
            break;
          }
          if ( !lneg[lp][adr] ) {/* condition always true: skip it */
            continue;
          }
          
          a->effects[a->num_effects].conditions[a->effects[a->num_effects].num_conditions++] =
          lindex[lp][adr];
        }
        
        if ( i < pae->num_conditions ) {/* found unreachable condition: free condition space */
          free( a->effects[a->num_effects].conditions );
          continue;
        }
        
        /* now create the add and del effects.
         */
        if ( pae->num_adds > 0 ) {
          a->effects[a->num_effects].adds = ( int * ) calloc( pae->num_adds, sizeof( int ) );
        }
        a->effects[a->num_effects].num_adds = 0;
        for ( i = 0; i < pae->num_adds; i++ ) {
          lp = pae->adds[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = pae->adds[i].args[j];
          }
          adr = fact_adress();
          
          if ( !lneg[lp][adr] ) {/* effect always true: skip it */
            continue;
          }
          
          a->effects[a->num_effects].adds[a->effects[a->num_effects].num_adds++] = lindex[lp][adr];
        }
        
        if ( pae->num_dels > 0 ) {
          a->effects[a->num_effects].dels = ( int * ) calloc( pae->num_dels, sizeof( int ) );
        }
        a->effects[a->num_effects].num_dels = 0;
        for ( i = 0; i < pae->num_dels; i++ ) {
          lp = pae->dels[i].predicate;
          for ( j = 0; j < garity[lp]; j++ ) {
            largs[j] = pae->dels[i].args[j];
          }
          adr = fact_adress();
          
          if ( !lpos[lp][adr] ) {/* effect always false: skip it */
            continue;
          }
          
          a->effects[a->num_effects].dels[a->effects[a->num_effects].num_dels++] = lindex[lp][adr];
        }
        if ( i < pae->num_dels ) break;
        
        /* this effect is OK. go to next one in PseudoAction.
         */
        a->num_effects++;
        lnum_effects++;
      }
      if ( pae ) {
        /* we get here if one effect was faulty
         */
        if ( p ) {
          p->next = a->next;
          t = a;
          a = a->next;
          free_single_Action( t );
        } else {
          gactions = a->next;
          t = a;
          a = a->next;
          free_single_Action( t );
        }
      } else {
        p = a;
        a = a->next;
      }
      continue;
    }/* end of if clause for PseudoAction */
    /* if action was neither normop, nor pseudo action determined,
     * then it is an artificial action due to disjunctive goal
     * conditions.
     *
     * these are already in final form.
     */
    p = a;
    a = a->next;
  }/* endfor all actions ! */
  
}















/**************************************************
 * CONNECTIVITY GRAPH. ULTRA CLEAN REPRESENTATION *
 **************************************************/













void build_connectivity_graph( void )

{
  
  int i, j, k, l, n_op, n_ef, n_br, na, nd, ef, ef_,  br, br_, m, l_;
  Action *a;
  int *same_effects, sn;
  Bool *had_effects;
  int *same_branch;
  Bool *had_branch;
  ActionEffect *e, *e_, *e__;
  Operator *oop;
  
  struct timeb tp;
  
  ftime( &tp );
  srandom( tp.millitm );
  
  gnum_ft_conn = gnum_relevant_facts;
  gnum_op_conn = gnum_actions;
  gft_conn = ( FtConn * ) malloc( gnum_ft_conn * sizeof( FtConn ) );
  gop_conn = ( OpConn * ) malloc( gnum_op_conn * sizeof( OpConn ) );
  gef_conn = ( EfConn * ) malloc( lnum_effects * sizeof( EfConn ) );
  gbr_conn = ( EfConn * ) malloc( lnum_branches * sizeof( EfConn ) );
  gnum_ef_conn = 0;
  gnum_br_conn = 0;

  same_effects = ( int * ) calloc( lnum_effects, sizeof( int ) );
  had_effects = ( Bool * ) calloc( lnum_effects, sizeof( Bool ) );
  same_branch = ( int * ) calloc( lnum_branches, sizeof( int ) );
  had_branch = ( Bool * ) calloc( lnum_branches, sizeof( Bool ) );
  
  for ( i = 0; i < gnum_ft_conn; i++ ) {
    gft_conn[i].num_PC = 0;
    gft_conn[i].num_A = 0;
    gft_conn[i].num_D = 0;
    
    gft_conn[i].rand = random() % BIG_INT;
  }
  
  for ( i = 0; i < gnum_op_conn; i++ ) {
    gop_conn[i].num_E = 0;
    gop_conn[i].num_B = 0;
  }
  
  for ( i = 0; i < lnum_effects; i++ ) {
    gef_conn[i].num_PC = 0;
    gef_conn[i].num_A = 0;
    gef_conn[i].num_D = 0;
    gef_conn[i].num_I = 0;
    
    gef_conn[i].removed = FALSE;
  }
  
  for ( i = 0; i < lnum_branches; i++ ) {
    gbr_conn[i].num_PC = 0;
    gbr_conn[i].num_A = 0;
    gbr_conn[i].num_D = 0;
    gbr_conn[i].num_I = 0;
    gbr_conn[i].removed = FALSE;
    
  }
  
  n_op = 0;
  n_ef = 0;  
  n_br = 0;
  for ( a = gactions; a; a = a->next ) {
    
    gop_conn[n_op].action = a;
    
    gop_conn[n_op].E = ( int * ) calloc( a->num_effects, sizeof( int ) );
    for ( i = 0; i < a->num_effects; i++ ) {
      had_effects[i] = FALSE;
    }
    for ( i = 0; i < a->num_branches; i++ ) {
      had_branch[i] = FALSE;
    }
    
    for ( i = 0; i < a->num_effects; i++ ) {
      if ( had_effects[i] ) {
        continue;
      }
      had_effects[i] = TRUE;
      e = &(a->effects[i]);
      gop_conn[n_op].E[gop_conn[n_op].num_E++] = n_ef;
      gef_conn[n_ef].op = n_op;
      
      gef_conn[n_ef].PC = ( int * ) 
      calloc( e->num_conditions + a->num_preconds, sizeof( int ) );
      /* Takes in accound only effects with at least 2 conditional effects */
      for ( j = 0; j < a->num_preconds; j++ ) {
        for ( k = 0; k < gef_conn[n_ef].num_PC; k++ ) {
          if ( gef_conn[n_ef].PC[k] == a->preconds[j] ) break;
        }
        if ( k < gef_conn[n_ef].num_PC ) continue;
        gef_conn[n_ef].PC[gef_conn[n_ef].num_PC++] = a->preconds[j];
      }
      for ( j = 0; j < e->num_conditions; j++ ) {
        for ( k = 0; k < gef_conn[n_ef].num_PC; k++ ) {
          if ( gef_conn[n_ef].PC[k] == e->conditions[j] ) break;
        }
        if ( k < gef_conn[n_ef].num_PC ) continue;
        gef_conn[n_ef].PC[gef_conn[n_ef].num_PC++] = e->conditions[j];
      }
      
      sn = 0;
      for ( j = i + 1; j < a->num_effects; j++ ) {
        if ( had_effects[j] ) {
          continue;
        }
        e_ = &(a->effects[j]);
        /* check conditions
         */
        for ( k = 0; k < e_->num_conditions; k++ ) {
          for ( l = 0; l < e->num_conditions; l++ ) {
            if ( e_->conditions[k] == e->conditions[l] ) {
              break;
            }
          }
          if ( l == e->num_conditions ) {
            break;
          }
        }
        if ( k < e_->num_conditions ) {
          continue;
        }
        if ( e->num_conditions == e_->num_conditions ) {
          same_effects[sn++] = j;
        }
      }
      
      na = e->num_adds;
      nd = e->num_dels;
      for ( j = 0; j < sn; j++ ) {
        na += a->effects[same_effects[j]].num_adds;
        nd += a->effects[same_effects[j]].num_dels;
      }
      gef_conn[n_ef].A = ( int * ) calloc( na, sizeof( int ) );
      gef_conn[n_ef].D = ( int * ) calloc( nd, sizeof( int ) );
      for ( j = 0; j < e->num_adds; j++ ) {
        for ( k = 0; k < gef_conn[n_ef].num_A; k++ ) {
          if ( gef_conn[n_ef].A[k] == e->adds[j] ) break;
        }
        if ( k < gef_conn[n_ef].num_A ) continue;
        /* exclude already true adds
         */
        for ( k = 0; k < gef_conn[n_ef].num_PC; k++ ) {
          if ( gef_conn[n_ef].PC[k] == e->adds[j] ) break;
        }
        if ( k < gef_conn[n_ef].num_PC ) continue;
        gef_conn[n_ef].A[gef_conn[n_ef].num_A++] = e->adds[j];
      }
      for ( j = 0; j < e->num_dels; j++ ) {
        for ( k = 0; k < gef_conn[n_ef].num_D; k++ ) {
          if ( gef_conn[n_ef].D[k] == e->dels[j] ) break;
        }
        if ( k < gef_conn[n_ef].num_D ) continue;
        /* exclude re-added dels; check against *all*
         * adds to be integrated.
         */
        for ( k = 0; k < e->num_adds; k++ ) {
          if ( e->adds[k] == e->dels[j] ) break;
        }
        if ( k < e->num_adds ) continue;
        for ( l = 0; l < sn; l++ ) {
          e_ = &(a->effects[same_effects[l]]);
          for ( k = 0; k < e_->num_adds; k++ ) {
            if ( e_->adds[k] == e->dels[j] ) break;
          }
          if ( k < e_->num_adds ) break;
        }
        if ( l < sn ) continue;
        gef_conn[n_ef].D[gef_conn[n_ef].num_D++] = e->dels[j];
      }
      for ( j = 0; j < sn; j++ ) {
        e_ = &(a->effects[same_effects[j]]);
        for ( l = 0; l < e_->num_adds; l++ ) {
          for ( k = 0; k < gef_conn[n_ef].num_A; k++ ) {
            if ( gef_conn[n_ef].A[k] == e_->adds[l] ) break;
          }
          if ( k < gef_conn[n_ef].num_A ) continue;
          for ( k = 0; k < gef_conn[n_ef].num_PC; k++ ) {
            if ( gef_conn[n_ef].PC[k] == e_->adds[l] ) break;
          }
          if ( k < gef_conn[n_ef].num_PC ) continue;
          gef_conn[n_ef].A[gef_conn[n_ef].num_A++] = e_->adds[l];
        }
        for ( l = 0; l < e_->num_dels; l++ ) {
          for ( k = 0; k < gef_conn[n_ef].num_D; k++ ) {
            if ( gef_conn[n_ef].D[k] == e_->dels[l] ) break;
          }
          if ( k < gef_conn[n_ef].num_D ) continue;
          /* exclude re-added dels; check against *all*
           * adds to be integrated.
           */
          for ( k = 0; k < e->num_adds; k++ ) {
            if ( e->adds[k] == e_->dels[l] ) break;
          }
          if ( k < e->num_adds ) continue;
          for ( l_ = 0; l_ < sn; l_++ ) {
            e__ = &(a->effects[same_effects[l_]]);
            for ( k = 0; k < e__->num_adds; k++ ) {
              if ( e__->adds[k] == e_->dels[l] ) break;
            }
            if ( k < e__->num_adds ) break;
          }
          if ( l_ < sn ) continue;
          gef_conn[n_ef].D[gef_conn[n_ef].num_D++] = e_->dels[l];
        }
      }
      for ( j = 0; j < sn; j++ ) {
        had_effects[same_effects[j]] = TRUE;
      }
      
      n_ef++;
      gnum_ef_conn++;
    }/* ende all a->effects */
    
    /** VERIFY ef */
    oop = a->norm_operator->operatore;
    
    if((oop->is_obs) && ( a->num_branches > 0)) { /* TODO: da levare questo if */
      
      gop_conn[n_op].B = ( int * ) calloc( a->num_branches, sizeof( int ) );
      
      for ( i = 0; i < a->num_branches; i++ ) {
        if ( had_branch[i] ) {
          continue;
        }
        had_branch[i] = TRUE;
        
        e = &(a->branch[i]);
        gop_conn[n_op].B[gop_conn[n_op].num_B++] = n_br;
        gbr_conn[n_br].op = n_op;
        
        gbr_conn[n_br].PC = ( int * ) 
        calloc( e->num_conditions + a->num_preconds, sizeof( int ) );
        
        for ( j = 0; j < a->num_preconds; j++ ) {
          for ( k = 0; k < gbr_conn[n_br].num_PC; k++ ) {
            if ( gbr_conn[n_br].PC[k] == a->preconds[j] ) break;
          }
          if ( k < gbr_conn[n_br].num_PC )
            continue;
          gbr_conn[n_br].PC[gbr_conn[n_br].num_PC++] = a->preconds[j];
        }
        for ( j = 0; j < e->num_conditions; j++ ) {
          for ( k = 0; k < gbr_conn[n_br].num_PC; k++ ) {
            if ( gbr_conn[n_br].PC[k] == e->conditions[j] ) break;
          }
          if ( k < gbr_conn[n_br].num_PC )
            continue;
          gbr_conn[n_br].PC[gbr_conn[n_br].num_PC++] = e->conditions[j];
        }
        
        /* For the branches, we do not verify (as for the effects)
         * that they are the same if they have the same precondition:
         * the branches are a disjunction, so they are always considered
         * as indipendent.
         */
        
        na = e->num_adds;
        nd = e->num_dels;
        
        gbr_conn[n_br].A = ( int * ) calloc( na, sizeof( int ) );
        gbr_conn[n_br].D = ( int * ) calloc( nd, sizeof( int ) );
        for ( j = 0; j < e->num_adds; j++ ) {
          for ( k = 0; k < gbr_conn[n_br].num_A; k++ ) {
            if ( gbr_conn[n_br].A[k] == e->adds[j] ) break;
          }
          if ( k < gbr_conn[n_br].num_A ) continue;
          /* exclude already true adds
           */
          for ( k = 0; k < gbr_conn[n_br].num_PC; k++ ) {
            if ( gbr_conn[n_br].PC[k] == e->adds[j] ) break;
          }
          if ( k < gbr_conn[n_br].num_PC )
            continue;
          gbr_conn[n_br].A[gbr_conn[n_br].num_A++] = e->adds[j];
        }
        for ( j = 0; j < e->num_dels; j++ ) {
          for ( k = 0; k < gbr_conn[n_br].num_D; k++ ) {
            if ( gbr_conn[n_br].D[k] == e->dels[j] ) break;
          }
          if ( k < gbr_conn[n_br].num_D ) continue;
          /* exclude re-added dels; check against *all*
           * adds to be integrated.
           */
          for ( k = 0; k < e->num_adds; k++ ) {
            if ( e->adds[k] == e->dels[j] ) break;
          }
          if ( k < e->num_adds ) continue;
          if ( l < sn ) continue;
          gbr_conn[n_br].D[gbr_conn[n_br].num_D++] = e->dels[j];
        }
        
        n_br++;
        gnum_br_conn++;
      }
    } /* end of branches */
    
    if ( gop_conn[n_op].num_E >= 1 ) {
      /* CHECK EMPTY EFFECTS!
       *
       * two step process --- first, remove all effects that are entirely empty.
       *                      second, check if all remaining effects are illegal
       *                      or only delete:
       *                      in that case, the op will never do any good so we 
       *                      remove all its effects.
       */
      i = 0;
      while ( i < gop_conn[n_op].num_E ) {
        if ( gef_conn[gop_conn[n_op].E[i]].num_A != 0 ||
            gef_conn[gop_conn[n_op].E[i]].num_D != 0 ) {
          i++;
          continue;
        }
        /* we keep it in the gef_conn (seems easier), 
         * but mark it as removed, which will exclude it from everything.
         */
        gef_conn[gop_conn[n_op].E[i]].removed = TRUE;
        for ( j = i; j < gop_conn[n_op].num_E - 1; j++ ) {
          gop_conn[n_op].E[j] = gop_conn[n_op].E[j+1];
        }
        gop_conn[n_op].num_E--;
      }
      
      m = 0;
      for ( i = 0; i < gop_conn[n_op].num_E; i++ ) {
        if ( gef_conn[gop_conn[n_op].E[i]].num_A == 0 ) {
          m++;
        }
      }
      if ( m == gop_conn[n_op].num_E ) {
        /* all remaining effects solely-deleters.
         */
        for ( i = 0; i < gop_conn[n_op].num_E; i++ ) {
          gef_conn[gop_conn[n_op].E[i]].removed = TRUE;
        }
        gop_conn[n_op].num_E = 0;
      }
    }
    /* NOT SURE OF THAT */
    if ( gop_conn[n_op].num_B >= 1 ) {
      /* CHECK EMPTY BRANCHES!
       *
       * one step process --- first, remove all branches that are entirely empty.
       */
      i = 0;
      while ( i < gop_conn[n_op].num_B ) {
        if ( gbr_conn[gop_conn[n_op].B[i]].num_A != 0 ||
            gbr_conn[gop_conn[n_op].B[i]].num_D != 0 ) {
          i++;
          continue;
        }
        /* we keep it in the gef_conn (seems easier), 
         * but mark it as removed, which will exclude it from everything.
         */
        gbr_conn[gop_conn[n_op].B[i]].removed = TRUE;
        for ( j = i; j < gop_conn[n_op].num_B - 1; j++ ) {
          gop_conn[n_op].B[j] = gop_conn[n_op].B[j+1];
        }
        gop_conn[n_op].num_B--;
      }
    }
    
    /* setup implied effects info
     */
    if ( gop_conn[n_op].num_E > 1 ) {
      for ( i = 0; i < gop_conn[n_op].num_E; i++ ) {
        ef = gop_conn[n_op].E[i];
        gef_conn[ef].I = ( int * ) calloc( gop_conn[n_op].num_E, sizeof( int ) );
        gef_conn[ef].num_I = 0;
      }    
      for ( i = 0; i < gop_conn[n_op].num_E - 1; i++ ) {
        ef = gop_conn[n_op].E[i];
        for ( j = i+1; j < gop_conn[n_op].num_E; j++ ) {
          ef_ = gop_conn[n_op].E[j];
          /* ef ==> ef_ ? */
          for ( k = 0; k < gef_conn[ef_].num_PC; k++ ) {
            for ( l = 0; l < gef_conn[ef].num_PC; l++ ) {
              if ( gef_conn[ef].PC[l] == gef_conn[ef_].PC[k] ) break;
            }
            if ( l == gef_conn[ef].num_PC ) break;
          }
          if ( k == gef_conn[ef_].num_PC ) {
            gef_conn[ef].I[gef_conn[ef].num_I++] = ef_;
          }
          /* j ==> i ? */
          for ( k = 0; k < gef_conn[ef].num_PC; k++ ) {
            for ( l = 0; l < gef_conn[ef_].num_PC; l++ ) {
              if ( gef_conn[ef_].PC[l] == gef_conn[ef].PC[k] ) break;
            }
            if ( l == gef_conn[ef_].num_PC ) break;
          }
          if ( k == gef_conn[ef].num_PC ) {
            gef_conn[ef_].I[gef_conn[ef_].num_I++] = ef;
          }
        }
      }
    }
    
    /* setup implied branches info
     */
    if ( gop_conn[n_op].num_B > 1 ) {
      
      for ( i = 0; i < gop_conn[n_op].num_B; i++ ) {
        br = gop_conn[n_op].B[i];
        gbr_conn[br].I = ( int * ) calloc( gop_conn[n_op].num_B, sizeof( int ) );
        gbr_conn[br].num_I = 0;
      }    
      for ( i = 0; i < gop_conn[n_op].num_B - 1; i++ ) {
        br = gop_conn[n_op].B[i];
        for ( j = i+1; j < gop_conn[n_op].num_B; j++ ) {
          br_ = gop_conn[n_op].B[j];
          /* br ==> br_ ? */
          for ( k = 0; k < gbr_conn[br_].num_PC; k++ ) {
            for ( l = 0; l < gbr_conn[br].num_PC; l++ ) {
              if ( gbr_conn[br].PC[l] == gbr_conn[br_].PC[k] ) break;
            }
            if ( l == gbr_conn[br].num_PC ) break;
          }
          if ( k == gbr_conn[br_].num_PC ) {
            gbr_conn[br].I[gbr_conn[br].num_I++] = br_;
          }
          /* j ==> i ? */
          for ( k = 0; k < gbr_conn[br].num_PC; k++ ) {
            for ( l = 0; l < gbr_conn[br_].num_PC; l++ ) {
              if ( gbr_conn[br_].PC[l] == gbr_conn[br].PC[k] ) break;
            }
            if ( l == gbr_conn[br_].num_PC ) break;
          }
          if ( k == gbr_conn[br].num_PC ) {
            gbr_conn[br_].I[gbr_conn[br_].num_I++] = br;
          }
        }
      }
      
      /** Sets deleted branches.
       * If you select 
       * if (gbr_conn[n_br].A[i] == quita_fact[k]) 
       *     then continue;
       * you'll be able to delete branches where
       * the quita_fact is NOT present.
       */
      
      if (gnum_quita_facts != 0)
      {
        for ( i = 0; i < gop_conn[n_op].num_B; i++ ) 
          for ( j = 0; j < gbr_conn[gop_conn[n_op].B[i]].num_A; j++)
            for ( k = 0; k < gnum_quita_facts; k++)
              if (gbr_conn[gop_conn[n_op].B[i]].A[j] == quita_branch[k]) {
                /*remove the branch into gop_conn[gbr_conn[n_br].op] */
                gbr_conn[gop_conn[n_op].B[i]].removed = TRUE;
                for ( l = i; l < gop_conn[n_op].num_B - 1; l++ ) 
                  gop_conn[n_op].B[l] =  gop_conn[n_op].B[l+1];
                gop_conn[n_op].num_B--;
                continue; /* dovrebbe saltare al prossimo branch i */
              }
      }
      
    }
    
    /* first sweep: only count the space we need for the fact arrays !
     */
    if ( gop_conn[n_op].num_E > 0 ) {
      for ( i = 0; i < gop_conn[n_op].num_E; i++ ) {
        ef = gop_conn[n_op].E[i];
        for ( j = 0; j < gef_conn[ef].num_PC; j++ ) {
          gft_conn[gef_conn[ef].PC[j]].num_PC++;
        }
        for ( j = 0; j < gef_conn[ef].num_A; j++ ) {
          gft_conn[gef_conn[ef].A[j]].num_A++;
        }
        for ( j = 0; j < gef_conn[ef].num_D; j++ ) {
          gft_conn[gef_conn[ef].D[j]].num_D++;
        }
      }
    } 
    
    /** VERIFICARE SE L'array dei FACTS e' costruito bene 
     * Questa rpocedura dovrebbe interferire con 
     * quanto fatto x gli effects qua sopra. */
    /*  if ( gop_conn[n_op].num_B > 0 ) { */
    /*       for ( i = 0; i < gop_conn[n_op].num_B; i++ ) { */
    /* 	br = gop_conn[n_op].B[i]; */
    /* 	for ( j = 0; j < gbr_conn[br].num_PC; j++ ) { */
    /* 	  gft_conn[gbr_conn[br].PC[j]].num_PC++; */
    /* 	} */
    /*  	for ( j = 0; j < gbr_conn[br].num_A; j++ ) { */
    /* 	  gbr_conn[gbr_conn[br].A[j]].num_A++; */
    /* 	} */
    /* 	for ( j = 0; j < gbr_conn[br].num_D; j++ ) { */
    /* 	  gft_conn[gbr_conn[br].D[j]].num_D++; */
    /* 	} */
    /*       } */
    /*     } */
    
    n_op++;
  }
  
  
  for ( i = 0; i < gnum_ft_conn; i++ ) {
    if ( gft_conn[i].num_PC > 0 ) {
      gft_conn[i].PC = ( int * ) calloc( gft_conn[i].num_PC, sizeof( int ) );
    }
    gft_conn[i].num_PC = 0;
    if ( gft_conn[i].num_A > 0 ) {
      gft_conn[i].A = ( int * ) calloc( gft_conn[i].num_A, sizeof( int ) );
    }
    gft_conn[i].num_A = 0;
    if ( gft_conn[i].num_D > 0 ) {
      gft_conn[i].D = ( int * ) calloc( gft_conn[i].num_D, sizeof( int ) );
    }
    gft_conn[i].num_D = 0;
    
    gft_conn[i].is_global_goal = FALSE;
  }
  for ( i = 0; i < ggoal_state.num_F; i++ ) {
    gft_conn[ggoal_state.F[i]].is_global_goal = TRUE;
  }
  
  for ( i = 0; i < gnum_ef_conn; i++ ) {
    if ( gef_conn[i].removed ) continue;
    
    for ( j = 0; j < gef_conn[i].num_PC; j++ ) {
      gft_conn[gef_conn[i].PC[j]].PC[gft_conn[gef_conn[i].PC[j]].num_PC++] = i;
    }
    for ( j = 0; j < gef_conn[i].num_A; j++ ) {
      gft_conn[gef_conn[i].A[j]].A[gft_conn[gef_conn[i].A[j]].num_A++] = i;
    }
    for ( j = 0; j < gef_conn[i].num_D; j++ ) {
      gft_conn[gef_conn[i].D[j]].D[gft_conn[gef_conn[i].D[j]].num_D++] = i;
    }
  }
  
  free( same_effects );
  free( had_effects );
  free( same_branch );
  free( had_branch );
  
  if ( gcmd_line.display_info == 121 ) {
    printf("\n\ncreated connectivity graph as follows:");
    
    printf("\n\n------------------OP ARRAY:-----------------------");
    for ( i = 0; i < gnum_op_conn; i++ ) {
      printf("\n\nOP: ");
      print_op_name( i );
      printf("\n----------EFFS:");
      for ( j = 0; j < gop_conn[i].num_E; j++ ) {
        printf("\neffect %d", gop_conn[i].E[j]);
      }
      if ( gop_conn[i].num_B > 0 ) {
        printf("\n----------BRANCHES:");
        for ( j = 0; j < gop_conn[i].num_B; j++ ) {
          printf("\nbranch effect %d", gop_conn[i].B[j]);
        }
      }
    }
    
    printf("\n\n-------------------EFFECT ARRAY:----------------------");
    for ( i = 0; i < gnum_ef_conn; i++ ) {
      printf("\n\neffect %d of op %d: ", i, gef_conn[i].op);
      print_op_name( gef_conn[i].op );
      if ( gef_conn[i].removed ) {
        printf(" --- REMOVED ");
        continue;
      }
      printf("\n----------PCS:");
      for ( j = 0; j < gef_conn[i].num_PC; j++ ) {
        printf("\n");
        print_ft_name( gef_conn[i].PC[j] );
      }
      printf("\n----------ADDS:");
      for ( j = 0; j < gef_conn[i].num_A; j++ ) {
        printf("\n");
        print_ft_name( gef_conn[i].A[j] );
      }
      printf("\n----------DELS:");
      for ( j = 0; j < gef_conn[i].num_D; j++ ) {
        printf("\n");
        print_ft_name( gef_conn[i].D[j] );
      }
      printf("\n----------IMPLIEDS:");
      for ( j = 0; j < gef_conn[i].num_I; j++ ) {
        printf("\nimplied effect %d of op %d: ", 
               gef_conn[i].I[j], gef_conn[gef_conn[i].I[j]].op);
        print_op_name( gef_conn[gef_conn[i].I[j]].op );
      }
    }
    
    if (a_mode) {
      printf("\n\n------------------BRANCHES ARRAY:----------------------");
      for ( i = 0; i < gnum_br_conn; i++ ) {
        printf("\n\nbranch %d of op %d: ", i, gbr_conn[i].op);
        print_op_name( gbr_conn[i].op );
        if ( gbr_conn[i].removed ) {
          printf(" --- REMOVED ");
          continue;
        }
        printf("\n----------PCS:");
        for ( j = 0; j < gbr_conn[i].num_PC; j++ ) {
          printf("\n");
          print_ft_name( gbr_conn[i].PC[j] );
        }
        printf("\n----------ADDS:");
        for ( j = 0; j < gbr_conn[i].num_A; j++ ) {
          printf("\n");
          print_ft_name( gbr_conn[i].A[j] );
        }
        printf("\n----------DELS:");
        for ( j = 0; j < gbr_conn[i].num_D; j++ ) {
          printf("\n");
          print_ft_name( gbr_conn[i].D[j] );
        }
        printf("\n----------IMPLIEDS:");
        for ( j = 0; j < gbr_conn[i].num_I; j++ ) {
          printf("\nimplied effect %d of op %d: ", 
                 gbr_conn[i].I[j], gbr_conn[gbr_conn[i].I[j]].op);
          print_op_name( gbr_conn[gbr_conn[i].I[j]].op );
        }
      }}
    
    printf("\n\n----------------------FT ARRAY:-----------------------------");
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      printf("\n\nFT: ");
      print_ft_name( i );
      printf(" rand: %d", gft_conn[i].rand);
      printf("\n----------PRE COND OF:");
      for ( j = 0; j < gft_conn[i].num_PC; j++ ) {
        printf("\neffect %d", gft_conn[i].PC[j]);
      }
      printf("\n----------ADD BY:");
      for ( j = 0; j < gft_conn[i].num_A; j++ ) {
        printf("\neffect %d", gft_conn[i].A[j]);
      }
      printf("\n----------DEL BY:");
      for ( j = 0; j < gft_conn[i].num_D; j++ ) {
        printf("\neffect %d", gft_conn[i].D[j]);
      }
    }
  }
  
}

#define CTIME() gclos_time += ( float ) ( ( clend.tms_utime - clstart.tms_utime + \
					 clend.tms_stime - clstart.tms_stime  ) / 100.0 )

void build_closure_operators()

{
  
  int i, j, k;
  int * nsl;
  S_adds tt;
  int num_sl_t = 0; /* num of FM add actions */
   int num_clauses = 0;
  gcl_conn = ( int * ) malloc( gnum_op_conn * sizeof( int ) );
  gnum_cl_conn = 0;
  gsim_conn = ( int * ) calloc(gnum_op_conn, sizeof( int ) );
  gnum_del_conn = 0;
  gnum_clef_conn = 0;
 
  
  for ( i = 0; i < gnum_op_conn; i++ ) {
    gcl_conn[i] = 0;
    gop_conn[i].is_clos = FALSE;
  }
  
  
  /* Initialization */
  if (MINISAT) 
  {  
    int f, nf;
    Bool done = FALSE;
    
    nvalues = 0;
    values =  ( int * ) calloc( gnum_ft_conn , sizeof( int ) );
    
    init_solver();

    gnot_corr = ( int * ) calloc( gnum_relevant_facts , sizeof( int ) );

    
    /* Now gets the list of not-facts */ 
    for (i = 0; i < gnum_not_conn; i++) {
      for (j = 0; j < gnum_relevant_facts; j++)
        if ( grelevant_facts[j].predicate == gnot_conn[i][0] ) { 
          f = j; 
          done = TRUE;
          break; 
        } 
      if (done)
        for (j = 0; j < gnum_relevant_facts; j++) 
          if ( grelevant_facts[j].predicate == gnot_conn[i][1] ) { 
            nf = j; 
            done = TRUE && done;
            break; 
          }
      if (done) {
        gnot_corr[ f ] = nf;
        gnot_corr[ nf ] = f;  
      } 
      done = FALSE;
    } 
    if ( gcmd_line.display_info == 108 ) {
      printf("\n\tCorrespondence table between Not-facts:");
      
      for (j = 0; j < gnum_relevant_facts; j++)
        if (gnot_corr[j] != 0)
          printf("\n\t%d:%s vs %s",j,gpredicates[grelevant_facts[j].predicate], 
		 gpredicates[grelevant_facts[gnot_corr[j]].predicate]);
    }
  }
  
  /* Now checks the names of the closure operators */
  for ( i = 0; i < gnum_op_conn; i++ ) {
    if ( (strncmp("CLO", gop_conn[i].action->name, 3) == 0) && (strncmp("RE", gop_conn[i].action->name+5, 2) == 0) ) {
      
      if (MINISAT) {  
        int ef, l;
        for (j=0; j < gop_conn[i].num_E; j++) {
          ef = gop_conn[i].E[j];
          /* a & b -> c & d ==> (c | -a | -b), (d | -a | -b) */ 

          /* Added literals */
          for (k = 0; k < gef_conn[ef].num_A; k++) {
            add_lit(gef_conn[ef].A[k]);
	     
            for (l = 0; l < gef_conn[ef].num_PC; l++) {
              add_lit(-(int)gef_conn[ef].PC[l] );
              }
            finish_clause();
            num_clauses++;  
          }
	}
      }
      
      gcl_conn[gnum_cl_conn++] = i;
      gop_conn[i].is_clos = TRUE;
      gnum_clef_conn += gop_conn[i].num_E;      
      
      if (strncmp("ADD", gop_conn[i].action->name+8, 3) == 0 ) {
        num_sl_t++;
        for (k = 0; k < gop_conn[i].num_E; k++) {

          if ( gef_conn[ gop_conn[i].E[k] ].num_PC == 0 ) {
            S_counter[*gef_conn[ gop_conn[i].E[k] ].A] = 1; /* It has only one condition left, so that SL persists */
            continue;
          }
          tt = gSlt_ft_conn[  *gef_conn[ gop_conn[i].E[k] ].PC ];
          if (gcmd_line.debug == 3) {
            printf("\nconsidering = "); 
            print_ft_name(*gef_conn[ gop_conn[i].E[k] ].PC);
          }
          if ( ((tt.num_S + 1) %  SLT_MEMPORTION) == 0) {
            nsl = 
            ( int * ) realloc(tt.sl_t, (tt.num_S + SLT_MEMPORTION + 1) * sizeof( int ));
            if (nsl == NULL) {
              printf("\n\t Insufficient memory! ... exiting");
              fflush(stdout);
              exit(-1);
            }
            tt.sl_t = nsl;
          }
          
          /* we assume that we do not have effect duplicates in the add_sl rules */
          tt.sl_t[ tt.num_S++ ] = *gef_conn[ gop_conn[i].E[k] ].A;
          gSlt_ft_conn[  *gef_conn[ gop_conn[i].E[k] ].PC ] = tt;
          
        }
      }
    }
    /* Now checks the names of the delete SL\t operators */
    else if (strncmp("DELET", gop_conn[i].action->name, 5) == 0)
      gsim_conn[gnum_del_conn++] = i;
  }
  
  if ((gcmd_line.debug == 2) && (num_sl_t != 0)) {
    if (MINISAT)
      printf("\t\tWe have %d MiniSAT clauses\n", num_clauses);
    printf("\n\nwe check our list of %d actions:", num_sl_t);
    printf("\n\n------------------\n\n");
    for (i = 0 ;i < gnum_ft_conn; i++) {
      if (gS_index[ i ] == 0 ) 
        continue;
      for (j = 0; j < gSlt_ft_conn[i].num_S; j++) {
        printf("\nThe literal ");
        print_ft_name(i);
        printf(" implies ");
        print_ft_name(gSlt_ft_conn[i].sl_t[j]);
      }
    }
  }
  
  
  if (MINISAT) {
  int nn = 0;
  
/* time counting variables */
struct tms clstart, clend;

    reset_sat_solver();

   }
   /* Finally checks the names of the 'simplifying' SL\t operators */

   /* Gets the flag operators */
  for ( i = 0; i < gnum_op_conn; i++ ) {
    if (strcmp(DEFLAG, gop_conn[i].action->name) == 0) {
      unhabil = i;
      break;
    }
  }
  
  for ( i = 0; i < gnum_op_conn; i++ ) {
    if (strcmp(REFLAG, gop_conn[i].action->name) == 0) {
      habilcl = i;
      break;
    }
  }
  
  if ( gcmd_line.display_info == 130 ) {
    
    printf("\n\nwe have %d closure operators:", gnum_cl_conn);
    printf("\nand have %d closure delete operators:", gnum_del_conn);
    printf("\n\n------------------OP ARRAY:-----------------------");
    for ( i = 0; i < gnum_cl_conn; i++ ) {
      printf("\n\nOP: ");
      print_op_name( gcl_conn[i] );
      for ( j = 0; j < gop_conn[ gcl_conn[i] ].num_E; j++ ) {
        printf("\n----------PCS:");
        for ( k = 0; k < gef_conn[gop_conn[gcl_conn[i]].E[j]].num_PC; k++ ) {
          printf("\n");
          print_ft_name(  gef_conn[gop_conn[gcl_conn[i]].E[j]].PC[k] );
        }
        printf("\n----------ADDS:");
        for ( k = 0; k < gef_conn[ gop_conn[gcl_conn[i]].E[j] ].num_A; k++ ) {
          printf("\n");
          print_ft_name( gef_conn[ gop_conn[gcl_conn[i]].E[j] ].A[k] );
        }
        printf("\n----------DELS:");
        for ( k = 0; k < gef_conn[ gop_conn[gcl_conn[i]].E[j] ].num_D; k++ ) {
          printf("\n");
          print_ft_name( gef_conn[ gop_conn[gcl_conn[i]].E[j] ].D[k] );
        }
        printf("\n----------IMPLIEDS:");
        for ( k = 0; k < gef_conn[ gop_conn[gcl_conn[i]].E[j] ].num_I; k++ ) {
          printf("\nimplied effect %d of op %d: ", 
                 gef_conn[gop_conn[gcl_conn[i]].E[j] ].I[k], gef_conn[gef_conn[j].I[k]].op);
          print_op_name( gef_conn[gef_conn[j].I[k]].op );
        }
        
        printf("\neffect %d", gop_conn[gcl_conn[i] ].E[j]);
      }
    } 
    printf("\n==========Simple delete operator(s): ======\n");
    for ( i = 0; i < gnum_del_conn; i++ ) {
      printf("\n\nOP: ");
      print_op_name( gsim_conn[i] );
      for ( j = 0; j < gop_conn[ gsim_conn[i] ].num_E; j++ ) {
        printf("\n----------PCS:");
        for ( k = 0; k < gef_conn[gop_conn[gsim_conn[i]].E[j]].num_PC; k++ ) {
          printf("\n");
          print_ft_name(  gef_conn[gop_conn[gsim_conn[i]].E[j]].PC[k] );
        }
        printf("\n----------ADDS:");
        for ( k = 0; k < gef_conn[ gop_conn[gsim_conn[i]].E[j] ].num_A; k++ ) {
          printf("\n");
          print_ft_name( gef_conn[ gop_conn[gsim_conn[i]].E[j] ].A[k] );
        }
        printf("\n----------DELS:");
        for ( k = 0; k < gef_conn[ gop_conn[gsim_conn[i]].E[j] ].num_D; k++ ) {
          printf("\n");
          print_ft_name( gef_conn[ gop_conn[gsim_conn[i]].E[j] ].D[k] );
        }
        printf("\n----------IMPLIEDS:");
        for ( k = 0; k < gef_conn[ gop_conn[gsim_conn[i]].E[j] ].num_I; k++ ) {
          printf("\nimplied effect %d of op %d: ", 
                 gef_conn[gop_conn[gsim_conn[i]].E[j] ].I[k], gef_conn[gef_conn[j].I[k]].op);
          print_op_name( gef_conn[gef_conn[j].I[k]].op );
        }
        
        printf("\neffect %d", gop_conn[ gsim_conn[i] ].E[j]);
      }
    }
  } 
}

void print_values( int values[], int nvalues )
{
  int i;
  for( i = 0; i < nvalues; i++ )
    printf("%d ", values[i]);
  printf("\n");
}




void load_description_file( char *filename )

{
  
  FILE * fp;/* pointer to input files */
  char tmp[MAX_LENGTH] = "";
  char word[100]; 
  int i;
  
  quita_branch = (Bool *) malloc(gnum_relevant_facts*sizeof(Bool));
  
  /* Initializes quita_branch */
  for (i=0; i < gnum_ft_conn; i++)
    quita_branch[i] = FALSE;
  
  /* open operator file 
   */
  if( ( fp = fopen( filename, "r" ) ) == NULL ) {
    sprintf(tmp, "\nff: can't find operator file: %s\n\n", filename );
    perror(tmp);
    exit( 1 );
  }
  
  /* use while construction to use fscanf until an EOF is reached */
  /* %s reads a string - a word and != means not equal */
  /* do not place semicolon at the end of the line */
  /* do not put & before word as it is an array */
  while( fscanf(fp, " %s", word)!=EOF)
  {
    /* e mo ce l'andiamo a cercare... */
    for (i=0; i < gnum_relevant_facts; i++)
      if (strcmp( word, name_Fact(&grelevant_facts[i])) == 0) {
        quita_branch[ i ] = TRUE;
        gnum_quita_facts++;
      }
  } 
  /* Verify what gathered */
  for (i=0; i < gnum_quita_facts; i++)
    print_ft_name(quita_branch[i]);
  fflush(stdout);
  fclose( fp );/* and close file again */
  
}


int get_atom(char *string) 
{
  int i, this_fact;
  
  this_fact = -1;
  if (MINISAT) {
    for (i=1; i < gnum_relevant_facts; i++)
      if (strcmp( string, name_Fact(&grelevant_facts[i])) == 0)
        this_fact = i;
  } else
    for (i=0; i < gnum_relevant_facts; i++)
      if (strcmp( string, name_Fact(&grelevant_facts[i])) == 0)
        this_fact = i;  
  printf("fact %s tagged as %d  \n", string, this_fact );
  return this_fact;
}


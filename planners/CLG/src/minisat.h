void init_solver(void);
void print_values( int *values, int nvalues );
void add_lit( int lit ); /* Add lits to new clause */
void finish_clause(void); /* Load new clause into sat solver */
int sat_simplify( int values[] );
/* void sat_simplify(void); */
void add_fact( int lit );
void reset_sat_solver(); /* do it before adding any fact */

int add_predicate( char* predicate, int i );
int find_predicate( char* predicate );
int report_predicate_hash_quality();


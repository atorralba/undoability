
/*********************************************************************
 * (C) Copyright 2001 Albert Ludwigs University Freiburg
 *     Institute of Computer Science
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 *********************************************************************/



/*
 * THIS SOURCE CODE IS SUPPLIED  ``AS IS'' WITHOUT WARRANTY OF ANY KIND, 
 * AND ITS AUTHOR AND THE JOURNAL OF ARTIFICIAL INTELLIGENCE RESEARCH 
 * (JAIR) AND JAIR'S PUBLISHERS AND DISTRIBUTORS, DISCLAIM ANY AND ALL 
 * WARRANTIES, INCLUDING BUT NOT LIMITED TO ANY IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND
 * ANY WARRANTIES OR NON INFRINGEMENT.  THE USER ASSUMES ALL LIABILITY AND
 * RESPONSIBILITY FOR USE OF THIS SOURCE CODE, AND NEITHER THE AUTHOR NOR
 * JAIR, NOR JAIR'S PUBLISHERS AND DISTRIBUTORS, WILL BE LIABLE FOR 
 * DAMAGES OF ANY KIND RESULTING FROM ITS USE.  Without limiting the 
 * generality of the foregoing, neither the author, nor JAIR, nor JAIR's
 * publishers and distributors, warrant that the Source Code will be 
 * error-free, will operate without interruption, or will meet the needs 
 * of the user.
 */






/*********************************************************************
* File: main.c
 * Description: The main routine for the FastForward Planner.
 *
 * Author: Joerg Hoffmann 2000
 * 
 *********************************************************************/ 







#include "ff.h"

#include "memory.h"
#include "output.h"

#include "parse.h"

#include "inst_pre.h"
#include "inst_easy.h"
#include "inst_hard.h"
#include "inst_final.h"

#include "orderings.h"

#include "relax.h"
#include "search.h"
#include <unistd.h>

#include "minisat.h"









/*
 *  ----------------------------- GLOBAL VARIABLES ----------------------------
 */












/*******************
 * GENERAL HELPERS *
 *******************/








/* used to time the different stages of the planner
 */
float gtempl_time = 0, greach_time = 0, grelev_time = 0, gconn_time = 0;
float gsearch_time = 0, gclos_time = 0;


/* the command line inputs
 */
struct _command_line gcmd_line;
int skip_hc = 0;
int a_mode = 0;
int validation = 0;
int obs_selection = 0;
int acl = 0;
int pp = 1;
int stop_on_fail = 1;
int run_cycle = 1;
Bool last = FALSE;
/* probability to observe something over 50% of times */
float proba = 0;
/* number of states that got heuristically evaluated
 */
int gevaluated_states = 0;

/* maximal depth of breadh first search
 */
int gmax_search_depth = 0;

/* Number of nodes expanded */
int gtot_nodes = 0;



/***********
 * PARSING *
 ***********/







/* used for pddl parsing, flex only allows global variables
 */
int gbracket_count;
char *gproblem_name;

/* The current input line number
 */
int lineno = 1;

/* The current input filename
 */
char *gact_filename;

/* The pddl domain name
 */
char *gdomain_name = NULL;

/* loaded, uninstantiated operators
 */
PlOperator *gloaded_ops = NULL;

/* stores initials as fact_list 
 */
PlNode *gorig_initial_facts = NULL;

/* not yet preprocessed goal facts
 */
PlNode *gorig_goal_facts = NULL;

/* axioms as in UCPOP before being changed to ops
 */
PlOperator *gloaded_axioms = NULL;

/* the types, as defined in the domain file
 */
TypedList *gparse_types = NULL;

/* the constants, as defined in domain file
 */
TypedList *gparse_constants = NULL;

/* the predicates and their arg types, as defined in the domain file
 */
TypedListList *gparse_predicates = NULL;

/* the objects, declared in the problem file
 */
TypedList *gparse_objects = NULL;


/* connection to instantiation ( except ops, goal, initial )
 */

/* all typed objects 
 */
FactList *gorig_constant_list = NULL;

/* the predicates and their types
 */
FactList *gpredicates_and_types = NULL;












/*****************
 * INSTANTIATING *
 *****************/









/* global arrays of constant names,
 *               type names (with their constants),
 *               predicate names,
 *               predicate aritys,
 *               defined types of predicate args
 */
Token gconstants[MAX_CONSTANTS];
int gnum_constants = 0;
Token gtype_names[MAX_TYPES];
int gtype_consts[MAX_TYPES][MAX_TYPE];
Bool gis_member[MAX_CONSTANTS][MAX_TYPES];
int gmember_nr[MAX_CONSTANTS][MAX_TYPES];/* nr of object within a type */
int gtype_size[MAX_TYPES];
int gnum_types = 0;
Token gpredicates[MAX_PREDICATES];
int garity[MAX_PREDICATES];
int gpredicates_args_type[MAX_PREDICATES][MAX_ARITY];
int gnum_predicates = 0;





/* the domain in integer (Fact) representation
 */
Operator_pointer goperators[MAX_OPERATORS];
int gnum_operators = 0;
Fact *gfull_initial;
int gnum_full_initial = 0;
WffNode *ggoal = NULL;




/* stores inertia - information: is any occurence of the predicate
 * added / deleted in the uninstantiated ops ?
 */
Bool gis_added[MAX_PREDICATES];
Bool gis_deleted[MAX_PREDICATES];



/* splitted initial state:
 * initial non static facts,
 * initial static facts, divided into predicates
 * (will be two dimensional array, allocated directly before need)
 */
Facts *ginitial = NULL;
int gnum_initial = 0;
Fact **ginitial_predicate;
int *gnum_initial_predicate;



/* the type numbers corresponding to any unary inertia
 */
int gtype_to_predicate[MAX_PREDICATES];
int gpredicate_to_type[MAX_TYPES];

/* (ordered) numbers of types that new type is intersection of
 */
TypeArray gintersected_types[MAX_TYPES];
int gnum_intersected_types[MAX_TYPES];



/* splitted domain: hard n easy ops
 */
Operator_pointer *ghard_operators;
int gnum_hard_operators;
NormOperator_pointer *geasy_operators;
int gnum_easy_operators;



/* so called Templates for easy ops: possible inertia constrained
 * instantiation constants
 */
EasyTemplate *geasy_templates;
int gnum_easy_templates;



/* first step for hard ops: create mixed operators, with conjunctive
 * precondition and arbitrary effects
 */
MixedOperator *ghard_mixed_operators;
int gnum_hard_mixed_operators;



/* hard ''templates'' : pseudo actions
 */
PseudoAction_pointer *ghard_templates;
int gnum_hard_templates;



/* store the final "relevant facts"
 */
Fact grelevant_facts[MAX_RELEVANT_FACTS];

#if MINISAT
int gnum_relevant_facts = 1; /* for MINISAT, otherwise = 0 */
#else
int gnum_relevant_facts = 0; 
#endif
int gnum_pp_facts = 0;

/* the total number of facts into description file */
int gnum_quita_facts = 0;
Bool *quita_branch;

/* the final actions and problem representation
 */
Action *gactions;
int gnum_actions;
int plan_espurias = 0;
int plan_branches = 0;
int plan_failures = 0;
State ginitial_state;
State ggoal_state;




int gnot_conn[MAX_PREDICATES][2];
int gnum_not_conn;
 Bool gnot_fact[MAX_PREDICATES]; /* List of facts that are negative */
int *gnot_corr;




/**********************
 * CONNECTIVITY GRAPH *
 **********************/







/* one ops (actions) array ...
 */
OpConn *gop_conn;
int gnum_op_conn;



/* one effects array ...
 */
EfConn *gef_conn;
int gnum_ef_conn;


/* one facts array.
 */
FtConn *gft_conn;
int gnum_ft_conn;


/* one conditional effects (branching) array ...
 */
EfConn *gbr_conn;
int gnum_br_conn;

/* the closure operators */
int *gcl_conn;
int gnum_cl_conn;
int gnum_clef_conn;

int *gsim_conn;
int gnum_del_conn;

int nvalues;
int *values;

/*******************
 * SEARCHING NEEDS *
 *******************/





/* the initial state of the solution plan */
TreeNode *root;

/* the goal state, divided into subsets
 */
State *ggoal_agenda;
int gnum_goal_agenda;



/* byproduct of fixpoint: applicable actions
 */
int *gA;
int gnum_A;



/* communication from extract 1.P. to search engines:
 * 1P action choice
 */
int *gH;
int gnum_H;


/* the effects that are considered true in relaxed plan
 */
int *gin_plan_E;
int gnum_in_plan_E;



/* always stores (current) serial plan
 */
/* int gplan_ops[MAX_PLAN_LENGTH]; */
int *gplan_ops;
int gnum_plan_ops = 0;

int fail_fact = -1;
int contradiction_op;
int unhabil = -1;
int habilcl = -1;

/* stores the states that the current plan goes through
 * ( for knowing where new agenda entry starts from )
 State gplan_states[MAX_PLAN_LENGTH + 1];  */
State *gplan_states;
int  gnum_kplan_ops = 0;
int ml = 0;

Bool description_file = FALSE;

State gkplan_current_goal;

State gadd_list;
int *gS_index;
int *S_counter;
S_adds *gSlt_ft_conn;

unsigned int glabel = 1;

/*
 *  ----------------------------- HEADERS FOR PARSING ----------------------------
 * ( fns defined in the scan-* files )
 */





void get_fct_file_name( char *filename );
void load_ops_file( char *filename );
void load_fct_file( char *filename );
void load_description_file( char *filename );

void build_closure_operators( void );

void randomize( void );







/*
 *  ----------------------------- MAIN ROUTINE ----------------------------
 */




struct tms lstart, lend;

int used_mem = MAX_PLAN_LENGTH + 1;





int main( int argc, char *argv[] )

{

  /* resulting name for ops file
   */
  char ops_file[MAX_LENGTH] = "";
  /* same for fct file 
   */
  char fct_file[MAX_LENGTH] = "";
  /* same for description file 
   */
  char des_file[MAX_LENGTH] = "";

  struct tms start, end;

  State current_start, current_end;
  int i, j;
  Bool found_plan;
  int max_branch_lenght = 0;

  randomize();
  times ( &lstart );


  /* command line treatment
   */
  if ( argc == 1 || ( argc == 2 && *++argv[0] == '?' ) ) {
    ff_usage();
    exit( 1 );
  }
  if ( !process_command_line( argc, argv ) ) {
    ff_usage();
    exit( 1 );
  }

  if (( !stop_on_fail && !validation ) || ( !stop_on_fail && skip_hc)) {
    ff_usage();
    exit( 1 );
  }
  if (proba > 100)
    proba = (float)100;

  proba /= 100;

  if ( (run_cycle > 1) && (validation || pp) ) {
    ff_usage();
    exit( 1 );
  }

  /* make file names
   */

  /* one input name missing
   */
  if ( !gcmd_line.ops_file_name || 
       !gcmd_line.fct_file_name ) {
    fprintf(stdout, "\nff: two input files needed\n\n");
    ff_usage();      
    exit( 1 );
  }
  /* add path info, complete file names will be stored in
   * ops_file and fct_file 
   */
  sprintf(ops_file, "%s%s", gcmd_line.path, gcmd_line.ops_file_name);
  sprintf(fct_file, "%s%s", gcmd_line.path, gcmd_line.fct_file_name);


  /* parse the input files
   */

  /* start parse & instantiation timing
   */
  times( &start );
  /* domain file (ops)
   */
  if ( gcmd_line.display_info >= 1 ) {
    printf("\nff: parsing domain file");
  } 
  /* it is important for the pddl language to define the domain before 
   * reading the problem 
   */
  load_ops_file( ops_file );
  /* problem file (facts)
   */  
  if ( gcmd_line.display_info >= 1 ) {
    printf(" ... done.\nff: parsing problem file"); 
  }
  load_fct_file( fct_file );
  if ( gcmd_line.display_info >= 1 ) {
    printf(" ... done.\n\n");
  }

  /* This is needed to get all types.
   */
  build_orig_constant_list();


  /* last step of parsing: see if it's an ADL domain!
   */
  if ( !make_adl_domain() ) {
    printf("\nff: this is not an ADL problem!");
    printf("\n    can't be handled by this version.\n\n");
    exit( 1 );
  }


  /* now instantiate operators;
   */



  /**************************
   * first do PREPROCESSING * 
   **************************/


  /* start by collecting all strings and thereby encoding 
   * the domain in integers.
   */
  encode_domain_in_integers();
  report_predicate_hash_quality();

  /* inertia preprocessing, first step:
   *   - collect inertia information
   *   - split initial state into
   *        _ arrays for individual predicates
   *        - arrays for all static relations
   *        - array containing non - static relations
   */
  do_inertia_preprocessing_step_1();

  /* normalize all PL1 formulae in domain description:
   * (goal, preconds and effect conditions)
   *   - simplify formula
   *   - expand quantifiers
   *   - NOTs down
   */
  normalize_all_wffs();

  /* Collect negative preconds: introduce symmetric new predicate
   * NOT-p(..) (e.g., not-in(?ob) in briefcaseworld)
   */
  collect_negative_preconds();
  
  /* finally translate negative preconds
   */
  translate_negative_preconds();

  /* split domain in easy (disjunction of conjunctive preconds)
   * and hard (non DNF preconds) part, to apply 
   * different instantiation algorithms
   */
  split_domain();


  /***********************************************
   * PREPROCESSING FINISHED                      *
   *                                             *
   * NOW MULTIPLY PARAMETERS IN EFFECTIVE MANNER *
   ***********************************************/

  build_easy_action_templates();
  build_hard_action_templates();

  times( &end );
  TIME( gtempl_time );

  times( &start );

  /* perform reachability analysis in terms of relaxed 
   * fixpoint
   */
  perform_reachability_analysis();

  times( &end );
  TIME( greach_time );

  times( &start );

  /* collect the relevant facts and build final domain
   * and problem representations.
   */
  collect_relevant_facts();

  times( &end );
  TIME( grelev_time );

  /* Parses the domain description file */
  description_file = FALSE;

  if (a_mode) {
    if( strcmp(gcmd_line.des_file_name, "" ) == 0 )
      fprintf(stdout, "ff-cond: no description file\n");
    else {
      /* add path info, complete file names will be stored in des_file */
      sprintf(des_file, "%s%s", gcmd_line.path, gcmd_line.des_file_name);
      if ( gcmd_line.display_info >= 1 ) {
        printf("ff-cond: parsing description file..."); 
      } 
      
      load_description_file( des_file );
      description_file = TRUE;
      if ( gcmd_line.display_info >= 1 ) {
        printf(" ... done.\n\n");
      }
    }
    fail_fact = get_atom( FAILATOM );
  }
  times( &start );
  
  /* now build globally accessable connectivity graph
   */
  build_connectivity_graph();
  
  if (a_mode) {
  
    initialize_S_index();
    gSlt_ft_conn = (S_adds *) malloc(gnum_ft_conn * sizeof(State)); 
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      gSlt_ft_conn[i].sl_t = (int *) malloc( SLT_MEMPORTION * sizeof(int));
      gSlt_ft_conn[i].num_S = 0;
    } 
    build_closure_operators();

  }
  
  /* Now checks the names of the closure check contradiction operator */
  if (fail_fact == -1)
    contradiction_op = -1;
  else if( gft_conn[fail_fact].A )
    contradiction_op = (fail_fact >= 0) ? gef_conn[*gft_conn[fail_fact].A].op : 0;
  else contradiction_op = -1;

      
  times( &end );
  TIME( gconn_time );

  /***********************************************************
   * we are finally through with preprocessing and can worry *
   * bout finding a plan instead.                            *
   ***********************************************************/
  if(0)
    {
      output_planner_info();
      exit(0);
    }

 restart:
 
  times( &start );

  /* another quick preprocess: approximate goal orderings and split
   * goal set into sequence of smaller sets, the goal agenda
   */
  compute_goal_agenda();

  /* make space in plan states info, and relax
   */
   
  if (a_mode) {
  
    root = new_TreeNode("root\0");
    
    gplan_ops = (int *) malloc((MAX_FRAGMENT_LENGTH -1) * sizeof(int));
    /* gplan_states = (State *) malloc(MAX_FRAGMENT_LENGTH * sizeof(State)); 
    for ( i = 0; i < MAX_FRAGMENT_LENGTH; i++ ) {
      make_state( &(gplan_states[i]), gnum_ft_conn );
      gplan_states[i].max_F = gnum_ft_conn;
    } */
    
    make_state( &gkplan_current_goal, gnum_ft_conn );
    gkplan_current_goal.max_F =  gnum_ft_conn;

    make_state( &gadd_list, gnum_ft_conn );
    gadd_list.max_F = gnum_ft_conn;
  } else {
    gplan_ops = (int *) malloc((used_mem - 1) * sizeof(int));
    gplan_states = (State *) malloc(used_mem * sizeof(State));
    for ( i = 0; i < used_mem ; i++ ) {
      make_state( &(gplan_states[i]), gnum_ft_conn );
      gplan_states[i].max_F = gnum_ft_conn;
    }  
  }

  make_state( &current_start, gnum_ft_conn );
  current_start.max_F = gnum_ft_conn;
  make_state( &current_end, gnum_ft_conn );
  current_end.max_F = gnum_ft_conn;
  initialize_relax();

  source_to_dest( &current_start, &ginitial_state );
  source_to_dest( &current_end, &(ggoal_agenda[0]) );
  
  if( a_mode)
  {
    /** Don't need to do search but just to progress 
     internal state and give observation to K-plan 
     **/
    printf("\n\nSkipping All searches!");
    printf("\nStarting K procedure.\n"); 
    
    /* Set the flag to search without merges */
     if (unhabil > -1)  {
      result_to_dest( &gkplan_current_goal, &ginitial_state, unhabil );
      source_to_dest( &current_start, &gkplan_current_goal);
    }
    else
      source_to_dest( &gkplan_current_goal, &ginitial_state );
    
    /*  Do the whole EHC search */
    for ( i = 0; i < gnum_goal_agenda; i++ ) {

      /*  time_t a = time(); */
      found_plan = do_k_plan_search(&current_start, &current_end);
      
      if (!found_plan) {
        printf("\n\n\tEnforced Hill Climbing Failed");
        plan_failures++;
        plan_branches++;
        break;
      }
      
      /* copies the (partial) goal into current_start */
      source_to_dest( &current_start, &gkplan_current_goal );
      
      if ( i < gnum_goal_agenda - 1 ) {
        for ( j = 0; j < ggoal_agenda[i+1].num_F; j++ ) {
          current_end.F[current_end.num_F++] = ggoal_agenda[i+1].F[j];
        }
      }
    }
  }
  else if( BEST_FIRST && skip_hc )
  {
    printf("\n\nSkipping Hill-climbing !");
    printf("\nStarting Best-first Search.\n");
    found_plan = do_best_first_search( );
  }
  else
  {

    source_to_dest( &(gplan_states[0]), &ginitial_state );
    for ( i = 0; i < gnum_goal_agenda; i++ ) {
      if ( !do_enforced_hill_climbing( &current_start, &current_end ) ) {
        break;
      }
      source_to_dest( &current_start, &(gplan_states[gnum_plan_ops]) );
      if ( i < gnum_goal_agenda - 1 ) {
        for ( j = 0; j < ggoal_agenda[i+1].num_F; j++ ) {
          current_end.F[current_end.num_F++] = ggoal_agenda[i+1].F[j];
        }
      }
    }
    
    found_plan = ( i == gnum_goal_agenda ) ? TRUE : FALSE;
    
    if (BEST_FIRST && !found_plan ) {
      printf("\n\tswitching to Best-first Search now.\n");
      found_plan = do_best_first_search();
    }
  }
  

  times( &end );
  TIME( gsearch_time );

  if (last) {
    printf("\nPrinting last state of the plan.......\n\n");
    print_state(current_start);
  }

  if ( found_plan ) {
    if (a_mode) 
      print_kplan(print_TreeNodes(root,0,0));
    else
      print_plan( );
  }

  output_planner_info();

  printf("\n\n");
  if ( a_mode && (--run_cycle > 0) ) {
    printf("cycle %d\n", run_cycle);
    fflush(stdout);
    plan_branches = 0;
    plan_espurias = 0;
    plan_failures = 0;
    gnum_kplan_ops = 0;
    treeNodes_destroy(root);
    gsearch_time = 0;
    gclos_time = 0;
    gevaluated_states = 0;
    gtot_nodes = 0;
    gmax_search_depth = 0;
    goto  restart;
  } 

  exit( 0 );

}








/*
 *  ----------------------------- HELPING FUNCTIONS ----------------------------
 */



/* H.P. es un pesado */
void randomize( void ) {
  FILE *fd;
  unsigned int read;
  unsigned int buffer;
  fd = fopen("/dev/random","r");
  read = fread((void *)&buffer, sizeof(unsigned int), 1, fd);
  srand(buffer+getpid());
  fclose(fd);
}







void output_planner_info( void )

{

  printf( "\n\ntime spent: %7.2f seconds instantiating %d easy, %d hard action templates", 
	  gtempl_time, gnum_easy_templates, gnum_hard_mixed_operators );
  printf( "\n            %7.2f seconds reachability analysis, yielding %d facts and %d actions", 
	  greach_time, gnum_pp_facts, gnum_actions );
  printf( "\n            %7.2f seconds creating final representation with %d relevant facts", 
	  grelev_time, gnum_relevant_facts );
  printf( "\n            %7.2f seconds building connectivity graph",
	  gconn_time );
  printf( "\n            %7.2f seconds computing closures operations",
	  gclos_time );
  printf( "\n            %7.2f seconds searching, evaluating %d states, expanding %d nodes, to a max depth of %d", 
	  gsearch_time, gevaluated_states, gtot_nodes, gmax_search_depth );
  printf( "\n            %7.2f seconds total time", 
	  gtempl_time + greach_time + grelev_time + gconn_time + gsearch_time );

  printf("\n\n");

  /*  exit( 0 );

  print_official_result();*/

}


FILE *out;

void print_official_result( void )

{

  int i;
  char name[MAX_LENGTH];

  sprintf( name, "%s.soln", gcmd_line.fct_file_name );

  if ( (out = fopen( name, "w")) == NULL ) {
    printf("\n\nCan't open official output file!\n\n");
    return;
  }

  times( &lend );
  fprintf(out, "Time %d\n", 
	 (int) ((lend.tms_utime - lstart.tms_utime + lend.tms_stime - lstart.tms_stime) * 10.0));

  for ( i = 0; i < gnum_plan_ops; i++ ) {
    print_official_op_name( gplan_ops[i] );
    fprintf(out, "\n");
  }

  fclose( out );

}



void print_official_op_name( int index )

{

  int i;
  Action *a = gop_conn[index].action;

  if ( a->norm_operator ||
       a->pseudo_action ) {
    fprintf(out, "(%s", a->name ); 
    for ( i = 0; i < a->num_name_vars; i++ ) {
      fprintf(out, " %s", gconstants[a->name_inst_table[i]]);
    }
    fprintf(out, ")");
  }

}



void ff_usage( void )

{

  printf("\nusage of ff:\n");

  printf("\nOPTIONS   DESCRIPTIONS\n\n");
  printf("-p <str>    path for operator and fact file\n");
  printf("-o <str>    operator file name\n");
  printf("-f <str>    fact file name\n\n");
  printf("-b {0|1}    0 = default, if (1) skip hill-climbing and do best first search\n\n");
  printf("-a {0|1}    0 = default, if [1] don't do search but progress k-plans\n\n");
  printf("-v {0|1}    0 = default, validation: [1] full plan search, tree-shaped solution.\n \t\t\t\t\t\t[2] full plan search, graph-shaped solution.\n\n");
  printf("-s {0|1}    0 = default, random: if [1] prompt for manual observation selection\n\n");
  printf("-c {0|1|2}  0 = default (apply closure operators after each info gathering action), if [1] applies closure ops also after each regular action, [2] do not apply closure operators implicitely\n\n");
  printf("-k {0|1}    1 = default, print Kplan: if [1] do not print plan\n\n");
  printf("-l {0|1}    0 = default, prints last state: if [1] prints it\n\n");
  printf("-e {0|1}    1 = default, if (0) don't stop after a failure, continuing the search (needs -v 1 -b 1)\n\n");
  printf("-n <num>    1 = default, number of run cycles (goes with -v 0)\n\n");
  printf("-q <num>    0 = default, probability of having an observation rather than another\n\n");
  printf("-i <num>    run-time information level( preset: 1 )\n");
  printf("      0     only times\n");
  printf("      1     problem name, planning process infos\n");
  printf("    101     parsed problem data\n");
  printf("    102     cleaned up ADL problem\n");
  printf("    103     collected string tables\n");
  printf("    104     encoded domain\n");
  printf("    105     predicates inertia info\n");
  printf("    106     splitted initial state\n");
  printf("    107     domain with Wff s normalized\n");
  printf("    108     domain with NOT conds translated\n");
  printf("    109     splitted domain\n");
  printf("    110     cleaned up easy domain\n");
  printf("    111     unaries encoded easy domain\n");
  printf("    112     effects multiplied easy domain\n");
  printf("    113     inertia removed easy domain\n");
  printf("    114     easy action templates\n");
  printf("    115     cleaned up hard domain representation\n");
  printf("    116     mixed hard domain representation\n");
  printf("    117     final hard domain representation\n");
  printf("    118     reachability analysis results\n");
  printf("    119     facts selected as relevant\n");
  printf("    120     final domain and problem representations\n");
  printf("    121     connectivity graph\n");
  printf("    122     fixpoint result on each evaluated state\n");
  printf("    123     1P extracted on each evaluated state\n");
  printf("    124     H set collected for each evaluated state\n");
  printf("    125     False sets of goals <GAM>\n");
  printf("    126     detected ordering constraints leq_h <GAM>\n");
  printf("    127     the Goal Agenda <GAM>\n");
  printf("    130     closure actions\n");


/*   printf("    109     reachability analysis results\n"); */
/*   printf("    110     final domain representation\n"); */
/*   printf("    111     connectivity graph\n"); */
/*   printf("    112     False sets of goals <GAM>\n"); */
/*   printf("    113     detected ordering constraints leq_h <GAM>\n"); */
/*   printf("    114     the Goal Agenda <GAM>\n"); */
/*   printf("    115     fixpoint result on each evaluated state <1Ph>\n"); */
/*   printf("    116     1P extracted on each evaluated state <1Ph>\n"); */
/*   printf("    117     H set collected for each evaluated state <1Ph>\n"); */
  
  printf("\n-d <num>    switch on debugging\n\n");

}




Bool process_command_line( int argc, char *argv[] )

{

  char option;

  gcmd_line.display_info = 1;
  gcmd_line.debug = 0;
  
  memset(gcmd_line.ops_file_name, 0, MAX_LENGTH);
  memset(gcmd_line.fct_file_name, 0, MAX_LENGTH);
  memset(gcmd_line.des_file_name, 0, MAX_LENGTH);
  memset(gcmd_line.path, 0, MAX_LENGTH);


  while ( --argc && ++argv ) {
    if ( *argv[0] != '-' || strlen(*argv) != 2 ) {
      return FALSE;
    }
    option = *++argv[0];
    switch ( option ) {
    default:
      if ( --argc && ++argv ) {
        switch ( option ) {
        case 'a':
          a_mode = atoi(*argv);
          break;
        case 'b':
          skip_hc = atoi(*argv);
          break;
        case 'c':
          acl = atoi(*argv);
          break;
        case 'd':
          sscanf( *argv, "%d", &gcmd_line.debug );
          break;
        case 'e':
            stop_on_fail = atoi(*argv);
          break;
        case 'f':
          strncpy( gcmd_line.fct_file_name, *argv, MAX_LENGTH );
          break;
        case 'i':
          sscanf( *argv, "%d", &gcmd_line.display_info );
          break;
        case 'k':
          pp = atoi(*argv);
          break;
        case 'l':
          last = atoi(*argv);
          break;
        case 'n':
          run_cycle = (Bool)atoi(*argv);
          break;
        case 'o':
          strncpy( gcmd_line.ops_file_name, *argv, MAX_LENGTH );
          break;
        case 'p':
          strncpy( gcmd_line.path, *argv, MAX_LENGTH );
          break;
        case 'q':
          proba = atof(*argv);
          break;
        case 's':
          obs_selection = atoi(*argv);
          break;
        case 'v':
          validation = atoi(*argv);
          break;
        case 'w':
          strncpy( gcmd_line.des_file_name, *argv, MAX_LENGTH );
          break;
        default:
          printf( "\nff: unknown option: %c entered\n\n", option );
          return FALSE;
        }
      } else {
        return FALSE;
      }
    }
  }

  return TRUE;

}


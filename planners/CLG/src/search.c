/*********************************************************************
 * (C) Copyright 2001 Albert Ludwigs University Freiburg
 *     Institute of Computer Science
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 *********************************************************************/




/*
 * THIS SOURCE CODE IS SUPPLIED  ``AS IS'' WITHOUT WARRANTY OF ANY KIND, 
 * AND ITS AUTHOR AND THE JOURNAL OF ARTIFICIAL INTELLIGENCE RESEARCH 
 * (JAIR) AND JAIR'S PUBLISHERS AND DISTRIBUTORS, DISCLAIM ANY AND ALL 
 * WARRANTIES, INCLUDING BUT NOT LIMITED TO ANY IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND
 * ANY WARRANTIES OR NON INFRINGEMENT.  THE USER ASSUMES ALL LIABILITY AND
 * RESPONSIBILITY FOR USE OF THIS SOURCE CODE, AND NEITHER THE AUTHOR NOR
 * JAIR, NOR JAIR'S PUBLISHERS AND DISTRIBUTORS, WILL BE LIABLE FOR 
 * DAMAGES OF ANY KIND RESULTING FROM ITS USE.  Without limiting the 
 * generality of the foregoing, neither the author, nor JAIR, nor JAIR's
 * publishers and distributors, warrant that the Source Code will be 
 * error-free, will operate without interruption, or will meet the needs 
 * of the user.
 */










/*********************************************************************
 *
 * File: search.c
 *
 * Description: implementation of routines that search the state space
 *
 *              ADL version, Goal Agenda driven
 *                           Enforced Hill-climbing enhanced with
 *                           informed Goal Added Deletion Heuristic,
 *
 *                           and, alternatively, standard Best First Search
 *
 *
 * Author: Joerg Hoffmann 2000
 *
 *********************************************************************/ 








#include <stdlib.h>

#include "ff.h"

#include "output.h"
#include "memory.h"

#include "relax.h"
#include "search.h"

/* APPLY_WHOLE_SUB_PLAN 1 if the whole heuristic improving sequence is applied
 *                      0 if only the first action is applied      */
#define APPLY_WHOLE_SUB_PLAN 1

/* Osbervation Selection: 0: random selection, 1: manual selection */

/* 1: calcola se lo stato e' presente nell'hash */
#define DEBUG_REPLICATED_STATES 0





/*****************
 * LOCAL GLOBALS *
 *****************/





/* struct tms search_start, search_end; */



/* in agenda driven algorithm, the current set of goals is this
 */
State lcurrent_goals;



/* search space for EHC
 */
EhcNode *lehc_space_head, *lehc_space_end, *lehc_current_start, *lehc_current_end;



/* memory (hash table) for states that are already members
 * of the breadth - first search space in EHC
 */
EhcHashEntry_pointer lehc_hash_entry[EHC_HASH_SIZE];
int lnum_ehc_hash_entry[EHC_HASH_SIZE];
int lchanged_ehc_entrys[EHC_HASH_SIZE];
int lnum_changed_ehc_entrys;
Bool lchanged_ehc_entry[EHC_HASH_SIZE];



/* memory (hash table) for states that are already 
 * encountered by current serial plan
 */
PlanHashEntry_pointer lplan_hash_entry[PLAN_HASH_SIZE];

/* memory (hash table) for states that are already 
 * encountered by current contingent plan
 */
CtgHashEntry_pointer lcontingent_hash_entry[EHC_HASH_SIZE];


/* search space
 */
BfsNode *lbfs_space_head, *lbfs_space_had;



/* memory (hash table) for states that are already members
 * of the best first search space
 */
BfsHashEntry_pointer lbfs_hash_entry[BFS_HASH_SIZE];


/* To avoid repeated sensing */
/* TODO: dynamic association or control visited search space */
/* Bool *sens_list[MAX_BACKTRACKS]; */
Bool *sens_list;
Bool *psl = NULL;
/* int nsl = 0; */

/* branches in plan */
/* int plan_branches = 0; */

/* time counting variables */
struct tms clstart, clend;

static int *added;




/********************************
 * EHC FUNCTION, CALLED BY MAIN *
 ********************************/











Bool do_enforced_hill_climbing( State *start, State *end )

{
  
  static Bool first_call = TRUE;
  static Bool first_kall = TRUE;
  static State S, S_;
  int i, h, h_;
  
  if ( first_call ) {
    /* on first call, initialize plan hash table, search space, search hash table */
    
    for ( i = 0; i < PLAN_HASH_SIZE; i++ ) {
      if (!first_kall) {
        destroy_PlanHashEntry(lplan_hash_entry[i]); 
      }
      
      lplan_hash_entry[i] = NULL;
    }
    /* on subsequent calls, the start is already hashed, as it's the end
     * of the previous calls
     */
    hash_plan_state( start, 0 );
    
    if (first_kall) {

      for ( i = 0; i < EHC_HASH_SIZE; i++ ) {
        lehc_hash_entry[i] = NULL;
        lnum_ehc_hash_entry[i] = 0;
        lchanged_ehc_entry[i] = FALSE;
      }
      lnum_changed_ehc_entrys = 0;
      
      
      lehc_space_head = new_EhcNode();
      lehc_space_end = lehc_space_head;
      
      make_state( &S, gnum_ft_conn ); 
      S.max_F = gnum_ft_conn;
      make_state( &S_, gnum_ft_conn );
      S_.max_F = gnum_ft_conn;
      
      make_state( &lcurrent_goals, gnum_ft_conn );
      lcurrent_goals.max_F = gnum_ft_conn;

    }


    if ( (!a_mode ) && (!validation) )
      first_call = FALSE;

    first_kall = FALSE;
  } 
  
  
  /* Start enforced Hill-climbing
   */
  
  source_to_dest( &lcurrent_goals, end );  
  
  source_to_dest( &S, start );    
  

  h = get_1P_and_H( &S, &lcurrent_goals );
  printf("\n          h:                      %4d            ", h); 
  
  if ( h == INFINITY ) {
    return FALSE;
  }
  if ( h == 0 ) {
    return TRUE;
  }  
  
  h_ = INFINITY;

  if (a_mode) {
    do {
      if ( !search_for_better_state( &S, h, &S_, &h_ ) ) {
	     return FALSE;
	   }
	   source_to_dest( &S, &S_ ); 
	   printf("\n          h:                      %4d  , h_  : %4d       ", h, h_);

      } while ( h < h_ ) ;


    if (1) {

    for ( i = 0; i < EHC_HASH_SIZE; i++ ) {
      destroy_EhcHashEntry(lehc_hash_entry[i]);
     
      lehc_hash_entry[i] = NULL;
      lnum_ehc_hash_entry[i] = 0;
      lchanged_ehc_entry[i] = FALSE;
      }
    lnum_changed_ehc_entrys = 0;

    }

  }
  else
    while ( h != 0 ) {
      if ( !search_for_better_state_classical( &S, h, &S_, &h_ ) ) {
        return FALSE;
      }
      source_to_dest( &S, &S_ );
      h = h_;
      printf("\n          h:                      %4d            ", h);
    }
  h = h_;

  return TRUE;
  
}











/*************************************************
 * FUNCTIONS FOR BREADTH FIRST SEARCH IN H SPACE *
 *************************************************/








Bool search_for_better_state( State *S, int h, State *S_, int *h_ )

{
  
  static Bool first_call = TRUE;
  static State S__;
  
  int i, h__, depth = 0, g;
  EhcNode *tmp;
  
  if ( first_call ) {
    make_state( &S__, gnum_ft_conn );
    S__.max_F = gnum_ft_conn;
    first_call = FALSE;
  }
  
  /* don't hash states, but search nodes.
   * this way, don't need to keep states twice in memory
   */
  tmp = new_EhcNode();
  copy_source_to_dest( &(tmp->S), S);
  hash_ehc_node( tmp );
 
  lehc_current_end = lehc_space_head->next;
  for ( i = 0; i < gnum_H; i++ ) {
    g = result_to_dest( &S__, S, gH[i] );
    if (MINISAT)
      apply_closures_minisat(&S__, FALSE);
    else
      apply_closures(&S__, FALSE); 
    
    add_to_ehc_space( &S__, gH[i], NULL, g );
   }
  lehc_current_start = lehc_space_head->next;

  while ( TRUE ) {  
    if ( lehc_current_start == lehc_current_end ) {
      reset_ehc_hash_entrys();
      {
	tmp = NULL;
      }
      return FALSE;
    }
    
    if ( lehc_current_start->depth > depth ) {
      depth = lehc_current_start->depth;
      if ( depth > gmax_search_depth ) {
        gmax_search_depth = depth;
      }
      printf("[%d]", depth); 
      fflush( stdout );
    }
    h__ = expand_first_node( h );
    if ( LESS( h__, h ) ) {
      break;
    }

 }
  
  reset_ehc_hash_entrys();

    
  extract_Kplan_fragment( S );
 
  
  source_to_dest( S_, &(lehc_current_start->S) );
  *h_ = h__;
  
  return TRUE;
  
}



void add_to_ehc_space( State *S, int op, EhcNode *father, int new_goal )

{
  
  /* see if state is already a part of this search space
   */
  if ( ehc_state_hashed( S ) ) {
    return;
  }
  
  if ( !lehc_current_end ) {
    lehc_current_end = new_EhcNode();
    lehc_space_end->next = lehc_current_end;
    lehc_space_end = lehc_current_end;
  }
  
  copy_source_to_dest( &(lehc_current_end->S), S );
  lehc_current_end->op = op;
  lehc_current_end->father = father;
  if ( !father ) {
    lehc_current_end->depth = 1;
  } else {
    lehc_current_end->depth = father->depth + 1;
  }
  lehc_current_end->new_goal = new_goal;
  
  hash_ehc_node( lehc_current_end );
  
  lehc_current_end = lehc_current_end->next;
  
}


/* Expands the first node of the ehc space */
int expand_first_node( int h )

{
  
  static Bool fc = TRUE;
  static State S_;
  
  int h_, i, g;
  
  if ( fc ) {
    make_state( &S_, gnum_ft_conn );
    S_.max_F = gnum_ft_conn;
    fc = FALSE;
  }
  gtot_nodes++;
  
  /* The heristics of a sensing is given by the father's h minus one */
  if (gop_conn[lehc_current_start->op].action->norm_operator->operatore->is_obs)
    h_ = h-1;
  else 
    h_ = get_1P_and_H( &(lehc_current_start->S), &lcurrent_goals );
  
  if ( h_ == INFINITY ) {
    lehc_current_start = lehc_current_start->next;
    return h_;
  }
  
  if ( lehc_current_start->new_goal != -1 &&
      new_goal_gets_deleted( lehc_current_start ) ) {
    lehc_current_start = lehc_current_start->next;
    return INFINITY;
  }
  
  if ( h_ < h ) {
    return h_;
  }
  
  for ( i = 0; i < gnum_H; i++ ) {
    g = result_to_dest( &S_, &(lehc_current_start->S), gH[i] );
    if (MINISAT)
      apply_closures_minisat(&S_, FALSE);
    else
      apply_closures(&S_,FALSE); 
    add_to_ehc_space( &S_, gH[i], lehc_current_start, g );
  }
  
  lehc_current_start = lehc_current_start->next;
  
  return h_;
  
}



/*********************** Classical FF funtions **********/




/** Function specific to Classical FF 
 *  Calls the 'classical' versions of the functions
 *    i.e. without S-literals   (a_mode = 0) 
 *       
 *  Expands the first node of the ehc space.
 */
int expand_first_node_classical( int h )

{
  
  static Bool fc = TRUE;
  static State S_;
  
  int h_, i, g;
  
  if ( fc ) {
    make_state( &S_, gnum_ft_conn );
    S_.max_F = gnum_ft_conn;
    fc = FALSE;
  }
  gtot_nodes++;
  h_ = get_1P_and_H( &(lehc_current_start->S), &lcurrent_goals );
  
  if ( h_ == INFINITY ) {
    lehc_current_start = lehc_current_start->next;
    return h_;
  }
  
  if ( lehc_current_start->new_goal != -1 &&
      new_goal_gets_deleted( lehc_current_start ) ) {
    lehc_current_start = lehc_current_start->next;
    return INFINITY;
  }
  
  if ( h_ < h ) {
    return h_;
  }
  
  for ( i = 0; i < gnum_H; i++ ) {
    g = result_to_dest_classical( &S_, &(lehc_current_start->S), gH[i] );
    apply_closures2(&S_,FALSE); 
    add_to_ehc_space( &S_, gH[i], lehc_current_start, g );
  }
  
  lehc_current_start = lehc_current_start->next;
  
  return h_;
  
}





/** Function specific to Classical FF 
 *  Calls the 'classical' versions of the functions
 * i.e. without S-literals   (a_mode = 0)        
 */

Bool search_for_better_state_classical( State *S, int h, State *S_, int *h_ )

{
  
  static Bool first_call = TRUE;
  static State S__;
  
  int i, h__, depth = 0, g;
  EhcNode *tmp;
  
  if ( first_call ) {
    make_state( &S__, gnum_ft_conn );
    S__.max_F = gnum_ft_conn;
    first_call = FALSE;
  }
  
  /* don't hash states, but search nodes.
   * this way, don't need to keep states twice in memory
   */
  tmp = new_EhcNode();
  copy_source_to_dest( &(tmp->S), S);
  hash_ehc_node( tmp );
  
  lehc_current_end = lehc_space_head->next;
  for ( i = 0; i < gnum_H; i++ ) {
    g = result_to_dest_classical( &S__, S, gH[i] );
    apply_closures2(&S__, FALSE); 
    
    add_to_ehc_space( &S__, gH[i], NULL, g );
  }
  lehc_current_start = lehc_space_head->next;
  
  while ( TRUE ) {  
    if ( lehc_current_start == lehc_current_end ) {
      reset_ehc_hash_entrys();
      free( tmp );
      tmp = NULL;
      return FALSE;
    }
    
    if ( lehc_current_start->depth > depth ) {
      depth = lehc_current_start->depth;
      if ( depth > gmax_search_depth ) {
        gmax_search_depth = depth;
      }
      printf("[%d]", depth); 
      fflush( stdout );
    }
    h__ = expand_first_node_classical( h );
    if ( LESS( h__, h ) ) {
      break;
    }
  }
  
  reset_ehc_hash_entrys();
  free( tmp );
  tmp = NULL;
  extract_plan_fragment( S );
  
  source_to_dest( S_, &(lehc_current_start->S) );
  *h_ = h__;
  
  return TRUE;
  
}









/********************************************************
 * HASHING ALGORITHM FOR RECOGNIZING REPEATED STATES IN *
 * EHC BREADTH FIRST SEARCH                             *
 ********************************************************/












void hash_ehc_node( EhcNode *n )

{
  
  int i, sum, index;
  EhcHashEntry *h, *prev = NULL;
  
  sum = state_sum( &(n->S) );
  index = sum & EHC_HASH_BITS;
  
  h = lehc_hash_entry[index];
  if ( !h ) {
    h = new_EhcHashEntry();
    h->sum = sum;
    h->ehc_node = n;
    lehc_hash_entry[index] = h;
    lnum_ehc_hash_entry[index]++;
    if ( !lchanged_ehc_entry[index] ) {
      lchanged_ehc_entrys[lnum_changed_ehc_entrys++] = index;
      lchanged_ehc_entry[index] = TRUE;
    }
    return;
  }
  i = 0;
  while ( h ) {
    if ( i == lnum_ehc_hash_entry[index] ) {
      break;
    }
    i++;
    prev = h;
    h = h->next;
  }
  
  if ( h ) {
    /* current list end is still in allocated list of hash entrys
     */
    h->sum = sum;
    h->ehc_node = n;
    lnum_ehc_hash_entry[index]++;
    if ( !lchanged_ehc_entry[index] ) {
      lchanged_ehc_entrys[lnum_changed_ehc_entrys++] = index;
      lchanged_ehc_entry[index] = TRUE;
    }
    return;
  }
  /* allocated list ended; connect a new hash entry to it.
   */
  h = new_EhcHashEntry();
  h->sum = sum;
  h->ehc_node = n;
  prev->next = h;
  lnum_ehc_hash_entry[index]++;
  if ( !lchanged_ehc_entry[index] ) {
    lchanged_ehc_entrys[lnum_changed_ehc_entrys++] = index;
    lchanged_ehc_entry[index] = TRUE;
  }
  return;
  
}



Bool ehc_state_hashed( State *S )

{
  
  int i, sum, index;
  EhcHashEntry *h;
  
  sum = state_sum( S );
  index = sum & EHC_HASH_BITS;
  
  h = lehc_hash_entry[index];
  for ( i = 0; i < lnum_ehc_hash_entry[index]; i++ ) {
    if ( h->sum != sum ) {
      h = h->next;
      continue;
    }
    if ( same_state( &(h->ehc_node->S), S ) ) {
      return TRUE;
    }
    h = h->next;
  }
  
  return FALSE;
  
}


/* Checks whether the atom tagging an inconsistency.
 * Returns TRUE if fail_fact is encountered. */
Bool fail_state_check( State *S )
{
  
  int i;
  Bool faglia = FALSE;
  
  for ( i = 0; i < S->num_F; i++ ) 
    if(S->F[i] == fail_fact) {
      faglia = TRUE;
      break;
    }
  
  return faglia;
}


Bool same_state( State *S1, State *S2 ) 

{
  
  static Bool first_call = TRUE;
  static Bool *in_source;
  
  int i;
  
  if ( first_call ) {
    in_source = ( Bool * ) malloc( gnum_ft_conn * sizeof( Bool ) );
    
    /* Initializes the lists */
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      in_source[i] = FALSE;
    }  
    first_call = FALSE;
  }
  
  if ( S2->num_F != S1->num_F ) 
    return FALSE;
  
  
  for ( i = 0; i < S1->num_F; i++ ) {
    in_source[ S1->F[i] ] = TRUE;
  }
  
  
  for ( i = 0; i < S2->num_F; i++ ) {
    if (in_source[S2->F[i]])
      continue;
    else {
      for ( i = 0; i < S1->num_F; i++ ) 
        in_source[ S1->F[i] ] = FALSE;
      
      return FALSE;
    }
  }
  for ( i = 0; i < S1->num_F; i++ ) 
    in_source[ S1->F[i] ] = FALSE;
  
  return TRUE;
  
}



int state_sum( State *S )

{
  
  int i, sum = 0;
  
  for ( i = 0; i < S->num_F; i++ ) {
    sum += gft_conn[S->F[i]].rand;
  }
  
  return sum;
  
}



void reset_ehc_hash_entrys( void )

{
  
  int i;
  
  for ( i = 0; i < lnum_changed_ehc_entrys; i++ ) {
    lnum_ehc_hash_entry[lchanged_ehc_entrys[i]] = 0;
    lchanged_ehc_entry[lchanged_ehc_entrys[i]] = FALSE;
  }
  lnum_changed_ehc_entrys = 0;
  
}











/***************************************************
 * FUNCTIONS FOR UPDATING THE CURRENT SERIAL PLAN, *
 * BASED ON SEARCH SPACE INFORMATION .             *
 *                                                 *
 * EMPLOY SOMEWHAT TEDIOUS HASHING PROCEDURE TO    *
 * AVOID REPEATED STATES IN THE PLAN               *
 ***************************************************/





void extract_plan_fragment( State *S )

{
  
  EhcNode *i;
  int ops[MAX_PLAN_LENGTH], num_ops;
  State_pointer states[MAX_PLAN_LENGTH];
  int j;
  PlanHashEntry *start = NULL, *i_ph;
  
  num_ops = 0;
  for ( i = lehc_current_start; i; i = i->father ) {
    if ( (start = plan_state_hashed( &(i->S) )) != NULL ) {
      for ( i_ph = start->next_step; i_ph; i_ph = i_ph->next_step ) {
        i_ph->step = -1;
      }
      gnum_plan_ops = start->step;
      break;
    }
    if ( num_ops == MAX_PLAN_LENGTH ) {
      printf("\nincrease MAX_PLAN_LENGTH! currently %d use %d\n\n",
             MAX_PLAN_LENGTH, used_mem);
      exit( 1 );
    }
    states[num_ops] = &(i->S);
    ops[num_ops++] = i->op;
  }
  if ( !start ) {
    start = plan_state_hashed( S );
    if ( !start ) {
      printf("\n\ncurrent start state not hashed! debug me!\n\n");
      exit( 1 );
    }
    if ( start->step == -1 ) {
      printf("\n\ncurrent start state marked removed from plan! debug me!\n\n");
      exit( 1 );
    }
  }
  
  for ( j = num_ops - 1; j > -1; j-- ) {
    if ( gnum_plan_ops == MAX_PLAN_LENGTH ) {
      printf("\nincrease MAX_PLAN_LENGTH! currently %d\n\n",
             MAX_PLAN_LENGTH);
      exit( 1 );
    }
    start->next_step = hash_plan_state( states[j], gnum_plan_ops + 1 );
    start = start->next_step;
    source_to_dest( &(gplan_states[gnum_plan_ops+1]), states[j] );
    gplan_ops[gnum_plan_ops++] = ops[j];
  }
  
}


/** This is search of a 'fragment', and the behaviour
 *  of that functions varies if we are extracting the 
 *  whole plan (usual FF) or just an improving heuristic
 *  sequence (CLG). In the first case, the plan is 
 *  stored in gplan_ops; in the second case, what is
 * stored is the fragment of the plan, that will be 
 * just part of a branch of the final plan.
 * NB: Here we don't store the states of the plan (clg).
 */


void extract_Kplan_fragment( State *S )

{
  
  EhcNode *i;
  int ops[MAX_FRAGMENT_LENGTH], num_ops;
  State_pointer states[MAX_FRAGMENT_LENGTH];
  int j;
  PlanHashEntry *start = NULL, *i_ph;
  
  num_ops = 0;
  for ( i = lehc_current_start; i; i = i->father ) {
    if ( (start = plan_state_hashed( &(i->S) )) != NULL ) {
      for ( i_ph = start->next_step; i_ph; i_ph = i_ph->next_step ) {
        i_ph->step = -1;
      }
      gnum_plan_ops = start->step;
      break;
    }
    if ( num_ops == MAX_FRAGMENT_LENGTH ) {
      printf("\nincrease MAX_FRAGMENT_LENGTH! currently %d\n\n",
             MAX_FRAGMENT_LENGTH);
      exit( 1 );
    }
    states[num_ops] = &(i->S);
    ops[num_ops++] = i->op;
  }
  if ( !start ) {
    start = plan_state_hashed( S );
    if ( !start ) {
      printf("\n\ncurrent start state not hashed! debug me!\n\n");
      exit( 1 );
    }
    if ( start->step == -1 ) {
      printf("\n\ncurrent start state marked removed from plan! debug me!\n\n");
      exit( 1 );
    }
  }
  
  for ( j = num_ops - 1; j > -1; j-- ) {
    if ( gnum_plan_ops >= MAX_FRAGMENT_LENGTH ) {
      printf("\nincrease MAX_FRAGMENT_LENGTH! currently %d\n\n",
             MAX_FRAGMENT_LENGTH);
      exit( -1 );
    }
    start->next_step = hash_plan_state( states[j], gnum_plan_ops + 1 );
    start = start->next_step;
    gplan_ops[gnum_plan_ops++] = ops[j];
  }
  
}




PlanHashEntry *hash_plan_state( State *S, int step )

{
  
  int sum, index;
  PlanHashEntry *h, *tmp;
  
  sum = state_sum( S );
  index = sum & PLAN_HASH_BITS;
  
  for ( h = lplan_hash_entry[index]; h; h = h->next ) {
    if ( h->sum != sum ) continue;
    if ( same_state( S, &(h->S) ) ) break;
  }
  
  if ( h ) {
    if ( h->step != -1 ) {
      printf("\n\nreencountering a state that is already in plan! debug me\n\n");
      exit( 1 );
    }
    h->step = step;
    return h;
  }
  
  for ( h = lplan_hash_entry[index]; h && h->next; h = h->next )
    ;
  
  tmp = new_PlanHashEntry();
  tmp->sum = sum;
  copy_source_to_dest( &(tmp->S), S );
  tmp->step = step;
  
  if ( h ) {
    h->next = tmp;
  } else {
    lplan_hash_entry[index] = tmp;
  }
  
  return tmp;
  
}



PlanHashEntry *plan_state_hashed( State *S )

{
  
  int sum, index;
  PlanHashEntry *h;
  
  sum = state_sum( S );
  index = sum & PLAN_HASH_BITS;
  
  for ( h = lplan_hash_entry[index]; h; h = h->next ) {
    if ( h->sum != sum ) continue;
    if ( same_state( S, &(h->S) ) ) break;
  }
  
  if ( h && h->step != -1 ) {
    return h;
  }
  
  return NULL;
  
}




void propagate_success(State *S) {
  
  CtgHashEntry *hh;
  TreeNode *T;
  
  if ( (hh = ctg_hashed(S)) )
    return;
    
 /*   create new hash entry */
  if (hh->inPlan)
    return;
  
  (hh->sons_to_go)--;
  if (hh->sons_to_go == 0) {
    hh->inPlan = TRUE;
    
    T = hh->treeNode; 
    hh = hash_ctg_state( S, T , hh->father);
    hh->father = hh->father;
    
  }
  
}









/*********************************
 * ADDED GOAL DELETION HEURISTIC *
 *********************************/











Bool new_goal_gets_deleted( EhcNode *n )

{
  
  int i, j, ef, new_goal = n->new_goal;
  
  for ( i = 0; i < gnum_in_plan_E; i++ ) {
    ef = gin_plan_E[i];
    for ( j = 0; j < gef_conn[ef].num_D; j++ ) {
      if ( gef_conn[ef].D[j] == new_goal ) {
        return TRUE;
      }
    }
  }
  
  return FALSE;
  
}





/************************************
 *  K-PLAN PROGRESSION AND STUFF    *
 ************************************/






/** Progresses from the initial state **/
Bool  do_k_plan_search( State *kstart, State *kend )

{
  
  static State S;
  static State Father;
  static State ks;
  static State *other_branches;
  static unsigned int dafare = 0;
  static unsigned int max_branches = MAX_BACKTRACKS;
  static State fixS;
  static Bool fc = TRUE;
  static TreeNode *currentTN;
  static TreeNodeStack *stackTN;

  Bool espuria = FALSE;
        
  /* Last search ended with best first search */
  Bool best_s = FALSE;
  /* Added new branches to 'verify' */
  Bool recently_added = FALSE;
  /* A solution plan was found */
  Bool found_plan = FALSE;
  int ehc_op, i, l;
  int max_ops;
  TreeNode * n;
  
  if (fc) {
    fc = FALSE;
    if (validation || acl) {
      make_state( &fixS, gnum_ft_conn );
      fixS.max_F = gnum_ft_conn;
      
       other_branches = (State*) malloc(max_branches * sizeof(State));
       
      /* on first call, initialize plan hash table
       */
      if (validation == 2)
      for ( i = 0; i < EHC_HASH_SIZE; i++ ) {
        lcontingent_hash_entry[i] = NULL;
      }
    }
 
    currentTN = root; 

    make_state( &S, gnum_ft_conn );
    S.max_F = gnum_ft_conn;
    make_state( &Father, gnum_ft_conn );
    Father.max_F = gnum_ft_conn;
    make_state( &ks, gnum_ft_conn );
    ks.max_F = gnum_ft_conn;
    
    if (gcmd_line.debug == 2) { 
      printf("Init:\n");
      print_state(*kstart); 
    }
    /* Applies closure operators */
    if (acl != 2) { 
      
      if (MINISAT)
        espuria = apply_closures_minisat(kstart, TRUE);
      else
	{
	  espuria = apply_closures2(kstart, TRUE);
	}
    }
    if (gcmd_line.debug == 2) { 
      printf("\n\nInit after closures:\n");
      print_state(*kstart);
    }
  }
  if (MINISAT)
    espuria = apply_closures_minisat(kstart, TRUE);
  else
    {
      /* diff_machine(  &Father,kstart); */
      {
	gadd_list.num_F = 0;
	for ( i = 0; i < kstart->num_F; i++ ) 
	  gadd_list.F[gadd_list.num_F++] = kstart->F[i];
      }
      espuria = apply_closures_with_update(kstart, TRUE);
    }
  
   if (fail_fact >= 0)
     espuria = fail_state_check( kstart ) ;
  
  copy_source_to_dest( &ks, kstart );
  
  if (espuria) {
    printf("\n      Spurious initial state: exiting...\n\n\n");
    plan_branches++;
    plan_espurias++;
    print_plan( );
    exit(0);
  }
  
  if (gcmd_line.debug == 3) {
    printf("\n      Relaxed plan from Init: \n");
    printf("\n      Initial state heuristics    h:              %4d           \n\n",  
           get_1P_and_H( &ks, kend )); 
     exit(0);
  }
  while(TRUE) {
    Bool  hashed = FALSE;

  
    /* Goal check */ 
    if (get_1P_and_H( kstart, kend ) == 0) {
      
      printf("\nGoal reached in a branch \n");
      plan_branches++;
      
      if (dafare == 0) {
        found_plan = TRUE;
        break;
      }
      /* Selects next state */
      copy_source_to_dest( &ks, &(other_branches[ --dafare ]));
      empty_State( &(other_branches[ dafare ]) );
      copy_source_to_dest( kstart, &ks );

      if (pp) {
        
        currentTN =   treeNodeStack_peek(stackTN);
        stackTN = treeNodeStack_pop(stackTN);
      }
      

      /* Here you can put something to eventually reset the plan */
      continue;
    }
    
    /* Here we assume that a goal is not a spurious branch */

    if (espuria) {
      plan_branches++;
      plan_espurias++;
      found_plan = TRUE;
      if (dafare < 1) {
        break;
      }
      /* Selects next state */
      printf("\tPopping state.%d ",dafare);
      {
        currentTN =   treeNodeStack_peek(stackTN);
        stackTN = treeNodeStack_pop(stackTN);
      }
      copy_source_to_dest( &ks, &(other_branches[ --dafare ]));
      copy_source_to_dest( kstart, &ks );
      empty_State( &(other_branches[ dafare ]) );
      
      espuria = 0;
      /* Here you can put something to eventually reset the plan */
      continue;
    }
    
    for (i = 0; i < gnum_plan_ops ; i++) {
      gplan_ops[i] = 0; 
    }
    gnum_plan_ops = 0;
    
    /*  Do the whole EHC search */
    if( !skip_hc ) 
      found_plan = do_enforced_hill_climbing( kstart, kend ) ;
    
    if (BEST_FIRST && (!found_plan || skip_hc )) {
      printf("\n\nEnforced Hill-climbing failed !");
      printf("\nswitching to Best-first Search now.\n");
      best_s = TRUE;
      copy_source_to_dest( &ginitial_state, kstart);
      found_plan = do_best_first_search();
    }
    
    if (!found_plan && !stop_on_fail)
    {
      /* writes fail in the plan */
      printf("\nFail reached in a branch\n");
      plan_branches++;
      plan_failures++;
      if (dafare < 1) {
        found_plan = TRUE;
        break;
      } else {  /* Selects next state */
        copy_source_to_dest( &ks, &(other_branches[ --dafare ]));
        empty_State( &(other_branches[ dafare ]) );
        copy_source_to_dest( kstart, &ks );
        
        if (pp) {
          
          currentTN =   treeNodeStack_peek(stackTN);
          stackTN = treeNodeStack_pop(stackTN);
        }
        
        continue;
      }
    }
    
    best_s = FALSE;
    
    /** Now continues to search on new state,
     * another thing would be to apply the whole plan to the kstate.
     */
    if (found_plan) {
      
      if (APPLY_WHOLE_SUB_PLAN)
        max_ops = gnum_plan_ops;
      else
        max_ops = 1;
      
      
      for( l = 0; l < max_ops; l++) {
        
        ehc_op = gplan_ops[l];
        
        if (pp) {
          /* Creates the Tree structure */
          if(currentTN->is_obs == TRUE) {
            strcpy(currentTN->idy, gop_conn[ehc_op].action->name);
            currentTN->is_obs = FALSE;
          } else {
            n = new_TreeNode(gop_conn[ehc_op].action->name);
            add_TreeNode_son(currentTN, n);
            currentTN = n;
          }
        }
        gnum_kplan_ops++;
        
        ks.max_length++;
	if (ks.max_length > ml)
	  ml = ks.max_length;

        printf("\n::::::::::::::::::::::::current action:::");
        print_op_name( ehc_op );
        printf("\n");
        /**  The selected action is an observation:
         *  execution case: we just select one pf the possible branches,
         *  validation: we carry on all the branches. */
        if (gop_conn[ehc_op].action->norm_operator->operatore->is_obs) {
          if ( ! validation ) {
            int selected_branch;
            
            if (obs_selection) {
              /** Manual selection **/
              selected_branch = observation_manual_selection(&gop_conn[ehc_op]);
            }
            else if (description_file)
              observation_hiddenstate_selection(&gop_conn[ehc_op]);
            else  if (proba == 0)
	      selected_branch =
		(int) ((float)(gop_conn[ehc_op].num_B) *  (rand() / ( RAND_MAX + 1.0)));
	    else {
	      float branch_proba;
	      branch_proba = (rand() / ( RAND_MAX + 1.0)) + (proba - .5);
	      branch_proba = (branch_proba > 1) ? 0.99 :
		( (branch_proba < 0) ? 0 : branch_proba );

	      selected_branch =
		(int) ((float)(gop_conn[ehc_op].num_B) * branch_proba);
            
	    }
 
            /* Prints observation */
            printf("\n Observation selected after action %s:\n\t adding.. ",
                   gop_conn[ehc_op].action->name );
            print_ft_name(gbr_conn[gop_conn[ehc_op].B[selected_branch]].A[0]);
            fflush(stdout);
            
            if (gcmd_line.debug == 2) {
              printf("\n\tBefore action %s:",gop_conn[ehc_op].action->name);
              print_state(ks);
              
              observe_to_dest(&S, &ks, &(gbr_conn[gop_conn[ehc_op].B[selected_branch]]) );
              
              printf("\n\tAfter action %s:",gop_conn[ehc_op].action->name);
              print_state(S);
            } else
              observe_to_dest(&S, &ks, &(gbr_conn[gop_conn[ehc_op].B[selected_branch]]) );
            /* Applies closure operators */
            if (acl != 2) {
              if (MINISAT)
                espuria =  apply_closures_minisat(&S, TRUE);
              else
                espuria =  apply_closures_with_update(&S,TRUE);
            }  
            if (gcmd_line.debug == 2) {
              printf("\n\tAfter closures :");
              print_state(S); 
            }
            if (!espuria) {
              
              if (unhabil > -1) 
                result_to_dest( &ks, &S, unhabil );
              else
                copy_source_to_dest( &ks, &S );
              
              source_to_dest( &gkplan_current_goal, &ks );
              
            } else {
              
              printf("\n      Spurious state encountered;\n");
              printf("\t...printing plan till now and exiting...\n");
              plan_branches++;
              plan_espurias++;
	      printf("Total branches in this plan: %d \n", plan_branches);
	      printf("Total spurious branches in this plan: %d\n",  plan_espurias);
              source_to_dest( &gkplan_current_goal, &ks );
              return FALSE;
              print_plan( );
              exit(0);
              
            }
            
          } else {
            /** we are performing VALIDATION
             * and this is an observation   **/
            
            copy_source_to_dest( &Father, &ks );
            printf("\nBranching....\n");
            
            /* Resets the plan */
            for (i = 0; i < gnum_plan_ops ; i++) {
              gplan_ops[i] = 0; 
            } 
            gnum_plan_ops = 0;
            
            for ( i = 0; i < gop_conn[ehc_op].action->num_branches; i++ ) {
              
              
              observe_to_dest(&ks, &Father, &(gbr_conn[gop_conn[ehc_op].B[i]]) );
              
              
              if (gcmd_line.debug == 2) {
                printf("\n Observation selected after action %s:\n\t adding.. ",
                       gop_conn[ehc_op].action->name );
                print_ft_name(gbr_conn[gop_conn[ehc_op].B[i]].A[0]);
                print_state(ks);
                fflush(stdout);  
              }
              
              /* Applies closure operators */
              if (acl != 2) { 
                
                if (MINISAT)
                  espuria = apply_closures_minisat(&ks, TRUE);
                else
                  espuria =  apply_closures_with_update(&ks,TRUE); 
              }
              
              /* HASH son */
              if (validation == 2)
              {
                CtgHashEntry *goal_found;
                goal_found = ctg_hashed(&ks);
                if ( goal_found ) {
                  if (!(is_ancestor( goal_found , ctg_hashed( &Father )) )) {
                    printf("\nREPEATED STATE\n\n");
                    
                    printf("\nGoal reached in a graph\n");
                    if (pp) {
                      if (goal_found->treeNode->lbl == 0)
                      /* Assigns a label to the node */
                        goal_found->treeNode->lbl = glabel++;
                      
                      sprintf( currentTN->idy, "%s (goto: %d)", currentTN->idy, goal_found->treeNode->lbl);
                    }
                    
                    continue; /* i.e. don't add the node on the branches stack */
                  }
                }
                else 
                  hash_ctg_state( &ks, currentTN,  ctg_hashed(&Father));       
              } /* End of hashing */
              
              
              if (espuria) {
                plan_branches++;
                plan_espurias++;
                found_plan = TRUE;
		espuria = FALSE;
                continue;
              } 
              /* we assume we've a new state without unabil */           
              if ( (int)(other_branches[ dafare ]).F == 0)
                make_state( &(other_branches[ dafare ]), gnum_ft_conn );
              
              if (dafare >= (max_branches-1)) {
                State * tmp_branches;
                
                max_branches *= 2;
                tmp_branches = (State*) realloc ( other_branches, max_branches * sizeof(State));
                if (tmp_branches == NULL) {
                  printf("Out of memory while allocating space for a new branch: exiting......\n\n\n");
                  exit(1);
                }       
                other_branches = tmp_branches;
              }
              copy_source_to_dest( &(other_branches[ dafare++ ]), &ks );
              
              if (pp) {
                /* Creates the Tree structure */     
		n = new_TreeNode("F\0"); 

                n->is_obs = TRUE;
                add_TreeNode_son(currentTN, n);
                stackTN = treeNodeStack_push(stackTN, treeNodeStack_Create(n));
              }
            }
            recently_added = TRUE;
            
          }
          /* This is because when there is a branching, the ehc has to restart */
          l = max_ops;
        } else { 
          /* It is not and observation */
          
          if (gcmd_line.debug == 2) {
            printf("\n\tBefore action %s:",gop_conn[ehc_op].action->name);
            print_state(ks);
            result_to_dest( &S, &ks, ehc_op );
            printf("\n\tAfter action %s:",gop_conn[ehc_op].action->name);
            
            print_state(S);
            
            if (MINISAT)
              espuria = apply_closures_minisat(&S, TRUE);
            else
              espuria =  apply_closures(&S,TRUE); 
            
            printf("\n\tAfter the closures of action %s:", gop_conn[ehc_op].action->name);
            print_state(S);
            
          } else
            result_to_dest( &S, &ks, ehc_op );
          /*  if (gop_conn[ehc_op].is_clos) {
           copy_source_to_dest( kstart, &S );  
           continue;
           } */
          if (acl == 1) {
            if (MINISAT)
              espuria = apply_closures_minisat(&S, TRUE);
            else
              espuria =  apply_closures(&S,TRUE); 
            /* if (fail_fact >= 0)    */
            /*   espuria = fail_state_check( &S ); */
          }  
          
          /* HASH state */
          if (validation == 2)
          {
            CtgHashEntry* goal_found;
            goal_found = ctg_hashed(&S);
            if ( goal_found ) {
              
              if( !(is_ancestor( goal_found , ctg_hashed(&ks) )) ) {               
                printf("\nGoal reached in a graph\n");
                if (pp) {
                  if (goal_found->treeNode->lbl == 0)
                  /* Assigns a label to the node */
                    goal_found->treeNode->lbl = glabel++;
                  
                  sprintf( currentTN->idy, "%s (%d)", currentTN->idy, goal_found->treeNode->lbl);
                }
                
                if (dafare < 1) {
                  found_plan = TRUE;
                  goto nomorebranches;
                  
                } else { 
                  /* Selects next state */
                  copy_source_to_dest( &ks, &(other_branches[ --dafare ]));
                  empty_State( &(other_branches[ dafare ]) );
                  copy_source_to_dest( kstart, &ks );
                  
		  /*   gnum_kplan_ops++; */
                  
                  if (pp) {
                    currentTN =   treeNodeStack_peek(stackTN);
                    stackTN = treeNodeStack_pop(stackTN);
                  }
                  l = max_ops;
                  break;
                }
              }
              
            }
            else  hash_ctg_state( &S, currentTN,  ctg_hashed(&ks));
            
          } /* end of hashing */
          
          
          if (espuria) {
            if (!validation) {
              printf("\n      Spurious state encountered;\n");
              printf("\t...printing plan till now and exiting...\n");
              plan_branches++;
              plan_espurias++;
              printf("Total spurious branches: %d\n",plan_espurias);
              printf("Total spurious branches: %d\n",plan_espurias);

              source_to_dest( &gkplan_current_goal, &ks );
              return FALSE;
              print_plan( );
              exit(0);
              
            } else {
              copy_source_to_dest( kstart, &S );  
              break;
            }
          }
          if (unhabil > -1) 
            result_to_dest( &ks, &S, unhabil );
          else
            copy_source_to_dest( &ks, &S );
          
          source_to_dest( &gkplan_current_goal, &ks );
          
        }
        
        if (recently_added) {
          
	  /*          gnum_kplan_ops++;  */
          
          if (pp) {
            
            currentTN = treeNodeStack_peek(stackTN);
            stackTN = treeNodeStack_pop(stackTN);
          }    
          
          /* Pops next state from verification list */
          printf("\tPopping state:%d",dafare);
          
          copy_source_to_dest( &ks, &(other_branches[ --dafare ]) );
          
          source_to_dest( &gkplan_current_goal, &ks );
          
          empty_State( &(other_branches[ dafare ]) );
          recently_added = FALSE;
        }
        copy_source_to_dest( kstart, &ks );  
      }
    } else break;
  }
  
nomorebranches:
  return found_plan;
}




/** All S-lterals have value 0 in the index
 * the other ones, value -1. */
void  initialize_S_index()
{
  int i;
  
  gS_index = (int *) malloc((gnum_ft_conn) * sizeof(int));
  S_counter = (int *) malloc((gnum_ft_conn) * sizeof(int));
  
  for ( i = 0; i < gnum_ft_conn; i++ ) {
    S_counter[i] = 0;
    if ( strncmp("S_", gpredicates[(grelevant_facts[i]).predicate], 2 ) == 0) {
      gS_index[i] = 0;
    }
    else {
      gS_index[i] = -1;
    }
  } 
}



int update_S_counter(State *kstart)
{
  
  static Bool first_call = TRUE;
  S_adds tmp;
  int i, j;
  int num_added = 0;
  
  if ( first_call ) {
    added = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    
    first_call = FALSE;
  }
  
  for ( i = 0; i < kstart->num_F; i++ ) 
    if (gS_index[ kstart->F[i] ] == -1) {
      tmp =  gSlt_ft_conn[ kstart->F[i] ];
      for (j = 0; j < tmp.num_S; j++) {
        S_counter[ tmp.sl_t[j] ]++; 
        added[ num_added++ ] = tmp.sl_t[j];
      }
    }
  
  return num_added;
}



void reset_S_counter(int num_added) {
  int i;
  /* unset infos
   */
  for ( i = 0; i < num_added; i++ ) 
    S_counter[added[i]] = 0;
  
}



/************************************
 * BEST FIRST SEARCH IMPLEMENTATION *
 ************************************/




Bool do_best_first_search()

{
  
  static Bool fc = TRUE;
  static State S;
  Bool espuria = FALSE;
  
  BfsNode *first;
  int i, min = INFINITY;
  Bool start = TRUE;
  int h = INFINITY;
  
  if ( fc ) {
    make_state( &S, gnum_ft_conn );
    S.max_F = gnum_ft_conn;
    /* if (!a_mode)*/
      fc = FALSE;
    
    
    lbfs_space_head = new_BfsNode();

    if ((a_mode)|| fc) {
    lbfs_space_had = NULL;
    
    for ( i = 0; i < BFS_HASH_SIZE; i++ ) {
      lbfs_hash_entry[i] = NULL;
    }}

  } else {

    for ( i = 0; i < BFS_HASH_SIZE; i++ ) {
      destroy_BfsHashEntry(lbfs_hash_entry[i]);
     
      lbfs_hash_entry[i] = NULL;
      }

  }

  add_to_bfs_space( &ginitial_state , -1, NULL );
 
  while ( TRUE ) {
    if ( (first = lbfs_space_head->next) == NULL ) {
      printf("\n\nbest first search space empty! problem proven unsolvable.\n\n");
      return FALSE;
    }
    
    lbfs_space_head->next = first->next;
    if ( first->next ) {
      first->next->prev = lbfs_space_head;
    }
    
    if ( LESS( first->h, min ) ) {
      min = first->h;

      if ( start ) {
        printf("\nadvancing to distance : %4d", min);
        h = min;
	start = FALSE;
      } else {
        printf("\n                        %4d", min);
      }
    }
    
    if ( first->h == 0 ) {
      break;
    }
    if ((a_mode) && (first->h < h))
      break;

    get_A( &(first->S) );
    for ( i = 0; i < gnum_A; i++ ) {
      result_to_dest( &S, &(first->S), gA[i] );
      if (MINISAT)
        espuria = apply_closures_minisat(&S, TRUE);
      else
        espuria = apply_closures(&S, TRUE);
      if (espuria) { 
	espuria = FALSE;
        continue;
      }
      add_to_bfs_space( &S, gA[i], first );
    }
    
    first->next = lbfs_space_had;
    lbfs_space_had = first;
  }
  
  extract_plan( first );


  return TRUE;
  
}



void add_to_bfs_space( State *S, int op, BfsNode *father )

{
  
  BfsNode *new, *i;
  int h;
  
  /* see if state is already a part of this search space
   */
  if ( bfs_state_hashed( S ) ) {
    return;
  }
  
  h = get_1P( S, &ggoal_state );
  
  if ( h == INFINITY ) {
    return;
  }
  
  for ( i = lbfs_space_head; i->next; i = i->next ) {
    if ( i->next->h > h ) break;
  }
  
  new = new_BfsNode();
  copy_source_to_dest( &(new->S), S );
  new->op = op;
  new->h = h;
  new->father = father;
  
  new->next = i->next;
  new->prev = i;
  i->next = new;
  if ( new->next ) {
    new->next->prev = new;
  }
  
  hash_bfs_node( new );

}


/* Greedy search algorithm */
void add_to_grs_space( State *S, int op, BfsNode *father )

{
  
  BfsNode *new, *i;
  int h;
  
  /* see if state is already a part of this search space
   */
  if ( bfs_state_hashed( S ) ) {
    return;
  }
  
  h = get_1P( S, &ggoal_state );
  
  if ( h == INFINITY ) {
    return;
  }
  
  /* S is put at head of list 
   * (ordered between successors of the same state) */
  for ( i = lbfs_space_head; i->next; i = i->next ) {
    if ((father != i->father) &&  ( i->next->h > h ))
      break;
  }
  new = new_BfsNode();
  copy_source_to_dest( &(new->S), S );
  new->op = op;
  new->h = h;
  new->father = father; 
  
  new->next = i->next;
  new->prev = i;
  i->next = new;
  
  if (gop_conn[op].action && (gop_conn[op].action->norm_operator->operatore->is_obs)) 
    copy_source_to_dest( &(new->kstate), &(father->S) );
  else
    copy_source_to_dest( &(new->kstate), S );
  
  if ( new->next ) {
    new->next->prev = new;
  }
  
  hash_bfs_node( new );
}




/* Insert into subspace-list the node S */
BfsNode *add_to_grs_subspace(  BfsNode *subspace, State *S, int op, BfsNode *father )
{
  
  BfsNode *new, *i;
  int h;
  
  /* see if state is already a part of this search space
   */
  if ( bfs_state_hashed( S ) ) {
    return subspace;
  }
  
  h = get_1P( S, &ggoal_state );
  
  if ( h == INFINITY ) {
    /* TODO: maybe exit, maybe return subspace */
    return  new_BfsNode();
  }
  
  /* S is put at head of list 
   * (ordered between successors of the same state) */
  if (subspace)
    for ( i = subspace; i->next; i = i->next ) {
      if ( i->next->h > h )
        break;
    }
  new = new_BfsNode();
  copy_source_to_dest( &(new->S), S );
  new->op = op;
  new->h = h;
  new->father = father;
  
  if (subspace) {
    new->next = i->next;
    new->prev = i;
    i->next = new;
  } else subspace = new;
  
  if (gop_conn[op].action->norm_operator->operatore->is_obs)
    copy_source_to_dest( &(new->kstate), &(father->kstate) );
  else {
    make_state( &(new->kstate), gnum_ft_conn );
    new->kstate.max_F = gnum_ft_conn;
    result_to_dest(  &(new->kstate),  &(father->kstate), op );
  }
  
  if ( new->next ) {
    new->next->prev = new;
  }
  
  hash_bfs_node( new );
  
  return new;
}




void delete_grs_subspace( BfsNode *subspace)
{
  BfsNode *i, *f;
  f = subspace;
  
  if (!subspace)
    return;
  
  for ( i = subspace; i->next; i = i->next ) {
    i->father = NULL;
    i->prev->next = NULL;
    i->prev = NULL;
    i->op = -1;
    i->h = INFINITY;
    i->S.F = NULL; 
    i->S.num_F = 0;
    i->S.max_F = 0;
    i->kstate.F = NULL; 
    i->kstate.num_F = 0;
    i->kstate.max_F = 0;
  }
  if (i) {
    i->father = NULL;
    i->prev = NULL;
    i->op = -1;
    i->h = INFINITY;
    i->S.F = NULL; 
    i->S.num_F = 0;
    i->S.max_F = 0;
    i->kstate.F = NULL; 
    i->kstate.num_F = 0;
    i->kstate.max_F = 0;
    /*  i = NULL; */
  }
  return;
}



void extract_plan( BfsNode *last )

{
  
  BfsNode *i;
  int ops[MAX_PLAN_LENGTH], num_ops;
  int j;
  
  num_ops = 0;
  for ( i = last; i->op != -1; i = i->father ) {
    if ( num_ops == MAX_PLAN_LENGTH ) {
      printf("\nincrease MAX_PLAN_LENGTH!... currently %d\n\n",
             MAX_PLAN_LENGTH);
      exit( 1 );
    }
    ops[num_ops++] = i->op;
  }
  
  gnum_plan_ops = 0;
  for ( j = num_ops - 1; j > -1; j-- ) {
    gplan_ops[gnum_plan_ops++] = ops[j];
  }
  
}



/** Manual op selection **/
int operator_manual_selection(void)

{
  int result, i, r, k = 0;
  
  /* Prints operatore list */
  printf("\n Nodes to expand: \n");
  for ( i = 0; i < gnum_A; i++ ) {
    /* skip if it's an observation */
    printf("\n%d) \t operator: ",i);
    print_op_name(gA[i]);
  }
  printf("\n%d) \t Stop plan search\n", i);
  /* Select an operator */
  do {   
    fprintf(stdout, "\nChoose a number among %d--%d (%d to stop): ",0,i,i);
    r = scanf("%d", &k);
    getchar();
    fflush(stdin);
    if (k == i) {
      r = EOF;
    }
  } while ( ((k > i) || (k < 0)) && (r != EOF) );
  if (r == EOF)
  {
    printf("\nError! no value entered or no executable action\n exiting...............\n");
    exit(1);
  }
  /*assert(k);*/
  
  result = gA[k];
  
  return result;
}


/** Manual obs selection **/
int observation_manual_selection(OpConn * a)

{
  int i, r, k = 0;
  
  /* Prints operator list */
  printf("\n Available branches: \n");
  
  for ( i = 0; i < a->num_B; i++ ) {
    printf("\n%d) \t branch's first fact: ",i);
    print_ft_name(gbr_conn[a->B[i]].A[0]);
  }
  printf("\n%d) \t Stop it.\n", i);
  /* Select an operator */
  do {   
    fprintf(stdout, "\nChoose a number among %d--%d (%d to stop): ",0,i,i);
    r = scanf("%d", &k);
    getchar();
    fflush(stdin);
    if (k == i) {
      r = EOF;
    }
  } while ( (k > i) && (r != EOF) && (k < 0) );
  if (r == EOF)
  {
    printf("\nError! no value entered or no executable action\n exiting...............\n");
    exit(1);
  }
  /*assert(k);*/
  
  return k;
}



/** File obs selection. Checks the branches, if an atom in the 
 * hidden state file is present, it returns that branch. 
 * If nothing is encountered, it returns a negative branch. **/
int observation_hiddenstate_selection(OpConn * a)
{
  int ret, i, j, b;
  
  /* Checks the branches that contain the facts in the hidden state */
  for ( i = 0; i < a->num_B; i++ ) {
    b = a->B[i]; /* the branch */
    
    for (j= 0; j < gbr_conn[ b ].num_A; j++)
      if (quita_branch[ gbr_conn[b].A[j] ]) 
        return b;
      else if (strncmp( (char*)"KN",  (char*)name_Fact(&grelevant_facts[gbr_conn[b].A[j]]), 2) == 0)
        ret = b;
  }
  
  return ret;
  
}





/***********************************
 **** sensing list functions    ****
 ***********************************/
Bool * make_sens_list()
{
  Bool * sl;
  int r;
  
  sl = ( Bool * ) malloc( gnum_op_conn * sizeof( Bool ) );
  
  for(r = 0; r < gnum_op_conn; r++) 
    sl[r] = FALSE;
  
  return sl;
}





/** Checks whether the op is already into sens_list
 * i.e. the list of already performed sensing actions.
 * if ins is FALSE, whether the op is already into sens_list.
 * if ins is TRUE, it inserts the op into the list
 **/
Bool sensed(int sop, Bool ins)
{
  if (sop < -1)
    exit(sop);
  
  if(ins) 
    psl[sop] = TRUE;
  else {
    if (psl[sop])
      return TRUE;
    else
      return FALSE;
  }
  return TRUE;
}








/************************************************************
 * HASHING ALGORITHM FOR RECOGNIZING REPEATED STATES IN BFS *
 ************************************************************/




void show_hash_table()
{
  int index = 0;
  int j;
  
  printf("\n\n\t\tHASH TABLE :::::\n");
  for ( j = 0; j < BFS_HASH_SIZE; j++ ) 
    if (lbfs_hash_entry[j]) {
      print_state(  (lbfs_hash_entry[j])->bfs_node->S );
      index++;
    }
  printf("\t\t HASH SIZE: %d\n", index);
  return ;
}





/* useless function. used to save some operations. */
void hash_table_operations()
{
  /* memory (hash table) for states that are already members
   * of the best first search space
   */
  BfsHashEntry_pointer auxbfs_hash_entry[BFS_HASH_SIZE];
  
  BfsNode *tmp, *tmp2; 
  int i;
  
  /* Make a copy of the hash table */
  for ( i = 0; i < BFS_HASH_SIZE; i++ )
    if (lbfs_hash_entry[i]) {
      auxbfs_hash_entry[i] = new_BfsHashEntry();
      /* new node */
      tmp = new_BfsNode();
      tmp2 = lbfs_hash_entry[i]->bfs_node;
      
      copy_source_to_dest( &(tmp->S), &(tmp2->S) );
      tmp->h = tmp2->h;
      tmp->father = tmp2->father;
      tmp->op = tmp2->op;
      tmp->next = tmp2->next;
      tmp->prev = tmp2->prev;
      auxbfs_hash_entry[i]->bfs_node = new_BfsNode();
      auxbfs_hash_entry[i]->bfs_node = tmp;
      auxbfs_hash_entry[i]->sum = lbfs_hash_entry[i]->sum;
      auxbfs_hash_entry[i]->next = lbfs_hash_entry[i]->next;
    }
  
  
  
  /* Revert the hash table */
  for ( i = 0; i < BFS_HASH_SIZE; i++ ) 
    if (auxbfs_hash_entry[i]) {
      tmp = new_BfsNode();
      tmp2 = auxbfs_hash_entry[i]->bfs_node;
      
      copy_source_to_dest( &(tmp->S), &(tmp2->S) );
      tmp->h = tmp2->h;
      tmp->father = tmp2->father;
      tmp->op = tmp2->op;
      tmp->next = tmp2->next;
      tmp->prev = tmp2->prev;
      lbfs_hash_entry[i]->bfs_node = tmp;
      lbfs_hash_entry[i]->sum = auxbfs_hash_entry[i]->sum;
      lbfs_hash_entry[i]->next = auxbfs_hash_entry[i]->next;
    } 
    else lbfs_hash_entry[i] = NULL;
}


void hash_bfs_node( BfsNode *n )

{
  
  int sum, index;
  BfsHashEntry *h, *tmp;
  
  sum = state_sum( &(n->S) );
  index = sum & BFS_HASH_BITS;
  
  h = lbfs_hash_entry[index];
  if ( !h ) {
    h = new_BfsHashEntry();
    h->sum = sum;
    h->bfs_node = n;
    lbfs_hash_entry[index] = h;
    return;
  }
  for ( ; h->next; h = h->next );
  
  tmp = new_BfsHashEntry();
  tmp->sum = sum;
  tmp->bfs_node = n;
  h->next = tmp;
  
}



Bool bfs_state_hashed( State *S )

{
  
  int sum, index;
  BfsHashEntry *h;
  
  sum = state_sum( S );
  index = sum & BFS_HASH_BITS;
  
  h = lbfs_hash_entry[index];
  for ( h = lbfs_hash_entry[index]; h; h = h->next ) {
    if ( h->sum != sum ) {
      continue;
    }
    if ( same_state( &(h->bfs_node->S), S ) ) {
      return TRUE;
    }
  }
  
  return FALSE;
  
}





/**** Contingent Hashing functions **/


CtgHashEntry *ctg_hashed( State *S )

{
  int sum, index;
  CtgHashEntry *h;
  
  sum = state_sum( S );
  index = sum & EHC_HASH_BITS;
  
  for ( h = lcontingent_hash_entry[index]; h; h = h->next ) {
    if ( h->sum != sum ) {
      continue;
    }
    if ( same_state( &(h->S), S ) ) {
      return h;
    }
  }
  
  return NULL;
  
}




void show_ctghash_table()
{
  int index = 0;
  int j;
  
  printf("\n\n\t\tHASH TABLE :::::\n");
  for ( j = 0; j < EHC_HASH_SIZE; j++ ) 
    if (lcontingent_hash_entry[j]) {
     ++index;
    }
  printf("\t\t HASH SIZE: %d\n", index);
  return ;
}

CtgHashEntry *hash_ctg_state( State *S, TreeNode *tn, CtgHashEntry *Father )

{
  
  int sum, index;
  CtgHashEntry *h, *tmp;
  
  sum = state_sum( S );
  index = sum & EHC_HASH_BITS;
    
  for ( h = lcontingent_hash_entry[index]; h && h->next; h = h->next )
    ;
  
  tmp = new_CtgHashEntry();
  tmp->sum = sum;
  copy_source_to_dest( &(tmp->S), S );
  tmp->treeNode = tn;
  tmp->inPlan = FALSE;
  tmp->father = Father;
  
  if ( h ) {
    h->next = tmp;
  } else {
    lcontingent_hash_entry[index] = tmp;
  }
  
  return tmp;
  
}




/****************************
 * STATE HANDLING FUNCTIONS *
 ****************************/





/* function that computes state transition as induced by a
 * normalized ADL action. 
 * This function doesn't take the preconditions in account:
 * so only the effects are added (or deleted). Adds go before deletes!
 */
int onlyeff_to_dest( State *dest, State *source, int op )

{
  
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest, *in_del, *true_ef;
  static int *del, num_del;
  
  int i, j, ef;
  int r = -1;
  
  if ( first_call ) {
    in_source = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_del = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    true_ef = ( Bool * ) calloc( gnum_ef_conn, sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      in_source[i] = FALSE;
      in_dest[i] = FALSE;
      in_del[i] = FALSE;
    }
    for ( i = 0; i < gnum_ef_conn; i++ ) {
      true_ef[i] = FALSE;
    }
    first_call = FALSE;
  }
  
  /* setup true facts for effect cond evaluation
   */
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = TRUE;
  }
  
  /* setup deleted facts
   */
  num_del = 0;
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    ef = gop_conn[op].E[i];
    
    for ( j = 0; j < gef_conn[ef].num_D; j++ ) {
      if ( in_del[gef_conn[ef].D[j]] ) continue;
      in_del[gef_conn[ef].D[j]] = TRUE;
      del[num_del++] = gef_conn[ef].D[j];
    }
  }
  
  /* put all non-deleted facts from source into dest.
   * need not check for put-in facts here,
   * as initial state is made doubles-free, and invariant keeps
   * true through the transition procedure
   */
  dest->num_F = 0;
  for ( i = 0; i < source->num_F; i++ ) {
    if ( in_del[source->F[i]] ) {
      continue;
    }
    dest->F[dest->num_F++] = source->F[i];
    in_dest[source->F[i]] = TRUE;
  }
  
  /* now, finally, add all fullfilled effect adds to dest; 
   * each fact at most once!
   */
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    ef = gop_conn[op].E[i];
    for ( j = 0; j < gef_conn[ef].num_A; j++ ) {
      if ( in_dest[gef_conn[ef].A[j]] ) {
        continue;
      }
      dest->F[dest->num_F++] = gef_conn[ef].A[j];
      in_dest[gef_conn[ef].A[j]] = TRUE;
      if ( gft_conn[gef_conn[ef].A[j]].is_global_goal ) {
        r = gef_conn[ef].A[j];
      }
    }
  }
  dest->max_length = source->max_length;
  /* unset infos
   */
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = FALSE;
  }
  for ( i = 0; i < dest->num_F; i++ ) {
    in_dest[dest->F[i]] = FALSE;
  }
  for ( i = 0; i < num_del; i++ ) {
    in_del[del[i]] = FALSE;
  }
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    true_ef[i] = FALSE;
  }
  
  return r;
  
}



void  print_the_counter(void)
{
  int i;
  
  printf("\n\n\tPRINTING THE COUNTER\n");
  
  for (i=0;i< gnum_ft_conn;i++) {
    
    if ((S_counter[i] != 0)){
      print_ft_name(i);
      
      printf(": %d\n",S_counter[i]);
    }
  }
  return;  
}

void diff_machine(State *new, State *old)
{
  int i;
  static Bool fc = TRUE;
  static Bool *in_dest;
  
  if (fc) {
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    
    for ( i = 0; i < gnum_ft_conn; i++ ) 
      in_dest[i] = FALSE;
    
    fc = FALSE;
  }
  
  for ( i = 0; i < new->num_F; i++ )
    in_dest[new->F[i]] = TRUE;
  
  gadd_list.num_F = 0;
  
  for ( i = 0; i < old->num_F; i++ ) {
    if (in_dest[old->F[i]])
      continue;
    else
      gadd_list.F[gadd_list.num_F++] = old->F[i];
  }
  
  /* unset infos
   */
  
  for ( i = 0; i < new->num_F; i++ ) {
    in_dest[new->F[i]] = FALSE;
  }
  
  printf("\n\tPRINTING LIST\n"); /* Debug purposes */
  for (i = 0; i < gadd_list.num_F; i++) {
    print_ft_name(gadd_list.F[i]);
    printf("\t");
  }
  
}




/* function that computes state transition as induced by a
 * normalized action branches. Adds go before deletes!
 *
 * a bit odd in implementation:
 * function returns number of new goal that came in when applying
 * op to source; needed for Goal Added Deletion Heuristic
 */
int observe_to_dest( State *dest, State *source, EfConn * bref)

{
  
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest, *in_del;
  static int *del, num_del;
  
  int i, j;
  int r = -1;
  S_adds tmp;
  int a;
  int num_added = 0;
  
  
  if ( first_call ) {
    in_source = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_del = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      in_source[i] = FALSE;
      in_dest[i] = FALSE;
      in_del[i] = FALSE;
    }
    first_call = FALSE;
  }
  
  /* setup true facts for effect cond evaluation
   */
  for ( i = 0; i < source->num_F; i++ ) 
    in_source[source->F[i]] = TRUE;
  
  num_added = update_S_counter(source);
  
  /* setup deleted facts
   */
  num_del = 0;
  
  for ( j = 0; j < bref->num_PC; j++ ) {
    if ( !in_source[ bref->PC[j] ] ) break;
  }
  if ( j < bref->num_PC ) {
    printf("\nWarning: a problem with preconditions of an observation.\n");
  }
  
  
  for ( j = 0; j < bref->num_D; j++ ) {
    if ( in_del[ bref->D[j] ] || !in_source[ bref->D[j] ] ) continue; 
    in_del[ bref->D[j] ] = TRUE;
    del[ num_del++ ] = bref->D[j];
    
    if (gS_index[del[num_del-1]] == -1) {
      tmp =  gSlt_ft_conn[ del[num_del-1] ];
      for (i = 0; i < tmp.num_S; i++) 
        if  (--S_counter[ tmp.sl_t[i] ] == 0) { 
          if ( in_del[ tmp.sl_t[i] ]) continue; 
          in_del[ tmp.sl_t[i] ] = TRUE;
          del[num_del++] = tmp.sl_t[i];
        }
    }
  }
  fflush(stdout); 
  
  /* put all non-deleted facts from source into dest.
   * need not check for put-in facts here,
   * as initial state is probably doubles-free, and invariant keeps
   * true through the transition procedure
   */
  dest->num_F = 0;
  gadd_list.num_F = 0; /* Can be put with the delete */
  
  for ( i = 0; i < source->num_F; i++ ) {
    if ( in_del[source->F[i]] ) {
      continue;
    }
    dest->F[dest->num_F++] = source->F[i];
    in_dest[source->F[i]] = TRUE;
  }
  
  
  for ( j = 0; j < bref->num_A; j++ ) {
    a = bref->A[j];
    if ( in_dest[ a ] ) {
      continue;
    }
    if ( !MINISAT ) gadd_list.F[gadd_list.num_F++] = a;
    dest->F[dest->num_F++] = a;
    in_dest[a ] = TRUE;
    if ( gft_conn[ a ].is_global_goal ) {
      r = a;
    }
  }
    dest->max_length = source->max_length;

  /* unset infos
   */
  
  reset_S_counter(num_added);
  num_added = 0;
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = FALSE;
  }
  for ( i = 0; i < dest->num_F; i++ ) {
    in_dest[dest->F[i]] = FALSE;
  }
  for ( i = 0; i < num_del; i++ ) {
    in_del[del[i]] = FALSE;
  }
  
  
  return r;
  
}


void inconsistency( State *st) {
  int i,j;

  for ( i = 0; i < st->num_F; i++ ) {
     for ( j = i+1; j < st->num_F; j++ ) {
       if (st->F[i] == gnot_corr[ st->F[j] ]) {
	 printf("\n\t\t INCONSIST!!  ");
	 printf("\n              %s vs. %s\n\n",
		gpredicates[grelevant_facts[ st->F[i] ].predicate],
		gpredicates[grelevant_facts[ st->F[j] ].predicate]);
	 break;
       }
     }
  }
}




/* function that computes state transition as induced by a
 * normalized ADL action. Adds go before deletes!
 *
 * a bit odd in implementation:
 * function returns number of new goal that came in when applying
 * op to source; needed for Goal Added Deletion Heuristic
 */
int result_to_dest( State *dest, State *source, int op )

{
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest, *in_del, *true_ef;
  static int *del, num_del;
  
  int i, j, ef, a, k;
  int r = -1;
  int num_added = 0;
  S_adds tmp;
  
  if ( first_call ) {
    in_source = ( Bool * ) malloc( gnum_ft_conn * sizeof( Bool ) );
    in_dest = ( Bool * ) malloc( gnum_ft_conn * sizeof( Bool ) );
    in_del = ( Bool * ) malloc( gnum_ft_conn * sizeof( Bool ) );
    true_ef = ( Bool * ) malloc( gnum_ef_conn * sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      in_source[i] = FALSE;
      in_dest[i] = FALSE;
      in_del[i] = FALSE;
    }
    for ( i = 0; i < gnum_ef_conn; i++ ) {
      true_ef[i] = FALSE;
    }
    first_call = FALSE;
  }
  
  /* setup true facts for effect cond evaluation
   */
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = TRUE;
  }
  
  /* Updates the counter */
  num_added = update_S_counter(source);  
  
  /* setup deleted facts
   */
  num_del = 0;
  
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    ef = gop_conn[op].E[i];

    for ( j = 0; j < gef_conn[ef].num_PC; j++ ) {
      if ( !in_source[gef_conn[ef].PC[j]] ) break;
    }
    if ( j < gef_conn[ef].num_PC ) continue;
    true_ef[i] = TRUE;

    for ( j = 0; j < gef_conn[ef].num_D; j++ ) {
      if ( in_del[gef_conn[ef].D[j]] || !in_source[gef_conn[ef].D[j]] ) continue;
      in_del[gef_conn[ef].D[j]] = TRUE;
      del[num_del++] = gef_conn[ef].D[j];
      if (gS_index[del[num_del-1]] == -1) {
        tmp =  gSlt_ft_conn[ del[num_del-1] ];
        for (k = 0; k < tmp.num_S; k++) 
          if  (--S_counter[ tmp.sl_t[k] ] == 0) { 
            if ( in_del[ tmp.sl_t[k] ]) continue; 
            in_del[ tmp.sl_t[k] ] = TRUE;
            del[num_del++] = tmp.sl_t[k];
          }
      }
    }
  }
  
  
  /* put all non-deleted facts from source into dest.
   * need not check for put-in facts here,
   * as initial state is made doubles-free, and invariant keeps
   * true through the transition procedure
   */
  dest->num_F = 0;
  gadd_list.num_F = 0;
  
  
  for ( i = 0; i < source->num_F; i++ ) {
    if ( in_del[source->F[i]] ) {
      if (!MINISAT) gadd_list.F[gadd_list.num_F++] = source->F[i];
      continue;
    }
    dest->F[dest->num_F++] = source->F[i];
    in_dest[source->F[i]] = TRUE;
  }
  
  
  /* now, finally, add all fullfilled effect adds to dest; 
   * each fact at most once!
   */
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    if ( !true_ef[i] ) continue;
    ef = gop_conn[op].E[i];
    for ( j = 0; j < gef_conn[ef].num_A; j++ ) {
      if ( in_dest[gef_conn[ef].A[j]] ) {
        continue; /* Avoid duplicates */
      }
      if ( in_del[gef_conn[ef].A[j]] ) {
	/* There is a deleted-then-added fact */
	printf ("\nWarning, addind and deleting ");
	print_ft_name(gef_conn[ef].A[j]);
	printf ("\n");
	continue;
      }
      a = gef_conn[ef].A[j];
      dest->F[dest->num_F++] = a;
      if (!MINISAT) gadd_list.F[gadd_list.num_F++] = a;
      in_dest[ a ] = TRUE;
      if ( gft_conn[ a ].is_global_goal ) {
        r = a;
      }
    }
  }
    dest->max_length = source->max_length;

  
  /* unset infos
   */
  for ( i = 0; i < num_added; i++ ) {
    S_counter[added[i]] = 0;
  }
  num_added = 0;
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = FALSE;
  }
  for ( i = 0; i < dest->num_F; i++ ) {
    in_dest[dest->F[i]] = FALSE;
  }
  for ( i = 0; i < num_del; i++ ) {
    in_del[del[i]] = FALSE;
  }
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    true_ef[i] = FALSE;
  }
  
  return r;
  
}






/* function that computes state transition as induced by a
 * normalized ADL action. Adds go before deletes!
 *
 * a bit odd in implementation:
 * function returns number of new goal that came in when applying
 * op to source; needed for Goal Added Deletion Heuristic
 * Do not reset the gadd_list.num_F.
 */
int result_to_dest_additive( State *dest, State *source, int op )

{
  
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest, *in_del, *true_ef;
  static int *del, num_del;
  int i, j, ef, k, a;
  int r = -1;
  S_adds tmp;
  
  if ( first_call ) {
    in_source = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_del = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    true_ef = ( Bool * ) calloc( gnum_ef_conn, sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      in_source[i] = FALSE;
      in_dest[i] = FALSE;
      in_del[i] = FALSE;
    }
    for ( i = 0; i < gnum_ef_conn; i++ ) {
      true_ef[i] = FALSE;
    }
    first_call = FALSE;
  }
  
  /* setup true facts for effect cond evaluation
   */
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = TRUE;
    
  }
  
  
  /* setup deleted facts
   */
  num_del = 0;
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    ef = gop_conn[op].E[i];
    for ( j = 0; j < gef_conn[ef].num_PC; j++ ) {
      if ( !in_source[gef_conn[ef].PC[j]] ) break;
    }
    if ( j < gef_conn[ef].num_PC ) continue;
    true_ef[i] = TRUE;
    for ( j = 0; j < gef_conn[ef].num_D; j++ ) {
      if ( in_del[gef_conn[ef].D[j]] || !in_source[gef_conn[ef].D[j]]) continue;
      in_del[gef_conn[ef].D[j]] = TRUE;
      del[num_del++] = gef_conn[ef].D[j];
      
      if (gS_index[del[num_del-1]] == -1) {
        tmp =  gSlt_ft_conn[ del[num_del-1] ];
        for (k = 0; k < tmp.num_S; k++) 
          if  (--S_counter[ tmp.sl_t[k] ] == 0) { 
            if ( in_del[ tmp.sl_t[k] ]) continue; 
            in_del[ tmp.sl_t[k] ] = TRUE;
            del[num_del++] = tmp.sl_t[k];
          }
      }
    }
  }
  
  
  
  /* put all non-deleted facts from source into dest.
   * need not check for put-in facts here,
   * as initial state is made doubles-free, and invariant keeps
   * true through the transition procedure
   */
  dest->num_F = 0;
  
  for ( i = 0; i < source->num_F; i++ ) {
    if ( in_del[source->F[i]] ) { 
      if (!MINISAT) gadd_list.F[gadd_list.num_F++] = source->F[i]; 
      continue;
    }
    dest->F[dest->num_F++] = source->F[i];
    in_dest[source->F[i]] = TRUE;
  }
  
  
  /* now, finally, add all fullfilled effect adds to dest; 
   * each fact at most once!
   */
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    if ( !true_ef[i] ) continue;
    ef = gop_conn[op].E[i];
    for ( j = 0; j < gef_conn[ef].num_A; j++ ) {
      if ( in_dest[gef_conn[ef].A[j]] ) {
        continue;
      }
      a = gef_conn[ef].A[j];
      dest->F[dest->num_F++] = a;
      if (!MINISAT) gadd_list.F[gadd_list.num_F++] = a; 
      in_dest[ a ] = TRUE;
      if ( gft_conn[ a ].is_global_goal ) {
        r = a;
      }
    }
  }
  dest->max_length = source->max_length;

  /* unset infos
   */
  
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = FALSE;
  }
  for ( i = 0; i < dest->num_F; i++ ) {
    in_dest[dest->F[i]] = FALSE;
  }
  for ( i = 0; i < num_del; i++ ) {
    in_del[del[i]] = FALSE;
  }
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    true_ef[i] = FALSE;
  }
  
  return r;
  
}




/* function that computes state transition as induced by a
 * normalized ADL action. Adds go before deletes!
 *
 * a bit odd in implementation:
 * function returns number of new goal that came in when applying
 * op to source; needed for Goal Added Deletion Heuristic
 * This functions doesn't take into account the S-literals.
 */
int result_to_dest_classical( State *dest, State *source, int op )

{
  
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest, *in_del, *true_ef;
  static int *del, num_del;
  
  int i, j, ef;
  int r = -1;
  
  if ( first_call ) {
    in_source = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_del = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    true_ef = ( Bool * ) calloc( gnum_ef_conn, sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      in_source[i] = FALSE;
      in_dest[i] = FALSE;
      in_del[i] = FALSE;
    }
    for ( i = 0; i < gnum_ef_conn; i++ ) {
      true_ef[i] = FALSE;
    }
    first_call = FALSE;
  }
  
  /* setup true facts for effect cond evaluation
   */
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = TRUE;
  }
  
  /* setup deleted facts
   */
  num_del = 0;
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    ef = gop_conn[op].E[i];
    for ( j = 0; j < gef_conn[ef].num_PC; j++ ) {
      if ( !in_source[gef_conn[ef].PC[j]] ) break;
    }
    if ( j < gef_conn[ef].num_PC ) continue;
    true_ef[i] = TRUE;
    for ( j = 0; j < gef_conn[ef].num_D; j++ ) {
      if ( in_del[gef_conn[ef].D[j]] ) continue;
      in_del[gef_conn[ef].D[j]] = TRUE;
      del[num_del++] = gef_conn[ef].D[j];
    }
  }
  
  /* put all non-deleted facts from source into dest.
   * need not check for put-in facts here,
   * as initial state is made doubles-free, and invariant keeps
   * true through the transition procedure
   */
  dest->num_F = 0;
  for ( i = 0; i < source->num_F; i++ ) {
    if ( in_del[source->F[i]] ) {
      continue;
    }
    dest->F[dest->num_F++] = source->F[i];
    in_dest[source->F[i]] = TRUE;
  }
  
  /* now, finally, add all fullfilled effect adds to dest; 
   * each fact at most once!
   */
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    if ( !true_ef[i] ) continue;
    ef = gop_conn[op].E[i];
    for ( j = 0; j < gef_conn[ef].num_A; j++ ) {
      if ( in_dest[gef_conn[ef].A[j]] ) {
        continue;
      }
      dest->F[dest->num_F++] = gef_conn[ef].A[j];
      in_dest[gef_conn[ef].A[j]] = TRUE;
      if ( gft_conn[gef_conn[ef].A[j]].is_global_goal ) {
        r = gef_conn[ef].A[j];
      }
    }
  }
    dest->max_length = source->max_length;

  /* unset infos
   */
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = FALSE;
  }
  for ( i = 0; i < dest->num_F; i++ ) {
    in_dest[dest->F[i]] = FALSE;
  }
  for ( i = 0; i < num_del; i++ ) {
    in_del[del[i]] = FALSE;
  }
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    true_ef[i] = FALSE;
  }
  
  return r;
  
}





int effect_to_dest( State *dest, State *source, int eff )

{
  
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest, *in_del, true_ef;
  static int *del, num_del;
  
  int i, j;
  int r = -1;
  
  if ( first_call ) {
    in_source = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_del = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      in_source[i] = FALSE;
      in_dest[i] = FALSE;
      in_del[i] = FALSE;
    }
    
    true_ef = FALSE;
    
    first_call = FALSE;
  }
  
  /* setup true facts for effect cond evaluation
   */
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = TRUE;
  }
  
  /* setup deleted facts
   */
  num_del = 0;
  
  for ( j = 0; j < gef_conn[eff].num_PC; j++ ) {
    if ( !in_source[gef_conn[eff].PC[j]] ) break;
  }
  true_ef = TRUE;
  for ( j = 0; j < gef_conn[eff].num_D; j++ ) {
    if ( in_del[gef_conn[eff].D[j]] ) continue;
    in_del[gef_conn[eff].D[j]] = TRUE;
    del[num_del++] = gef_conn[eff].D[j];
  }
  
  
  /* put all non-deleted facts from source into dest.
   * need not check for put-in facts here,
   * as initial state is made doubles-free, and invariant keeps
   * true through the transition procedure
   */
  dest->num_F = 0;
  for ( i = 0; i < source->num_F; i++ ) {
    if ( in_del[source->F[i]] ) {
      continue;
    }
    dest->F[dest->num_F++] = source->F[i];
    in_dest[source->F[i]] = TRUE;
  }
  
  /* now, finally, add all fullfilled effect adds to dest; 
   * each fact at most once!
   */
  for ( j = 0; j < gef_conn[eff].num_A; j++ ) {
    if ( in_dest[gef_conn[eff].A[j]] ) {
      continue;
    }
    dest->F[dest->num_F++] = gef_conn[eff].A[j];
    in_dest[gef_conn[eff].A[j]] = TRUE;
    if ( gft_conn[gef_conn[eff].A[j]].is_global_goal ) {
      r = gef_conn[eff].A[j];
    }
  }
    dest->max_length = source->max_length;

  /* unset infos
   */
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[source->F[i]] = FALSE;
  }
  for ( i = 0; i < dest->num_F; i++ ) {
    in_dest[dest->F[i]] = FALSE;
  }
  for ( i = 0; i < num_del; i++ ) {
    in_del[del[i]] = FALSE;
  }
  
  true_ef = FALSE;
  
  return r;
  
}


/* Function for the closure application.
 * Applies a single effect of an operator,
 * as long as the closures have only conditional
 * effects. Do not reset the gadd_list.num_F.
 */
int effect_to_dest_additive( State *dest, State *source, int eff )

{
  
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest, *in_del, true_ef;
  static int *del, num_del;
  static int ft;
  
  
  int j, k;
  int r = -1;
  S_adds tmp;
  
  if ( first_call ) {
    in_source = ( Bool * ) malloc( gnum_ft_conn * sizeof( Bool ) );
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_del = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    for ( j = 0; j < gnum_ft_conn; j++ ) {
      in_source[j] = FALSE;
      in_dest[j] = FALSE;
      in_del[j] = FALSE;
    }
    
    true_ef = FALSE;
    
    first_call = FALSE;
  }
  
  /* setup true facts for effect cond evaluation
   */
  for ( j = 0; j < source->num_F; j++ ) {
    in_source[source->F[j]] = TRUE;
  }
  
  /* setup deleted facts
   */
  num_del = 0;
  
  /* Here we assume that previously the function
   * uneffective_effect has done its job.
   * So we don't check the preconditions.
   */
  
  true_ef = TRUE;
  for ( j = 0; j < gef_conn[eff].num_D; j++ ) {
    ft = gef_conn[eff].D[j];
    
    if ( in_del[ft] || !in_source[ft]) continue;
    in_del[ft] = TRUE;
    del[num_del++] = ft;
    
    if (gS_index[del[num_del-1]] == -1) { /* i.e. If it's not a SL */
      tmp =  gSlt_ft_conn[ del[num_del-1] ];
      for (k = 0; k < tmp.num_S; k++) 
        if  (--S_counter[ tmp.sl_t[k] ] == 0) { 
          if ( in_del[ tmp.sl_t[k] ]) continue; 
          in_del[ tmp.sl_t[k] ] = TRUE;
          del[num_del++] = tmp.sl_t[k];
        }
    }
  }
  
  
  /* put all non-deleted facts from source into dest.
   * need not check for put-in facts here,
   * as initial state is made doubles-free, and invariant keeps
   * true through the transition procedure
   */
  dest->num_F = 0;
  for ( j = 0; j < source->num_F; j++ ) {
    if ( in_del[source->F[j]] ) {
      if ( !MINISAT ) gadd_list.F[gadd_list.num_F++] = source->F[j]; 
      continue;
    }
    dest->F[dest->num_F++] = source->F[j];
    in_dest[source->F[j]] = TRUE;
  }
  
  /* now, finally, add all fullfilled effect adds to dest; 
   * each fact at most once!
   */
  for ( j = 0; j < gef_conn[eff].num_A; j++ ) {
    if ( in_dest[gef_conn[eff].A[j]] ) {
      continue;
    }
    ft = gef_conn[eff].A[j];
    dest->F[dest->num_F++] = ft;
    if ( !MINISAT ) gadd_list.F[gadd_list.num_F++] = ft;
    in_dest[ft] = TRUE;
    if ( gft_conn[ft].is_global_goal ) {
      r = ft;
    }  
  }
    dest->max_length = source->max_length;

  /* unset infos  */
  for ( j = 0; j < source->num_F; j++ ) {
    in_source[source->F[j]] = FALSE;
  }
  for ( j = 0; j < dest->num_F; j++ ) {
    in_dest[dest->F[j]] = FALSE;
  }
  for ( j = 0; j < num_del; j++ ) {
    in_del[del[j]] = FALSE;
  }
  
  true_ef = FALSE;
  
  return r;
  
}





/* Function for the closure application.
 * Applies a single effect of an operator,
 * as long as the closures have only conditional
 * effects. Do not reset the gadd_list.num_F.
 */
int effect_to_dest_triggered( State *dest, State *source, int eff )

{
  
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest, *in_del, true_ef;
  static int *del, num_del;
  static int ft;
  
  /* TODO: Remove and make global */
  static Bool *to_apply_counter;
  
  static int **not_trigger;
  
  int i, j, k;
  int r = -1;
  S_adds tmp;
  int tmp_ef;
  if ( first_call ) {
    in_source = ( Bool * ) malloc( gnum_ft_conn * sizeof( Bool ) );
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_del = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    for ( j = 0; j < gnum_ft_conn; j++ ) {
      in_source[j] = FALSE;
      in_dest[j] = FALSE;
      in_del[j] = FALSE;
    }
    
    true_ef = FALSE;
    
    first_call = FALSE;
    
    /* CLOSURE LIST */
    to_apply_counter = ( Bool * ) malloc( gnum_ef_conn * sizeof( Bool ) );
    
    not_trigger = (int **) malloc(gnum_ft_conn * sizeof(int *));
    for(i = 0; i < gnum_ft_conn; i++)
      not_trigger[i] = (int *) calloc(50 , sizeof(int));
    
  }
  
  /* setup true facts for effect cond evaluation
   */
  for ( j = 0; j < source->num_F; j++ ) {
    in_source[source->F[j]] = TRUE;
  }
  
  /* setup deleted facts
   */
  num_del = 0;
  
  /* Here we assume that previously the function
   * uneffective_effect has done its job.
   * So we don't check the preconditions.
   */
  
  true_ef = TRUE;
  for ( j = 0; j < gef_conn[eff].num_D; j++ ) {
    ft = gef_conn[eff].D[j];
    
    if ( in_del[ft] || !in_source[ft]) continue;
    in_del[ft] = TRUE;
    del[num_del++] = ft;
    
    /* CLOSURE LISTS */
    /* if a support is removed, the closures doesn't apply */    
    for (k= 0; k < gft_conn[ft].num_PC; k++) {
      if (to_apply_counter[ gft_conn[ft].PC[k] ]) {
        to_apply_counter[ gft_conn[ft].PC[k] ] = FALSE;
      }
    }
    
    if (gS_index[del[num_del-1]] == -1) { /* i.e. If it's not a SL */
      tmp =  gSlt_ft_conn[ del[num_del-1] ];
      for (k = 0; k < tmp.num_S; k++) 
        if  (--S_counter[ tmp.sl_t[k] ] == 0) { 
          if ( in_del[ tmp.sl_t[k] ]) continue; 
          in_del[ tmp.sl_t[k] ] = TRUE;
          del[num_del++] = tmp.sl_t[k];
        }
    }
  }
  
  
  /* put all non-deleted facts from source into dest.
   * need not check for put-in facts here,
   * as initial state is made doubles-free, and invariant keeps
   * true through the transition procedure
   */
  dest->num_F = 0;
  for ( j = 0; j < source->num_F; j++ ) {
    if ( in_del[source->F[j]] ) {
      gadd_list.F[gadd_list.num_F++] = source->F[j]; 
      continue;
    }
    dest->F[dest->num_F++] = source->F[j];
    in_dest[source->F[j]] = TRUE;
  }
  
  /* now, finally, add all fullfilled effect adds to dest; 
   * each fact at most once!
   */
  for ( j = 0; j < gef_conn[eff].num_A; j++ ) {
    if ( in_dest[gef_conn[eff].A[j]] ) {
      continue;
    }
    ft = gef_conn[eff].A[j];
    dest->F[dest->num_F++] = ft;
    gadd_list.F[gadd_list.num_F++] = ft;
    in_dest[ft] = TRUE;
    if ( gft_conn[ft].is_global_goal ) {
      r = ft;
    }
    /* CLOSURE LIST */
    for (i=0; i < 50 ; i++) {
      if ((not_trigger[ft][i] == 0) || (to_apply_counter[not_trigger[ft][i]]) )
        continue;
      tmp_ef = not_trigger[ft][i];
      /* Removes this fact from not-preconds */
      not_trigger[ft][i] = 0;
      /* Verifies the prefonds of the effect */
      for (k=0; k <  gef_conn[tmp_ef].num_PC ; k++) 
        if (in_dest[gef_conn[tmp_ef].PC[k]])
          continue;
        else  break;
      
      /* Check all the other preconditions */
      /* Put in a 'maybe' list */
      /* if (is_clos the effect that we check)
       verify the other preconds*/
      if (k == gef_conn[tmp_ef].num_PC)
        to_apply_counter[tmp_ef] = TRUE;
      
    }
    
  }
    dest->max_length = source->max_length;

  /* unset infos  */
  for ( j = 0; j < source->num_F; j++ ) {
    in_source[source->F[j]] = FALSE;
  }
  for ( j = 0; j < dest->num_F; j++ ) {
    in_dest[dest->F[j]] = FALSE;
  }
  for ( j = 0; j < num_del; j++ ) {
    in_del[del[j]] = FALSE;
  }
  
  true_ef = FALSE;
  
  return r;
}


/* Funtion that checks if there are any add or del, or if the preconds
 * exist in the state.
 * Returns TRUE if the effect is NOT effective */
Bool uneffective_effect( State *source, int ef)

{
  static Bool first_call = TRUE;
  static Bool *in_source;
  int n_changes = 0;
  
  int i;
  
  if ( first_call ) {
    in_source = ( Bool * ) malloc( gnum_ft_conn * sizeof( Bool ) );
    
    /* Initializes the lists */
    for ( i = 0; i < gnum_ft_conn; i++ ) {
      in_source[i] = FALSE;
    }  
    first_call = FALSE;
  }
  
  
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[ source->F[i] ] = TRUE;
  }
  
  /* Checks the preconds */
  for ( i = 0; i < gef_conn[ef].num_PC; i++ ) {
    if ( !in_source[gef_conn[ef].PC[i]] ) {
      int j;
      for ( j = 0; j < source->num_F; j++ ) 
        in_source[ source->F[j] ] = FALSE;
      
      return TRUE;
    }
  }
  
  /* Checks the adds */
  for ( i = 0; i < gef_conn[ef].num_A; i++ ) {
    if (in_source[ gef_conn[ef].A[i] ])
      continue;
    else {
      ++n_changes;
      break;
    }
  }
  
  /* Checks the dels */
  if (n_changes == 0)
    for ( i = 0; i < gef_conn[ef].num_D; i++ ) 
      if (in_source[ gef_conn[ef].D[i] ])
      {
        ++n_changes;
        break;
      }
  
  
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[ source->F[i] ] = FALSE;
  }
  
  return (n_changes == 0);
  
}



/* Returns TRUE if the action has no effects on the state */
Bool has_effects( State *source, int op)

{
  static Bool first_call = TRUE;
  static Bool *in_source;
  int n_changes = 0;
  
  int i,j;
  
  if ( first_call ) {
    in_source = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    
    first_call = FALSE;
  }
  
  /* Initialize the lists */
  for ( i = 0; i < gnum_ft_conn; i++ ) {
    in_source[i] = FALSE;
  }
  
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[ source->F[i] ] = TRUE;
  }
  
  for ( i = 0; i < gop_conn[op].num_E; i++ ) {
    for ( j = 0; j < gef_conn[gop_conn[op].E[i]].num_A; j++ ) {
      if (in_source[ gef_conn[gop_conn[op].E[i]].A[j] ])
        continue;
      else {
        ++n_changes;
        break;
      }
    }
  }
  
  return (n_changes == 0);
  
}





/* Returns the number of atoms that are different from one state to the other */
int different_atoms( State *dest, State *source, int *new_list, int *del_list)

{
  static Bool first_call = TRUE;
  static Bool *in_source, *in_dest;
  static int n_new, n_del;
  
  int i;
  
  if ( first_call ) {
    in_source = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_dest = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    
    first_call = FALSE;
  }
  
  /* Initialize the lists */
  for ( i = 0; i < gnum_ft_conn; i++ ) {
    in_source[i] = FALSE;
    in_dest[i] = FALSE;
    new_list[i] = -1;
    del_list[i] = -1;
  }
  
  for ( i = 0; i < source->num_F; i++ ) {
    in_source[ source->F[i] ] = TRUE;
  }
  for ( i = 0; i < dest->num_F; i++ ) {
    in_dest[ dest->F[i] ] = TRUE;
  }
  
  /** Builds the output list of fluents that
   *  are present in a list and not in the other */
  n_new= 0;
  n_del = 0;
  for ( i = 0; i < gnum_ft_conn; i++) {
    if ( (in_source[i]) == (in_dest[i]) )
      continue;
    if  (in_source[i] == TRUE)
      del_list[n_del++] = i;
    else 
      new_list[n_new++] = i;
  }
  
  return (n_del + n_new);
}





/* The boolean mi indicates whether or not to apply
 * merge-imply rules (heuristic search or execution).
 * We assume that the state is in sinc with SLiterals.
 * Returns TRUE if a spurious state is encountered. 
 */
Bool apply_closures( State *S, Bool mi ) {
  
  int j, i, closure_to_apply, ac_op;
  static Bool fc = TRUE;
  static State ks;
  static int *cl_list;
  static Bool *counter;
  Bool ret = FALSE;
  int list_elements = 0;
  
  if (fc) {
    fc = FALSE;
    
    make_state( &ks, gnum_ft_conn );
    ks.max_F = gnum_ft_conn;
    
    counter = ( Bool * ) malloc( gnum_op_conn * sizeof( int ) );
    cl_list =  ( int * ) malloc( gnum_cl_conn * sizeof( int ) );
    
  }
  /* Start counting the time */
  times(&clstart);
  
  
  if (mi && (habilcl > -1)) {
    /* Apply the habilitation-tag for merge-imply */
    onlyeff_to_dest( &ks, S, habilcl );
    source_to_dest( S, &ks);
  }
  
  /* Initialises the list of applicable actions */
  for (j = 0; j <   gnum_cl_conn; j++) {
    cl_list[ j ] = -1;
  }
  
  
  /* Initialises the counter of applicable effects */
  for (j = 0; j <   gnum_op_conn; j++) {
    counter[ j ] = FALSE;
  }   
  
  do {
    /* Check facts preconditions */
    for ( i = 0; i < gadd_list.num_F; i++ ) {
      for ( j = 0; j < ((gft_conn[ gadd_list.F[i] ]).num_PC); j++ ) {
        ac_op = gef_conn[ gft_conn[ gadd_list.F[i] ].PC[j] ].op;
        if (counter[ ac_op ])
          continue;
        
        else if (gop_conn[ ac_op ].is_clos) {
          counter[ ac_op ] = TRUE;              
          cl_list [list_elements++] =  ac_op;
        }
      }
    }
    gadd_list.num_F = 0;
    
    /* Now applies all the closures in the list */
    for ( i = 0; i < list_elements; i++ ) { 
      
      closure_to_apply =   cl_list [i];
      
      counter[closure_to_apply] = FALSE;
      
      result_to_dest_additive( &ks, S, closure_to_apply ); 
      
      if (closure_to_apply == contradiction_op)
        if (fail_state_check( &ks )) {
          ret = TRUE;
          source_to_dest( S, &ks );
          break;
        }
      source_to_dest( S, &ks );
    }
    list_elements = 0;
    
  } while (gadd_list.num_F);
  
  if (mi && (unhabil > -1)) {
    result_to_dest( &ks, S, unhabil );
    source_to_dest( S, &ks );
  }
  times(&clend);
  CTIME( ); 
  
  return ret;
}




/* The boolean mi indicates whether or not to apply
 * merge-imply rules (heuristic search or execution).
 * Returns TRUE if a spurious state is encountered. 
 */
Bool apply_closures_with_update( State *S, Bool mi ) {
  
  int j, i, closure_to_apply, ac_ef;
  static Bool fc = TRUE;
  static State ks;
  static int *cl_list;
  static Bool *counter;
  Bool ret = FALSE;
  int list_elements = 0;
  int num_added =  0;
  
  if (fc) {
    fc = FALSE;
    
    make_state( &ks, gnum_ft_conn );
    ks.max_F = gnum_ft_conn;
    
    counter = ( Bool * ) malloc( gnum_ef_conn * sizeof( int ) );
    cl_list =  ( int * ) malloc( gnum_clef_conn * sizeof( int ) );
  }

  times(&clstart);
  
  
  if (mi && (habilcl > -1)) {
    /* Apply the habilitation-tag for merge-imply */
    onlyeff_to_dest( &ks, S, habilcl );
    source_to_dest( S, &ks);
  }
  
  /* Initialises the list of applicable actions */
  for (j = 0; j <   gnum_clef_conn; j++) {
    cl_list[ j ] = -1;
  }
  
  
  /* Initialises the counter of applicable effects */
  for (j = 0; j <   gnum_ef_conn; j++) {
    counter[ j ] = FALSE;
  }    
  
  num_added = update_S_counter(S);
  
  do {
    
    /* Check facts preconditions */
    for ( i = 0; i < gadd_list.num_F; i++ ) {
      for ( j = 0; j < ((gft_conn[ gadd_list.F[i] ]).num_PC); j++ ) {
        ac_ef = gft_conn[ gadd_list.F[i] ].PC[j]; 
        if (counter[ ac_ef ])                               
          continue;
        
        else if (gop_conn[ gef_conn[ac_ef].op ].is_clos) {
          counter[ ac_ef ] = TRUE;              
          cl_list [list_elements++] =  ac_ef;
        }
      }
    }
    gadd_list.num_F = 0;
    
    /* Now applies all the closures in the list */
    for ( i = 0; i < list_elements; i++ ) { 
      closure_to_apply =   cl_list [i];
      
      counter[closure_to_apply] = FALSE;
      
      /* If has no effect, then continue  */
      if (uneffective_effect(S, closure_to_apply))
        continue; 
      effect_to_dest_additive( &ks, S, closure_to_apply );
      /*  result_to_dest_additive( &ks, S, closure_to_apply ); */
      
      if (closure_to_apply == contradiction_op)
        if (fail_state_check( &ks )) {
          ret = TRUE;
          source_to_dest( S, &ks );
          break;
        }
      source_to_dest( S, &ks );
    }
    
    /* Prepares itself for the new cycle */
    list_elements = 0;
    
  } while (gadd_list.num_F);
  
  
  reset_S_counter(num_added);
  num_added = 0;
  /* Applies the deletes closure ops  
   for (j = 0; j <   gnum_del_conn; j++) {
   result_to_dest( &ks, S, gsim_conn[j] );
   source_to_dest( S, &ks );
   } */
  if (mi && (unhabil > -1)) {
    result_to_dest( &ks, S, unhabil );
    source_to_dest( S, &ks );
  }
  times(&clend);
  CTIME( ); 
  
  return ret;
}


/** This function implements the AC-3 algorithm
 * to apply the closures until fixpoint:
 * only the actions involving literals that have changes
 * are applied at next step.
 * The boolean mi indicates whether or not to apply
 * merge-imply rules (heuristic search or execution).
 * Returns TRUE if a spurious state is encountered. 
 */
Bool apply_closures2( State *S, Bool mi ) {
  
  int j, i, h, applicable, closure_to_apply, ac_op;
  static Bool fc = TRUE;
  static State ks;
  static int *counter, *new_list, *cl_list, *ef_list, *del_list;
  Bool ret = FALSE;
  int min = 0;
  
  if (fc) {
    fc = FALSE;
    
    make_state( &ks, gnum_ft_conn );
    ks.max_F = gnum_ft_conn;
    
    counter = ( int * ) calloc( gnum_op_conn, sizeof( int ) );
    cl_list =  ( int * ) calloc( gnum_cl_conn, sizeof( int ) );
    ef_list =  ( int * ) calloc( gnum_ef_conn, sizeof( int ) );
    new_list = ( int * ) calloc( gnum_ft_conn, sizeof( int ) ); 
    del_list  = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
  }
  /* Start counting the time */
  times(&clstart);
  
  
  if (mi && (habilcl > -1)) {
    /* Apply the habilitation-tag for merge-imply */
    onlyeff_to_dest( &ks, S, habilcl );
    source_to_dest( S, &ks);
  }
  
  /* Initializes the number of applicable closure rules */
  applicable = 0;
  
  /* Initialises the list of applicable actions */
  for (j = 0; j <   gnum_cl_conn; j++) {
    cl_list[ j ] = -1;
  }
  /* Initialises the counter of applicable effects */
  for (j = 0; j <   gnum_ef_conn; j++) {
    ef_list[ j ] = 0;
  }
  
  /* Implementation of AC-3 */
  /* Check facts preconditions */
  for ( i = 0; i < S->num_F; i++ ) 
    for ( j = 0; j < ((gft_conn[ S->F[i] ]).num_PC); j++ ) 
      ++(ef_list[ (gft_conn[ S->F[i] ]).PC[j] ]);
  
  
  /* Counts how many preconditions are left to be satisfied */
  for (j = 0; j <  gnum_cl_conn; j++) {
    ac_op = gcl_conn[j];
    
    /* Computes the num. of preconds to check */
    if (gop_conn[ac_op].num_E > 0)
      min = 
      (gef_conn[gop_conn[ac_op].E[0]].num_PC - ef_list[ gop_conn[ac_op].E[0] ]);
    
    /* Sets the min of precs for each operator */
    for (i = 1; i < gop_conn[ac_op].num_E; i++) 
      min = 
      ( min < (gef_conn[gop_conn[ac_op].E[i]].num_PC - ef_list[ gop_conn[ac_op].E[i]]) 
       ? min 
       : (gef_conn[gop_conn[ac_op].E[i]].num_PC - ef_list[ gop_conn[ac_op].E[i]]) ); 
    counter[ ac_op ] = min;
    
    if (counter[ ac_op ] == 0) 
      cl_list[applicable++] = ac_op;   
  }
  
  while (applicable > 0) {
    
    /* Extracts an action */
    closure_to_apply = cl_list[--applicable]; /* it is the lst one in the list */
    
    /* This was an alternative way to calculate the min:
     * a lot of ado for nothing... */  
    /*       min = */
    /* 	(gef_conn[gop_conn[closure_to_apply ].E[0]].num_PC - ef_list[ gop_conn[closure_to_apply ].E[0] ]); */
    /*       /\* ...that is removed from the list *\/ */
    /*       for (i = 1; i < gop_conn[closure_to_apply].num_E; i++)  */
    /* 	min = */
    /* 	  ( min < (gef_conn[gop_conn[closure_to_apply].E[i]].num_PC - ef_list[ gop_conn[closure_to_apply].E[i]]) */
    /* 	    ? min */
    /* 	    : (gef_conn[gop_conn[closure_to_apply].E[i]].num_PC - ef_list[ gop_conn[closure_to_apply].E[i]]) ); */
    /*       counter[  closure_to_apply ] = min; */
    
    /* Removes the current action from the list */
    cl_list[applicable] = -1; 
    
    /* If has no effect, then continue; */
    if (has_effects(S, closure_to_apply))
      continue;
    
    result_to_dest_additive( &ks, S, closure_to_apply ); 
    
    if (closure_to_apply == contradiction_op)
      if (fail_state_check( &ks )) {
        ret = TRUE;
        source_to_dest( S, &ks );
        break;
      }
    
    /* Checks the new literals */
    ac_op = different_atoms( &ks, S, new_list, del_list ) ;
    
    if (ac_op > 0) {
      /* First setup the deletes */
      for (i = 0; i < gnum_ft_conn; i++)  {
        if (del_list[i] > -1)
          for (h = 0; h < ((gft_conn[del_list[i]]).num_PC); h++) {
            --(ef_list[gft_conn[ del_list[i] ].PC[h]]);
            ++(counter[ gef_conn[ gft_conn[del_list[i]].PC[h] ].op ]);
          }
      } 
      /* Updates the counters */
      for (i = 0; i < gnum_ft_conn; i++)  {
        if (new_list[i] > -1)
          for (h = 0; h < ((gft_conn[new_list[i]]).num_PC); h++)   
            if ( gop_conn[ gef_conn[ gft_conn[new_list[i]].PC[h] ].op].is_clos ) {
              /* ac_op changes meaning on more time */		
              ac_op = gef_conn[ gft_conn[new_list[i]].PC[h] ].op;
              /* decreases the minimum possible value of the counter */
              /* only if it isn't already in the list */
              if ( counter[ ac_op ] >  
                  (gef_conn[ gft_conn[new_list[i]].PC[h] ].num_PC - (++(ef_list[gft_conn[ new_list[i] ].PC[h]])))
                  )
                (--(counter[ ac_op ]));
              
              if ( counter[ ac_op ] <= 0 ) {
                int t = 0;
                /* Verify if it is into the list */
                for (j  = 0; j < applicable; j++)
                  if (ac_op == cl_list[j]) {
                    t++;
                    break;
                  }
                if (t == 0)
                  cl_list[applicable++] = ac_op;
              }
            }
      }
    }
    source_to_dest( S, &ks );
  }
  if (mi && (unhabil > -1)) {
    result_to_dest( &ks, S, unhabil );
    source_to_dest( S, &ks );
  }
  times(&clend);
  CTIME( );  
  
  return ret;
}




/* Closure veriant with applicable actions lists */
/* The boolean mi indicates whether or not to apply
 * merge-imply rules (heuristic search or execution).
 * Returns TRUE if a spurious state is encountered. 
 */
Bool apply_closures_with_lists( State *S, Bool mi ) {
  
  int j, i, closure_to_apply, ac_ef;
  static Bool fc = TRUE;
  static State ks;
  static int *cl_list;
  static Bool *to_apply_counter;
  Bool ret = FALSE;
  int list_elements = 0;
  int num_added =  0;
  
  if (fc) {
    fc = FALSE;
    
    make_state( &ks, gnum_ft_conn );
    ks.max_F = gnum_ft_conn;
    
    to_apply_counter = ( Bool * ) malloc( gnum_ef_conn * sizeof( int ) );
    cl_list =  ( int * ) malloc( gnum_clef_conn * sizeof( int ) );
  }
  /* Start counting the time */
  times(&clstart);
  
  
  if (mi && (habilcl > -1)) {
    /* Apply the habilitation-tag for merge-imply */
    onlyeff_to_dest( &ks, S, habilcl );
    source_to_dest( S, &ks);
  }
  
  
  /* Initialises the counter of applicable effects */
  for (j = 0; j <   gnum_ef_conn; j++) {
    to_apply_counter[ j ] = FALSE;
    /* Checks the present supports */
  } 
  
  
  
  
  /* Initialises the list of applicable actions */
  for (j = 0; j <   gnum_clef_conn; j++) {
    cl_list[ j ] = -1;
  }
  
  
  
  num_added = update_S_counter(S);
  
  do {
    
    /* Check facts preconditions */
    for ( i = 0; i < gadd_list.num_F; i++ ) {
      for ( j = 0; j < ((gft_conn[ gadd_list.F[i] ]).num_PC); j++ ) {
        ac_ef = gft_conn[ gadd_list.F[i] ].PC[j]; 
        if (to_apply_counter[ ac_ef ])                               
          continue;
        
        else if (gop_conn[ gef_conn[ac_ef].op ].is_clos) {
          to_apply_counter[ ac_ef ] = TRUE;              
          cl_list [list_elements++] =  ac_ef;
        }
      }
    }
    gadd_list.num_F = 0;
    
    /* Now applies all the closures in the list */
    for ( i = 0; i < list_elements; i++ ) { 
      closure_to_apply =   cl_list [i];
      
      to_apply_counter[closure_to_apply] = FALSE;
      
      /* If has no effect, then continue  */
      if (uneffective_effect(S, closure_to_apply))
        continue; 
      effect_to_dest_additive( &ks, S, closure_to_apply );
      
      if (closure_to_apply == contradiction_op)
        if (fail_state_check( &ks )) {
          ret = TRUE;
          source_to_dest( S, &ks );
          break;
        }
      source_to_dest( S, &ks );
    }
    
    /* Prepares itself for the new cycle */
    list_elements = 0;
    
  } while (gadd_list.num_F);
  
  
  reset_S_counter(num_added);
  num_added = 0;
  if (mi && (unhabil > -1)) {
    result_to_dest( &ks, S, unhabil );
    source_to_dest( S, &ks );
  }
  times(&clend);
  CTIME( ); 
  
  return ret;
}


/** MINISAT implementation within FF */
Bool apply_closures_minisat( State *S, Bool mi ) {
  
  int i, j, v, w;
  int espuria = FALSE;
  static int nd;
  static Bool fc = TRUE;
  static State ks;
  static Bool *in_source;
  static Bool *in_del;
  static Bool *dupl_list;
  static int *del;

  if (fc) {
    fc = FALSE;
    
    make_state( &ks, gnum_ft_conn );
    ks.max_F = gnum_ft_conn;
    
    values = ( int * ) calloc( gnum_ft_conn, sizeof( int ) ); 
    in_source = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    in_del = ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
    del = ( int * ) calloc( gnum_ft_conn, sizeof( int ) );
    dupl_list =  ( Bool * ) calloc( gnum_ft_conn, sizeof( Bool ) );
  }
  /* Start counting the time */
  times(&clstart);
  
  
  if (mi && (habilcl > -1)) {
    /* Apply the habilitation-tag for merge-imply */
    onlyeff_to_dest( &ks, S, habilcl );
    source_to_dest( S, &ks);
  }
  
  /* add all the facts of that state */
  for ( i = 0; i < S->num_F; i++ ) {
    if (gS_index[S->F[i]] == -1) {
	add_fact(S->F[i]);
	in_source[S->F[i]] = TRUE;
    }
  }
 
    nvalues = sat_simplify(values);
  
  nd = 0;

  /* Checks if KL and NOT-KL are present in S */
  for( i = 0; i < nvalues; i++ ) {
    v = values[i] ;

    if ( dupl_list[ v ] ) {
 
      if  (gnot_fact[ grelevant_facts[v].predicate]) 
	{
	  w =  v;
	} 
      else 
	w = gnot_corr[ v ];
      if (in_source [ w ]) {
	if (in_source [ gnot_corr[w] ]) {
	  printf("\n\t\tWarning! Inconsistency detected!\n");
	  printf(" %s and %s in the state... goodbye.\n",
		 gpredicates[grelevant_facts[gnot_corr[w] ].predicate], gpredicates[grelevant_facts[ w ].predicate]);
	exit(-1);
	} else {
	  del[nd++] =  w ;
	  in_del[w] = TRUE;
	}
      } else if (in_source [ gnot_corr[w] ]) {
          del[nd++] =  gnot_corr[w]  ;
          in_del[ gnot_corr[w] ] = TRUE;
      }
      continue;
    }
    if (gnot_corr[ v ])     /* add in check list */
      dupl_list[ gnot_corr[ v ] ] = TRUE;
  }

  j = 0;
  /* write to source */
  for( i = 0; i < nvalues; i++ ) { 
      v = values[i] ;

      if (v < 0)
	continue;
 
      if (in_del[v])
	continue;

      S->F[j++] = v;
    }

  S->num_F = j;

  for ( i = 0; i < nd; i++ ) { 
     in_del[del[i]] = FALSE; 
   }
  nvalues = 0;


  for( i = 0; i < gnum_ft_conn; i++ ) { 
    dupl_list[ i ] = FALSE;
    in_source[i] = FALSE; 
  }

  /* Function that checks the preconditions
   * of an action.
   *
   * Function returns the minumum number of literals
   *  necessary to satisfy the preconds.
   */
  
  if (mi && (fail_fact >= 0))
      espuria = fail_state_check( S );

  if (mi && (unhabil > -1)) {
    result_to_dest( &ks, S, unhabil );
    source_to_dest( S, &ks );
  }
  times(&clend);
  CTIME( );  
  
  return espuria;
}





/* unused function: it was there to check preconds before aplying a closure */
int check_preconds(State *source, int op )
{
  
  static Bool first_call = TRUE;
  static int num_prec;
  
  int min;
  
  int i, j;
  int *sat_effect;
  
  if ( first_call ) {
    sat_effect = ( int * ) calloc( gnum_ef_conn , sizeof( int ) );
    first_call = FALSE;
  }
  
  num_prec = 0;
  for (i = 0; i < gop_conn[op].num_E; i++) {
    sat_effect[i] = (gef_conn[ gop_conn[op].E[i] ]).num_PC;
    
  }
  
  for ( i = 0; i < source->num_F; i++ ) {
    for ( j = 0; j < ((gft_conn[source->F[i]]).num_PC); j++ ) {
      if ( (gef_conn[gft_conn[source->F[i]].PC[j]].op) == op)
        (sat_effect[gft_conn[source->F[i]].PC[j]])--;
    }
  }
  
  min = sat_effect[0]; 
  if(gop_conn[op].num_E > 0)
    for (i = 1; i < gop_conn[op].num_E; i++) { 
      min = ( min < sat_effect[i] ? min : sat_effect[i] ); 
    }
  
  return min;
  
}

/** Checks whether a is an ancestor of p 
 * Useful when you look for repeated states
 * that aren't in the branch of p */
Bool is_ancestor( CtgHashEntry *a, CtgHashEntry *p)
{
  State_pointer g = &(a->S);

  for ( ; p; p = p->father)
    if (simple_same_state(&(p->S), g))
      return TRUE;

  return FALSE;

}

void source_to_dest( State *dest, State *source )

{
  
  int i;
  
  for ( i = 0; i < source->num_F; i++ ) {
    dest->F[i] = source->F[i];
  }
  dest->num_F = source->num_F;
  
}



void copy_source_to_dest( State *dest, State *source )

{
  
  int i, m;
  
  if ( dest->max_F < source->num_F ) {
    if ( dest->F ) {
      free( dest->F );
      dest->F = NULL;
    }
    if ( source->num_F + 50 > gnum_ft_conn ) {
      m = gnum_ft_conn;
    } else {
      m = source->num_F + 50;
    }
    dest->F = ( int * ) calloc( m, sizeof( int ) );
    dest->max_F = m;
  }
  
  for ( i = 0; i < source->num_F; i++ ) {
    dest->F[i] = source->F[i];
  }
  dest->num_F = source->num_F;
  dest->max_length = source->max_length;
}


void print_state( State S )

{
  
  int i;
  
  for ( i = 0; i < S.num_F; i++ ) {
    printf("\n");
    print_ft_name( S.F[i] );
  }
  printf("\n");
}

/** Compares two states.
 * returns: TRUE if they are the same
 *          FALSE if they are different (even in the literals order)
 */
Bool simple_same_state( State *P , State *Q)
{ 
  int i;  
  if (P->num_F != Q->num_F)
    return FALSE;
  for ( i = 0; i < P->num_F; i++ ) {
    if(P->F[i] != Q->F[i])
      return FALSE;
  }
  return TRUE;
}


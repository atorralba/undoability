
/*********************************************************************
 * (C) Copyright 2001 Albert Ludwigs University Freiburg
 *     Institute of Computer Science
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 *********************************************************************/




/*
 * THIS SOURCE CODE IS SUPPLIED  ``AS IS'' WITHOUT WARRANTY OF ANY KIND, 
 * AND ITS AUTHOR AND THE JOURNAL OF ARTIFICIAL INTELLIGENCE RESEARCH 
 * (JAIR) AND JAIR'S PUBLISHERS AND DISTRIBUTORS, DISCLAIM ANY AND ALL 
 * WARRANTIES, INCLUDING BUT NOT LIMITED TO ANY IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND
 * ANY WARRANTIES OR NON INFRINGEMENT.  THE USER ASSUMES ALL LIABILITY AND
 * RESPONSIBILITY FOR USE OF THIS SOURCE CODE, AND NEITHER THE AUTHOR NOR
 * JAIR, NOR JAIR'S PUBLISHERS AND DISTRIBUTORS, WILL BE LIABLE FOR 
 * DAMAGES OF ANY KIND RESULTING FROM ITS USE.  Without limiting the 
 * generality of the foregoing, neither the author, nor JAIR, nor JAIR's
 * publishers and distributors, warrant that the Source Code will be 
 * error-free, will operate without interruption, or will meet the needs 
 * of the user.
 */










/*********************************************************************
 *
 * File: search.h
 *
 * Description: headers of routines that search the state space
 *
 *              ADL version, Enforced Hill-climbing enhanced with
 *                           Goal-adders deletion heuristic
 *
 * Author: Joerg Hoffmann 2000
 *
 *********************************************************************/ 






#ifndef _SEARCH_H
#define _SEARCH_H


/** Operator selection in greedy search algorithm
 *  1: automatic
 *  0: manual
 */
#define OP_SEL 0

/* To select only one branch at every observation */
#define RANDOM_SELECTION 0

/* add elapsed time from main local time vars to specified val using closure-time variables
 */
#define CTIME() gclos_time += ( float ) ( ( clend.tms_utime - clstart.tms_utime + \
					 clend.tms_stime - clstart.tms_stime  ) / 100.0 )
           
Bool do_enforced_hill_climbing( State *start, State *end );


Bool search_for_better_state( State *S, int h, State *S_, int *h_ );
void add_to_ehc_space( State *S, int op, EhcNode *father, int new_goal );
int expand_first_node( int h );

Bool search_for_better_state_classical( State *S, int h, State *S_, int *h_ );
int expand_first_node_classical( int h );
int result_to_dest_classical( State *dest, State *source, int op );

int result_to_dest_additive( State *dest, State *source, int op );
int effect_to_dest_additive( State *dest, State *source, int ef );

void hash_ehc_node( EhcNode *n );
Bool ehc_state_hashed( State *S );
Bool same_state( State *S1, State *S2 ) ;
Bool simple_same_state( State *S1 , State *S2);
int state_sum( State *S );
void reset_ehc_hash_entrys( void );

CtgHashEntry *ctg_hashed( State *S );
CtgHashEntry *hash_ctg_state( State *S, TreeNode *tn, CtgHashEntry *F );
void propagate_success(State *S);

void extract_plan_fragment( State *S );
PlanHashEntry *hash_plan_state( State *S, int step );
PlanHashEntry *plan_state_hashed( State *S );
void extract_Kplan_fragment( State *S );


Bool new_goal_gets_deleted( EhcNode *n );

int operator_manual_selection( void );
int observation_manual_selection(OpConn *a);

Bool do_best_first_search( void );
void add_to_bfs_space( State *S, int op, BfsNode *father );
void extract_plan( BfsNode *last );
void extract_k_plan(  BfsNode *last, int subplan_type );

void hash_bfs_node( BfsNode *n );
Bool bfs_state_hashed( State *S );

void show_hash_table(void);
void hash_table_operations(void);

int result_to_dest( State *dest, State *source, int op );
void source_to_dest( State *dest, State *source );
void copy_source_to_dest( State *dest, State *source );
void print_state( State S );

Bool fail_state_check( State *S );

Bool do_k_plan_search(  State *start, State *end );
void delete_grs_subspace(  BfsNode *subspace );

Bool sensed(int sensing_action, Bool insert_modality);
Bool *make_sens_list( void );

void add_to_grs_space( State *S, int op, BfsNode *father );
BfsNode *add_to_grs_subspace(  BfsNode *subspace, State *S, int op, BfsNode *father );
extern int testing;
extern TreeNode *root;

int obs_select( State *S);
int observation_hiddenstate_selection(OpConn * op);
int observe_to_dest( State *dest, State *source, EfConn * br);
Bool apply_closures_with_update( State *S, Bool f );
Bool apply_closures( State *S, Bool b );
Bool apply_closures2( State *S, Bool b );
Bool apply_closures_minisat( State *S, Bool b );
int effect_to_dest( State *dest, State *source, int eff );
Bool uneffective_effect( State *source, int ef);
void initialize_S_index( void );
int update_S_counter(State *ks);
void show_ctghash_table(void);
Bool is_ancestor( CtgHashEntry *a, CtgHashEntry *p);
void build_init_KL___t(State *s);
void extract_current_state (State *s,State *r);
#endif /* _SEARCH_H */

void
usage( std::ostream &os )
{
  os << "cf2cs v 2.1 -- A compiler for contingent planning problems into classical models." << std::endl;
  os << std::endl
     << "usage: cf2cs {-s <prefix>} {FLAGS} <domain> <problem>"
     << std::endl << std::endl
     << "\tCompiles the conformant PDDL problem specified in the domain and problem file" << std::endl
     << "\tinto a classical problem on files <prefix>-d.pddl and <prefix>-p.pddl.\n" << std::endl
     << "\tdefault prefix = 'new'"  << std::endl
     << "\tThis transformation is not complete, but sound. "  << std::endl
     << "\tRequires an ADL classical planner"  << std::endl
     << std::endl
     << "Flags:" << std::endl << std::endl
     << "  -t0              Use atoms L/xi, that guarantee L if xi is true at t=0." << std::endl
     << "  -mac             merge:  AND_L' K-L' -> KL" << std::endl
     << "  -cdisjm          merge:  AND_L' M-L' -> ML." << std::endl
     << std::endl 
     << "  -cond            ND observation actions." << std::endl 
     << "  -cod             D observations, introduces M-Literals, changes the precons." << std::endl 
     << std::endl 
     << "  -ckinl           merge imply: KL/t & K-L -> K-t" << std::endl 
     << "  -cminl           merge imply: KL/t & M-L -> M-t" << std::endl  
      << " -ckit            merge KL/t & Kt -> KL" << std::endl 
     << "  -cmit            merge KL/t & Mt -> ML" << std::endl
     << "  -cdisjk0         merge:  AND_t' K-t'@0 -> Kt@0." << std::endl
     << "  -cdisjm0         merge:  AND_t' M-t'@0 -> Mt@0." << std::endl
     << "  -csl             full merge, introduces SL/xi atoms" << std::endl 
     << "  -cfm             full merge. A lot of actions!" << std::endl 
     << std::endl
     << "  -mog             merge of everything (not only goal and preconds)." << std::endl 
     << "  -cfc             Problem fails when Kx & K-x" << std::endl    
     << "  -cmr             MC -> ML" << std::endl    
     << "  -fct             force contingent: move preconds." << std::endl 
     << "  -npc             no use of preconditions (force conformant)." << std::endl
     << "  -cost            habilitate action costs" << std::endl    
     << "  -cnap            dishabilitate HABILITATE flags..." << std::endl    
     << "  -v <n>           Set verbosity level to <n>. Useful values are 5, 10, 15, and 20." << std::endl
     << std::endl
     << std::endl;
  exit(1);
}

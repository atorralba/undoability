#ifndef __CF2CS__
#define __CF2CS__

#include <vector>
#include <string>
#include <iostream>

#include "nnf.h"
#include "planner.h"

void usage( std::ostream &os );
void parseArg( int argc, char **argv );
void do_cf2cs();
void prepare_action( Act& a );
void printDebugPDDL();

#define EXTERNC extern

#endif

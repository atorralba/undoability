#!/bin/bash


MEGAS_MEM=1800
MIN_TIME=30
SESSANTA=60
ulimit -S -v $((1024 * $MEGAS_MEM))
ulimit -S -t $(($MIN_TIME * $SESSANTA))


case "$3" in
    0)
./cf2cs.v2.2 -t0 -mac $1 $2 ;;
    1)
./cf2cs.v2.2 -t0 -mac -fct -npc $1 $2 ;;
    2)
./cf2cs.v2.2 -t0 -mac -cond $1 $2 ;;
    3)
./cf2cs.v2.2 -t0 -mac -cond -ckinl   $1 $2 ;;
    4)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cod -cminl    $1 $2 ;;
    5)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cod -cminl   -cdisjm $1 $2 ;;
    6)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cod -cminl  -cmr  $1 $2 ;;
    7)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cod -cminl  -cmr -cdisjm $1 $2 ;;
    8)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cod  -cmr  $1 $2 ;;
    9)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cod  -cmr -cdisjm $1 $2 ;;
    10)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cod  -cdisjm $1 $2 ;;
    11)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cod  $1 $2 ;;
    12)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0  $1 $2 ;;
    13)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod -cminl    $1 $2 ;;
    14)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod -cminl   -cdisjm $1 $2 ;;
    15)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod -cminl  -cmr  $1 $2 ;;
    16)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod -cminl  -cmr -cdisjm $1 $2 ;;
    17)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod -cminl  -cdisjm0   $1 $2 ;;
    18)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod -cminl  -cdisjm0  -cdisjm $1 $2 ;;
    19)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod -cminl  -cdisjm0 -cmr  $1 $2 ;;
    20)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod -cminl  -cdisjm0 -cmr -cdisjm $1 $2 ;;
    21)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod  -cmr  $1 $2 ;;
    22)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod  -cmr -cdisjm $1 $2 ;;
    23)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod  -cdisjm $1 $2 ;;
    24)
./cf2cs.v2.2 -t0 -mac -cond -ckinl  -cdisjk0 -cod  $1 $2 ;;
    25)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl  $1 $2 ;;
    26)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cminl    $1 $2 ;;
    27)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cminl   -cdisjm $1 $2 ;;
    28)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cminl  -cmr  $1 $2 ;;
    29)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cminl  -cmr -cdisjm $1 $2 ;;
    30)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cmit -cminl   $1 $2 ;;
    31)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cmit -cminl  -cdisjm $1 $2 ;;
    32)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cmit -cminl -cmr  $1 $2 ;;
    33)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cmit -cminl -cmr -cdisjm $1 $2 ;;
    34)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cmit    $1 $2 ;;
    35)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cmit   -cdisjm $1 $2 ;;
    36)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cmit  -cmr  $1 $2 ;;
    37)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod -cmit  -cmr -cdisjm $1 $2 ;;
    38)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod  -cmr  $1 $2 ;;
    39)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod  -cmr -cdisjm $1 $2 ;;
    40)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod  -cdisjm $1 $2 ;;
    41)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cod  $1 $2 ;;
    42)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0  $1 $2 ;;
    43)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cminl    $1 $2 ;;
    44)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cminl   -cdisjm $1 $2 ;;
    45)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cminl  -cmr  $1 $2 ;;
    46)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cminl  -cmr -cdisjm $1 $2 ;;
    47)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cminl  -cdisjm0   $1 $2 ;;
    48)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cminl  -cdisjm0  -cdisjm $1 $2 ;;
    49)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cminl  -cdisjm0 -cmr  $1 $2 ;;
    50)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cminl  -cdisjm0 -cmr -cdisjm $1 $2 ;;
    51)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit -cminl   $1 $2 ;;
    52)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit -cminl  -cdisjm $1 $2 ;;
    53)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit -cminl -cmr  $1 $2 ;;
    54)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit -cminl -cmr -cdisjm $1 $2 ;;
    55)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit -cminl -cdisjm0   $1 $2 ;;
    56)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit -cminl -cdisjm0  -cdisjm $1 $2 ;;
    57)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit -cminl -cdisjm0 -cmr  $1 $2 ;;
    58)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit -cminl -cdisjm0 -cmr -cdisjm $1 $2 ;;
    59)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit    $1 $2 ;;
    60)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit   -cdisjm $1 $2 ;;
    61)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit  -cmr  $1 $2 ;;
    62)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit  -cmr -cdisjm $1 $2 ;;
    63)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit  -cdisjm0   $1 $2 ;;
    64)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit  -cdisjm0  -cdisjm $1 $2 ;;
    65)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit  -cdisjm0 -cmr  $1 $2 ;;
    66)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod -cmit  -cdisjm0 -cmr -cdisjm $1 $2 ;;
    67)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod  -cmr  $1 $2 ;;
    68)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod  -cmr -cdisjm $1 $2 ;;
    69)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod  -cdisjm $1 $2 ;;
    70)
./cf2cs.v2.2 -t0 -mac -cond -ckit -ckinl -cdisjk0 -cod  $1 $2 ;;
    71)
./cf2cs.v2.2 -t0 -mac -cond -ckit   $1 $2 ;;
    72)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cod -cmit    $1 $2 ;;
    73)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cod -cmit   -cdisjm $1 $2 ;;
    74)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cod -cmit  -cmr  $1 $2 ;;
    75)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cod -cmit  -cmr -cdisjm $1 $2 ;;
    76)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cod  -cmr  $1 $2 ;;
    77)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cod  -cmr -cdisjm $1 $2 ;;
    78)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cod  -cdisjm $1 $2 ;;
    79)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cod  $1 $2 ;;
    80)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0  $1 $2 ;;
    81)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod -cmit    $1 $2 ;;
    82)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod -cmit   -cdisjm $1 $2 ;;
    83)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod -cmit  -cmr  $1 $2 ;;
    84)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod -cmit  -cmr -cdisjm $1 $2 ;;
    85)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod -cmit  -cdisjm0   $1 $2 ;;
    86)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod -cmit  -cdisjm0  -cdisjm $1 $2 ;;
    87)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod -cmit  -cdisjm0 -cmr  $1 $2 ;;
    88)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod -cmit  -cdisjm0 -cmr -cdisjm $1 $2 ;;
    89)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod  -cmr  $1 $2 ;;
    90)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod  -cmr -cdisjm $1 $2 ;;
    91)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod  -cdisjm $1 $2 ;;
    92)
./cf2cs.v2.2 -t0 -mac -cond -ckit  -cdisjk0 -cod  $1 $2 ;;
    93)
./cf2cs.v2.2 -t0 -mac -cond  -cod  -cmr  $1 $2 ;;
    94)
./cf2cs.v2.2 -t0 -mac -cond  -cod  -cmr -cdisjm $1 $2 ;;
    95)
./cf2cs.v2.2 -t0 -mac -cond  -cod  -cdisjm $1 $2 ;;
    96)
./cf2cs.v2.2 -t0 -mac -cond  -cod  $1 $2 ;;
    *) ;;
esac
#./ff -a 1 -c 1 -v 1 -k 0 -e 0 -p ./ -o new-d.pddl -f new-p.pddl

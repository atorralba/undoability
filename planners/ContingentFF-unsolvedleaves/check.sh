#! /bin/bash

./ff -p $1 -o $2 -f $3 $4 > output.txt
./cplantotree output.txt > output.dot
dot -Tps -o output.eps output.dot
evince output.eps &
